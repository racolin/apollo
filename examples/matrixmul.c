//===--- matrixmul.c ------------------------------------------------------===//
//
// APOLLO matrix multiplication example
//
//===---------------------------------------------------------------------===//

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define SIZE 2000

double rtclock()
{
    struct timezone Tzp;
    struct timeval Tp;
    int stat;
    stat = gettimeofday (&Tp, &Tzp);
    if (stat != 0) printf("Error return from gettimeofday: %d",stat);
    return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}

double t_start, t_end;

/* Initialize matrix: function prototype. */
void initializeMatrix(double **Matrix, const int Row, const int Col);

/* MAIN */
int main()
{
  /* Matrix A */
  const int RowA = SIZE;
  const int ColA = SIZE;
  double **Matrix_A = (double**)malloc(RowA * sizeof(double*));
  for (int i = 0; i < RowA; i++) {
    Matrix_A[i] = (double*)malloc(ColA * sizeof(double));
  }

  /* Matrix B */
  const int RowB = SIZE; /* should be the same size as ColA */
  const int ColB = SIZE;
  double **Matrix_B = (double**)malloc(RowB * sizeof(double*));
  for (int i = 0; i < RowB; i++)
    Matrix_B[i] = (double*)malloc(ColB * sizeof(double));

  /* Matrix C */
  double **Matrix_C = (double**)malloc(RowA * sizeof(double*));
  for (int i = 0; i < RowA; i++) {
    Matrix_C[i] = (double*)malloc(ColB * sizeof(double));
  }

  /* Initialize Matrix_A and Matrix_B */
  initializeMatrix(Matrix_A, RowA, ColA);
  initializeMatrix(Matrix_B, RowB, ColB);

  printf("Start Matrix Multiplication\n");
  t_start = rtclock();

  //kernel!
#pragma apollo dcop
{
  /* ======== Matrix multiplication =====//===== C = A * B ============= */
  for (int i = 0; i < RowA; i++) {
    for (int j = 0; j < ColB; j++) {
      for (int k = 0; k < ColA; k++) {
	Matrix_C[i][j] += Matrix_A[i][k] * Matrix_B[k][j];
      }
    }
  }
}

  t_end = rtclock();
  printf("End Matrix Multiplication\n");
  printf("Total time taken by CPU: %0.6lfs\n", t_end - t_start);

  /* free Matrix A */
  for (int i = 0; i < RowA; i++) {
    free(Matrix_A[i]);
  }
  free(Matrix_A);

  /* free Matrix B */
  for (int i = 0; i < RowB; i++) {
    free(Matrix_B[i]);
  }
  free(Matrix_B);

  /* free Matrix C */
  for (int i = 0; i < RowA; i++) {
    free(Matrix_C[i]);
  }
  free(Matrix_C);

  return 0;
}

/* Initialize matrix: function implementation. */
void initializeMatrix(double **Matrix, const int Row, const int Col) {
  for (int i = 0; i < Row; i++) {
    for (int j = 0; j < Col; j++) {
      Matrix[i][j] = (double)(rand() % 3);
    }
  }
}
