cmake_minimum_required(VERSION 3.4.3)
project(apollo_tests)

##
# Required values

# CMAKE_BINARY_DIR: The toplevel build directory
if (NOT DEFINED CMAKE_BINARY_DIR)
  message(ERROR "CMAKE_BINARY_DIR must be set")
endif ()
message(STATUS "Configuring tests for build in ${CMAKE_BINARY_DIR}")

# APOLLO_BUILD_DIR: The build directory for APOLLO
if (NOT DEFINED APOLLO_BUILD_DIR)
 set(APOLLO_BUILD_DIR ${CMAKE_BINARY_DIR}/apollo)
endif ()
message(STATUS "Tests will use APOLLO built in ${APOLLO_BUILD_DIR}")
set(APOLLO_BINARY_DIR ${APOLLO_BUILD_DIR}/bin)

# APOLLO_LLVM_BUILD_DIR: The build directory for the Apollo build of LLVM
if (NOT DEFINED APOLLO_LLVM_BUILD_DIR)
 set(APOLLO_LLVM_BUILD_DIR ${CMAKE_BINARY_DIR}/llvm)
endif ()
message(STATUS "Tests will use LLVM built in ${APOLLO_LLVM_BUILD_DIR}")
set(APOLLO_LLVM_BINARY_DIR ${APOLLO_LLVM_BUILD_DIR}/bin)

# APOLLO_LLVM_BUILD_DIR: The location of llvm-lit
if (NOT DEFINED APOLLO_LLVM_LIT)
 set(APOLLO_LLVM_LIT "${APOLLO_LLVM_BUILD_DIR}/bin/llvm-lit")
endif ()
message(STATUS "Tests will be run using ${APOLLO_LLVM_LIT}")

# Configure the test driver
configure_file(lit.cfg.in ${CMAKE_CURRENT_SOURCE_DIR}/lit.cfg @ONLY)

# check: Run the tests
add_custom_target(check
  ${APOLLO_LLVM_LIT} ${CMAKE_CURRENT_SOURCE_DIR}
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  COMMENT
  "Running tests from ${CMAKE_CURRENT_SOURCE_DIR} \
in ${CMAKE_CURRENT_BINARY_DIR}")
add_dependencies(check llvm apollo)
