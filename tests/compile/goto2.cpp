// RUN: apolloc++ -O3 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

#include <vector>

class node { public: int val; node* next; };

void test(node* head, std::vector<int> klist,
	  int size, std::vector<int>& f)
{
  bool skip = true;

 L:
  if (!skip)
    {
      return;
    }
  skip = false;

#pragma apollo dcop
  {
    for (int i = 0; i < size; i++)
      {
	int k = klist[i];
	node* ptr = head;
	while(ptr)
	  {
	    if (ptr->val == k) {
	      f[i] += 1;
	      goto L;
	    }
	    ptr = ptr->next;
	  }
      }
  }
}
