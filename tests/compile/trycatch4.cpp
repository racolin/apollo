// RUN: apolloc++ -O2 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

// Fails (but shouldn't)

// Check that Apollo can handle the try-catch block without a compiler exit

struct node { node* val; node* next; };
extern void S1(node*);
extern void S2(node*);

void while1(node* head)
{
#pragma apollo dcop
  {
    node* ptr = head;
    while(ptr)
      {
	node* val_ptr = ptr->val;
	try
	  {
	    while(val_ptr)
	      {
		S1(val_ptr);
		S2(val_ptr);
		val_ptr = val_ptr->val;
	      }
	  }
	catch(...) { /* skip */ }
	ptr = ptr->next;
      }
  }
}
