// RUN: apolloc -O2 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

struct node { struct node* val; struct node* next; };

extern void S1(struct node*);
extern void S2(struct node*);

void test(struct node* head)
{
#pragma apollo dcop
  {
    struct node* ptr = head;
    while(ptr)
      {
	struct node* val_ptr = ptr->val;
	while(val_ptr)
	  {
	    S1(val_ptr);
	    S2(val_ptr);
	    val_ptr = val_ptr->val;
	  }
	ptr = ptr->next;
      }
  }
}
