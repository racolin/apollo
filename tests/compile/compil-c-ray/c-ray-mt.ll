; RUN: apolloc -S -emit-llvm %s -o - | FileCheck %s

; Check that Apollo code is generated
; CHECK: @apollo
; CHECK: @apollo_loop_{{[0-9]+}}_skeleton
; CHECK: @apollo_loop_{{[0-9]+}}_jit

; ModuleID = 'c-ray-mt.c'
source_filename = "c-ray-mt.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%struct.vec3 = type { double, double, double }
%struct.sphere = type { %struct.vec3, double, %struct.material, %struct.sphere* }
%struct.material = type { %struct.vec3, double, double }
%struct.camera = type { %struct.vec3, %struct.vec3, double }
%struct.timeval = type { i64, i64 }
%struct.timezone = type { i32, i32 }
%struct.ray = type { %struct.vec3, %struct.vec3 }
%struct.spoint = type { %struct.vec3, %struct.vec3, %struct.vec3, double }

@xres = global i32 800, align 4
@yres = global i32 600, align 4
@rays_per_pixel = global i32 1, align 4
@aspect = global double 1.333333e+00, align 8
@lnum = global i32 0, align 4
@usage = constant [392 x i8] c"Usage: c-ray-mt [options]\0A  Reads a scene file from stdin, writes the image to stdout, and stats to stderr.\0A\0AOptions:\0A  -s WxH     where W is the width and H the height of the image\0A  -r <rays>  shoot <rays> rays per pixel (antialiasing)\0A  -i <file>  read from <file> instead of stdin\0A  -o <file>  write to <file> instead of stdout\0A  -n\09\09  do not write output\0A  -h         this help screen\0A\0A\00", align 16
@stdin = external global %struct._IO_FILE*, align 8
@stdout = external global %struct._IO_FILE*, align 8
@.str = private unnamed_addr constant [49 x i8] c"-s must be followed by something like \22640x480\22\0A\00", align 1
@stderr = external global %struct._IO_FILE*, align 8
@.str.1 = private unnamed_addr constant [3 x i8] c"rb\00", align 1
@.str.2 = private unnamed_addr constant [34 x i8] c"failed to open input file %s: %s\0A\00", align 1
@.str.3 = private unnamed_addr constant [3 x i8] c"wb\00", align 1
@.str.4 = private unnamed_addr constant [35 x i8] c"failed to open output file %s: %s\0A\00", align 1
@.str.5 = private unnamed_addr constant [50 x i8] c"-r must be followed by a number (rays per pixel)\0A\00", align 1
@.str.6 = private unnamed_addr constant [27 x i8] c"unrecognized argument: %s\0A\00", align 1
@.str.7 = private unnamed_addr constant [31 x i8] c"pixel buffer allocation failed\00", align 1
@urand = common global [1024 x %struct.vec3] zeroinitializer, align 16
@irand = common global [1024 x i32] zeroinitializer, align 16
@.str.8 = private unnamed_addr constant [13 x i8] c"CSS_NUM_CPUS\00", align 1
@.str.9 = private unnamed_addr constant [12 x i8] c"unspecified\00", align 1
@.str.10 = private unnamed_addr constant [20 x i8] c"Worker threads: %s\0A\00", align 1
@.str.11 = private unnamed_addr constant [48 x i8] c"Rendering took: %lu seconds (%lu milliseconds)\0A\00", align 1
@.str.12 = private unnamed_addr constant [14 x i8] c"P6\0A%d %d\0A255\0A\00", align 1
@obj_list = common global %struct.sphere* null, align 8
@lights = common global [16 x %struct.vec3] zeroinitializer, align 16
@get_primary_ray.j = private unnamed_addr constant %struct.vec3 { double 0.000000e+00, double 1.000000e+00, double 0.000000e+00 }, align 8
@cam = common global %struct.camera zeroinitializer, align 8
@get_sample_pos.sf = internal global double 0.000000e+00, align 8
@.str.13 = private unnamed_addr constant [4 x i8] c" \09\0A\00", align 1
@.str.14 = private unnamed_addr constant [18 x i8] c"unknown type: %c\0A\00", align 1
@get_msec.timeval = internal global %struct.timeval zeroinitializer, align 8
@get_msec.first_timeval = internal global %struct.timeval zeroinitializer, align 8

; Function Attrs: nounwind uwtable
define i32 @main(i32 %argc, i8** %argv, i8** %envp) #0 {
entry:
  %retval = alloca i32, align 4
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 8
  %envp.addr = alloca i8**, align 8
  %i = alloca i32, align 4
  %noout = alloca i32, align 4
  %rend_time = alloca i64, align 8
  %start_time = alloca i64, align 8
  %pixels = alloca i32*, align 8
  %infile = alloca %struct._IO_FILE*, align 8
  %outfile = alloca %struct._IO_FILE*, align 8
  %sep = alloca i8*, align 8
  %css_num_cpus = alloca i8*, align 8
  %walker = alloca %struct.sphere*, align 8
  %tmp = alloca %struct.sphere*, align 8
  store i32 0, i32* %retval, align 4
  store i32 %argc, i32* %argc.addr, align 4
  store i8** %argv, i8*** %argv.addr, align 8
  store i8** %envp, i8*** %envp.addr, align 8
  store i32 0, i32* %noout, align 4
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8
  store %struct._IO_FILE* %0, %struct._IO_FILE** %infile, align 8
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 8
  store %struct._IO_FILE* %1, %struct._IO_FILE** %outfile, align 8
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %argc.addr, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %i, align 4
  %idxprom = sext i32 %4 to i64
  %5 = load i8**, i8*** %argv.addr, align 8
  %arrayidx = getelementptr inbounds i8*, i8** %5, i64 %idxprom
  %6 = load i8*, i8** %arrayidx, align 8
  %arrayidx1 = getelementptr inbounds i8, i8* %6, i64 0
  %7 = load i8, i8* %arrayidx1, align 1
  %conv = sext i8 %7 to i32
  %cmp2 = icmp eq i32 %conv, 45
  br i1 %cmp2, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %idxprom4 = sext i32 %8 to i64
  %9 = load i8**, i8*** %argv.addr, align 8
  %arrayidx5 = getelementptr inbounds i8*, i8** %9, i64 %idxprom4
  %10 = load i8*, i8** %arrayidx5, align 8
  %arrayidx6 = getelementptr inbounds i8, i8* %10, i64 2
  %11 = load i8, i8* %arrayidx6, align 1
  %conv7 = sext i8 %11 to i32
  %cmp8 = icmp eq i32 %conv7, 0
  br i1 %cmp8, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %12 = load i32, i32* %i, align 4
  %idxprom10 = sext i32 %12 to i64
  %13 = load i8**, i8*** %argv.addr, align 8
  %arrayidx11 = getelementptr inbounds i8*, i8** %13, i64 %idxprom10
  %14 = load i8*, i8** %arrayidx11, align 8
  %arrayidx12 = getelementptr inbounds i8, i8* %14, i64 1
  %15 = load i8, i8* %arrayidx12, align 1
  %conv13 = sext i8 %15 to i32
  switch i32 %conv13, label %sw.default [
    i32 115, label %sw.bb
    i32 105, label %sw.bb42
    i32 111, label %sw.bb55
    i32 110, label %sw.bb68
    i32 114, label %sw.bb69
    i32 104, label %sw.bb87
  ]

sw.bb:                                            ; preds = %if.then
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  %idxprom14 = sext i32 %inc to i64
  %17 = load i8**, i8*** %argv.addr, align 8
  %arrayidx15 = getelementptr inbounds i8*, i8** %17, i64 %idxprom14
  %18 = load i8*, i8** %arrayidx15, align 8
  %arrayidx16 = getelementptr inbounds i8, i8* %18, i64 0
  %19 = load i8, i8* %arrayidx16, align 1
  %conv17 = sext i8 %19 to i32
  %idxprom18 = sext i32 %conv17 to i64
  %call = call i16** @__ctype_b_loc() #6
  %20 = load i16*, i16** %call, align 8
  %arrayidx19 = getelementptr inbounds i16, i16* %20, i64 %idxprom18
  %21 = load i16, i16* %arrayidx19, align 2
  %conv20 = zext i16 %21 to i32
  %and = and i32 %conv20, 2048
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then33

lor.lhs.false:                                    ; preds = %sw.bb
  %22 = load i32, i32* %i, align 4
  %idxprom21 = sext i32 %22 to i64
  %23 = load i8**, i8*** %argv.addr, align 8
  %arrayidx22 = getelementptr inbounds i8*, i8** %23, i64 %idxprom21
  %24 = load i8*, i8** %arrayidx22, align 8
  %call23 = call i8* @strchr(i8* %24, i32 120) #7
  store i8* %call23, i8** %sep, align 8
  %tobool24 = icmp ne i8* %call23, null
  br i1 %tobool24, label %lor.lhs.false25, label %if.then33

lor.lhs.false25:                                  ; preds = %lor.lhs.false
  %25 = load i8*, i8** %sep, align 8
  %add.ptr = getelementptr inbounds i8, i8* %25, i64 1
  %26 = load i8, i8* %add.ptr, align 1
  %conv26 = sext i8 %26 to i32
  %idxprom27 = sext i32 %conv26 to i64
  %call28 = call i16** @__ctype_b_loc() #6
  %27 = load i16*, i16** %call28, align 8
  %arrayidx29 = getelementptr inbounds i16, i16* %27, i64 %idxprom27
  %28 = load i16, i16* %arrayidx29, align 2
  %conv30 = zext i16 %28 to i32
  %and31 = and i32 %conv30, 2048
  %tobool32 = icmp ne i32 %and31, 0
  br i1 %tobool32, label %if.end, label %if.then33

if.then33:                                        ; preds = %lor.lhs.false25, %lor.lhs.false, %sw.bb
  %29 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %call34 = call i32 @fputs(i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str, i32 0, i32 0), %struct._IO_FILE* %29)
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false25
  %30 = load i32, i32* %i, align 4
  %idxprom35 = sext i32 %30 to i64
  %31 = load i8**, i8*** %argv.addr, align 8
  %arrayidx36 = getelementptr inbounds i8*, i8** %31, i64 %idxprom35
  %32 = load i8*, i8** %arrayidx36, align 8
  %call37 = call i32 @atoi(i8* %32) #7
  store i32 %call37, i32* @xres, align 4
  %33 = load i8*, i8** %sep, align 8
  %add.ptr38 = getelementptr inbounds i8, i8* %33, i64 1
  %call39 = call i32 @atoi(i8* %add.ptr38) #7
  store i32 %call39, i32* @yres, align 4
  %34 = load i32, i32* @xres, align 4
  %conv40 = sitofp i32 %34 to double
  %35 = load i32, i32* @yres, align 4
  %conv41 = sitofp i32 %35 to double
  %div = fdiv double %conv40, %conv41
  store double %div, double* @aspect, align 8
  br label %sw.epilog

sw.bb42:                                          ; preds = %if.then
  %36 = load i32, i32* %i, align 4
  %inc43 = add nsw i32 %36, 1
  store i32 %inc43, i32* %i, align 4
  %idxprom44 = sext i32 %inc43 to i64
  %37 = load i8**, i8*** %argv.addr, align 8
  %arrayidx45 = getelementptr inbounds i8*, i8** %37, i64 %idxprom44
  %38 = load i8*, i8** %arrayidx45, align 8
  %call46 = call %struct._IO_FILE* @fopen(i8* %38, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.1, i32 0, i32 0))
  store %struct._IO_FILE* %call46, %struct._IO_FILE** %infile, align 8
  %tobool47 = icmp ne %struct._IO_FILE* %call46, null
  br i1 %tobool47, label %if.end54, label %if.then48

if.then48:                                        ; preds = %sw.bb42
  %39 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %40 = load i32, i32* %i, align 4
  %idxprom49 = sext i32 %40 to i64
  %41 = load i8**, i8*** %argv.addr, align 8
  %arrayidx50 = getelementptr inbounds i8*, i8** %41, i64 %idxprom49
  %42 = load i8*, i8** %arrayidx50, align 8
  %call51 = call i32* @__errno_location() #6
  %43 = load i32, i32* %call51, align 4
  %call52 = call i8* @strerror(i32 %43) #8
  %call53 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %39, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.2, i32 0, i32 0), i8* %42, i8* %call52)
  store i32 1, i32* %retval, align 4
  br label %return

if.end54:                                         ; preds = %sw.bb42
  br label %sw.epilog

sw.bb55:                                          ; preds = %if.then
  %44 = load i32, i32* %i, align 4
  %inc56 = add nsw i32 %44, 1
  store i32 %inc56, i32* %i, align 4
  %idxprom57 = sext i32 %inc56 to i64
  %45 = load i8**, i8*** %argv.addr, align 8
  %arrayidx58 = getelementptr inbounds i8*, i8** %45, i64 %idxprom57
  %46 = load i8*, i8** %arrayidx58, align 8
  %call59 = call %struct._IO_FILE* @fopen(i8* %46, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.3, i32 0, i32 0))
  store %struct._IO_FILE* %call59, %struct._IO_FILE** %outfile, align 8
  %tobool60 = icmp ne %struct._IO_FILE* %call59, null
  br i1 %tobool60, label %if.end67, label %if.then61

if.then61:                                        ; preds = %sw.bb55
  %47 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %48 = load i32, i32* %i, align 4
  %idxprom62 = sext i32 %48 to i64
  %49 = load i8**, i8*** %argv.addr, align 8
  %arrayidx63 = getelementptr inbounds i8*, i8** %49, i64 %idxprom62
  %50 = load i8*, i8** %arrayidx63, align 8
  %call64 = call i32* @__errno_location() #6
  %51 = load i32, i32* %call64, align 4
  %call65 = call i8* @strerror(i32 %51) #8
  %call66 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %47, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.4, i32 0, i32 0), i8* %50, i8* %call65)
  store i32 1, i32* %retval, align 4
  br label %return

if.end67:                                         ; preds = %sw.bb55
  br label %sw.epilog

sw.bb68:                                          ; preds = %if.then
  store i32 1, i32* %noout, align 4
  br label %sw.epilog

sw.bb69:                                          ; preds = %if.then
  %52 = load i32, i32* %i, align 4
  %inc70 = add nsw i32 %52, 1
  store i32 %inc70, i32* %i, align 4
  %idxprom71 = sext i32 %inc70 to i64
  %53 = load i8**, i8*** %argv.addr, align 8
  %arrayidx72 = getelementptr inbounds i8*, i8** %53, i64 %idxprom71
  %54 = load i8*, i8** %arrayidx72, align 8
  %arrayidx73 = getelementptr inbounds i8, i8* %54, i64 0
  %55 = load i8, i8* %arrayidx73, align 1
  %conv74 = sext i8 %55 to i32
  %idxprom75 = sext i32 %conv74 to i64
  %call76 = call i16** @__ctype_b_loc() #6
  %56 = load i16*, i16** %call76, align 8
  %arrayidx77 = getelementptr inbounds i16, i16* %56, i64 %idxprom75
  %57 = load i16, i16* %arrayidx77, align 2
  %conv78 = zext i16 %57 to i32
  %and79 = and i32 %conv78, 2048
  %tobool80 = icmp ne i32 %and79, 0
  br i1 %tobool80, label %if.end83, label %if.then81

if.then81:                                        ; preds = %sw.bb69
  %58 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %call82 = call i32 @fputs(i8* getelementptr inbounds ([50 x i8], [50 x i8]* @.str.5, i32 0, i32 0), %struct._IO_FILE* %58)
  store i32 1, i32* %retval, align 4
  br label %return

if.end83:                                         ; preds = %sw.bb69
  %59 = load i32, i32* %i, align 4
  %idxprom84 = sext i32 %59 to i64
  %60 = load i8**, i8*** %argv.addr, align 8
  %arrayidx85 = getelementptr inbounds i8*, i8** %60, i64 %idxprom84
  %61 = load i8*, i8** %arrayidx85, align 8
  %call86 = call i32 @atoi(i8* %61) #7
  store i32 %call86, i32* @rays_per_pixel, align 4
  br label %sw.epilog

sw.bb87:                                          ; preds = %if.then
  %62 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 8
  %call88 = call i32 @fputs(i8* getelementptr inbounds ([392 x i8], [392 x i8]* @usage, i32 0, i32 0), %struct._IO_FILE* %62)
  store i32 0, i32* %retval, align 4
  br label %return

sw.default:                                       ; preds = %if.then
  %63 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %64 = load i32, i32* %i, align 4
  %idxprom89 = sext i32 %64 to i64
  %65 = load i8**, i8*** %argv.addr, align 8
  %arrayidx90 = getelementptr inbounds i8*, i8** %65, i64 %idxprom89
  %66 = load i8*, i8** %arrayidx90, align 8
  %call91 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %63, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.6, i32 0, i32 0), i8* %66)
  %67 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %call92 = call i32 @fputs(i8* getelementptr inbounds ([392 x i8], [392 x i8]* @usage, i32 0, i32 0), %struct._IO_FILE* %67)
  store i32 1, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %if.end83, %sw.bb68, %if.end67, %if.end54, %if.end
  br label %if.end97

if.else:                                          ; preds = %land.lhs.true, %for.body
  %68 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %69 = load i32, i32* %i, align 4
  %idxprom93 = sext i32 %69 to i64
  %70 = load i8**, i8*** %argv.addr, align 8
  %arrayidx94 = getelementptr inbounds i8*, i8** %70, i64 %idxprom93
  %71 = load i8*, i8** %arrayidx94, align 8
  %call95 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %68, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.6, i32 0, i32 0), i8* %71)
  %72 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %call96 = call i32 @fputs(i8* getelementptr inbounds ([392 x i8], [392 x i8]* @usage, i32 0, i32 0), %struct._IO_FILE* %72)
  store i32 1, i32* %retval, align 4
  br label %return

if.end97:                                         ; preds = %sw.epilog
  br label %for.inc

for.inc:                                          ; preds = %if.end97
  %73 = load i32, i32* %i, align 4
  %inc98 = add nsw i32 %73, 1
  store i32 %inc98, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %74 = load i32, i32* @xres, align 4
  %75 = load i32, i32* @yres, align 4
  %mul = mul nsw i32 %74, %75
  %conv99 = sext i32 %mul to i64
  %mul100 = mul i64 %conv99, 4
  %call101 = call noalias i8* @malloc(i64 %mul100) #8
  %76 = bitcast i8* %call101 to i32*
  store i32* %76, i32** %pixels, align 8
  %tobool102 = icmp ne i32* %76, null
  br i1 %tobool102, label %if.end104, label %if.then103

if.then103:                                       ; preds = %for.end
  call void @perror(i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.7, i32 0, i32 0))
  store i32 1, i32* %retval, align 4
  br label %return

if.end104:                                        ; preds = %for.end
  %77 = load %struct._IO_FILE*, %struct._IO_FILE** %infile, align 8
  call void @load_scene(%struct._IO_FILE* %77)
  store i32 0, i32* %i, align 4
  br label %for.cond105

for.cond105:                                      ; preds = %for.inc114, %if.end104
  %78 = load i32, i32* %i, align 4
  %cmp106 = icmp slt i32 %78, 1024
  br i1 %cmp106, label %for.body108, label %for.end116

for.body108:                                      ; preds = %for.cond105
  %call109 = call i32 @rand() #8
  %conv110 = sitofp i32 %call109 to double
  %div111 = fdiv double %conv110, 0x41DFFFFFFFC00000
  %sub = fsub double %div111, 5.000000e-01
  %79 = load i32, i32* %i, align 4
  %idxprom112 = sext i32 %79 to i64
  %arrayidx113 = getelementptr inbounds [1024 x %struct.vec3], [1024 x %struct.vec3]* @urand, i64 0, i64 %idxprom112
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %arrayidx113, i32 0, i32 0
  store double %sub, double* %x, align 8
  br label %for.inc114

for.inc114:                                       ; preds = %for.body108
  %80 = load i32, i32* %i, align 4
  %inc115 = add nsw i32 %80, 1
  store i32 %inc115, i32* %i, align 4
  br label %for.cond105

for.end116:                                       ; preds = %for.cond105
  store i32 0, i32* %i, align 4
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc127, %for.end116
  %81 = load i32, i32* %i, align 4
  %cmp118 = icmp slt i32 %81, 1024
  br i1 %cmp118, label %for.body120, label %for.end129

for.body120:                                      ; preds = %for.cond117
  %call121 = call i32 @rand() #8
  %conv122 = sitofp i32 %call121 to double
  %div123 = fdiv double %conv122, 0x41DFFFFFFFC00000
  %sub124 = fsub double %div123, 5.000000e-01
  %82 = load i32, i32* %i, align 4
  %idxprom125 = sext i32 %82 to i64
  %arrayidx126 = getelementptr inbounds [1024 x %struct.vec3], [1024 x %struct.vec3]* @urand, i64 0, i64 %idxprom125
  %y = getelementptr inbounds %struct.vec3, %struct.vec3* %arrayidx126, i32 0, i32 1
  store double %sub124, double* %y, align 8
  br label %for.inc127

for.inc127:                                       ; preds = %for.body120
  %83 = load i32, i32* %i, align 4
  %inc128 = add nsw i32 %83, 1
  store i32 %inc128, i32* %i, align 4
  br label %for.cond117

for.end129:                                       ; preds = %for.cond117
  store i32 0, i32* %i, align 4
  br label %for.cond130

for.cond130:                                      ; preds = %for.inc141, %for.end129
  %84 = load i32, i32* %i, align 4
  %cmp131 = icmp slt i32 %84, 1024
  br i1 %cmp131, label %for.body133, label %for.end143

for.body133:                                      ; preds = %for.cond130
  %call134 = call i32 @rand() #8
  %conv135 = sitofp i32 %call134 to double
  %div136 = fdiv double %conv135, 0x41DFFFFFFFC00000
  %mul137 = fmul double 1.024000e+03, %div136
  %conv138 = fptosi double %mul137 to i32
  %85 = load i32, i32* %i, align 4
  %idxprom139 = sext i32 %85 to i64
  %arrayidx140 = getelementptr inbounds [1024 x i32], [1024 x i32]* @irand, i64 0, i64 %idxprom139
  store i32 %conv138, i32* %arrayidx140, align 4
  br label %for.inc141

for.inc141:                                       ; preds = %for.body133
  %86 = load i32, i32* %i, align 4
  %inc142 = add nsw i32 %86, 1
  store i32 %inc142, i32* %i, align 4
  br label %for.cond130

for.end143:                                       ; preds = %for.cond130
  %call144 = call i8* @getenv(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.8, i32 0, i32 0)) #8
  store i8* %call144, i8** %css_num_cpus, align 8
  %cmp145 = icmp eq i8* %call144, null
  br i1 %cmp145, label %if.then147, label %if.end148

if.then147:                                       ; preds = %for.end143
  store i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.9, i32 0, i32 0), i8** %css_num_cpus, align 8
  br label %if.end148

if.end148:                                        ; preds = %if.then147, %for.end143
  %87 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %88 = load i8*, i8** %css_num_cpus, align 8
  %call149 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %87, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.10, i32 0, i32 0), i8* %88)
  %call150 = call i64 @get_msec()
  store i64 %call150, i64* %start_time, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond151

for.cond151:                                      ; preds = %for.inc159, %if.end148
  %89 = load i32, i32* %i, align 4
  %90 = load i32, i32* @yres, align 4
  %cmp152 = icmp slt i32 %89, %90
  br i1 %cmp152, label %for.body154, label %for.end161

for.body154:                                      ; preds = %for.cond151
  %91 = load i32, i32* @xres, align 4
  %92 = load i32, i32* @yres, align 4
  %93 = load i32, i32* %i, align 4
  %94 = load i32*, i32** %pixels, align 8
  %95 = bitcast i32* %94 to i8*
  %96 = load i32, i32* %i, align 4
  %97 = load i32, i32* @xres, align 4
  %mul155 = mul nsw i32 %96, %97
  %conv156 = sext i32 %mul155 to i64
  %mul157 = mul i64 %conv156, 4
  %add.ptr158 = getelementptr i8, i8* %95, i64 %mul157
  %98 = bitcast i8* %add.ptr158 to i32*
  %99 = load i32, i32* @rays_per_pixel, align 4
  call void @render_scanline(i32 %91, i32 %92, i32 %93, i32* %98, i32 %99)
  br label %for.inc159

for.inc159:                                       ; preds = %for.body154
  %100 = load i32, i32* %i, align 4
  %inc160 = add nsw i32 %100, 1
  store i32 %inc160, i32* %i, align 4
  br label %for.cond151

for.end161:                                       ; preds = %for.cond151
  %call162 = call i64 @get_msec()
  %101 = load i64, i64* %start_time, align 8
  %sub163 = sub i64 %call162, %101
  store i64 %sub163, i64* %rend_time, align 8
  %102 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %103 = load i64, i64* %rend_time, align 8
  %div164 = udiv i64 %103, 1000
  %104 = load i64, i64* %rend_time, align 8
  %call165 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %102, i8* getelementptr inbounds ([48 x i8], [48 x i8]* @.str.11, i32 0, i32 0), i64 %div164, i64 %104)
  %105 = load i32, i32* %noout, align 4
  %tobool166 = icmp ne i32 %105, 0
  br i1 %tobool166, label %if.end192, label %if.then167

if.then167:                                       ; preds = %for.end161
  %106 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 8
  %107 = load i32, i32* @xres, align 4
  %108 = load i32, i32* @yres, align 4
  %call168 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %106, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.12, i32 0, i32 0), i32 %107, i32 %108)
  store i32 0, i32* %i, align 4
  br label %for.cond169

for.cond169:                                      ; preds = %for.inc188, %if.then167
  %109 = load i32, i32* %i, align 4
  %110 = load i32, i32* @xres, align 4
  %111 = load i32, i32* @yres, align 4
  %mul170 = mul nsw i32 %110, %111
  %cmp171 = icmp slt i32 %109, %mul170
  br i1 %cmp171, label %for.body173, label %for.end190

for.body173:                                      ; preds = %for.cond169
  %112 = load i32, i32* %i, align 4
  %idxprom174 = sext i32 %112 to i64
  %113 = load i32*, i32** %pixels, align 8
  %arrayidx175 = getelementptr inbounds i32, i32* %113, i64 %idxprom174
  %114 = load i32, i32* %arrayidx175, align 4
  %shr = lshr i32 %114, 16
  %and176 = and i32 %shr, 255
  %115 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 8
  %call177 = call i32 @fputc(i32 %and176, %struct._IO_FILE* %115)
  %116 = load i32, i32* %i, align 4
  %idxprom178 = sext i32 %116 to i64
  %117 = load i32*, i32** %pixels, align 8
  %arrayidx179 = getelementptr inbounds i32, i32* %117, i64 %idxprom178
  %118 = load i32, i32* %arrayidx179, align 4
  %shr180 = lshr i32 %118, 8
  %and181 = and i32 %shr180, 255
  %119 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 8
  %call182 = call i32 @fputc(i32 %and181, %struct._IO_FILE* %119)
  %120 = load i32, i32* %i, align 4
  %idxprom183 = sext i32 %120 to i64
  %121 = load i32*, i32** %pixels, align 8
  %arrayidx184 = getelementptr inbounds i32, i32* %121, i64 %idxprom183
  %122 = load i32, i32* %arrayidx184, align 4
  %shr185 = lshr i32 %122, 0
  %and186 = and i32 %shr185, 255
  %123 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 8
  %call187 = call i32 @fputc(i32 %and186, %struct._IO_FILE* %123)
  br label %for.inc188

for.inc188:                                       ; preds = %for.body173
  %124 = load i32, i32* %i, align 4
  %inc189 = add nsw i32 %124, 1
  store i32 %inc189, i32* %i, align 4
  br label %for.cond169

for.end190:                                       ; preds = %for.cond169
  %125 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 8
  %call191 = call i32 @fflush(%struct._IO_FILE* %125)
  br label %if.end192

if.end192:                                        ; preds = %for.end190, %for.end161
  %126 = load %struct._IO_FILE*, %struct._IO_FILE** %infile, align 8
  %127 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8
  %cmp193 = icmp ne %struct._IO_FILE* %126, %127
  br i1 %cmp193, label %if.then195, label %if.end197

if.then195:                                       ; preds = %if.end192
  %128 = load %struct._IO_FILE*, %struct._IO_FILE** %infile, align 8
  %call196 = call i32 @fclose(%struct._IO_FILE* %128)
  br label %if.end197

if.end197:                                        ; preds = %if.then195, %if.end192
  %129 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 8
  %130 = load %struct._IO_FILE*, %struct._IO_FILE** @stdout, align 8
  %cmp198 = icmp ne %struct._IO_FILE* %129, %130
  br i1 %cmp198, label %if.then200, label %if.end202

if.then200:                                       ; preds = %if.end197
  %131 = load %struct._IO_FILE*, %struct._IO_FILE** %outfile, align 8
  %call201 = call i32 @fclose(%struct._IO_FILE* %131)
  br label %if.end202

if.end202:                                        ; preds = %if.then200, %if.end197
  %132 = load %struct.sphere*, %struct.sphere** @obj_list, align 8
  store %struct.sphere* %132, %struct.sphere** %walker, align 8
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end202
  %133 = load %struct.sphere*, %struct.sphere** %walker, align 8
  %tobool203 = icmp ne %struct.sphere* %133, null
  br i1 %tobool203, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %134 = load %struct.sphere*, %struct.sphere** %walker, align 8
  store %struct.sphere* %134, %struct.sphere** %tmp, align 8
  %135 = load %struct.sphere*, %struct.sphere** %walker, align 8
  %next = getelementptr inbounds %struct.sphere, %struct.sphere* %135, i32 0, i32 3
  %136 = load %struct.sphere*, %struct.sphere** %next, align 8
  store %struct.sphere* %136, %struct.sphere** %walker, align 8
  %137 = load %struct.sphere*, %struct.sphere** %tmp, align 8
  %138 = bitcast %struct.sphere* %137 to i8*
  call void @free(i8* %138) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %139 = load i32*, i32** %pixels, align 8
  %140 = bitcast i32* %139 to i8*
  call void @free(i8* %140) #8
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then103, %if.else, %sw.default, %sw.bb87, %if.then81, %if.then61, %if.then48, %if.then33
  %141 = load i32, i32* %retval, align 4
  ret i32 %141
}

; Function Attrs: nounwind readnone
declare i16** @__ctype_b_loc() #1

; Function Attrs: nounwind readonly
declare i8* @strchr(i8*, i32) #2

declare i32 @fputs(i8*, %struct._IO_FILE*) #3

; Function Attrs: nounwind readonly
declare i32 @atoi(i8*) #2

declare %struct._IO_FILE* @fopen(i8*, i8*) #3

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #3

; Function Attrs: nounwind
declare i8* @strerror(i32) #4

; Function Attrs: nounwind readnone
declare i32* @__errno_location() #1

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #4

declare void @perror(i8*) #3

; Function Attrs: nounwind uwtable
define void @load_scene(%struct._IO_FILE* %fp) #0 {
entry:
  %fp.addr = alloca %struct._IO_FILE*, align 8
  %line = alloca [256 x i8], align 16
  %ptr = alloca i8*, align 8
  %type = alloca i8, align 1
  %i = alloca i32, align 4
  %pos = alloca %struct.vec3, align 8
  %col = alloca %struct.vec3, align 8
  %rad = alloca double, align 8
  %spow = alloca double, align 8
  %refl = alloca double, align 8
  %sph = alloca %struct.sphere*, align 8
  store %struct._IO_FILE* %fp, %struct._IO_FILE** %fp.addr, align 8
  %call = call noalias i8* @malloc(i64 80) #8
  %0 = bitcast i8* %call to %struct.sphere*
  store %struct.sphere* %0, %struct.sphere** @obj_list, align 8
  %1 = load %struct.sphere*, %struct.sphere** @obj_list, align 8
  %next = getelementptr inbounds %struct.sphere, %struct.sphere* %1, i32 0, i32 3
  store %struct.sphere* null, %struct.sphere** %next, align 8
  br label %while.cond

while.cond:                                       ; preds = %if.end84, %if.then64, %if.then59, %if.then55, %if.then34, %if.then29, %if.then17, %if.then, %entry
  %arraydecay = getelementptr inbounds [256 x i8], [256 x i8]* %line, i32 0, i32 0
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** %fp.addr, align 8
  %call1 = call i8* @fgets(i8* %arraydecay, i32 256, %struct._IO_FILE* %2)
  store i8* %call1, i8** %ptr, align 8
  %tobool = icmp ne i8* %call1, null
  br i1 %tobool, label %while.body, label %while.end85

while.body:                                       ; preds = %while.cond
  br label %while.cond2

while.cond2:                                      ; preds = %while.body7, %while.body
  %3 = load i8*, i8** %ptr, align 8
  %4 = load i8, i8* %3, align 1
  %conv = sext i8 %4 to i32
  %cmp = icmp eq i32 %conv, 32
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %while.cond2
  %5 = load i8*, i8** %ptr, align 8
  %6 = load i8, i8* %5, align 1
  %conv4 = sext i8 %6 to i32
  %cmp5 = icmp eq i32 %conv4, 9
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %while.cond2
  %7 = phi i1 [ true, %while.cond2 ], [ %cmp5, %lor.rhs ]
  br i1 %7, label %while.body7, label %while.end

while.body7:                                      ; preds = %lor.end
  %8 = load i8*, i8** %ptr, align 8
  %incdec.ptr = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr, i8** %ptr, align 8
  br label %while.cond2

while.end:                                        ; preds = %lor.end
  %9 = load i8*, i8** %ptr, align 8
  %10 = load i8, i8* %9, align 1
  %conv8 = sext i8 %10 to i32
  %cmp9 = icmp eq i32 %conv8, 35
  br i1 %cmp9, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %while.end
  %11 = load i8*, i8** %ptr, align 8
  %12 = load i8, i8* %11, align 1
  %conv11 = sext i8 %12 to i32
  %cmp12 = icmp eq i32 %conv11, 10
  br i1 %cmp12, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %while.end
  br label %while.cond

if.end:                                           ; preds = %lor.lhs.false
  %arraydecay14 = getelementptr inbounds [256 x i8], [256 x i8]* %line, i32 0, i32 0
  %call15 = call i8* @strtok(i8* %arraydecay14, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0)) #8
  store i8* %call15, i8** %ptr, align 8
  %tobool16 = icmp ne i8* %call15, null
  br i1 %tobool16, label %if.end18, label %if.then17

if.then17:                                        ; preds = %if.end
  br label %while.cond

if.end18:                                         ; preds = %if.end
  %13 = load i8*, i8** %ptr, align 8
  %14 = load i8, i8* %13, align 1
  store i8 %14, i8* %type, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end18
  %15 = load i32, i32* %i, align 4
  %cmp19 = icmp slt i32 %15, 3
  br i1 %cmp19, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call21 = call i8* @strtok(i8* null, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0)) #8
  store i8* %call21, i8** %ptr, align 8
  %tobool22 = icmp ne i8* %call21, null
  br i1 %tobool22, label %if.end24, label %if.then23

if.then23:                                        ; preds = %for.body
  br label %for.end

if.end24:                                         ; preds = %for.body
  %16 = load i8*, i8** %ptr, align 8
  %call25 = call double @atof(i8* %16) #7
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %pos, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %idx.ext = sext i32 %17 to i64
  %add.ptr = getelementptr inbounds double, double* %x, i64 %idx.ext
  store double %call25, double* %add.ptr, align 8
  br label %for.inc

for.inc:                                          ; preds = %if.end24
  %18 = load i32, i32* %i, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then23, %for.cond
  %19 = load i8, i8* %type, align 1
  %conv26 = sext i8 %19 to i32
  %cmp27 = icmp eq i32 %conv26, 108
  br i1 %cmp27, label %if.then29, label %if.end31

if.then29:                                        ; preds = %for.end
  %20 = load i32, i32* @lnum, align 4
  %inc30 = add nsw i32 %20, 1
  store i32 %inc30, i32* @lnum, align 4
  %idxprom = sext i32 %20 to i64
  %arrayidx = getelementptr inbounds [16 x %struct.vec3], [16 x %struct.vec3]* @lights, i64 0, i64 %idxprom
  %21 = bitcast %struct.vec3* %arrayidx to i8*
  %22 = bitcast %struct.vec3* %pos to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %21, i8* %22, i64 24, i32 8, i1 false)
  br label %while.cond

if.end31:                                         ; preds = %for.end
  %call32 = call i8* @strtok(i8* null, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0)) #8
  store i8* %call32, i8** %ptr, align 8
  %tobool33 = icmp ne i8* %call32, null
  br i1 %tobool33, label %if.end35, label %if.then34

if.then34:                                        ; preds = %if.end31
  br label %while.cond

if.end35:                                         ; preds = %if.end31
  %23 = load i8*, i8** %ptr, align 8
  %call36 = call double @atof(i8* %23) #7
  store double %call36, double* %rad, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc49, %if.end35
  %24 = load i32, i32* %i, align 4
  %cmp38 = icmp slt i32 %24, 3
  br i1 %cmp38, label %for.body40, label %for.end51

for.body40:                                       ; preds = %for.cond37
  %call41 = call i8* @strtok(i8* null, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0)) #8
  store i8* %call41, i8** %ptr, align 8
  %tobool42 = icmp ne i8* %call41, null
  br i1 %tobool42, label %if.end44, label %if.then43

if.then43:                                        ; preds = %for.body40
  br label %for.end51

if.end44:                                         ; preds = %for.body40
  %25 = load i8*, i8** %ptr, align 8
  %call45 = call double @atof(i8* %25) #7
  %x46 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %idx.ext47 = sext i32 %26 to i64
  %add.ptr48 = getelementptr inbounds double, double* %x46, i64 %idx.ext47
  store double %call45, double* %add.ptr48, align 8
  br label %for.inc49

for.inc49:                                        ; preds = %if.end44
  %27 = load i32, i32* %i, align 4
  %inc50 = add nsw i32 %27, 1
  store i32 %inc50, i32* %i, align 4
  br label %for.cond37

for.end51:                                        ; preds = %if.then43, %for.cond37
  %28 = load i8, i8* %type, align 1
  %conv52 = sext i8 %28 to i32
  %cmp53 = icmp eq i32 %conv52, 99
  br i1 %cmp53, label %if.then55, label %if.end56

if.then55:                                        ; preds = %for.end51
  %29 = bitcast %struct.vec3* %pos to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (%struct.camera* @cam to i8*), i8* %29, i64 24, i32 8, i1 false)
  %30 = bitcast %struct.vec3* %col to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (%struct.vec3* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 1) to i8*), i8* %30, i64 24, i32 8, i1 false)
  %31 = load double, double* %rad, align 8
  store double %31, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 2), align 8
  br label %while.cond

if.end56:                                         ; preds = %for.end51
  %call57 = call i8* @strtok(i8* null, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0)) #8
  store i8* %call57, i8** %ptr, align 8
  %tobool58 = icmp ne i8* %call57, null
  br i1 %tobool58, label %if.end60, label %if.then59

if.then59:                                        ; preds = %if.end56
  br label %while.cond

if.end60:                                         ; preds = %if.end56
  %32 = load i8*, i8** %ptr, align 8
  %call61 = call double @atof(i8* %32) #7
  store double %call61, double* %spow, align 8
  %call62 = call i8* @strtok(i8* null, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.13, i32 0, i32 0)) #8
  store i8* %call62, i8** %ptr, align 8
  %tobool63 = icmp ne i8* %call62, null
  br i1 %tobool63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %if.end60
  br label %while.cond

if.end65:                                         ; preds = %if.end60
  %33 = load i8*, i8** %ptr, align 8
  %call66 = call double @atof(i8* %33) #7
  store double %call66, double* %refl, align 8
  %34 = load i8, i8* %type, align 1
  %conv67 = sext i8 %34 to i32
  %cmp68 = icmp eq i32 %conv67, 115
  br i1 %cmp68, label %if.then70, label %if.else

if.then70:                                        ; preds = %if.end65
  %call71 = call noalias i8* @malloc(i64 80) #8
  %35 = bitcast i8* %call71 to %struct.sphere*
  store %struct.sphere* %35, %struct.sphere** %sph, align 8
  %36 = load %struct.sphere*, %struct.sphere** @obj_list, align 8
  %next72 = getelementptr inbounds %struct.sphere, %struct.sphere* %36, i32 0, i32 3
  %37 = load %struct.sphere*, %struct.sphere** %next72, align 8
  %38 = load %struct.sphere*, %struct.sphere** %sph, align 8
  %next73 = getelementptr inbounds %struct.sphere, %struct.sphere* %38, i32 0, i32 3
  store %struct.sphere* %37, %struct.sphere** %next73, align 8
  %39 = load %struct.sphere*, %struct.sphere** %sph, align 8
  %40 = load %struct.sphere*, %struct.sphere** @obj_list, align 8
  %next74 = getelementptr inbounds %struct.sphere, %struct.sphere* %40, i32 0, i32 3
  store %struct.sphere* %39, %struct.sphere** %next74, align 8
  %41 = load %struct.sphere*, %struct.sphere** %sph, align 8
  %pos75 = getelementptr inbounds %struct.sphere, %struct.sphere* %41, i32 0, i32 0
  %42 = bitcast %struct.vec3* %pos75 to i8*
  %43 = bitcast %struct.vec3* %pos to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %42, i8* %43, i64 24, i32 8, i1 false)
  %44 = load double, double* %rad, align 8
  %45 = load %struct.sphere*, %struct.sphere** %sph, align 8
  %rad76 = getelementptr inbounds %struct.sphere, %struct.sphere* %45, i32 0, i32 1
  store double %44, double* %rad76, align 8
  %46 = load %struct.sphere*, %struct.sphere** %sph, align 8
  %mat = getelementptr inbounds %struct.sphere, %struct.sphere* %46, i32 0, i32 2
  %col77 = getelementptr inbounds %struct.material, %struct.material* %mat, i32 0, i32 0
  %47 = bitcast %struct.vec3* %col77 to i8*
  %48 = bitcast %struct.vec3* %col to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %47, i8* %48, i64 24, i32 8, i1 false)
  %49 = load double, double* %spow, align 8
  %50 = load %struct.sphere*, %struct.sphere** %sph, align 8
  %mat78 = getelementptr inbounds %struct.sphere, %struct.sphere* %50, i32 0, i32 2
  %spow79 = getelementptr inbounds %struct.material, %struct.material* %mat78, i32 0, i32 1
  store double %49, double* %spow79, align 8
  %51 = load double, double* %refl, align 8
  %52 = load %struct.sphere*, %struct.sphere** %sph, align 8
  %mat80 = getelementptr inbounds %struct.sphere, %struct.sphere* %52, i32 0, i32 2
  %refl81 = getelementptr inbounds %struct.material, %struct.material* %mat80, i32 0, i32 2
  store double %51, double* %refl81, align 8
  br label %if.end84

if.else:                                          ; preds = %if.end65
  %53 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8
  %54 = load i8, i8* %type, align 1
  %conv82 = sext i8 %54 to i32
  %call83 = call i32 (%struct._IO_FILE*, i8*, ...) @fprintf(%struct._IO_FILE* %53, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.14, i32 0, i32 0), i32 %conv82)
  br label %if.end84

if.end84:                                         ; preds = %if.else, %if.then70
  br label %while.cond

while.end85:                                      ; preds = %while.cond
  ret void
}

; Function Attrs: nounwind
declare i32 @rand() #4

; Function Attrs: nounwind
declare i8* @getenv(i8*) #4

; Function Attrs: nounwind uwtable
define i64 @get_msec() #0 {
entry:
  %retval = alloca i64, align 8
  %call = call i32 @gettimeofday(%struct.timeval* @get_msec.timeval, %struct.timezone* null) #8
  %0 = load i64, i64* getelementptr inbounds (%struct.timeval, %struct.timeval* @get_msec.first_timeval, i32 0, i32 0), align 8
  %cmp = icmp eq i64 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* bitcast (%struct.timeval* @get_msec.first_timeval to i8*), i8* bitcast (%struct.timeval* @get_msec.timeval to i8*), i64 16, i32 8, i1 false)
  store i64 0, i64* %retval, align 8
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i64, i64* getelementptr inbounds (%struct.timeval, %struct.timeval* @get_msec.timeval, i32 0, i32 0), align 8
  %2 = load i64, i64* getelementptr inbounds (%struct.timeval, %struct.timeval* @get_msec.first_timeval, i32 0, i32 0), align 8
  %sub = sub nsw i64 %1, %2
  %mul = mul nsw i64 %sub, 1000
  %3 = load i64, i64* getelementptr inbounds (%struct.timeval, %struct.timeval* @get_msec.timeval, i32 0, i32 1), align 8
  %4 = load i64, i64* getelementptr inbounds (%struct.timeval, %struct.timeval* @get_msec.first_timeval, i32 0, i32 1), align 8
  %sub1 = sub nsw i64 %3, %4
  %div = sdiv i64 %sub1, 1000
  %add = add nsw i64 %mul, %div
  store i64 %add, i64* %retval, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i64, i64* %retval, align 8
  ret i64 %5
}

; Function Attrs: nounwind uwtable
define void @render_scanline(i32 %xsz, i32 %ysz, i32 %sl, i32* %fb, i32 %samples) #0 {
entry:
  %xsz.addr = alloca i32, align 4
  %ysz.addr = alloca i32, align 4
  %sl.addr = alloca i32, align 4
  %fb.addr = alloca i32*, align 8
  %samples.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %s = alloca i32, align 4
  %rcp_samples = alloca double, align 8
  %r = alloca double, align 8
  %g = alloca double, align 8
  %b = alloca double, align 8
  %col = alloca %struct.vec3, align 8
  %agg.tmp = alloca %struct.ray, align 8
  store i32 %xsz, i32* %xsz.addr, align 4
  store i32 %ysz, i32* %ysz.addr, align 4
  store i32 %sl, i32* %sl.addr, align 4
  store i32* %fb, i32** %fb.addr, align 8
  store i32 %samples, i32* %samples.addr, align 4
  %0 = load i32, i32* %samples.addr, align 4
  %conv = sitofp i32 %0 to double
  %div = fdiv double 1.000000e+00, %conv
  store double %div, double* %rcp_samples, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc35, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %xsz.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end37

for.body:                                         ; preds = %for.cond
  store double 0.000000e+00, double* %b, align 8
  store double 0.000000e+00, double* %g, align 8
  store double 0.000000e+00, double* %r, align 8
  store i32 0, i32* %s, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %3 = load i32, i32* %s, align 4
  %4 = load i32, i32* %samples.addr, align 4
  %cmp3 = icmp slt i32 %3, %4
  br i1 %cmp3, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond2
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %sl.addr, align 4
  %7 = load i32, i32* %s, align 4
  call void @get_primary_ray(%struct.ray* sret %agg.tmp, i32 %5, i32 %6, i32 %7)
  call void @trace(%struct.vec3* sret %col, %struct.ray* byval align 8 %agg.tmp, i32 0)
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 0
  %8 = load double, double* %x, align 8
  %9 = load double, double* %r, align 8
  %add = fadd double %9, %8
  store double %add, double* %r, align 8
  %y = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 1
  %10 = load double, double* %y, align 8
  %11 = load double, double* %g, align 8
  %add6 = fadd double %11, %10
  store double %add6, double* %g, align 8
  %z = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 2
  %12 = load double, double* %z, align 8
  %13 = load double, double* %b, align 8
  %add7 = fadd double %13, %12
  store double %add7, double* %b, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %14 = load i32, i32* %s, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %s, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  %15 = load double, double* %r, align 8
  %16 = load double, double* %rcp_samples, align 8
  %mul = fmul double %15, %16
  store double %mul, double* %r, align 8
  %17 = load double, double* %g, align 8
  %18 = load double, double* %rcp_samples, align 8
  %mul8 = fmul double %17, %18
  store double %mul8, double* %g, align 8
  %19 = load double, double* %b, align 8
  %20 = load double, double* %rcp_samples, align 8
  %mul9 = fmul double %19, %20
  store double %mul9, double* %b, align 8
  %21 = load double, double* %r, align 8
  %cmp10 = fcmp olt double %21, 1.000000e+00
  br i1 %cmp10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %22 = load double, double* %r, align 8
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %22, %cond.true ], [ 1.000000e+00, %cond.false ]
  %mul12 = fmul double %cond, 2.550000e+02
  %conv13 = fptoui double %mul12 to i32
  %and = and i32 %conv13, 255
  %shl = shl i32 %and, 16
  %23 = load double, double* %g, align 8
  %cmp14 = fcmp olt double %23, 1.000000e+00
  br i1 %cmp14, label %cond.true16, label %cond.false17

cond.true16:                                      ; preds = %cond.end
  %24 = load double, double* %g, align 8
  br label %cond.end18

cond.false17:                                     ; preds = %cond.end
  br label %cond.end18

cond.end18:                                       ; preds = %cond.false17, %cond.true16
  %cond19 = phi double [ %24, %cond.true16 ], [ 1.000000e+00, %cond.false17 ]
  %mul20 = fmul double %cond19, 2.550000e+02
  %conv21 = fptoui double %mul20 to i32
  %and22 = and i32 %conv21, 255
  %shl23 = shl i32 %and22, 8
  %or = or i32 %shl, %shl23
  %25 = load double, double* %b, align 8
  %cmp24 = fcmp olt double %25, 1.000000e+00
  br i1 %cmp24, label %cond.true26, label %cond.false27

cond.true26:                                      ; preds = %cond.end18
  %26 = load double, double* %b, align 8
  br label %cond.end28

cond.false27:                                     ; preds = %cond.end18
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false27, %cond.true26
  %cond29 = phi double [ %26, %cond.true26 ], [ 1.000000e+00, %cond.false27 ]
  %mul30 = fmul double %cond29, 2.550000e+02
  %conv31 = fptoui double %mul30 to i32
  %and32 = and i32 %conv31, 255
  %shl33 = shl i32 %and32, 0
  %or34 = or i32 %or, %shl33
  %27 = load i32, i32* %i, align 4
  %idxprom = sext i32 %27 to i64
  %28 = load i32*, i32** %fb.addr, align 8
  %arrayidx = getelementptr inbounds i32, i32* %28, i64 %idxprom
  store i32 %or34, i32* %arrayidx, align 4
  br label %for.inc35

for.inc35:                                        ; preds = %cond.end28
  %29 = load i32, i32* %i, align 4
  %inc36 = add nsw i32 %29, 1
  store i32 %inc36, i32* %i, align 4
  br label %for.cond

for.end37:                                        ; preds = %for.cond
  ret void
}

declare i32 @fputc(i32, %struct._IO_FILE*) #3

declare i32 @fflush(%struct._IO_FILE*) #3

declare i32 @fclose(%struct._IO_FILE*) #3

; Function Attrs: nounwind
declare void @free(i8*) #4

; Function Attrs: nounwind uwtable
define void @trace(%struct.vec3* noalias sret %agg.result, %struct.ray* byval align 8 %ray, i32 %depth) #0 {
entry:
  %depth.addr = alloca i32, align 4
  %col = alloca %struct.vec3, align 8
  %sp = alloca %struct.spoint, align 8
  %nearest_sp = alloca %struct.spoint, align 8
  %nearest_obj = alloca %struct.sphere*, align 8
  %iter = alloca %struct.sphere*, align 8
  %tmp = alloca %struct.vec3, align 8
  store i32 %depth, i32* %depth.addr, align 4
  store %struct.sphere* null, %struct.sphere** %nearest_obj, align 8
  %0 = load %struct.sphere*, %struct.sphere** @obj_list, align 8
  %next = getelementptr inbounds %struct.sphere, %struct.sphere* %0, i32 0, i32 3
  %1 = load %struct.sphere*, %struct.sphere** %next, align 8
  store %struct.sphere* %1, %struct.sphere** %iter, align 8
  %2 = load i32, i32* %depth.addr, align 4
  %cmp = icmp sge i32 %2, 5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %z = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 2
  store double 0.000000e+00, double* %z, align 8
  %y = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 1
  store double 0.000000e+00, double* %y, align 8
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 0
  store double 0.000000e+00, double* %x, align 8
  %3 = bitcast %struct.vec3* %agg.result to i8*
  %4 = bitcast %struct.vec3* %col to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %3, i8* %4, i64 24, i32 8, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %if.end8, %if.end
  %5 = load %struct.sphere*, %struct.sphere** %iter, align 8
  %tobool = icmp ne %struct.sphere* %5, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %struct.sphere*, %struct.sphere** %iter, align 8
  %call = call i32 @ray_sphere(%struct.sphere* %6, %struct.ray* byval align 8 %ray, %struct.spoint* %sp)
  %tobool1 = icmp ne i32 %call, 0
  br i1 %tobool1, label %if.then2, label %if.end8

if.then2:                                         ; preds = %while.body
  %7 = load %struct.sphere*, %struct.sphere** %nearest_obj, align 8
  %tobool3 = icmp ne %struct.sphere* %7, null
  br i1 %tobool3, label %lor.lhs.false, label %if.then6

lor.lhs.false:                                    ; preds = %if.then2
  %dist = getelementptr inbounds %struct.spoint, %struct.spoint* %sp, i32 0, i32 3
  %8 = load double, double* %dist, align 8
  %dist4 = getelementptr inbounds %struct.spoint, %struct.spoint* %nearest_sp, i32 0, i32 3
  %9 = load double, double* %dist4, align 8
  %cmp5 = fcmp olt double %8, %9
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %lor.lhs.false, %if.then2
  %10 = load %struct.sphere*, %struct.sphere** %iter, align 8
  store %struct.sphere* %10, %struct.sphere** %nearest_obj, align 8
  %11 = bitcast %struct.spoint* %nearest_sp to i8*
  %12 = bitcast %struct.spoint* %sp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %11, i8* %12, i64 80, i32 8, i1 false)
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %lor.lhs.false
  br label %if.end8

if.end8:                                          ; preds = %if.end7, %while.body
  %13 = load %struct.sphere*, %struct.sphere** %iter, align 8
  %next9 = getelementptr inbounds %struct.sphere, %struct.sphere* %13, i32 0, i32 3
  %14 = load %struct.sphere*, %struct.sphere** %next9, align 8
  store %struct.sphere* %14, %struct.sphere** %iter, align 8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = load %struct.sphere*, %struct.sphere** %nearest_obj, align 8
  %tobool10 = icmp ne %struct.sphere* %15, null
  br i1 %tobool10, label %if.then11, label %if.else

if.then11:                                        ; preds = %while.end
  %16 = load %struct.sphere*, %struct.sphere** %nearest_obj, align 8
  %17 = load i32, i32* %depth.addr, align 4
  call void @shade(%struct.vec3* sret %tmp, %struct.sphere* %16, %struct.spoint* %nearest_sp, i32 %17)
  %18 = bitcast %struct.vec3* %col to i8*
  %19 = bitcast %struct.vec3* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %18, i8* %19, i64 24, i32 8, i1 false)
  br label %if.end15

if.else:                                          ; preds = %while.end
  %z12 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 2
  store double 0.000000e+00, double* %z12, align 8
  %y13 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 1
  store double 0.000000e+00, double* %y13, align 8
  %x14 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 0
  store double 0.000000e+00, double* %x14, align 8
  br label %if.end15

if.end15:                                         ; preds = %if.else, %if.then11
  %20 = bitcast %struct.vec3* %agg.result to i8*
  %21 = bitcast %struct.vec3* %col to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %20, i8* %21, i64 24, i32 8, i1 false)
  br label %return

return:                                           ; preds = %if.end15, %if.then
  ret void
}

; Function Attrs: nounwind uwtable
define void @get_primary_ray(%struct.ray* noalias sret %agg.result, i32 %x, i32 %y, i32 %sample) #0 {
entry:
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %sample.addr = alloca i32, align 4
  %ray = alloca %struct.ray, align 8
  %m = alloca [3 x [3 x float]], align 16
  %i = alloca %struct.vec3, align 8
  %j = alloca %struct.vec3, align 8
  %k = alloca %struct.vec3, align 8
  %dir = alloca %struct.vec3, align 8
  %orig = alloca %struct.vec3, align 8
  %foo = alloca %struct.vec3, align 8
  %len = alloca double, align 8
  %tmp = alloca %struct.vec3, align 8
  %tmp19 = alloca %struct.vec3, align 8
  %tmp61 = alloca %struct.vec3, align 8
  store i32 %x, i32* %x.addr, align 4
  store i32 %y, i32* %y.addr, align 4
  store i32 %sample, i32* %sample.addr, align 4
  %0 = bitcast %struct.vec3* %j to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %0, i8* bitcast (%struct.vec3* @get_primary_ray.j to i8*), i64 24, i32 8, i1 false)
  %1 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 1, i32 0), align 8
  %2 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 0, i32 0), align 8
  %sub = fsub double %1, %2
  %x1 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 0
  store double %sub, double* %x1, align 8
  %3 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 1, i32 1), align 8
  %4 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 0, i32 1), align 8
  %sub2 = fsub double %3, %4
  %y3 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 1
  store double %sub2, double* %y3, align 8
  %5 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 1, i32 2), align 8
  %6 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 0, i32 2), align 8
  %sub4 = fsub double %5, %6
  %z = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 2
  store double %sub4, double* %z, align 8
  br label %do.body

do.body:                                          ; preds = %entry
  %x5 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 0
  %7 = load double, double* %x5, align 8
  %x6 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 0
  %8 = load double, double* %x6, align 8
  %mul = fmul double %7, %8
  %y7 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 1
  %9 = load double, double* %y7, align 8
  %y8 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 1
  %10 = load double, double* %y8, align 8
  %mul9 = fmul double %9, %10
  %add = fadd double %mul, %mul9
  %z10 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 2
  %11 = load double, double* %z10, align 8
  %z11 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 2
  %12 = load double, double* %z11, align 8
  %mul12 = fmul double %11, %12
  %add13 = fadd double %add, %mul12
  %call = call double @sqrt(double %add13) #8
  store double %call, double* %len, align 8
  %13 = load double, double* %len, align 8
  %x14 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 0
  %14 = load double, double* %x14, align 8
  %div = fdiv double %14, %13
  store double %div, double* %x14, align 8
  %15 = load double, double* %len, align 8
  %y15 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 1
  %16 = load double, double* %y15, align 8
  %div16 = fdiv double %16, %15
  store double %div16, double* %y15, align 8
  %17 = load double, double* %len, align 8
  %z17 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 2
  %18 = load double, double* %z17, align 8
  %div18 = fdiv double %18, %17
  store double %div18, double* %z17, align 8
  br label %do.end

do.end:                                           ; preds = %do.body
  call void @cross_product(%struct.vec3* sret %tmp, %struct.vec3* byval align 8 %j, %struct.vec3* byval align 8 %k)
  %19 = bitcast %struct.vec3* %i to i8*
  %20 = bitcast %struct.vec3* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %19, i8* %20, i64 24, i32 8, i1 false)
  call void @cross_product(%struct.vec3* sret %tmp19, %struct.vec3* byval align 8 %k, %struct.vec3* byval align 8 %i)
  %21 = bitcast %struct.vec3* %j to i8*
  %22 = bitcast %struct.vec3* %tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %21, i8* %22, i64 24, i32 8, i1 false)
  %x20 = getelementptr inbounds %struct.vec3, %struct.vec3* %i, i32 0, i32 0
  %23 = load double, double* %x20, align 8
  %conv = fptrunc double %23 to float
  %arrayidx = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx21 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx, i64 0, i64 0
  store float %conv, float* %arrayidx21, align 16
  %x22 = getelementptr inbounds %struct.vec3, %struct.vec3* %j, i32 0, i32 0
  %24 = load double, double* %x22, align 8
  %conv23 = fptrunc double %24 to float
  %arrayidx24 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx25 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx24, i64 0, i64 1
  store float %conv23, float* %arrayidx25, align 4
  %x26 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 0
  %25 = load double, double* %x26, align 8
  %conv27 = fptrunc double %25 to float
  %arrayidx28 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx29 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx28, i64 0, i64 2
  store float %conv27, float* %arrayidx29, align 8
  %y30 = getelementptr inbounds %struct.vec3, %struct.vec3* %i, i32 0, i32 1
  %26 = load double, double* %y30, align 8
  %conv31 = fptrunc double %26 to float
  %arrayidx32 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx33 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx32, i64 0, i64 0
  store float %conv31, float* %arrayidx33, align 4
  %y34 = getelementptr inbounds %struct.vec3, %struct.vec3* %j, i32 0, i32 1
  %27 = load double, double* %y34, align 8
  %conv35 = fptrunc double %27 to float
  %arrayidx36 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx37 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx36, i64 0, i64 1
  store float %conv35, float* %arrayidx37, align 4
  %y38 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 1
  %28 = load double, double* %y38, align 8
  %conv39 = fptrunc double %28 to float
  %arrayidx40 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx41 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx40, i64 0, i64 2
  store float %conv39, float* %arrayidx41, align 4
  %z42 = getelementptr inbounds %struct.vec3, %struct.vec3* %i, i32 0, i32 2
  %29 = load double, double* %z42, align 8
  %conv43 = fptrunc double %29 to float
  %arrayidx44 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx45 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx44, i64 0, i64 0
  store float %conv43, float* %arrayidx45, align 8
  %z46 = getelementptr inbounds %struct.vec3, %struct.vec3* %j, i32 0, i32 2
  %30 = load double, double* %z46, align 8
  %conv47 = fptrunc double %30 to float
  %arrayidx48 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx49 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx48, i64 0, i64 1
  store float %conv47, float* %arrayidx49, align 4
  %z50 = getelementptr inbounds %struct.vec3, %struct.vec3* %k, i32 0, i32 2
  %31 = load double, double* %z50, align 8
  %conv51 = fptrunc double %31 to float
  %arrayidx52 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx53 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx52, i64 0, i64 2
  store float %conv51, float* %arrayidx53, align 8
  %orig54 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z55 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig54, i32 0, i32 2
  store double 0.000000e+00, double* %z55, align 8
  %orig56 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y57 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig56, i32 0, i32 1
  store double 0.000000e+00, double* %y57, align 8
  %orig58 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x59 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig58, i32 0, i32 0
  store double 0.000000e+00, double* %x59, align 8
  %dir60 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %32 = load i32, i32* %x.addr, align 4
  %33 = load i32, i32* %y.addr, align 4
  %34 = load i32, i32* %sample.addr, align 4
  call void @get_sample_pos(%struct.vec3* sret %tmp61, i32 %32, i32 %33, i32 %34)
  %35 = bitcast %struct.vec3* %dir60 to i8*
  %36 = bitcast %struct.vec3* %tmp61 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %35, i8* %36, i64 24, i32 8, i1 false)
  %dir62 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z63 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir62, i32 0, i32 2
  store double 0x40045F306F4445A0, double* %z63, align 8
  %dir64 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x65 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir64, i32 0, i32 0
  %37 = load double, double* %x65, align 8
  %mul66 = fmul double %37, 1.000000e+03
  store double %mul66, double* %x65, align 8
  %dir67 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y68 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir67, i32 0, i32 1
  %38 = load double, double* %y68, align 8
  %mul69 = fmul double %38, 1.000000e+03
  store double %mul69, double* %y68, align 8
  %dir70 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z71 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir70, i32 0, i32 2
  %39 = load double, double* %z71, align 8
  %mul72 = fmul double %39, 1.000000e+03
  store double %mul72, double* %z71, align 8
  %dir73 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x74 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir73, i32 0, i32 0
  %40 = load double, double* %x74, align 8
  %orig75 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x76 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig75, i32 0, i32 0
  %41 = load double, double* %x76, align 8
  %add77 = fadd double %40, %41
  %x78 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 0
  store double %add77, double* %x78, align 8
  %dir79 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y80 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir79, i32 0, i32 1
  %42 = load double, double* %y80, align 8
  %orig81 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y82 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig81, i32 0, i32 1
  %43 = load double, double* %y82, align 8
  %add83 = fadd double %42, %43
  %y84 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 1
  store double %add83, double* %y84, align 8
  %dir85 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z86 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir85, i32 0, i32 2
  %44 = load double, double* %z86, align 8
  %orig87 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z88 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig87, i32 0, i32 2
  %45 = load double, double* %z88, align 8
  %add89 = fadd double %44, %45
  %z90 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 2
  store double %add89, double* %z90, align 8
  %x91 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 0
  %46 = load double, double* %x91, align 8
  %arrayidx92 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx93 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx92, i64 0, i64 0
  %47 = load float, float* %arrayidx93, align 16
  %conv94 = fpext float %47 to double
  %mul95 = fmul double %46, %conv94
  %y96 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 1
  %48 = load double, double* %y96, align 8
  %arrayidx97 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx98 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx97, i64 0, i64 1
  %49 = load float, float* %arrayidx98, align 4
  %conv99 = fpext float %49 to double
  %mul100 = fmul double %48, %conv99
  %add101 = fadd double %mul95, %mul100
  %z102 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 2
  %50 = load double, double* %z102, align 8
  %arrayidx103 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx104 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx103, i64 0, i64 2
  %51 = load float, float* %arrayidx104, align 8
  %conv105 = fpext float %51 to double
  %mul106 = fmul double %50, %conv105
  %add107 = fadd double %add101, %mul106
  %x108 = getelementptr inbounds %struct.vec3, %struct.vec3* %foo, i32 0, i32 0
  store double %add107, double* %x108, align 8
  %x109 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 0
  %52 = load double, double* %x109, align 8
  %arrayidx110 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx111 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx110, i64 0, i64 0
  %53 = load float, float* %arrayidx111, align 4
  %conv112 = fpext float %53 to double
  %mul113 = fmul double %52, %conv112
  %y114 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 1
  %54 = load double, double* %y114, align 8
  %arrayidx115 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx116 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx115, i64 0, i64 1
  %55 = load float, float* %arrayidx116, align 4
  %conv117 = fpext float %55 to double
  %mul118 = fmul double %54, %conv117
  %add119 = fadd double %mul113, %mul118
  %z120 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 2
  %56 = load double, double* %z120, align 8
  %arrayidx121 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx122 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx121, i64 0, i64 2
  %57 = load float, float* %arrayidx122, align 4
  %conv123 = fpext float %57 to double
  %mul124 = fmul double %56, %conv123
  %add125 = fadd double %add119, %mul124
  %y126 = getelementptr inbounds %struct.vec3, %struct.vec3* %foo, i32 0, i32 1
  store double %add125, double* %y126, align 8
  %x127 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 0
  %58 = load double, double* %x127, align 8
  %arrayidx128 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx129 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx128, i64 0, i64 0
  %59 = load float, float* %arrayidx129, align 8
  %conv130 = fpext float %59 to double
  %mul131 = fmul double %58, %conv130
  %y132 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 1
  %60 = load double, double* %y132, align 8
  %arrayidx133 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx134 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx133, i64 0, i64 1
  %61 = load float, float* %arrayidx134, align 4
  %conv135 = fpext float %61 to double
  %mul136 = fmul double %60, %conv135
  %add137 = fadd double %mul131, %mul136
  %z138 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 2
  %62 = load double, double* %z138, align 8
  %arrayidx139 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx140 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx139, i64 0, i64 2
  %63 = load float, float* %arrayidx140, align 8
  %conv141 = fpext float %63 to double
  %mul142 = fmul double %62, %conv141
  %add143 = fadd double %add137, %mul142
  %z144 = getelementptr inbounds %struct.vec3, %struct.vec3* %foo, i32 0, i32 2
  store double %add143, double* %z144, align 8
  %orig145 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x146 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig145, i32 0, i32 0
  %64 = load double, double* %x146, align 8
  %arrayidx147 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx148 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx147, i64 0, i64 0
  %65 = load float, float* %arrayidx148, align 16
  %conv149 = fpext float %65 to double
  %mul150 = fmul double %64, %conv149
  %orig151 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y152 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig151, i32 0, i32 1
  %66 = load double, double* %y152, align 8
  %arrayidx153 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx154 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx153, i64 0, i64 1
  %67 = load float, float* %arrayidx154, align 4
  %conv155 = fpext float %67 to double
  %mul156 = fmul double %66, %conv155
  %add157 = fadd double %mul150, %mul156
  %orig158 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z159 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig158, i32 0, i32 2
  %68 = load double, double* %z159, align 8
  %arrayidx160 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 0
  %arrayidx161 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx160, i64 0, i64 2
  %69 = load float, float* %arrayidx161, align 8
  %conv162 = fpext float %69 to double
  %mul163 = fmul double %68, %conv162
  %add164 = fadd double %add157, %mul163
  %70 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 0, i32 0), align 8
  %add165 = fadd double %add164, %70
  %x166 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig, i32 0, i32 0
  store double %add165, double* %x166, align 8
  %orig167 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x168 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig167, i32 0, i32 0
  %71 = load double, double* %x168, align 8
  %arrayidx169 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx170 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx169, i64 0, i64 0
  %72 = load float, float* %arrayidx170, align 4
  %conv171 = fpext float %72 to double
  %mul172 = fmul double %71, %conv171
  %orig173 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y174 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig173, i32 0, i32 1
  %73 = load double, double* %y174, align 8
  %arrayidx175 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx176 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx175, i64 0, i64 1
  %74 = load float, float* %arrayidx176, align 4
  %conv177 = fpext float %74 to double
  %mul178 = fmul double %73, %conv177
  %add179 = fadd double %mul172, %mul178
  %orig180 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z181 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig180, i32 0, i32 2
  %75 = load double, double* %z181, align 8
  %arrayidx182 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 1
  %arrayidx183 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx182, i64 0, i64 2
  %76 = load float, float* %arrayidx183, align 4
  %conv184 = fpext float %76 to double
  %mul185 = fmul double %75, %conv184
  %add186 = fadd double %add179, %mul185
  %77 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 0, i32 1), align 8
  %add187 = fadd double %add186, %77
  %y188 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig, i32 0, i32 1
  store double %add187, double* %y188, align 8
  %orig189 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x190 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig189, i32 0, i32 0
  %78 = load double, double* %x190, align 8
  %arrayidx191 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx192 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx191, i64 0, i64 0
  %79 = load float, float* %arrayidx192, align 8
  %conv193 = fpext float %79 to double
  %mul194 = fmul double %78, %conv193
  %orig195 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y196 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig195, i32 0, i32 1
  %80 = load double, double* %y196, align 8
  %arrayidx197 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx198 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx197, i64 0, i64 1
  %81 = load float, float* %arrayidx198, align 4
  %conv199 = fpext float %81 to double
  %mul200 = fmul double %80, %conv199
  %add201 = fadd double %mul194, %mul200
  %orig202 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z203 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig202, i32 0, i32 2
  %82 = load double, double* %z203, align 8
  %arrayidx204 = getelementptr inbounds [3 x [3 x float]], [3 x [3 x float]]* %m, i64 0, i64 2
  %arrayidx205 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx204, i64 0, i64 2
  %83 = load float, float* %arrayidx205, align 8
  %conv206 = fpext float %83 to double
  %mul207 = fmul double %82, %conv206
  %add208 = fadd double %add201, %mul207
  %84 = load double, double* getelementptr inbounds (%struct.camera, %struct.camera* @cam, i32 0, i32 0, i32 2), align 8
  %add209 = fadd double %add208, %84
  %z210 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig, i32 0, i32 2
  store double %add209, double* %z210, align 8
  %orig211 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %85 = bitcast %struct.vec3* %orig211 to i8*
  %86 = bitcast %struct.vec3* %orig to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %85, i8* %86, i64 24, i32 8, i1 false)
  %x212 = getelementptr inbounds %struct.vec3, %struct.vec3* %foo, i32 0, i32 0
  %87 = load double, double* %x212, align 8
  %x213 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig, i32 0, i32 0
  %88 = load double, double* %x213, align 8
  %add214 = fadd double %87, %88
  %dir215 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x216 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir215, i32 0, i32 0
  store double %add214, double* %x216, align 8
  %y217 = getelementptr inbounds %struct.vec3, %struct.vec3* %foo, i32 0, i32 1
  %89 = load double, double* %y217, align 8
  %y218 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig, i32 0, i32 1
  %90 = load double, double* %y218, align 8
  %add219 = fadd double %89, %90
  %dir220 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y221 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir220, i32 0, i32 1
  store double %add219, double* %y221, align 8
  %z222 = getelementptr inbounds %struct.vec3, %struct.vec3* %foo, i32 0, i32 2
  %91 = load double, double* %z222, align 8
  %z223 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig, i32 0, i32 2
  %92 = load double, double* %z223, align 8
  %add224 = fadd double %91, %92
  %dir225 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z226 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir225, i32 0, i32 2
  store double %add224, double* %z226, align 8
  %93 = bitcast %struct.ray* %agg.result to i8*
  %94 = bitcast %struct.ray* %ray to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %93, i8* %94, i64 48, i32 8, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #5

; Function Attrs: nounwind uwtable
define i32 @ray_sphere(%struct.sphere* %sph, %struct.ray* byval align 8 %ray, %struct.spoint* %sp) #0 {
entry:
  %retval = alloca i32, align 4
  %sph.addr = alloca %struct.sphere*, align 8
  %sp.addr = alloca %struct.spoint*, align 8
  %a = alloca double, align 8
  %b = alloca double, align 8
  %c = alloca double, align 8
  %d = alloca double, align 8
  %sqrt_d = alloca double, align 8
  %t1 = alloca double, align 8
  %t2 = alloca double, align 8
  %tmp = alloca %struct.vec3, align 8
  %len = alloca double, align 8
  store %struct.sphere* %sph, %struct.sphere** %sph.addr, align 8
  store %struct.spoint* %sp, %struct.spoint** %sp.addr, align 8
  %dir = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %dir, i32 0, i32 0
  %0 = load double, double* %x, align 8
  %dir1 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x2 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir1, i32 0, i32 0
  %1 = load double, double* %x2, align 8
  %mul = fmul double %0, %1
  %dir3 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y = getelementptr inbounds %struct.vec3, %struct.vec3* %dir3, i32 0, i32 1
  %2 = load double, double* %y, align 8
  %dir4 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y5 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir4, i32 0, i32 1
  %3 = load double, double* %y5, align 8
  %mul6 = fmul double %2, %3
  %add = fadd double %mul, %mul6
  %dir7 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z = getelementptr inbounds %struct.vec3, %struct.vec3* %dir7, i32 0, i32 2
  %4 = load double, double* %z, align 8
  %dir8 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z9 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir8, i32 0, i32 2
  %5 = load double, double* %z9, align 8
  %mul10 = fmul double %4, %5
  %add11 = fadd double %add, %mul10
  store double %add11, double* %a, align 8
  %dir12 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x13 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir12, i32 0, i32 0
  %6 = load double, double* %x13, align 8
  %mul14 = fmul double 2.000000e+00, %6
  %orig = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x15 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig, i32 0, i32 0
  %7 = load double, double* %x15, align 8
  %8 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos = getelementptr inbounds %struct.sphere, %struct.sphere* %8, i32 0, i32 0
  %x16 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos, i32 0, i32 0
  %9 = load double, double* %x16, align 8
  %sub = fsub double %7, %9
  %mul17 = fmul double %mul14, %sub
  %dir18 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y19 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir18, i32 0, i32 1
  %10 = load double, double* %y19, align 8
  %mul20 = fmul double 2.000000e+00, %10
  %orig21 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y22 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig21, i32 0, i32 1
  %11 = load double, double* %y22, align 8
  %12 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos23 = getelementptr inbounds %struct.sphere, %struct.sphere* %12, i32 0, i32 0
  %y24 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos23, i32 0, i32 1
  %13 = load double, double* %y24, align 8
  %sub25 = fsub double %11, %13
  %mul26 = fmul double %mul20, %sub25
  %add27 = fadd double %mul17, %mul26
  %dir28 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z29 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir28, i32 0, i32 2
  %14 = load double, double* %z29, align 8
  %mul30 = fmul double 2.000000e+00, %14
  %orig31 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z32 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig31, i32 0, i32 2
  %15 = load double, double* %z32, align 8
  %16 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos33 = getelementptr inbounds %struct.sphere, %struct.sphere* %16, i32 0, i32 0
  %z34 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos33, i32 0, i32 2
  %17 = load double, double* %z34, align 8
  %sub35 = fsub double %15, %17
  %mul36 = fmul double %mul30, %sub35
  %add37 = fadd double %add27, %mul36
  store double %add37, double* %b, align 8
  %18 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos38 = getelementptr inbounds %struct.sphere, %struct.sphere* %18, i32 0, i32 0
  %x39 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos38, i32 0, i32 0
  %19 = load double, double* %x39, align 8
  %20 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos40 = getelementptr inbounds %struct.sphere, %struct.sphere* %20, i32 0, i32 0
  %x41 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos40, i32 0, i32 0
  %21 = load double, double* %x41, align 8
  %mul42 = fmul double %19, %21
  %22 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos43 = getelementptr inbounds %struct.sphere, %struct.sphere* %22, i32 0, i32 0
  %y44 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos43, i32 0, i32 1
  %23 = load double, double* %y44, align 8
  %24 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos45 = getelementptr inbounds %struct.sphere, %struct.sphere* %24, i32 0, i32 0
  %y46 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos45, i32 0, i32 1
  %25 = load double, double* %y46, align 8
  %mul47 = fmul double %23, %25
  %add48 = fadd double %mul42, %mul47
  %26 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos49 = getelementptr inbounds %struct.sphere, %struct.sphere* %26, i32 0, i32 0
  %z50 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos49, i32 0, i32 2
  %27 = load double, double* %z50, align 8
  %28 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos51 = getelementptr inbounds %struct.sphere, %struct.sphere* %28, i32 0, i32 0
  %z52 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos51, i32 0, i32 2
  %29 = load double, double* %z52, align 8
  %mul53 = fmul double %27, %29
  %add54 = fadd double %add48, %mul53
  %orig55 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x56 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig55, i32 0, i32 0
  %30 = load double, double* %x56, align 8
  %orig57 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x58 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig57, i32 0, i32 0
  %31 = load double, double* %x58, align 8
  %mul59 = fmul double %30, %31
  %add60 = fadd double %add54, %mul59
  %orig61 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y62 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig61, i32 0, i32 1
  %32 = load double, double* %y62, align 8
  %orig63 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y64 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig63, i32 0, i32 1
  %33 = load double, double* %y64, align 8
  %mul65 = fmul double %32, %33
  %add66 = fadd double %add60, %mul65
  %orig67 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z68 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig67, i32 0, i32 2
  %34 = load double, double* %z68, align 8
  %orig69 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z70 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig69, i32 0, i32 2
  %35 = load double, double* %z70, align 8
  %mul71 = fmul double %34, %35
  %add72 = fadd double %add66, %mul71
  %36 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos73 = getelementptr inbounds %struct.sphere, %struct.sphere* %36, i32 0, i32 0
  %x74 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos73, i32 0, i32 0
  %37 = load double, double* %x74, align 8
  %sub75 = fsub double -0.000000e+00, %37
  %orig76 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x77 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig76, i32 0, i32 0
  %38 = load double, double* %x77, align 8
  %mul78 = fmul double %sub75, %38
  %39 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos79 = getelementptr inbounds %struct.sphere, %struct.sphere* %39, i32 0, i32 0
  %y80 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos79, i32 0, i32 1
  %40 = load double, double* %y80, align 8
  %orig81 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y82 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig81, i32 0, i32 1
  %41 = load double, double* %y82, align 8
  %mul83 = fmul double %40, %41
  %sub84 = fsub double %mul78, %mul83
  %42 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos85 = getelementptr inbounds %struct.sphere, %struct.sphere* %42, i32 0, i32 0
  %z86 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos85, i32 0, i32 2
  %43 = load double, double* %z86, align 8
  %orig87 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z88 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig87, i32 0, i32 2
  %44 = load double, double* %z88, align 8
  %mul89 = fmul double %43, %44
  %sub90 = fsub double %sub84, %mul89
  %mul91 = fmul double 2.000000e+00, %sub90
  %add92 = fadd double %add72, %mul91
  %45 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %rad = getelementptr inbounds %struct.sphere, %struct.sphere* %45, i32 0, i32 1
  %46 = load double, double* %rad, align 8
  %47 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %rad93 = getelementptr inbounds %struct.sphere, %struct.sphere* %47, i32 0, i32 1
  %48 = load double, double* %rad93, align 8
  %mul94 = fmul double %46, %48
  %sub95 = fsub double %add92, %mul94
  store double %sub95, double* %c, align 8
  %49 = load double, double* %b, align 8
  %50 = load double, double* %b, align 8
  %mul96 = fmul double %49, %50
  %51 = load double, double* %a, align 8
  %mul97 = fmul double 4.000000e+00, %51
  %52 = load double, double* %c, align 8
  %mul98 = fmul double %mul97, %52
  %sub99 = fsub double %mul96, %mul98
  store double %sub99, double* %d, align 8
  %cmp = fcmp olt double %sub99, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %53 = load double, double* %d, align 8
  %call = call double @sqrt(double %53) #8
  store double %call, double* %sqrt_d, align 8
  %54 = load double, double* %b, align 8
  %sub100 = fsub double -0.000000e+00, %54
  %55 = load double, double* %sqrt_d, align 8
  %add101 = fadd double %sub100, %55
  %56 = load double, double* %a, align 8
  %mul102 = fmul double 2.000000e+00, %56
  %div = fdiv double %add101, %mul102
  store double %div, double* %t1, align 8
  %57 = load double, double* %b, align 8
  %sub103 = fsub double -0.000000e+00, %57
  %58 = load double, double* %sqrt_d, align 8
  %sub104 = fsub double %sub103, %58
  %59 = load double, double* %a, align 8
  %mul105 = fmul double 2.000000e+00, %59
  %div106 = fdiv double %sub104, %mul105
  store double %div106, double* %t2, align 8
  %60 = load double, double* %t1, align 8
  %cmp107 = fcmp olt double %60, 1.000000e-06
  br i1 %cmp107, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %if.end
  %61 = load double, double* %t2, align 8
  %cmp108 = fcmp olt double %61, 1.000000e-06
  br i1 %cmp108, label %if.then112, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %if.end
  %62 = load double, double* %t1, align 8
  %cmp109 = fcmp ogt double %62, 1.000000e+00
  br i1 %cmp109, label %land.lhs.true110, label %if.end113

land.lhs.true110:                                 ; preds = %lor.lhs.false
  %63 = load double, double* %t2, align 8
  %cmp111 = fcmp ogt double %63, 1.000000e+00
  br i1 %cmp111, label %if.then112, label %if.end113

if.then112:                                       ; preds = %land.lhs.true110, %land.lhs.true
  store i32 0, i32* %retval, align 4
  br label %return

if.end113:                                        ; preds = %land.lhs.true110, %lor.lhs.false
  %64 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %tobool = icmp ne %struct.spoint* %64, null
  br i1 %tobool, label %if.then114, label %if.end205

if.then114:                                       ; preds = %if.end113
  %65 = load double, double* %t1, align 8
  %cmp115 = fcmp olt double %65, 1.000000e-06
  br i1 %cmp115, label %if.then116, label %if.end117

if.then116:                                       ; preds = %if.then114
  %66 = load double, double* %t2, align 8
  store double %66, double* %t1, align 8
  br label %if.end117

if.end117:                                        ; preds = %if.then116, %if.then114
  %67 = load double, double* %t2, align 8
  %cmp118 = fcmp olt double %67, 1.000000e-06
  br i1 %cmp118, label %if.then119, label %if.end120

if.then119:                                       ; preds = %if.end117
  %68 = load double, double* %t1, align 8
  store double %68, double* %t2, align 8
  br label %if.end120

if.end120:                                        ; preds = %if.then119, %if.end117
  %69 = load double, double* %t1, align 8
  %70 = load double, double* %t2, align 8
  %cmp121 = fcmp olt double %69, %70
  br i1 %cmp121, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end120
  %71 = load double, double* %t1, align 8
  br label %cond.end

cond.false:                                       ; preds = %if.end120
  %72 = load double, double* %t2, align 8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %71, %cond.true ], [ %72, %cond.false ]
  %73 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %dist = getelementptr inbounds %struct.spoint, %struct.spoint* %73, i32 0, i32 3
  store double %cond, double* %dist, align 8
  %orig122 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %x123 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig122, i32 0, i32 0
  %74 = load double, double* %x123, align 8
  %dir124 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x125 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir124, i32 0, i32 0
  %75 = load double, double* %x125, align 8
  %76 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %dist126 = getelementptr inbounds %struct.spoint, %struct.spoint* %76, i32 0, i32 3
  %77 = load double, double* %dist126, align 8
  %mul127 = fmul double %75, %77
  %add128 = fadd double %74, %mul127
  %78 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %pos129 = getelementptr inbounds %struct.spoint, %struct.spoint* %78, i32 0, i32 0
  %x130 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos129, i32 0, i32 0
  store double %add128, double* %x130, align 8
  %orig131 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %y132 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig131, i32 0, i32 1
  %79 = load double, double* %y132, align 8
  %dir133 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y134 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir133, i32 0, i32 1
  %80 = load double, double* %y134, align 8
  %81 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %dist135 = getelementptr inbounds %struct.spoint, %struct.spoint* %81, i32 0, i32 3
  %82 = load double, double* %dist135, align 8
  %mul136 = fmul double %80, %82
  %add137 = fadd double %79, %mul136
  %83 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %pos138 = getelementptr inbounds %struct.spoint, %struct.spoint* %83, i32 0, i32 0
  %y139 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos138, i32 0, i32 1
  store double %add137, double* %y139, align 8
  %orig140 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %z141 = getelementptr inbounds %struct.vec3, %struct.vec3* %orig140, i32 0, i32 2
  %84 = load double, double* %z141, align 8
  %dir142 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z143 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir142, i32 0, i32 2
  %85 = load double, double* %z143, align 8
  %86 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %dist144 = getelementptr inbounds %struct.spoint, %struct.spoint* %86, i32 0, i32 3
  %87 = load double, double* %dist144, align 8
  %mul145 = fmul double %85, %87
  %add146 = fadd double %84, %mul145
  %88 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %pos147 = getelementptr inbounds %struct.spoint, %struct.spoint* %88, i32 0, i32 0
  %z148 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos147, i32 0, i32 2
  store double %add146, double* %z148, align 8
  %89 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %pos149 = getelementptr inbounds %struct.spoint, %struct.spoint* %89, i32 0, i32 0
  %x150 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos149, i32 0, i32 0
  %90 = load double, double* %x150, align 8
  %91 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos151 = getelementptr inbounds %struct.sphere, %struct.sphere* %91, i32 0, i32 0
  %x152 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos151, i32 0, i32 0
  %92 = load double, double* %x152, align 8
  %sub153 = fsub double %90, %92
  %93 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %rad154 = getelementptr inbounds %struct.sphere, %struct.sphere* %93, i32 0, i32 1
  %94 = load double, double* %rad154, align 8
  %div155 = fdiv double %sub153, %94
  %95 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %normal = getelementptr inbounds %struct.spoint, %struct.spoint* %95, i32 0, i32 1
  %x156 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal, i32 0, i32 0
  store double %div155, double* %x156, align 8
  %96 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %pos157 = getelementptr inbounds %struct.spoint, %struct.spoint* %96, i32 0, i32 0
  %y158 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos157, i32 0, i32 1
  %97 = load double, double* %y158, align 8
  %98 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos159 = getelementptr inbounds %struct.sphere, %struct.sphere* %98, i32 0, i32 0
  %y160 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos159, i32 0, i32 1
  %99 = load double, double* %y160, align 8
  %sub161 = fsub double %97, %99
  %100 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %rad162 = getelementptr inbounds %struct.sphere, %struct.sphere* %100, i32 0, i32 1
  %101 = load double, double* %rad162, align 8
  %div163 = fdiv double %sub161, %101
  %102 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %normal164 = getelementptr inbounds %struct.spoint, %struct.spoint* %102, i32 0, i32 1
  %y165 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal164, i32 0, i32 1
  store double %div163, double* %y165, align 8
  %103 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %pos166 = getelementptr inbounds %struct.spoint, %struct.spoint* %103, i32 0, i32 0
  %z167 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos166, i32 0, i32 2
  %104 = load double, double* %z167, align 8
  %105 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %pos168 = getelementptr inbounds %struct.sphere, %struct.sphere* %105, i32 0, i32 0
  %z169 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos168, i32 0, i32 2
  %106 = load double, double* %z169, align 8
  %sub170 = fsub double %104, %106
  %107 = load %struct.sphere*, %struct.sphere** %sph.addr, align 8
  %rad171 = getelementptr inbounds %struct.sphere, %struct.sphere* %107, i32 0, i32 1
  %108 = load double, double* %rad171, align 8
  %div172 = fdiv double %sub170, %108
  %109 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %normal173 = getelementptr inbounds %struct.spoint, %struct.spoint* %109, i32 0, i32 1
  %z174 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal173, i32 0, i32 2
  store double %div172, double* %z174, align 8
  %110 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref = getelementptr inbounds %struct.spoint, %struct.spoint* %110, i32 0, i32 2
  %dir175 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %111 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %normal176 = getelementptr inbounds %struct.spoint, %struct.spoint* %111, i32 0, i32 1
  call void @reflect(%struct.vec3* sret %tmp, %struct.vec3* byval align 8 %dir175, %struct.vec3* byval align 8 %normal176)
  %112 = bitcast %struct.vec3* %vref to i8*
  %113 = bitcast %struct.vec3* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %112, i8* %113, i64 24, i32 8, i1 false)
  br label %do.body

do.body:                                          ; preds = %cond.end
  %114 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref178 = getelementptr inbounds %struct.spoint, %struct.spoint* %114, i32 0, i32 2
  %x179 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref178, i32 0, i32 0
  %115 = load double, double* %x179, align 8
  %116 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref180 = getelementptr inbounds %struct.spoint, %struct.spoint* %116, i32 0, i32 2
  %x181 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref180, i32 0, i32 0
  %117 = load double, double* %x181, align 8
  %mul182 = fmul double %115, %117
  %118 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref183 = getelementptr inbounds %struct.spoint, %struct.spoint* %118, i32 0, i32 2
  %y184 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref183, i32 0, i32 1
  %119 = load double, double* %y184, align 8
  %120 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref185 = getelementptr inbounds %struct.spoint, %struct.spoint* %120, i32 0, i32 2
  %y186 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref185, i32 0, i32 1
  %121 = load double, double* %y186, align 8
  %mul187 = fmul double %119, %121
  %add188 = fadd double %mul182, %mul187
  %122 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref189 = getelementptr inbounds %struct.spoint, %struct.spoint* %122, i32 0, i32 2
  %z190 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref189, i32 0, i32 2
  %123 = load double, double* %z190, align 8
  %124 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref191 = getelementptr inbounds %struct.spoint, %struct.spoint* %124, i32 0, i32 2
  %z192 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref191, i32 0, i32 2
  %125 = load double, double* %z192, align 8
  %mul193 = fmul double %123, %125
  %add194 = fadd double %add188, %mul193
  %call195 = call double @sqrt(double %add194) #8
  store double %call195, double* %len, align 8
  %126 = load double, double* %len, align 8
  %127 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref196 = getelementptr inbounds %struct.spoint, %struct.spoint* %127, i32 0, i32 2
  %x197 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref196, i32 0, i32 0
  %128 = load double, double* %x197, align 8
  %div198 = fdiv double %128, %126
  store double %div198, double* %x197, align 8
  %129 = load double, double* %len, align 8
  %130 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref199 = getelementptr inbounds %struct.spoint, %struct.spoint* %130, i32 0, i32 2
  %y200 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref199, i32 0, i32 1
  %131 = load double, double* %y200, align 8
  %div201 = fdiv double %131, %129
  store double %div201, double* %y200, align 8
  %132 = load double, double* %len, align 8
  %133 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref202 = getelementptr inbounds %struct.spoint, %struct.spoint* %133, i32 0, i32 2
  %z203 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref202, i32 0, i32 2
  %134 = load double, double* %z203, align 8
  %div204 = fdiv double %134, %132
  store double %div204, double* %z203, align 8
  br label %do.end

do.end:                                           ; preds = %do.body
  br label %if.end205

if.end205:                                        ; preds = %do.end, %if.end113
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end205, %if.then112, %if.then
  %135 = load i32, i32* %retval, align 4
  ret i32 %135
}

; Function Attrs: nounwind uwtable
define void @shade(%struct.vec3* noalias sret %agg.result, %struct.sphere* %obj, %struct.spoint* %sp, i32 %depth) #0 {
entry:
  %obj.addr = alloca %struct.sphere*, align 8
  %sp.addr = alloca %struct.spoint*, align 8
  %depth.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %col = alloca %struct.vec3, align 8
  %ispec = alloca double, align 8
  %idiff = alloca double, align 8
  %ldir = alloca %struct.vec3, align 8
  %shadow_ray = alloca %struct.ray, align 8
  %iter = alloca %struct.sphere*, align 8
  %in_shadow = alloca i32, align 4
  %len = alloca double, align 8
  %ray = alloca %struct.ray, align 8
  %rcol = alloca %struct.vec3, align 8
  %tmp = alloca %struct.vec3, align 8
  store %struct.sphere* %obj, %struct.sphere** %obj.addr, align 8
  store %struct.spoint* %sp, %struct.spoint** %sp.addr, align 8
  store i32 %depth, i32* %depth.addr, align 4
  %0 = bitcast %struct.vec3* %col to i8*
  call void @llvm.memset.p0i8.i64(i8* %0, i8 0, i64 24, i32 8, i1 false)
  store i32 0, i32* %i, align 4, !apollo.pragma !1
  br label %for.cond, !apollo.pragma !1

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !apollo.pragma !1
  %2 = load i32, i32* @lnum, align 4, !apollo.pragma !1
  %cmp = icmp slt i32 %1, %2, !apollo.pragma !1
  br i1 %cmp, label %for.body, label %for.end, !apollo.pragma !1

for.body:                                         ; preds = %for.cond
  %3 = load %struct.sphere*, %struct.sphere** @obj_list, align 8, !apollo.pragma !1
  %next = getelementptr inbounds %struct.sphere, %struct.sphere* %3, i32 0, i32 3, !apollo.pragma !1
  %4 = load %struct.sphere*, %struct.sphere** %next, align 8, !apollo.pragma !1
  store %struct.sphere* %4, %struct.sphere** %iter, align 8, !apollo.pragma !1
  store i32 0, i32* %in_shadow, align 4, !apollo.pragma !1
  %5 = load i32, i32* %i, align 4, !apollo.pragma !1
  %idxprom = sext i32 %5 to i64, !apollo.pragma !1
  %arrayidx = getelementptr inbounds [16 x %struct.vec3], [16 x %struct.vec3]* @lights, i64 0, i64 %idxprom, !apollo.pragma !1
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %arrayidx, i32 0, i32 0, !apollo.pragma !1
  %6 = load double, double* %x, align 8, !apollo.pragma !1
  %7 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %pos = getelementptr inbounds %struct.spoint, %struct.spoint* %7, i32 0, i32 0, !apollo.pragma !1
  %x1 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos, i32 0, i32 0, !apollo.pragma !1
  %8 = load double, double* %x1, align 8, !apollo.pragma !1
  %sub = fsub double %6, %8, !apollo.pragma !1
  %x2 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  store double %sub, double* %x2, align 8, !apollo.pragma !1
  %9 = load i32, i32* %i, align 4, !apollo.pragma !1
  %idxprom3 = sext i32 %9 to i64, !apollo.pragma !1
  %arrayidx4 = getelementptr inbounds [16 x %struct.vec3], [16 x %struct.vec3]* @lights, i64 0, i64 %idxprom3, !apollo.pragma !1
  %y = getelementptr inbounds %struct.vec3, %struct.vec3* %arrayidx4, i32 0, i32 1, !apollo.pragma !1
  %10 = load double, double* %y, align 8, !apollo.pragma !1
  %11 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %pos5 = getelementptr inbounds %struct.spoint, %struct.spoint* %11, i32 0, i32 0, !apollo.pragma !1
  %y6 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos5, i32 0, i32 1, !apollo.pragma !1
  %12 = load double, double* %y6, align 8, !apollo.pragma !1
  %sub7 = fsub double %10, %12, !apollo.pragma !1
  %y8 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  store double %sub7, double* %y8, align 8, !apollo.pragma !1
  %13 = load i32, i32* %i, align 4, !apollo.pragma !1
  %idxprom9 = sext i32 %13 to i64, !apollo.pragma !1
  %arrayidx10 = getelementptr inbounds [16 x %struct.vec3], [16 x %struct.vec3]* @lights, i64 0, i64 %idxprom9, !apollo.pragma !1
  %z = getelementptr inbounds %struct.vec3, %struct.vec3* %arrayidx10, i32 0, i32 2, !apollo.pragma !1
  %14 = load double, double* %z, align 8, !apollo.pragma !1
  %15 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %pos11 = getelementptr inbounds %struct.spoint, %struct.spoint* %15, i32 0, i32 0, !apollo.pragma !1
  %z12 = getelementptr inbounds %struct.vec3, %struct.vec3* %pos11, i32 0, i32 2, !apollo.pragma !1
  %16 = load double, double* %z12, align 8, !apollo.pragma !1
  %sub13 = fsub double %14, %16, !apollo.pragma !1
  %z14 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  store double %sub13, double* %z14, align 8, !apollo.pragma !1
  %orig = getelementptr inbounds %struct.ray, %struct.ray* %shadow_ray, i32 0, i32 0, !apollo.pragma !1
  %17 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %pos15 = getelementptr inbounds %struct.spoint, %struct.spoint* %17, i32 0, i32 0, !apollo.pragma !1
  %18 = bitcast %struct.vec3* %orig to i8*, !apollo.pragma !1
  %19 = bitcast %struct.vec3* %pos15 to i8*, !apollo.pragma !1
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %18, i8* %19, i64 24, i32 8, i1 false)
  %dir = getelementptr inbounds %struct.ray, %struct.ray* %shadow_ray, i32 0, i32 1, !apollo.pragma !1
  %20 = bitcast %struct.vec3* %dir to i8*, !apollo.pragma !1
  %21 = bitcast %struct.vec3* %ldir to i8*, !apollo.pragma !1
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %20, i8* %21, i64 24, i32 8, i1 false)
  br label %while.cond, !apollo.pragma !1

while.cond:                                       ; preds = %if.end, %for.body
  %22 = load %struct.sphere*, %struct.sphere** %iter, align 8, !apollo.pragma !1
  %tobool = icmp ne %struct.sphere* %22, null, !apollo.pragma !1
  br i1 %tobool, label %while.body, label %while.end, !apollo.pragma !1

while.body:                                       ; preds = %while.cond
  %23 = load %struct.sphere*, %struct.sphere** %iter, align 8, !apollo.pragma !1
  %call = call i32 @ray_sphere(%struct.sphere* %23, %struct.ray* byval align 8 %shadow_ray, %struct.spoint* null), !apollo.pragma !1
  %tobool16 = icmp ne i32 %call, 0, !apollo.pragma !1
  br i1 %tobool16, label %if.then, label %if.end, !apollo.pragma !1

if.then:                                          ; preds = %while.body
  store i32 1, i32* %in_shadow, align 4, !apollo.pragma !1
  br label %while.end, !apollo.pragma !1

if.end:                                           ; preds = %while.body
  %24 = load %struct.sphere*, %struct.sphere** %iter, align 8, !apollo.pragma !1
  %next17 = getelementptr inbounds %struct.sphere, %struct.sphere* %24, i32 0, i32 3, !apollo.pragma !1
  %25 = load %struct.sphere*, %struct.sphere** %next17, align 8, !apollo.pragma !1
  store %struct.sphere* %25, %struct.sphere** %iter, align 8, !apollo.pragma !1
  br label %while.cond, !apollo.pragma !1

while.end:                                        ; preds = %if.then, %while.cond
  %26 = load i32, i32* %in_shadow, align 4, !apollo.pragma !1
  %tobool18 = icmp ne i32 %26, 0, !apollo.pragma !1
  br i1 %tobool18, label %if.end124, label %if.then19, !apollo.pragma !1

if.then19:                                        ; preds = %while.end
  br label %do.body, !apollo.pragma !1

do.body:                                          ; preds = %if.then19
  %x20 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  %27 = load double, double* %x20, align 8, !apollo.pragma !1
  %x21 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  %28 = load double, double* %x21, align 8, !apollo.pragma !1
  %mul = fmul double %27, %28, !apollo.pragma !1
  %y22 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  %29 = load double, double* %y22, align 8, !apollo.pragma !1
  %y23 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  %30 = load double, double* %y23, align 8, !apollo.pragma !1
  %mul24 = fmul double %29, %30, !apollo.pragma !1
  %add = fadd double %mul, %mul24, !apollo.pragma !1
  %z25 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  %31 = load double, double* %z25, align 8, !apollo.pragma !1
  %z26 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  %32 = load double, double* %z26, align 8, !apollo.pragma !1
  %mul27 = fmul double %31, %32, !apollo.pragma !1
  %add28 = fadd double %add, %mul27, !apollo.pragma !1
  %call29 = call double @sqrt(double %add28) #8, !apollo.pragma !1
  store double %call29, double* %len, align 8, !apollo.pragma !1
  %33 = load double, double* %len, align 8, !apollo.pragma !1
  %x30 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  %34 = load double, double* %x30, align 8, !apollo.pragma !1
  %div = fdiv double %34, %33, !apollo.pragma !1
  store double %div, double* %x30, align 8, !apollo.pragma !1
  %35 = load double, double* %len, align 8, !apollo.pragma !1
  %y31 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  %36 = load double, double* %y31, align 8, !apollo.pragma !1
  %div32 = fdiv double %36, %35, !apollo.pragma !1
  store double %div32, double* %y31, align 8, !apollo.pragma !1
  %37 = load double, double* %len, align 8, !apollo.pragma !1
  %z33 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  %38 = load double, double* %z33, align 8, !apollo.pragma !1
  %div34 = fdiv double %38, %37, !apollo.pragma !1
  store double %div34, double* %z33, align 8, !apollo.pragma !1
  br label %do.end, !apollo.pragma !1

do.end:                                           ; preds = %do.body
  %39 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %normal = getelementptr inbounds %struct.spoint, %struct.spoint* %39, i32 0, i32 1, !apollo.pragma !1
  %x35 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal, i32 0, i32 0, !apollo.pragma !1
  %40 = load double, double* %x35, align 8, !apollo.pragma !1
  %x36 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  %41 = load double, double* %x36, align 8, !apollo.pragma !1
  %mul37 = fmul double %40, %41, !apollo.pragma !1
  %42 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %normal38 = getelementptr inbounds %struct.spoint, %struct.spoint* %42, i32 0, i32 1, !apollo.pragma !1
  %y39 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal38, i32 0, i32 1, !apollo.pragma !1
  %43 = load double, double* %y39, align 8, !apollo.pragma !1
  %y40 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  %44 = load double, double* %y40, align 8, !apollo.pragma !1
  %mul41 = fmul double %43, %44, !apollo.pragma !1
  %add42 = fadd double %mul37, %mul41, !apollo.pragma !1
  %45 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %normal43 = getelementptr inbounds %struct.spoint, %struct.spoint* %45, i32 0, i32 1, !apollo.pragma !1
  %z44 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal43, i32 0, i32 2, !apollo.pragma !1
  %46 = load double, double* %z44, align 8, !apollo.pragma !1
  %z45 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  %47 = load double, double* %z45, align 8, !apollo.pragma !1
  %mul46 = fmul double %46, %47, !apollo.pragma !1
  %add47 = fadd double %add42, %mul46, !apollo.pragma !1
  %cmp48 = fcmp ogt double %add47, 0.000000e+00, !apollo.pragma !1
  br i1 %cmp48, label %cond.true, label %cond.false, !apollo.pragma !1

cond.true:                                        ; preds = %do.end
  %48 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %normal49 = getelementptr inbounds %struct.spoint, %struct.spoint* %48, i32 0, i32 1, !apollo.pragma !1
  %x50 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal49, i32 0, i32 0, !apollo.pragma !1
  %49 = load double, double* %x50, align 8, !apollo.pragma !1
  %x51 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  %50 = load double, double* %x51, align 8, !apollo.pragma !1
  %mul52 = fmul double %49, %50, !apollo.pragma !1
  %51 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %normal53 = getelementptr inbounds %struct.spoint, %struct.spoint* %51, i32 0, i32 1, !apollo.pragma !1
  %y54 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal53, i32 0, i32 1, !apollo.pragma !1
  %52 = load double, double* %y54, align 8, !apollo.pragma !1
  %y55 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  %53 = load double, double* %y55, align 8, !apollo.pragma !1
  %mul56 = fmul double %52, %53, !apollo.pragma !1
  %add57 = fadd double %mul52, %mul56, !apollo.pragma !1
  %54 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %normal58 = getelementptr inbounds %struct.spoint, %struct.spoint* %54, i32 0, i32 1, !apollo.pragma !1
  %z59 = getelementptr inbounds %struct.vec3, %struct.vec3* %normal58, i32 0, i32 2, !apollo.pragma !1
  %55 = load double, double* %z59, align 8, !apollo.pragma !1
  %z60 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  %56 = load double, double* %z60, align 8, !apollo.pragma !1
  %mul61 = fmul double %55, %56, !apollo.pragma !1
  %add62 = fadd double %add57, %mul61, !apollo.pragma !1
  br label %cond.end, !apollo.pragma !1

cond.false:                                       ; preds = %do.end
  br label %cond.end, !apollo.pragma !1

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %add62, %cond.true ], [ 0.000000e+00, %cond.false ], !apollo.pragma !1
  store double %cond, double* %idiff, align 8, !apollo.pragma !1
  %57 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8, !apollo.pragma !1
  %mat = getelementptr inbounds %struct.sphere, %struct.sphere* %57, i32 0, i32 2, !apollo.pragma !1
  %spow = getelementptr inbounds %struct.material, %struct.material* %mat, i32 0, i32 1, !apollo.pragma !1
  %58 = load double, double* %spow, align 8, !apollo.pragma !1
  %cmp63 = fcmp ogt double %58, 0.000000e+00, !apollo.pragma !1
  br i1 %cmp63, label %cond.true64, label %cond.false100, !apollo.pragma !1

cond.true64:                                      ; preds = %cond.end
  %59 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %vref = getelementptr inbounds %struct.spoint, %struct.spoint* %59, i32 0, i32 2, !apollo.pragma !1
  %x65 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref, i32 0, i32 0, !apollo.pragma !1
  %60 = load double, double* %x65, align 8, !apollo.pragma !1
  %x66 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  %61 = load double, double* %x66, align 8, !apollo.pragma !1
  %mul67 = fmul double %60, %61, !apollo.pragma !1
  %62 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %vref68 = getelementptr inbounds %struct.spoint, %struct.spoint* %62, i32 0, i32 2, !apollo.pragma !1
  %y69 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref68, i32 0, i32 1, !apollo.pragma !1
  %63 = load double, double* %y69, align 8, !apollo.pragma !1
  %y70 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  %64 = load double, double* %y70, align 8, !apollo.pragma !1
  %mul71 = fmul double %63, %64, !apollo.pragma !1
  %add72 = fadd double %mul67, %mul71, !apollo.pragma !1
  %65 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %vref73 = getelementptr inbounds %struct.spoint, %struct.spoint* %65, i32 0, i32 2, !apollo.pragma !1
  %z74 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref73, i32 0, i32 2, !apollo.pragma !1
  %66 = load double, double* %z74, align 8, !apollo.pragma !1
  %z75 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  %67 = load double, double* %z75, align 8, !apollo.pragma !1
  %mul76 = fmul double %66, %67, !apollo.pragma !1
  %add77 = fadd double %add72, %mul76, !apollo.pragma !1
  %cmp78 = fcmp ogt double %add77, 0.000000e+00, !apollo.pragma !1
  br i1 %cmp78, label %cond.true79, label %cond.false94, !apollo.pragma !1

cond.true79:                                      ; preds = %cond.true64
  %68 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %vref80 = getelementptr inbounds %struct.spoint, %struct.spoint* %68, i32 0, i32 2, !apollo.pragma !1
  %x81 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref80, i32 0, i32 0, !apollo.pragma !1
  %69 = load double, double* %x81, align 8, !apollo.pragma !1
  %x82 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 0, !apollo.pragma !1
  %70 = load double, double* %x82, align 8, !apollo.pragma !1
  %mul83 = fmul double %69, %70, !apollo.pragma !1
  %71 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %vref84 = getelementptr inbounds %struct.spoint, %struct.spoint* %71, i32 0, i32 2, !apollo.pragma !1
  %y85 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref84, i32 0, i32 1, !apollo.pragma !1
  %72 = load double, double* %y85, align 8, !apollo.pragma !1
  %y86 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 1, !apollo.pragma !1
  %73 = load double, double* %y86, align 8, !apollo.pragma !1
  %mul87 = fmul double %72, %73, !apollo.pragma !1
  %add88 = fadd double %mul83, %mul87, !apollo.pragma !1
  %74 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8, !apollo.pragma !1
  %vref89 = getelementptr inbounds %struct.spoint, %struct.spoint* %74, i32 0, i32 2, !apollo.pragma !1
  %z90 = getelementptr inbounds %struct.vec3, %struct.vec3* %vref89, i32 0, i32 2, !apollo.pragma !1
  %75 = load double, double* %z90, align 8, !apollo.pragma !1
  %z91 = getelementptr inbounds %struct.vec3, %struct.vec3* %ldir, i32 0, i32 2, !apollo.pragma !1
  %76 = load double, double* %z91, align 8, !apollo.pragma !1
  %mul92 = fmul double %75, %76, !apollo.pragma !1
  %add93 = fadd double %add88, %mul92, !apollo.pragma !1
  br label %cond.end95, !apollo.pragma !1

cond.false94:                                     ; preds = %cond.true64
  br label %cond.end95, !apollo.pragma !1

cond.end95:                                       ; preds = %cond.false94, %cond.true79
  %cond96 = phi double [ %add93, %cond.true79 ], [ 0.000000e+00, %cond.false94 ], !apollo.pragma !1
  %77 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8, !apollo.pragma !1
  %mat97 = getelementptr inbounds %struct.sphere, %struct.sphere* %77, i32 0, i32 2, !apollo.pragma !1
  %spow98 = getelementptr inbounds %struct.material, %struct.material* %mat97, i32 0, i32 1, !apollo.pragma !1
  %78 = load double, double* %spow98, align 8, !apollo.pragma !1
  %call99 = call double @pow(double %cond96, double %78) #8, !apollo.pragma !1
  br label %cond.end101, !apollo.pragma !1

cond.false100:                                    ; preds = %cond.end
  br label %cond.end101, !apollo.pragma !1

cond.end101:                                      ; preds = %cond.false100, %cond.end95
  %cond102 = phi double [ %call99, %cond.end95 ], [ 0.000000e+00, %cond.false100 ], !apollo.pragma !1
  store double %cond102, double* %ispec, align 8, !apollo.pragma !1
  %79 = load double, double* %idiff, align 8, !apollo.pragma !1
  %80 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8, !apollo.pragma !1
  %mat103 = getelementptr inbounds %struct.sphere, %struct.sphere* %80, i32 0, i32 2, !apollo.pragma !1
  %col104 = getelementptr inbounds %struct.material, %struct.material* %mat103, i32 0, i32 0, !apollo.pragma !1
  %x105 = getelementptr inbounds %struct.vec3, %struct.vec3* %col104, i32 0, i32 0, !apollo.pragma !1
  %81 = load double, double* %x105, align 8, !apollo.pragma !1
  %mul106 = fmul double %79, %81, !apollo.pragma !1
  %82 = load double, double* %ispec, align 8, !apollo.pragma !1
  %add107 = fadd double %mul106, %82, !apollo.pragma !1
  %x108 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 0, !apollo.pragma !1
  %83 = load double, double* %x108, align 8, !apollo.pragma !1
  %add109 = fadd double %83, %add107, !apollo.pragma !1
  store double %add109, double* %x108, align 8, !apollo.pragma !1
  %84 = load double, double* %idiff, align 8, !apollo.pragma !1
  %85 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8, !apollo.pragma !1
  %mat110 = getelementptr inbounds %struct.sphere, %struct.sphere* %85, i32 0, i32 2, !apollo.pragma !1
  %col111 = getelementptr inbounds %struct.material, %struct.material* %mat110, i32 0, i32 0, !apollo.pragma !1
  %y112 = getelementptr inbounds %struct.vec3, %struct.vec3* %col111, i32 0, i32 1, !apollo.pragma !1
  %86 = load double, double* %y112, align 8, !apollo.pragma !1
  %mul113 = fmul double %84, %86, !apollo.pragma !1
  %87 = load double, double* %ispec, align 8, !apollo.pragma !1
  %add114 = fadd double %mul113, %87, !apollo.pragma !1
  %y115 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 1, !apollo.pragma !1
  %88 = load double, double* %y115, align 8, !apollo.pragma !1
  %add116 = fadd double %88, %add114, !apollo.pragma !1
  store double %add116, double* %y115, align 8, !apollo.pragma !1
  %89 = load double, double* %idiff, align 8, !apollo.pragma !1
  %90 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8, !apollo.pragma !1
  %mat117 = getelementptr inbounds %struct.sphere, %struct.sphere* %90, i32 0, i32 2, !apollo.pragma !1
  %col118 = getelementptr inbounds %struct.material, %struct.material* %mat117, i32 0, i32 0, !apollo.pragma !1
  %z119 = getelementptr inbounds %struct.vec3, %struct.vec3* %col118, i32 0, i32 2, !apollo.pragma !1
  %91 = load double, double* %z119, align 8, !apollo.pragma !1
  %mul120 = fmul double %89, %91, !apollo.pragma !1
  %92 = load double, double* %ispec, align 8, !apollo.pragma !1
  %add121 = fadd double %mul120, %92, !apollo.pragma !1
  %z122 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 2, !apollo.pragma !1
  %93 = load double, double* %z122, align 8, !apollo.pragma !1
  %add123 = fadd double %93, %add121, !apollo.pragma !1
  store double %add123, double* %z122, align 8, !apollo.pragma !1
  br label %if.end124, !apollo.pragma !1

if.end124:                                        ; preds = %cond.end101, %while.end
  br label %for.inc, !apollo.pragma !1

for.inc:                                          ; preds = %if.end124
  %94 = load i32, i32* %i, align 4, !apollo.pragma !1
  %inc = add nsw i32 %94, 1, !apollo.pragma !1
  store i32 %inc, i32* %i, align 4, !apollo.pragma !1
  br label %for.cond, !apollo.pragma !1

for.end:                                          ; preds = %for.cond
  %95 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8
  %mat125 = getelementptr inbounds %struct.sphere, %struct.sphere* %95, i32 0, i32 2
  %refl = getelementptr inbounds %struct.material, %struct.material* %mat125, i32 0, i32 2
  %96 = load double, double* %refl, align 8
  %cmp126 = fcmp ogt double %96, 0.000000e+00
  br i1 %cmp126, label %if.then127, label %if.end160

if.then127:                                       ; preds = %for.end
  %orig128 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 0
  %97 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %pos129 = getelementptr inbounds %struct.spoint, %struct.spoint* %97, i32 0, i32 0
  %98 = bitcast %struct.vec3* %orig128 to i8*
  %99 = bitcast %struct.vec3* %pos129 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %98, i8* %99, i64 24, i32 8, i1 false)
  %dir130 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %100 = load %struct.spoint*, %struct.spoint** %sp.addr, align 8
  %vref131 = getelementptr inbounds %struct.spoint, %struct.spoint* %100, i32 0, i32 2
  %101 = bitcast %struct.vec3* %dir130 to i8*
  %102 = bitcast %struct.vec3* %vref131 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %101, i8* %102, i64 24, i32 8, i1 false)
  %dir132 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %x133 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir132, i32 0, i32 0
  %103 = load double, double* %x133, align 8
  %mul134 = fmul double %103, 1.000000e+03
  store double %mul134, double* %x133, align 8
  %dir135 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %y136 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir135, i32 0, i32 1
  %104 = load double, double* %y136, align 8
  %mul137 = fmul double %104, 1.000000e+03
  store double %mul137, double* %y136, align 8
  %dir138 = getelementptr inbounds %struct.ray, %struct.ray* %ray, i32 0, i32 1
  %z139 = getelementptr inbounds %struct.vec3, %struct.vec3* %dir138, i32 0, i32 2
  %105 = load double, double* %z139, align 8
  %mul140 = fmul double %105, 1.000000e+03
  store double %mul140, double* %z139, align 8
  %106 = load i32, i32* %depth.addr, align 4
  %add141 = add nsw i32 %106, 1
  call void @trace(%struct.vec3* sret %tmp, %struct.ray* byval align 8 %ray, i32 %add141)
  %107 = bitcast %struct.vec3* %rcol to i8*
  %108 = bitcast %struct.vec3* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %107, i8* %108, i64 24, i32 8, i1 false)
  %x142 = getelementptr inbounds %struct.vec3, %struct.vec3* %rcol, i32 0, i32 0
  %109 = load double, double* %x142, align 8
  %110 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8
  %mat143 = getelementptr inbounds %struct.sphere, %struct.sphere* %110, i32 0, i32 2
  %refl144 = getelementptr inbounds %struct.material, %struct.material* %mat143, i32 0, i32 2
  %111 = load double, double* %refl144, align 8
  %mul145 = fmul double %109, %111
  %x146 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 0
  %112 = load double, double* %x146, align 8
  %add147 = fadd double %112, %mul145
  store double %add147, double* %x146, align 8
  %y148 = getelementptr inbounds %struct.vec3, %struct.vec3* %rcol, i32 0, i32 1
  %113 = load double, double* %y148, align 8
  %114 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8
  %mat149 = getelementptr inbounds %struct.sphere, %struct.sphere* %114, i32 0, i32 2
  %refl150 = getelementptr inbounds %struct.material, %struct.material* %mat149, i32 0, i32 2
  %115 = load double, double* %refl150, align 8
  %mul151 = fmul double %113, %115
  %y152 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 1
  %116 = load double, double* %y152, align 8
  %add153 = fadd double %116, %mul151
  store double %add153, double* %y152, align 8
  %z154 = getelementptr inbounds %struct.vec3, %struct.vec3* %rcol, i32 0, i32 2
  %117 = load double, double* %z154, align 8
  %118 = load %struct.sphere*, %struct.sphere** %obj.addr, align 8
  %mat155 = getelementptr inbounds %struct.sphere, %struct.sphere* %118, i32 0, i32 2
  %refl156 = getelementptr inbounds %struct.material, %struct.material* %mat155, i32 0, i32 2
  %119 = load double, double* %refl156, align 8
  %mul157 = fmul double %117, %119
  %z158 = getelementptr inbounds %struct.vec3, %struct.vec3* %col, i32 0, i32 2
  %120 = load double, double* %z158, align 8
  %add159 = fadd double %120, %mul157
  store double %add159, double* %z158, align 8
  br label %if.end160

if.end160:                                        ; preds = %if.then127, %for.end
  %121 = bitcast %struct.vec3* %agg.result to i8*
  %122 = bitcast %struct.vec3* %col to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %121, i8* %122, i64 24, i32 8, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture, i8, i64, i32, i1) #5

; Function Attrs: nounwind
declare double @sqrt(double) #4

; Function Attrs: nounwind
declare double @pow(double, double) #4

; Function Attrs: nounwind uwtable
define void @reflect(%struct.vec3* noalias sret %agg.result, %struct.vec3* byval align 8 %v, %struct.vec3* byval align 8 %n) #0 {
entry:
  %res = alloca %struct.vec3, align 8
  %dot = alloca double, align 8
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %v, i32 0, i32 0
  %0 = load double, double* %x, align 8
  %x1 = getelementptr inbounds %struct.vec3, %struct.vec3* %n, i32 0, i32 0
  %1 = load double, double* %x1, align 8
  %mul = fmul double %0, %1
  %y = getelementptr inbounds %struct.vec3, %struct.vec3* %v, i32 0, i32 1
  %2 = load double, double* %y, align 8
  %y2 = getelementptr inbounds %struct.vec3, %struct.vec3* %n, i32 0, i32 1
  %3 = load double, double* %y2, align 8
  %mul3 = fmul double %2, %3
  %add = fadd double %mul, %mul3
  %z = getelementptr inbounds %struct.vec3, %struct.vec3* %v, i32 0, i32 2
  %4 = load double, double* %z, align 8
  %z4 = getelementptr inbounds %struct.vec3, %struct.vec3* %n, i32 0, i32 2
  %5 = load double, double* %z4, align 8
  %mul5 = fmul double %4, %5
  %add6 = fadd double %add, %mul5
  store double %add6, double* %dot, align 8
  %6 = load double, double* %dot, align 8
  %mul7 = fmul double 2.000000e+00, %6
  %x8 = getelementptr inbounds %struct.vec3, %struct.vec3* %n, i32 0, i32 0
  %7 = load double, double* %x8, align 8
  %mul9 = fmul double %mul7, %7
  %x10 = getelementptr inbounds %struct.vec3, %struct.vec3* %v, i32 0, i32 0
  %8 = load double, double* %x10, align 8
  %sub = fsub double %mul9, %8
  %sub11 = fsub double -0.000000e+00, %sub
  %x12 = getelementptr inbounds %struct.vec3, %struct.vec3* %res, i32 0, i32 0
  store double %sub11, double* %x12, align 8
  %9 = load double, double* %dot, align 8
  %mul13 = fmul double 2.000000e+00, %9
  %y14 = getelementptr inbounds %struct.vec3, %struct.vec3* %n, i32 0, i32 1
  %10 = load double, double* %y14, align 8
  %mul15 = fmul double %mul13, %10
  %y16 = getelementptr inbounds %struct.vec3, %struct.vec3* %v, i32 0, i32 1
  %11 = load double, double* %y16, align 8
  %sub17 = fsub double %mul15, %11
  %sub18 = fsub double -0.000000e+00, %sub17
  %y19 = getelementptr inbounds %struct.vec3, %struct.vec3* %res, i32 0, i32 1
  store double %sub18, double* %y19, align 8
  %12 = load double, double* %dot, align 8
  %mul20 = fmul double 2.000000e+00, %12
  %z21 = getelementptr inbounds %struct.vec3, %struct.vec3* %n, i32 0, i32 2
  %13 = load double, double* %z21, align 8
  %mul22 = fmul double %mul20, %13
  %z23 = getelementptr inbounds %struct.vec3, %struct.vec3* %v, i32 0, i32 2
  %14 = load double, double* %z23, align 8
  %sub24 = fsub double %mul22, %14
  %sub25 = fsub double -0.000000e+00, %sub24
  %z26 = getelementptr inbounds %struct.vec3, %struct.vec3* %res, i32 0, i32 2
  store double %sub25, double* %z26, align 8
  %15 = bitcast %struct.vec3* %agg.result to i8*
  %16 = bitcast %struct.vec3* %res to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %15, i8* %16, i64 24, i32 8, i1 false)
  ret void
}

; Function Attrs: nounwind uwtable
define void @cross_product(%struct.vec3* noalias sret %agg.result, %struct.vec3* byval align 8 %v1, %struct.vec3* byval align 8 %v2) #0 {
entry:
  %res = alloca %struct.vec3, align 8
  %y = getelementptr inbounds %struct.vec3, %struct.vec3* %v1, i32 0, i32 1
  %0 = load double, double* %y, align 8
  %z = getelementptr inbounds %struct.vec3, %struct.vec3* %v2, i32 0, i32 2
  %1 = load double, double* %z, align 8
  %mul = fmul double %0, %1
  %z1 = getelementptr inbounds %struct.vec3, %struct.vec3* %v1, i32 0, i32 2
  %2 = load double, double* %z1, align 8
  %y2 = getelementptr inbounds %struct.vec3, %struct.vec3* %v2, i32 0, i32 1
  %3 = load double, double* %y2, align 8
  %mul3 = fmul double %2, %3
  %sub = fsub double %mul, %mul3
  %x = getelementptr inbounds %struct.vec3, %struct.vec3* %res, i32 0, i32 0
  store double %sub, double* %x, align 8
  %z4 = getelementptr inbounds %struct.vec3, %struct.vec3* %v1, i32 0, i32 2
  %4 = load double, double* %z4, align 8
  %x5 = getelementptr inbounds %struct.vec3, %struct.vec3* %v2, i32 0, i32 0
  %5 = load double, double* %x5, align 8
  %mul6 = fmul double %4, %5
  %x7 = getelementptr inbounds %struct.vec3, %struct.vec3* %v1, i32 0, i32 0
  %6 = load double, double* %x7, align 8
  %z8 = getelementptr inbounds %struct.vec3, %struct.vec3* %v2, i32 0, i32 2
  %7 = load double, double* %z8, align 8
  %mul9 = fmul double %6, %7
  %sub10 = fsub double %mul6, %mul9
  %y11 = getelementptr inbounds %struct.vec3, %struct.vec3* %res, i32 0, i32 1
  store double %sub10, double* %y11, align 8
  %x12 = getelementptr inbounds %struct.vec3, %struct.vec3* %v1, i32 0, i32 0
  %8 = load double, double* %x12, align 8
  %y13 = getelementptr inbounds %struct.vec3, %struct.vec3* %v2, i32 0, i32 1
  %9 = load double, double* %y13, align 8
  %mul14 = fmul double %8, %9
  %y15 = getelementptr inbounds %struct.vec3, %struct.vec3* %v1, i32 0, i32 1
  %10 = load double, double* %y15, align 8
  %x16 = getelementptr inbounds %struct.vec3, %struct.vec3* %v2, i32 0, i32 0
  %11 = load double, double* %x16, align 8
  %mul17 = fmul double %10, %11
  %sub18 = fsub double %mul14, %mul17
  %z19 = getelementptr inbounds %struct.vec3, %struct.vec3* %res, i32 0, i32 2
  store double %sub18, double* %z19, align 8
  %12 = bitcast %struct.vec3* %agg.result to i8*
  %13 = bitcast %struct.vec3* %res to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %12, i8* %13, i64 24, i32 8, i1 false)
  ret void
}

; Function Attrs: nounwind uwtable
define void @get_sample_pos(%struct.vec3* noalias sret %agg.result, i32 %x, i32 %y, i32 %sample) #0 {
entry:
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %sample.addr = alloca i32, align 4
  %pt = alloca %struct.vec3, align 8
  %jt = alloca %struct.vec3, align 8
  store i32 %x, i32* %x.addr, align 4
  store i32 %y, i32* %y.addr, align 4
  store i32 %sample, i32* %sample.addr, align 4
  %0 = load double, double* @get_sample_pos.sf, align 8
  %cmp = fcmp oeq double %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* @xres, align 4
  %conv = sitofp i32 %1 to double
  %div = fdiv double 1.500000e+00, %conv
  store double %div, double* @get_sample_pos.sf, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %x.addr, align 4
  %conv1 = sitofp i32 %2 to double
  %3 = load i32, i32* @xres, align 4
  %conv2 = sitofp i32 %3 to double
  %div3 = fdiv double %conv1, %conv2
  %sub = fsub double %div3, 5.000000e-01
  %x4 = getelementptr inbounds %struct.vec3, %struct.vec3* %pt, i32 0, i32 0
  store double %sub, double* %x4, align 8
  %4 = load i32, i32* %y.addr, align 4
  %conv5 = sitofp i32 %4 to double
  %5 = load i32, i32* @yres, align 4
  %conv6 = sitofp i32 %5 to double
  %div7 = fdiv double %conv5, %conv6
  %sub8 = fsub double %div7, 6.500000e-01
  %sub9 = fsub double -0.000000e+00, %sub8
  %6 = load double, double* @aspect, align 8
  %div10 = fdiv double %sub9, %6
  %y11 = getelementptr inbounds %struct.vec3, %struct.vec3* %pt, i32 0, i32 1
  store double %div10, double* %y11, align 8
  %7 = load i32, i32* %sample.addr, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then12, label %if.end20

if.then12:                                        ; preds = %if.end
  %8 = load i32, i32* %x.addr, align 4
  %9 = load i32, i32* %y.addr, align 4
  %10 = load i32, i32* %sample.addr, align 4
  call void @jitter(%struct.vec3* sret %jt, i32 %8, i32 %9, i32 %10)
  %x13 = getelementptr inbounds %struct.vec3, %struct.vec3* %jt, i32 0, i32 0
  %11 = load double, double* %x13, align 8
  %12 = load double, double* @get_sample_pos.sf, align 8
  %mul = fmul double %11, %12
  %x14 = getelementptr inbounds %struct.vec3, %struct.vec3* %pt, i32 0, i32 0
  %13 = load double, double* %x14, align 8
  %add = fadd double %13, %mul
  store double %add, double* %x14, align 8
  %y15 = getelementptr inbounds %struct.vec3, %struct.vec3* %jt, i32 0, i32 1
  %14 = load double, double* %y15, align 8
  %15 = load double, double* @get_sample_pos.sf, align 8
  %mul16 = fmul double %14, %15
  %16 = load double, double* @aspect, align 8
  %div17 = fdiv double %mul16, %16
  %y18 = getelementptr inbounds %struct.vec3, %struct.vec3* %pt, i32 0, i32 1
  %17 = load double, double* %y18, align 8
  %add19 = fadd double %17, %div17
  store double %add19, double* %y18, align 8
  br label %if.end20

if.end20:                                         ; preds = %if.then12, %if.end
  %18 = bitcast %struct.vec3* %agg.result to i8*
  %19 = bitcast %struct.vec3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %18, i8* %19, i64 24, i32 8, i1 false)
  ret void
}

; Function Attrs: nounwind uwtable
define void @jitter(%struct.vec3* noalias sret %agg.result, i32 %x, i32 %y, i32 %s) #0 {
entry:
  %x.addr = alloca i32, align 4
  %y.addr = alloca i32, align 4
  %s.addr = alloca i32, align 4
  %pt = alloca %struct.vec3, align 8
  store i32 %x, i32* %x.addr, align 4
  store i32 %y, i32* %y.addr, align 4
  store i32 %s, i32* %s.addr, align 4
  %0 = load i32, i32* %x.addr, align 4
  %1 = load i32, i32* %y.addr, align 4
  %shl = shl i32 %1, 2
  %add = add nsw i32 %0, %shl
  %2 = load i32, i32* %x.addr, align 4
  %3 = load i32, i32* %s.addr, align 4
  %add1 = add nsw i32 %2, %3
  %and = and i32 %add1, 1023
  %idxprom = sext i32 %and to i64
  %arrayidx = getelementptr inbounds [1024 x i32], [1024 x i32]* @irand, i64 0, i64 %idxprom
  %4 = load i32, i32* %arrayidx, align 4
  %add2 = add nsw i32 %add, %4
  %and3 = and i32 %add2, 1023
  %idxprom4 = sext i32 %and3 to i64
  %arrayidx5 = getelementptr inbounds [1024 x %struct.vec3], [1024 x %struct.vec3]* @urand, i64 0, i64 %idxprom4
  %x6 = getelementptr inbounds %struct.vec3, %struct.vec3* %arrayidx5, i32 0, i32 0
  %5 = load double, double* %x6, align 8
  %x7 = getelementptr inbounds %struct.vec3, %struct.vec3* %pt, i32 0, i32 0
  store double %5, double* %x7, align 8
  %6 = load i32, i32* %y.addr, align 4
  %7 = load i32, i32* %x.addr, align 4
  %shl8 = shl i32 %7, 2
  %add9 = add nsw i32 %6, %shl8
  %8 = load i32, i32* %y.addr, align 4
  %9 = load i32, i32* %s.addr, align 4
  %add10 = add nsw i32 %8, %9
  %and11 = and i32 %add10, 1023
  %idxprom12 = sext i32 %and11 to i64
  %arrayidx13 = getelementptr inbounds [1024 x i32], [1024 x i32]* @irand, i64 0, i64 %idxprom12
  %10 = load i32, i32* %arrayidx13, align 4
  %add14 = add nsw i32 %add9, %10
  %and15 = and i32 %add14, 1023
  %idxprom16 = sext i32 %and15 to i64
  %arrayidx17 = getelementptr inbounds [1024 x %struct.vec3], [1024 x %struct.vec3]* @urand, i64 0, i64 %idxprom16
  %y18 = getelementptr inbounds %struct.vec3, %struct.vec3* %arrayidx17, i32 0, i32 1
  %11 = load double, double* %y18, align 8
  %y19 = getelementptr inbounds %struct.vec3, %struct.vec3* %pt, i32 0, i32 1
  store double %11, double* %y19, align 8
  %12 = bitcast %struct.vec3* %agg.result to i8*
  %13 = bitcast %struct.vec3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* %12, i8* %13, i64 24, i32 8, i1 false)
  ret void
}

declare i8* @fgets(i8*, i32, %struct._IO_FILE*) #3

; Function Attrs: nounwind
declare i8* @strtok(i8*, i8*) #4

; Function Attrs: nounwind readonly
declare double @atof(i8*) #2

; Function Attrs: nounwind
declare i32 @gettimeofday(%struct.timeval*, %struct.timezone*) #4

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind readonly "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind }
attributes #6 = { nounwind readnone }
attributes #7 = { nounwind readonly }
attributes #8 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.0 "}
!1 = !{!"dcop"}
