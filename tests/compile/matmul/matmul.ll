; RUN: apolloc -S -emit-llvm %s -o - | FileCheck %s

; Check that Apollo code is generated
; CHECK: @apollo
; CHECK: @apollo_loop_{{[0-9]+}}_skeleton
; CHECK: @apollo_loop_{{[0-9]+}}_jit

; ModuleID = 'matmul.c'
source_filename = "matmul.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@.str = private unnamed_addr constant [29 x i8] c"Start Matrix Multiplication\0A\00", align 1
@.str.1 = private unnamed_addr constant [27 x i8] c"End Matrix Multiplication\0A\00", align 1
@.str.2 = private unnamed_addr constant [37 x i8] c"Total time taken by CPU: %f seconds\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %RowA = alloca i32, align 4
  %ColA = alloca i32, align 4
  %Matrix_A = alloca double**, align 8
  %i = alloca i32, align 4
  %RowB = alloca i32, align 4
  %ColB = alloca i32, align 4
  %Matrix_B = alloca double**, align 8
  %i3 = alloca i32, align 4
  %Matrix_C = alloca double**, align 8
  %i14 = alloca i32, align 4
  %start_time = alloca i64, align 8
  %i26 = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %end_time = alloca i64, align 8
  %total_time = alloca double, align 8
  %i60 = alloca i32, align 4
  %i70 = alloca i32, align 4
  %i80 = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 1000, i32* %RowA, align 4
  store i32 1000, i32* %ColA, align 4
  %call = call noalias i8* @malloc(i64 8000) #3
  %0 = bitcast i8* %call to double**
  store double** %0, double*** %Matrix_A, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 1000
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call1 = call noalias i8* @malloc(i64 8000) #3
  %2 = bitcast i8* %call1 to double*
  %3 = load i32, i32* %i, align 4
  %idxprom = sext i32 %3 to i64
  %4 = load double**, double*** %Matrix_A, align 8
  %arrayidx = getelementptr inbounds double*, double** %4, i64 %idxprom
  store double* %2, double** %arrayidx, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1000, i32* %RowB, align 4
  store i32 3000, i32* %ColB, align 4
  %call2 = call noalias i8* @malloc(i64 8000) #3
  %6 = bitcast i8* %call2 to double**
  store double** %6, double*** %Matrix_B, align 8
  store i32 0, i32* %i3, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc10, %for.end
  %7 = load i32, i32* %i3, align 4
  %cmp5 = icmp slt i32 %7, 1000
  br i1 %cmp5, label %for.body6, label %for.end12

for.body6:                                        ; preds = %for.cond4
  %call7 = call noalias i8* @malloc(i64 24000) #3
  %8 = bitcast i8* %call7 to double*
  %9 = load i32, i32* %i3, align 4
  %idxprom8 = sext i32 %9 to i64
  %10 = load double**, double*** %Matrix_B, align 8
  %arrayidx9 = getelementptr inbounds double*, double** %10, i64 %idxprom8
  store double* %8, double** %arrayidx9, align 8
  br label %for.inc10

for.inc10:                                        ; preds = %for.body6
  %11 = load i32, i32* %i3, align 4
  %inc11 = add nsw i32 %11, 1
  store i32 %inc11, i32* %i3, align 4
  br label %for.cond4

for.end12:                                        ; preds = %for.cond4
  %call13 = call noalias i8* @malloc(i64 8000) #3
  %12 = bitcast i8* %call13 to double**
  store double** %12, double*** %Matrix_C, align 8
  store i32 0, i32* %i14, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc21, %for.end12
  %13 = load i32, i32* %i14, align 4
  %cmp16 = icmp slt i32 %13, 1000
  br i1 %cmp16, label %for.body17, label %for.end23

for.body17:                                       ; preds = %for.cond15
  %call18 = call noalias i8* @malloc(i64 24000) #3
  %14 = bitcast i8* %call18 to double*
  %15 = load i32, i32* %i14, align 4
  %idxprom19 = sext i32 %15 to i64
  %16 = load double**, double*** %Matrix_C, align 8
  %arrayidx20 = getelementptr inbounds double*, double** %16, i64 %idxprom19
  store double* %14, double** %arrayidx20, align 8
  br label %for.inc21

for.inc21:                                        ; preds = %for.body17
  %17 = load i32, i32* %i14, align 4
  %inc22 = add nsw i32 %17, 1
  store i32 %inc22, i32* %i14, align 4
  br label %for.cond15

for.end23:                                        ; preds = %for.cond15
  %18 = load double**, double*** %Matrix_A, align 8
  call void @initializeMatrix(double** %18, i32 1000, i32 1000)
  %19 = load double**, double*** %Matrix_B, align 8
  call void @initializeMatrix(double** %19, i32 1000, i32 3000)
  %call24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str, i32 0, i32 0))
  %call25 = call i64 @clock() #3
  store i64 %call25, i64* %start_time, align 8
  store i32 0, i32* %i26, align 4, !apollo.pragma !1
  br label %for.cond27, !apollo.pragma !1

for.cond27:                                       ; preds = %for.inc54, %for.end23
  %20 = load i32, i32* %i26, align 4, !apollo.pragma !1
  %cmp28 = icmp slt i32 %20, 1000, !apollo.pragma !1
  br i1 %cmp28, label %for.body29, label %for.end56, !apollo.pragma !1

for.body29:                                       ; preds = %for.cond27
  store i32 0, i32* %j, align 4, !apollo.pragma !1
  br label %for.cond30, !apollo.pragma !1

for.cond30:                                       ; preds = %for.inc51, %for.body29
  %21 = load i32, i32* %j, align 4, !apollo.pragma !1
  %cmp31 = icmp slt i32 %21, 3000, !apollo.pragma !1
  br i1 %cmp31, label %for.body32, label %for.end53, !apollo.pragma !1

for.body32:                                       ; preds = %for.cond30
  store i32 0, i32* %k, align 4, !apollo.pragma !1
  br label %for.cond33, !apollo.pragma !1

for.cond33:                                       ; preds = %for.inc48, %for.body32
  %22 = load i32, i32* %k, align 4, !apollo.pragma !1
  %cmp34 = icmp slt i32 %22, 1000, !apollo.pragma !1
  br i1 %cmp34, label %for.body35, label %for.end50, !apollo.pragma !1

for.body35:                                       ; preds = %for.cond33
  %23 = load i32, i32* %k, align 4, !apollo.pragma !1
  %idxprom36 = sext i32 %23 to i64, !apollo.pragma !1
  %24 = load i32, i32* %i26, align 4, !apollo.pragma !1
  %idxprom37 = sext i32 %24 to i64, !apollo.pragma !1
  %25 = load double**, double*** %Matrix_A, align 8, !apollo.pragma !1
  %arrayidx38 = getelementptr inbounds double*, double** %25, i64 %idxprom37, !apollo.pragma !1
  %26 = load double*, double** %arrayidx38, align 8, !apollo.pragma !1
  %arrayidx39 = getelementptr inbounds double, double* %26, i64 %idxprom36, !apollo.pragma !1
  %27 = load double, double* %arrayidx39, align 8, !apollo.pragma !1
  %28 = load i32, i32* %j, align 4, !apollo.pragma !1
  %idxprom40 = sext i32 %28 to i64, !apollo.pragma !1
  %29 = load i32, i32* %k, align 4, !apollo.pragma !1
  %idxprom41 = sext i32 %29 to i64, !apollo.pragma !1
  %30 = load double**, double*** %Matrix_B, align 8, !apollo.pragma !1
  %arrayidx42 = getelementptr inbounds double*, double** %30, i64 %idxprom41, !apollo.pragma !1
  %31 = load double*, double** %arrayidx42, align 8, !apollo.pragma !1
  %arrayidx43 = getelementptr inbounds double, double* %31, i64 %idxprom40, !apollo.pragma !1
  %32 = load double, double* %arrayidx43, align 8, !apollo.pragma !1
  %mul = fmul double %27, %32, !apollo.pragma !1
  %33 = load i32, i32* %j, align 4, !apollo.pragma !1
  %idxprom44 = sext i32 %33 to i64, !apollo.pragma !1
  %34 = load i32, i32* %i26, align 4, !apollo.pragma !1
  %idxprom45 = sext i32 %34 to i64, !apollo.pragma !1
  %35 = load double**, double*** %Matrix_C, align 8, !apollo.pragma !1
  %arrayidx46 = getelementptr inbounds double*, double** %35, i64 %idxprom45, !apollo.pragma !1
  %36 = load double*, double** %arrayidx46, align 8, !apollo.pragma !1
  %arrayidx47 = getelementptr inbounds double, double* %36, i64 %idxprom44, !apollo.pragma !1
  %37 = load double, double* %arrayidx47, align 8, !apollo.pragma !1
  %add = fadd double %37, %mul, !apollo.pragma !1
  store double %add, double* %arrayidx47, align 8, !apollo.pragma !1
  br label %for.inc48, !apollo.pragma !1

for.inc48:                                        ; preds = %for.body35
  %38 = load i32, i32* %k, align 4, !apollo.pragma !1
  %inc49 = add nsw i32 %38, 1, !apollo.pragma !1
  store i32 %inc49, i32* %k, align 4, !apollo.pragma !1
  br label %for.cond33, !apollo.pragma !1

for.end50:                                        ; preds = %for.cond33
  br label %for.inc51, !apollo.pragma !1

for.inc51:                                        ; preds = %for.end50
  %39 = load i32, i32* %j, align 4, !apollo.pragma !1
  %inc52 = add nsw i32 %39, 1, !apollo.pragma !1
  store i32 %inc52, i32* %j, align 4, !apollo.pragma !1
  br label %for.cond30, !apollo.pragma !1

for.end53:                                        ; preds = %for.cond30
  br label %for.inc54, !apollo.pragma !1

for.inc54:                                        ; preds = %for.end53
  %40 = load i32, i32* %i26, align 4, !apollo.pragma !1
  %inc55 = add nsw i32 %40, 1, !apollo.pragma !1
  store i32 %inc55, i32* %i26, align 4, !apollo.pragma !1
  br label %for.cond27, !apollo.pragma !1

for.end56:                                        ; preds = %for.cond27
  %call57 = call i64 @clock() #3
  store i64 %call57, i64* %end_time, align 8
  %41 = load i64, i64* %end_time, align 8
  %42 = load i64, i64* %start_time, align 8
  %sub = sub nsw i64 %41, %42
  %conv = sitofp i64 %sub to double
  %div = fdiv double %conv, 1.000000e+06
  store double %div, double* %total_time, align 8
  %call58 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0))
  %43 = load double, double* %total_time, align 8
  %call59 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([37 x i8], [37 x i8]* @.str.2, i32 0, i32 0), double %43)
  store i32 0, i32* %i60, align 4
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc67, %for.end56
  %44 = load i32, i32* %i60, align 4
  %cmp62 = icmp slt i32 %44, 1000
  br i1 %cmp62, label %for.body64, label %for.end69

for.body64:                                       ; preds = %for.cond61
  %45 = load i32, i32* %i60, align 4
  %idxprom65 = sext i32 %45 to i64
  %46 = load double**, double*** %Matrix_A, align 8
  %arrayidx66 = getelementptr inbounds double*, double** %46, i64 %idxprom65
  %47 = load double*, double** %arrayidx66, align 8
  %48 = bitcast double* %47 to i8*
  call void @free(i8* %48) #3
  br label %for.inc67

for.inc67:                                        ; preds = %for.body64
  %49 = load i32, i32* %i60, align 4
  %inc68 = add nsw i32 %49, 1
  store i32 %inc68, i32* %i60, align 4
  br label %for.cond61

for.end69:                                        ; preds = %for.cond61
  %50 = load double**, double*** %Matrix_A, align 8
  %51 = bitcast double** %50 to i8*
  call void @free(i8* %51) #3
  store i32 0, i32* %i70, align 4
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc77, %for.end69
  %52 = load i32, i32* %i70, align 4
  %cmp72 = icmp slt i32 %52, 1000
  br i1 %cmp72, label %for.body74, label %for.end79

for.body74:                                       ; preds = %for.cond71
  %53 = load i32, i32* %i70, align 4
  %idxprom75 = sext i32 %53 to i64
  %54 = load double**, double*** %Matrix_B, align 8
  %arrayidx76 = getelementptr inbounds double*, double** %54, i64 %idxprom75
  %55 = load double*, double** %arrayidx76, align 8
  %56 = bitcast double* %55 to i8*
  call void @free(i8* %56) #3
  br label %for.inc77

for.inc77:                                        ; preds = %for.body74
  %57 = load i32, i32* %i70, align 4
  %inc78 = add nsw i32 %57, 1
  store i32 %inc78, i32* %i70, align 4
  br label %for.cond71

for.end79:                                        ; preds = %for.cond71
  %58 = load double**, double*** %Matrix_B, align 8
  %59 = bitcast double** %58 to i8*
  call void @free(i8* %59) #3
  store i32 0, i32* %i80, align 4
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc87, %for.end79
  %60 = load i32, i32* %i80, align 4
  %cmp82 = icmp slt i32 %60, 1000
  br i1 %cmp82, label %for.body84, label %for.end89

for.body84:                                       ; preds = %for.cond81
  %61 = load i32, i32* %i80, align 4
  %idxprom85 = sext i32 %61 to i64
  %62 = load double**, double*** %Matrix_C, align 8
  %arrayidx86 = getelementptr inbounds double*, double** %62, i64 %idxprom85
  %63 = load double*, double** %arrayidx86, align 8
  %64 = bitcast double* %63 to i8*
  call void @free(i8* %64) #3
  br label %for.inc87

for.inc87:                                        ; preds = %for.body84
  %65 = load i32, i32* %i80, align 4
  %inc88 = add nsw i32 %65, 1
  store i32 %inc88, i32* %i80, align 4
  br label %for.cond81

for.end89:                                        ; preds = %for.cond81
  %66 = load double**, double*** %Matrix_C, align 8
  %67 = bitcast double** %66 to i8*
  call void @free(i8* %67) #3
  ret i32 0
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #1

; Function Attrs: nounwind uwtable
define void @initializeMatrix(double** %Matrix, i32 %Row, i32 %Col) #0 {
entry:
  %Matrix.addr = alloca double**, align 8
  %Row.addr = alloca i32, align 4
  %Col.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store double** %Matrix, double*** %Matrix.addr, align 8
  store i32 %Row, i32* %Row.addr, align 4
  store i32 %Col, i32* %Col.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc6, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %Row.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end8

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %2 = load i32, i32* %j, align 4
  %3 = load i32, i32* %Col.addr, align 4
  %cmp2 = icmp slt i32 %2, %3
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %call = call i32 @rand() #3
  %rem = srem i32 %call, 3
  %conv = sitofp i32 %rem to double
  %4 = load i32, i32* %j, align 4
  %idxprom = sext i32 %4 to i64
  %5 = load i32, i32* %i, align 4
  %idxprom4 = sext i32 %5 to i64
  %6 = load double**, double*** %Matrix.addr, align 8
  %arrayidx = getelementptr inbounds double*, double** %6, i64 %idxprom4
  %7 = load double*, double** %arrayidx, align 8
  %arrayidx5 = getelementptr inbounds double, double* %7, i64 %idxprom
  store double %conv, double* %arrayidx5, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %8 = load i32, i32* %j, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  br label %for.inc6

for.inc6:                                         ; preds = %for.end
  %9 = load i32, i32* %i, align 4
  %inc7 = add nsw i32 %9, 1
  store i32 %inc7, i32* %i, align 4
  br label %for.cond

for.end8:                                         ; preds = %for.cond
  ret void
}

declare i32 @printf(i8*, ...) #2

; Function Attrs: nounwind
declare i64 @clock() #1

; Function Attrs: nounwind
declare void @free(i8*) #1

; Function Attrs: nounwind
declare i32 @rand() #1

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.9.0 "}
!1 = !{!"dcop"}
