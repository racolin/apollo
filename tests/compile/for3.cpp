// RUN: apolloc++ -O3 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

class node { public: int val; node* next; };

#include <vector>
void test_vector(node* head, std::vector<int> &klist,
		 int size, std::vector<int> &f)
{
#pragma apollo dcop
  {
    for (int i = 0; i < size; i++)
      {
	int k = klist[i];
	node* ptr = head;
	while(ptr)
	  {
	    if (ptr->val == k)
	      f[i] += 1;
	    ptr = ptr->next;
	  }
      }
  }
}
