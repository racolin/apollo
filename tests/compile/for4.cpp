// RUN: apolloc++ -O3 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

class node { public: int val; node* next; };

#include <vector>
#include <list>
void test_list(node* head, std::list<int> &klist,
	       int size, std::vector<int> &f)
{
#pragma apollo dcop
  {
    for (int i = 0; i < size; i++)
      {
	int k = klist.front();
	klist.pop_front();
	node* ptr = head;
	while(ptr)
	  {
	    if (ptr->val == k)
	      f[i] += 1;
	    ptr = ptr->next;
	  }
      }
  } // end #pragma apollo
}
