// RUN: apolloc++ -O2 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

struct node { struct node* val; struct node* next; };

extern void S1(node*);
extern void S2(node*);

#include <list>
void test2(std::list<node>& l)
{
#pragma apollo dcop
  {
    while(!l.empty())
      {
	node& ptr = l.front();
	node* val_ptr = ptr.val;
	while(val_ptr)
	  {
	    S1(val_ptr);
	    S2(val_ptr);
	    val_ptr = val_ptr->val;
	  }
	l.pop_front();
      }
  }
}
