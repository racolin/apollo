// RUN: apolloc++ -O2 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

// Fails (but shouldn't)

// Check that Apollo can handle the try-catch block without a compiler exit

struct node { int val; struct node* next; };

extern void S1(node*);
extern void S2(node*);

void for1(node* head, int klist[], int size, int f[], node* end)
{
#pragma apollo dcop
  {
    for (int i = 0; i < size; i++)
      {
	try
	  {
	    int k = klist[i];
	    node* ptr = head;
	    while(ptr)
	      {
		if (ptr->val == k)
		  f[i] += 1;
		ptr = ptr->next;
	      }
	    if (ptr == end)
	      goto L;
	  }
	catch(...) { /* skip */ }
      }
  }
 L:
  return;
}
