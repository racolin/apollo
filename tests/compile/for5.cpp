// RUN: apolloc++ -O3 -S -emit-llvm %s -o - | FileCheck %s

// Check that Apollo code is generated
// CHECK: @apollo
// CHECK: @apollo_loop_{{[0-9]+}}_skeleton
// CHECK: @apollo_loop_{{[0-9]+}}_jit

#include <vector>
#include <list>

struct data { int val; data* next; };
class node { public: data* val; node* next; };

extern void S1(data*);
extern void S2(data*);

void test_list(node* head, std::list<int> &klist,
	       int size, std::vector<int> &f)
{
  std::list<node> l;

#pragma apollo dcop
  {
    for (int i = 0; i < size; i++)
      {
	int k = klist.front();
	klist.pop_front();
	node* ptr = head;
	while(ptr)
	  {
	    l.push_back(*ptr);
	    if (ptr->val->val == k)
	      f[i] += 1;
	    ptr = ptr->next;
	  }
      }

    while(!l.empty())
      {
	node& ptr = l.front();
	data* val_ptr = ptr.val;
	while(val_ptr)
	  {
	    S1(val_ptr);
	    S2(val_ptr);
	    val_ptr = val_ptr->next;
	  }
	l.pop_front();
      }
  } // end #pragma apollo
}
