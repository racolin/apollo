//===--- ArrayInfo.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/ArrayInfo.h"
#include "Interpolation/Interpolation.h"
#include "APOLLO_PROG.h"
#include "Backdoor/Backdoor.h"

#include <iostream>

static long integerDiv(const long A, const long B) {
	long Res = A / B;
	if (A * B < 0 && (A % B != 0))
		Res -= 1;
	return Res;
}

static long integerMod(const long A, const long B) {
	return A - integerDiv(A, B) * B;
}

static long gcd(long A, long B) {
	A = std::abs(A);
	B = std::abs(B);
	if (A == 0)
		return B;
	if (B == 0)
		return A;
	if (A == B)
		return A;
	return ((A > B) ? gcd(A % B, B) : gcd(A, B % A));
}

static long gcd(std::vector<long> &Numbers) {
	if (Numbers.empty())
		return 1;
	return std::accumulate<std::vector<long>::iterator, long, long(long, long)>(
			Numbers.begin(), Numbers.end(), 0, gcd);
}

ArrayAnalysis::DelinearizeOption ArrayAnalysis::getDelinearizeOption() {
	const std::string Delinearization = backdoorutils::getEnv("APOLLO_BONES",
			"--delinearize");
	if (Delinearization == "force")
		return ArrayAnalysis::DelinearizeOption::ForceDelinearize;
	if (Delinearization == "yes")
		return ArrayAnalysis::DelinearizeOption::Delinearize;
	if (Delinearization == "no")
		return ArrayAnalysis::DelinearizeOption::NoDelinearize;
	return ArrayAnalysis::DelinearizeOption::Delinearize;
}

void ArrayAnalysis::filterUnsolvedIds() {
	const PredictionModel &PM = *apolloProg->PredictionModel;
	for (StaticMemInfo &Mem : apolloProg->StaticInfo->Mem) {
		const int Id = Mem.Id;
		const auto &MemPred = PM.getMemPrediction(Id);
		if (MemPred.isLinear() || MemPred.isTube()) {
			SolvedMemoryIds.insert(Id);
		}
	}
}

long ArrayAnalysis::wordSizeForId(const int Id) {
	std::vector<long> Coefs;
	Coefs.push_back(apolloProg->StaticInfo->Mem[Id].SizeInBytes);

	const auto &MemPred = apolloProg->PredictionModel->getMemPrediction(Id);
	if (MemPred.isLinearOrTube()) {
		const auto &LF = apolloProg->PredictionModel->getMemPrediction(Id).LF;
		for (unsigned i = 0; i < LF.size() - 1; ++i) {
			Coefs.push_back(LF[i]);
		}
		Coefs.push_back(MemPred.TubeWidth);
	}
	const long Size = gcd(Coefs);
	return Size;
}

/// \brief Init with one array for each access
std::vector<ArrayAnalysis::ArrayInfo> ArrayAnalysis::initArrays() {
	std::vector<ArrayInfo> V;
	const PredictionModel &PM = *apolloProg->PredictionModel;
	for (auto id : SolvedMemoryIds) {
		ArrayInfo Array;
		Array.Id = id;
		Array.MemIds.insert(id);
		Array.Range = MemToRange[id];
		Array.WordSizeInBytes = 1;
		Array.Dimentions = 1;
		Array.ReadOnly = !apolloProg->StaticInfo->Mem[id].IsWrite;
		Array.ContainsNonLinearAccess = !PM.getMemPrediction(id).isLinear();
		V.push_back(Array);
	}
	return V;
}

bool ArrayAnalysis::arrayOverlap(const ArrayInfo &ArrayA,
		const ArrayInfo &ArrayB) {
	return !(ArrayA.Range.second < ArrayB.Range.first
			|| ArrayA.Range.first > ArrayB.Range.second);
}

bool ArrayAnalysis::arrayAlias(const ArrayInfo &ArrayA,
		const ArrayInfo &ArrayB) {

	const std::set<int> &IdsA = ArrayA.MemIds;
	const std::set<int> &IdsB = ArrayB.MemIds;
	for (int id_a : IdsA) {
		const StaticMemInfo &IdAInfo = apolloProg->StaticInfo->Mem[id_a];
		for (int id_b : IdsB) {
			StaticMemInfo &IdBInfo = apolloProg->StaticInfo->Mem[id_b];
			const bool MayAlias = !IdAInfo.NoAlias.count(&IdBInfo);
			if (MayAlias) {
				std::vector<long> ForGCD;
				const auto &LFa = apolloProg->PredictionModel->getMemPrediction(
						id_a).LF;
				const auto &LFb = apolloProg->PredictionModel->getMemPrediction(
						id_b).LF;
				const long CTE = labs(
						labs(LFa[LFa.size() - 1]) - labs(LFb[LFb.size() - 1]));
				ForGCD.insert(ForGCD.end(), ++LFa.rbegin(), LFa.rend());
				ForGCD.insert(ForGCD.end(), ++LFb.rbegin(), LFb.rend());
				long GCDResult = gcd(ForGCD);
				// if all the coefficients are 0 the gcd returns 0. I set it to 1.
				if (!GCDResult)
					GCDResult = 1;
				if ((CTE % GCDResult) == 0)
					return true;
			}
		}
	}
	return false;
}

bool ArrayAnalysis::canMerge(const ArrayInfo &ArrayA, const ArrayInfo &ArrayB) {
	const bool Overlap = arrayOverlap(ArrayA, ArrayB);
	// sometimes, even if they overlap, the llvm alias analysis tells us that the
	// accesses do not alias.
	if (Overlap) {
		return arrayAlias(ArrayA, ArrayB);
	}
	return false;
}

void ArrayAnalysis::mergeArrays(ArrayInfo *ArrayA, const ArrayInfo &ArrayB) {
	ArrayA->Range.first = std::min<MemTy>(ArrayA->Range.first,
			ArrayB.Range.first);
	ArrayA->Range.second = std::max<MemTy>(ArrayA->Range.second,
			ArrayB.Range.second);
	ArrayA->MemIds.insert(ArrayB.MemIds.begin(), ArrayB.MemIds.end());
	ArrayA->ReadOnly = ArrayA->ReadOnly && ArrayB.ReadOnly;
	ArrayA->ContainsNonLinearAccess = ArrayA->ContainsNonLinearAccess
			|| ArrayB.ContainsNonLinearAccess;
}

/// \brief For each array:
///     - for each access in the array
///     	+ computes the GCD of all the (SizeInBytes, coeffs, tube width)
///         + computes the GCD of the difference between mem first and array first
///     - computes the GCD of the GCDs above
void ArrayAnalysis::computeArrayWordSize(std::vector<ArrayInfo> &Arrays) {
	for (ArrayInfo &array : Arrays) {
		std::vector<long> ForGCD;
		// put the max size supported by each access.
		for (int id : array.MemIds) {
			const long Size = wordSizeForId(id);
			ForGCD.push_back(Size);
		}
		// the difference between mem first and array first
		for (int id : array.MemIds) {
			const long Size = (MemToRange[id].first - array.Range.first);
			ForGCD.push_back(Size);
		}
		const long WordSize = gcd(ForGCD);
		array.WordSizeInBytes = WordSize;
	}
}

void ArrayAnalysis::computeMemToArray(
		std::map<int, std::pair<ArrayInfo *, long>> &Mem2Array) {

	const long OutermostLowerBound = LoopBounds[0].first;
	for (ArrayInfo &array : Arrays) {
		for (int memId : array.MemIds) {
			const auto &MemPred = apolloProg->PredictionModel->getMemPrediction(
					memId);
			const auto &LF = MemPred.LF;
			const long OutermostCoef = LF[0];
			// The lower bound of the range is calculated when the outermost lower
			// bound is != 0
			const long OffsetCorrection =
					(OutermostCoef / array.WordSizeInBytes)
							* OutermostLowerBound;
			const long OffsetFromArrayBase = (MemToRange[memId].first
					- array.Range.first) / array.WordSizeInBytes;
			// NOTE: The offset can be negative! But, since the lower access starts
			// with i=chunk_lower, so never a position lower than 0 is accessed
			const long Offset = OffsetFromArrayBase - OffsetCorrection;
			Mem2Array[memId] = std::pair<ArrayInfo*, long>(&array, Offset);
		}
	}
	assert(MemToRange.size() == SolvedMemoryIds.size());
}

void ArrayAnalysis::computeAccesses(
		std::map<int, std::vector<AccessInfo>> &MemToAccess) {

	for (int id : SolvedMemoryIds) {
		std::vector<AccessInfo> Accesses;
		const auto &MemPred = apolloProg->PredictionModel->getMemPrediction(id);
		const auto &LF = MemPred.LF;
		const std::pair<ArrayInfo*, long> &MemArrayInfo = MemToArray[id];
		ArrayInfo &Array = *MemArrayInfo.first;

		const int AccessSize = apolloProg->StaticInfo->Mem[id].SizeInBytes;
		const int ArrayWordSizeInBytes = Array.WordSizeInBytes;

		// build the coefficients vector.
		long TubeWidthValue = 0;
		const unsigned int coef_size = LF.size();
		if (!MemPred.isLinear()) {
			TubeWidthValue = MemPred.TubeWidth;
		}

		// 'LinearFunction' is the same for linear and nonlinear
		AccessInfo::LinearFunctionTy Coefs(coef_size, 0);
		for (unsigned coef = 0; coef < LF.size() - 1; coef++) {
			Coefs[coef] = LF[coef] / ArrayWordSizeInBytes;
		}
		// insert the linear function
		for (int word_number = 0;
				word_number < (AccessSize / ArrayWordSizeInBytes);
				word_number++) {
			Coefs[Coefs.size() - 1] = MemArrayInfo.second + word_number;
			AccessInfo Info;
			Info.Access.push_back(Coefs);
			Info.Array = &Array;
			Info.MemId = id;
			Info.TubeWidth = TubeWidthValue / ArrayWordSizeInBytes;
			Accesses.push_back(Info);
		}
		MemToAccess[id] = Accesses;
	}
}

void ArrayAnalysis::normalizeArraysIDs(std::vector<ArrayInfo> &Arrays) {
	for (unsigned i = 0; i < Arrays.size(); ++i) {
		Arrays[i].Id = i + 1;
	}
}

ArrayAnalysis::ArrayToAccessTy ArrayAnalysis::mapArrayToAccess() {
	ArrayToAccessTy Map;
	std::vector<AccessInfo *> Accesses;
	for (ArrayInfo &array : Arrays) {
		for (int mem_id : array.MemIds) {
			for (AccessInfo &access : MemToAccess[mem_id]) {
				Accesses.push_back(&access);
			}
		}
		Map[&array] = Accesses;
		Accesses.clear();
	}
	return Map;
}

static bool coefsFromSameDim(const long A, const long B) {
	const long ABSa = abs(A);
	const long ABSb = abs(B);
	const long Min = std::min<long>(ABSa, ABSb);
	const long Max = std::max<long>(ABSa, ABSb);
	return (Min != 0) && (Max % Min == 0) && (Max / Min <= 4);
}

ArrayAnalysis::AccessToDimentionsTy ArrayAnalysis::getAccessDimentions(
		ArrayAnalysis::ArrayToAccessTy &Arr2Acc) {

	AccessToDimentionsTy Acc2Dim;
	for (auto &pair : Arr2Acc) {
		for (AccessInfo *access : pair.second) {
			std::vector<DimensionInfo> Dims;
			const AccessInfo::LinearFunctionTy LF = access->Access[0];
			std::set<long> CandidateDimentions;
			std::set<long> VisitedCandidates;
			for (size_t coef_idx = 0; coef_idx < LF.size() - 1; ++coef_idx) {
				const long Candidate = abs(LF[coef_idx]);
				if (Candidate != 0)
					CandidateDimentions.insert(Candidate);
			}
			for (auto &candidate : CandidateDimentions) {
				if (!VisitedCandidates.count(candidate)) {
					DimensionInfo Dim;
					Dim.Dimension = candidate;
					for (size_t coef_idx = 0; coef_idx < LF.size() - 1;
							++coef_idx) {
						const long Coef = LF[coef_idx];
						if (coefsFromSameDim(candidate, Coef)) {
							Dim.CoefIdx.push_back(coef_idx);
							VisitedCandidates.insert(abs(Coef));
						}
					}
					Dim.Access = AccessInfo::LinearFunctionTy(LF.size(), 0);
					Dims.push_back(Dim);
				}
			}

			// bound the number of dimensions
			const size_t MaxNumDims = 3;
			if (Dims.size() > MaxNumDims) {
				std::vector<DimensionInfo> ConsideredDimentions;
				ConsideredDimentions.push_back(Dims[0]);
				for (size_t i = 1; i < Dims.size(); ++i) {
					if (i < Dims.size() - MaxNumDims + 1)
						ConsideredDimentions[0].CoefIdx.insert(
								ConsideredDimentions[0].CoefIdx.begin(),
								Dims[i].CoefIdx.begin(), Dims[i].CoefIdx.end());
					else
						ConsideredDimentions.push_back(Dims[i]);
				}
				Dims = ConsideredDimentions;
			}
			// if it has no dimension, is a simple scalar, it has a dimension of 1,
			// and no iterator participates.
			if (Dims.empty()) {
				DimensionInfo BasicDimention;
				BasicDimention.Dimension = 1;
				BasicDimention.Access = AccessInfo::LinearFunctionTy(LF.size(),
						0);
				Dims.push_back(BasicDimention);
			}
			Acc2Dim[access] = Dims;
		}
	}
	return Acc2Dim;
}

bool ArrayAnalysis::fixLowerExtremes(std::vector<AccessInfo *> &Accesses,
		AccessToDimentionsTy &Acc2Dim) {

	const std::vector<DimensionInfo> &DimentionsVec = Acc2Dim[Accesses[0]];
	bool CorrectLowerBound = false;
	std::vector<int> Correction(DimentionsVec.size(), 0);

	for (size_t access_idx = 0; access_idx < Accesses.size(); ++access_idx) {
		AccessInfo *Access = Accesses[access_idx];
		const std::vector<DimensionInfo> &Dimentions = Acc2Dim[Access];
		for (size_t dim_idx = 0; dim_idx < Dimentions.size(); ++dim_idx) {
			const auto &StmtParents =
					apolloProg->StaticInfo->Mem[Access->MemId].Parents;
			const DimensionInfo &DimInfo = Dimentions[dim_idx];
			long LowerExtreme = DimInfo.Access[DimInfo.Access.size() - 1];
			for (int coef_idx : DimInfo.CoefIdx) {
				const int LoopId = StmtParents[coef_idx]->Id;
				const long Coef = DimInfo.Access[coef_idx];
				const long LowerBound = LoopBounds[LoopId].first;
				const long UpperBound = LoopBounds[LoopId].second;
				const long Min = std::min<long>(Coef * LowerBound,
						Coef * UpperBound);
				LowerExtreme += Min;
			}
			const bool InRange = LowerExtreme >= 0;
			if (!InRange) {
				Correction[dim_idx] = std::max<long>(-LowerExtreme,
						Correction[dim_idx]);
				CorrectLowerBound = true;
			}
		}
	}
	if (CorrectLowerBound) {
		for (size_t access_idx = 0; access_idx < Accesses.size();
				++access_idx) {
			AccessInfo *Access = Accesses[access_idx];
			std::vector<DimensionInfo> &Dimentions = Acc2Dim[Access];
			for (size_t dim_idx = 0; dim_idx < Dimentions.size(); ++dim_idx) {
				DimensionInfo &DimInfo = Dimentions[dim_idx];
				DimInfo.Access[DimInfo.Access.size() - 1] +=
						Correction[dim_idx];
			}
		}
	}
	return CorrectLowerBound;
}

bool ArrayAnalysis::fixUpperExtremes(std::vector<AccessInfo *> &Accesses,
		AccessToDimentionsTy &Acc2Dim) {
	bool CorrectUpperBound = false;
	std::set<std::pair<AccessInfo *, int>> ToCorrect;
	for (size_t access_idx = 0; access_idx < Accesses.size(); ++access_idx) {
		AccessInfo *Access = Accesses[access_idx];
		std::vector<DimensionInfo> &Dimentions = Acc2Dim[Access];
		for (size_t dim_idx = 0; dim_idx < Dimentions.size() - 1; ++dim_idx) {
			const auto &StmtParents =
					apolloProg->StaticInfo->Mem[Access->MemId].Parents;
			DimensionInfo &Dimention = Dimentions[dim_idx];
			long UpperExtreme = Dimention.Access[Dimention.Access.size() - 1];
			for (int coef_idx : Dimention.CoefIdx) {
				const int LoopId = StmtParents[coef_idx]->Id;
				const long Coef = Dimention.Access[coef_idx];
				const long LowerBound = LoopBounds[LoopId].first;
				const long UpperBound = LoopBounds[LoopId].second;
				const long Min = std::max<long>(Coef * LowerBound,
						Coef * UpperBound);
				UpperExtreme += Min;
			}
			const long UpperLimit = Dimentions[dim_idx + 1].Dimension;
			const bool InRange = UpperLimit > UpperExtreme;
			if (!InRange) {
				CorrectUpperBound = true;
				ToCorrect.insert(std::pair<AccessInfo*, int>(Access, dim_idx));
			}
		}
	}
	if (CorrectUpperBound) {
		for (const std::pair<AccessInfo*, int> &pair : ToCorrect) {
			AccessInfo *Access = pair.first;
			const int DimIdx = pair.second;
			std::vector<DimensionInfo> &Dimentions = Acc2Dim[Access];
			DimensionInfo &Dim = Dimentions[DimIdx];
			DimensionInfo &NextDim = Dimentions[DimIdx + 1];
			Dim.Access[Dim.Access.size() - 1] -= NextDim.Dimension;
			NextDim.Access[Dim.Access.size() - 1] += 1;
		}
	}
	return CorrectUpperBound;
}

void ArrayAnalysis::proposeDelinearizedAccessFunctions(
		AccessToDimentionsTy &Acc2Dim) {
	// compute the new access functions.
	for (auto &pair : Acc2Dim) {
		AccessInfo *Access = pair.first;
		std::vector<DimensionInfo> &Dimentions = pair.second;
		// compute the access coef for each iterator.
		for (int dim_idx = Dimentions.size() - 1; dim_idx >= 0; --dim_idx) {
			DimensionInfo &Dim = Dimentions[dim_idx];
			for (int coef_idx : Dim.CoefIdx) {
				Dim.Access[coef_idx] = Access->Access[0][coef_idx]
						/ Dim.Dimension;
			}
		}
		// compute the offset for each dimention.
		const size_t ConstantCoefPosition = Access->Access[0].size() - 1;
		long Offset = MemToArray[Access->MemId].second;
		for (size_t dim_idx = Dimentions.size() - 1; dim_idx > 0; --dim_idx) {
			const long DimentionSize = Dimentions[dim_idx].Dimension;
			const long OffsetForDimention = integerDiv(Offset, DimentionSize);
			Offset = integerMod(Offset, DimentionSize);
			Dimentions[dim_idx].Access[ConstantCoefPosition] =
					OffsetForDimention;
		}
		Dimentions[0].Access[ConstantCoefPosition] = Offset;
	}
}

void ArrayAnalysis::updateDelinearizedAccessFunctions(ArrayInfo *Array,
		std::vector<AccessInfo *> &Accesses, AccessToDimentionsTy &Acc2Dim) {
	// std::vector<DimentionInfo>& Dimentions = Acc2Dim[Accesses[0]];
	Array->Dimentions = Acc2Dim[Accesses[0]].size();
	for (AccessInfo *access : Accesses) {
		std::vector<DimensionInfo> &Dimentions = Acc2Dim[access];
		access->Access.clear();
		for (DimensionInfo &dim : Dimentions) {
			access->Access.push_back(dim.Access);
		}
		std::reverse(access->Access.begin(), access->Access.end());
	}
}

static bool dimentionSizesAreCompatible(
		std::vector<ArrayAnalysis::DimensionInfo> &ProposedDimentions,
		std::vector<ArrayAnalysis::DimensionInfo> &DesiredDimentions) {

	for (auto &proposed_dim : ProposedDimentions) {
		const long Size = proposed_dim.Dimension;
		auto DimentionWithSize = [&Size](
				ArrayAnalysis::DimensionInfo &Dim) -> bool {
			return Dim.Dimension == Size;
		};

		if (std::find_if(DesiredDimentions.begin(), DesiredDimentions.end(),
				DimentionWithSize) == DesiredDimentions.end())
			return false;
	}
	return true;
}

bool ArrayAnalysis::fixDelinearizedDimentions(
		std::vector<AccessInfo *> &Accesses, AccessToDimentionsTy &Acc2Dim) {

	std::vector<DimensionInfo> *Dimentions = &Acc2Dim[Accesses[0]];
	// get the pruposed max number of dimentions
	for (AccessInfo *access : Accesses) {
		std::vector<DimensionInfo> &OtherDimentions = Acc2Dim[access];
		if (OtherDimentions.size() > Dimentions->size())
			Dimentions = &OtherDimentions;
	}
	// check if the accesses have the same number of dimentions, and the same size
	// for each dimention. If not try to fix them.
	for (AccessInfo *access : Accesses) {
		std::vector<DimensionInfo> &OtherDimentions = Acc2Dim[access];
		const bool CompatibleDimentions = dimentionSizesAreCompatible(
				OtherDimentions, *Dimentions);

		if (!CompatibleDimentions)
			return false;
		const bool SameNumberOfDimentions = OtherDimentions.size()
				== Dimentions->size();
		if (!SameNumberOfDimentions) {
			std::vector<DimensionInfo> NewAccessDimentions(Dimentions->size());
			for (size_t dim_idx = 0; dim_idx < NewAccessDimentions.size();
					++dim_idx) {
				const long DimSize = Dimentions->at(dim_idx).Dimension;
				NewAccessDimentions[dim_idx].Dimension = DimSize;
				auto DimentionWithSize = [&DimSize](
						ArrayAnalysis::DimensionInfo &Dim) -> bool {
					return Dim.Dimension == DimSize;
				};
				auto OriginalDimention = std::find_if(OtherDimentions.begin(),
						OtherDimentions.end(), DimentionWithSize);

				if (OriginalDimention != OtherDimentions.end()) {
					// the extended access function
					NewAccessDimentions[dim_idx].CoefIdx =
							OriginalDimention->CoefIdx;
					NewAccessDimentions[dim_idx].Access =
							OriginalDimention->Access;
				} else
					NewAccessDimentions[dim_idx].Access =
							AccessInfo::LinearFunctionTy(
									OtherDimentions[0].Access.size(), 0);
			}
			Acc2Dim[access] = NewAccessDimentions;
		}
	}
	return true;
}

void ArrayAnalysis::delinearizeAccesses() {
	ArrayToAccessTy Arr2Acc = mapArrayToAccess();
	AccessToDimentionsTy Acc2Dim = getAccessDimentions(Arr2Acc);
	proposeDelinearizedAccessFunctions(Acc2Dim);
	// validate the delinearization for each array.
	for (auto &pair : Arr2Acc) {
		ArrayInfo *Array = pair.first;
		if (!Array->ContainsNonLinearAccess) {
			std::vector<AccessInfo *> &Accesses = pair.second;
			const bool ValidDimention = fixDelinearizedDimentions(Accesses,
					Acc2Dim);
			if (ValidDimention) {
				bool ValidExtremes = false;
				for (int i = 0; i < 4 && !ValidExtremes; ++i) {
					const bool FixedLowerExtremes = fixLowerExtremes(Accesses,
							Acc2Dim);
					const bool FixedUpperExtremes = fixUpperExtremes(Accesses,
							Acc2Dim);
					ValidExtremes = !FixedLowerExtremes && !FixedUpperExtremes;
				}
				if ((Delinearize == DelinearizeOption::ForceDelinearize)
						|| ValidExtremes) {
					updateDelinearizedAccessFunctions(Array, Accesses, Acc2Dim);
				}
			}
		}
	}
}

void ArrayAnalysis::mergeArrays(std::vector<ArrayInfo> &Arrays) {
	Arrays = std::move(initArrays());
	bool HasChanged = false;
	do {
		HasChanged = false;

		// Compare all arrays to each other
		for (unsigned a = 0; a < Arrays.size(); ++a) {
			for (unsigned b = a + 1; b < Arrays.size(); ++b) {

				// Merge the 2 arrays if they overlap
				ArrayInfo &ArrayA = Arrays[a], &array_b = Arrays[b];
				if (canMerge(ArrayA, array_b)) {
					mergeArrays(&ArrayA, array_b);
					Arrays[b] = Arrays.back();
					Arrays.pop_back();
					HasChanged = true;
				}
			}
		}
	} while (HasChanged);
}

void ArrayAnalysis::computeArrayInfo() {
	Arrays = std::move(initArrays());
	mergeArrays(Arrays);
	normalizeArraysIDs(Arrays);
	computeArrayWordSize(Arrays);

	computeMemToArray(MemToArray);
	computeAccesses(MemToAccess);

	if (Delinearize != DelinearizeOption::NoDelinearize)
		delinearizeAccesses();
}

ArrayAnalysis::ArrayAnalysis(APOLLO_PROG *apolloProg_) {
	traceEnter("apollo::array_analysis::array_analysis()");
	eventStart("code_bones_arrayinfo");

	apolloProg = apolloProg_;
	LoopBounds = apolloProg->ExtremeBounds;
	MemToRange = apolloProg->ExtremeMemRanges;
	Delinearize = getDelinearizeOption();

	filterUnsolvedIds();
	computeArrayInfo();

	if (backdoorEnabled("info")) {
		std::stringstream ToDump;
		ToDump << "Array Analysis Results [\n";
		this->dump(ToDump);
		ToDump << "]";
		Info(ToDump.str());
	}

	eventEnd("code_bones_arrayinfo");
	traceExit("apollo::array_analysis::array_analysis()");
}

void ArrayAnalysis::dump(std::ostream &OS) const {
	for (auto Ar : Arrays) {
		OS << "Array " << Ar.Id << "\n";
		OS << "\trange = " << Ar.Range.first << "-" << Ar.Range.second << " \n";
		OS << "\tword size = " << Ar.WordSizeInBytes << "\n";
		OS << "\tdimensions = " << Ar.Dimentions << "\n";
		OS << "\tread only = " << Ar.ReadOnly << "\n";
		for (auto MemId : Ar.MemIds) {
			OS << "\tmem" << MemId << "\n";
		}
	}
}

