//===--- BoneComposedStmt.cpp ---------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesInfo/BoneAst.h"
#include "CodeBones/BonesInfo/BoneComposedStmt.h"
#include "CodeBones/BonesInfo/BoneLoop.h"
#include "CodeBones/BonesInfo/BoneStmt.h"

namespace apollo { 
namespace codebones {

static void removeDuplicates(IdVector &V) {
  size_t size = V.size();
  if (size == 0)
    return;
  size_t Idx = 0;
  while (Idx < size - 1) {
    if (V[Idx + 1] == V[Idx]) {
      for (size_t j = Idx + 1; j < size - 1; ++j) {
        V[j] = V[j + 1];
      }
      V.pop_back();
      size--;
    } else {
      Idx++;
    }
  }
}

BoneComposedStmt::BoneComposedStmt(BoneStmt *A, BoneStmt *B,
                                   const CompositionType &CompTy) {
  Composition = CompTy;
  First = A;
  Second = B;
  if (Composition == CompositionType::Sequential)
    BoneName = "sequential(";
  else {
    if (Composition == CompositionType::Guarded)
      BoneName = "guarded(";
    else
      BoneName = "concurrent(";
  }
  BoneName.append(First->name());
  BoneName.append("__");
  BoneName.append(Second->name());
  BoneName.append(")");

  std::vector<BoneStmt *> Bones = this->getBones();
  for (BoneStmt *bone : Bones) {
    // output
    StoreIds.insert(StoreIds.end(), bone->stores().begin(),
                    bone->stores().end());
    MemVerifIds.insert(MemVerifIds.end(), bone->memVerif().begin(),
                       bone->memVerif().end());
    BoundVerifIds.insert(BoundVerifIds.end(), bone->boundVerif().begin(),
                         bone->boundVerif().end());
    ScalarVerifIds.insert(ScalarVerifIds.end(), bone->scalarVerif().begin(),
                          bone->scalarVerif().end());

    // input
    LoadIds.insert(LoadIds.end(), bone->loads().begin(), bone->loads().end());
    ScalarIds.insert(ScalarIds.end(), bone->scalars().begin(),
                     bone->scalars().end());
  }

  std::sort(StoreIds.begin(), StoreIds.end());
  std::sort(MemVerifIds.begin(), MemVerifIds.end());
  std::sort(BoundVerifIds.begin(), BoundVerifIds.end());
  std::sort(ScalarVerifIds.begin(), ScalarVerifIds.end());
  std::sort(LoadIds.begin(), LoadIds.end());
  std::sort(ScalarIds.begin(), ScalarIds.end());

  removeDuplicates(StoreIds);
  removeDuplicates(MemVerifIds);
  removeDuplicates(BoundVerifIds);
  removeDuplicates(ScalarVerifIds);
  removeDuplicates(LoadIds);
  removeDuplicates(ScalarIds);
}

bool BoneComposedStmt::isComposed() const {
  return true;
}

bool BoneComposedStmt::executesSequentially() const {
  return Composition != CompositionType::Concurrent || isVerificationOnly();
}

bool BoneComposedStmt::isGuarded() const {
  return Composition == CompositionType::Guarded;
}

std::vector<BoneStmt *> BoneComposedStmt::getBones() {
  return std::vector<BoneStmt *>({First, Second});
}

BoneComposedStmt *
BoneComposedStmt::compose(BoneStmt *A, BoneStmt *B,
                          const BoneComposedStmt::CompositionType &Ty) {

  assert(BoneComposedStmt::canCompose(A, B));
  if (!BoneComposedStmt::canCompose(A, B, false))
    std::swap(A, B);
  BoneComposedStmt *Comp = new BoneComposedStmt(A, B, Ty);
  BoneLoop *Parent = A->getParent();
  // add the new, and remove the old
  Parent->addChild(Comp, A).removeChild(A).removeChild(B);
  return Comp;
}

bool BoneComposedStmt::canCompose(BoneStmt *A, BoneStmt *B, bool TryReverse) {
  const bool SameParent = A->getParent() == B->getParent();
  if (!SameParent)
    return false;
  BoneLoop *Parent = A->getParent();
  if (Parent == nullptr)
    return true;
  std::vector<BoneAst *> Children = Parent->children();
  auto FIterator = std::find(Children.begin(), Children.end(), A);
  FIterator++;
  const bool Contiguous = *FIterator == B;
  return Contiguous || (TryReverse && canCompose(B, A, false));
}

} // end namespace codebones 
} // end namsepace apollo

