//===--- BoneStmt.cpp -----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesInfo/BoneAst.h"
#include "CodeBones/BonesInfo/BoneLoop.h"
#include "CodeBones/BonesInfo/BoneStmt.h"

namespace apollo {
namespace codebones {

static void OutputIds(const char *Field, const std::vector<int> &V,
		std::ostream &OS) {
	if (!V.empty()) {
		OS << Field << "[ ";
		for (auto id : V) {
			OS << id << " ";
		}
		OS << "] ";
	}
}

BoneStmt::BoneStmt(const std::string &Name, const IdVector &Loads,
		const IdVector &Stores) {
	BoneName = Name;
	LoadIds = Loads;
	StoreIds = Stores;

	std::sort(LoadIds.begin(), LoadIds.end());
	std::sort(StoreIds.begin(), StoreIds.end());
}

BoneStmt::BoneStmt(struct apollo::CodeGenerator::CodeBonesDescriptor &CBD) :
		BoneAst() {
	BoneName = CBD.Name;

	StoreIds = CBD.Stores;
	LoadIds = CBD.Loads;
	ScalarIds = CBD.Scalars;
	MemVerifIds = CBD.MemVerifIds;
	BoundVerifIds = CBD.BoundVerifIds;
	ScalarVerifIds = CBD.ScalarVerifIds;

	std::sort(LoadIds.begin(), LoadIds.end());
	std::sort(StoreIds.begin(), StoreIds.end());
	std::sort(ScalarIds.begin(), ScalarIds.end());
	std::sort(MemVerifIds.begin(), MemVerifIds.end());
	std::sort(BoundVerifIds.begin(), BoundVerifIds.end());
	std::sort(ScalarVerifIds.begin(), ScalarVerifIds.end());
}

const std::string &BoneStmt::name() const {
	return BoneName;
}

const IdVector &BoneStmt::stores() const {
	return StoreIds;
}

const IdVector &BoneStmt::loads() const {
	return LoadIds;
}

const IdVector &BoneStmt::scalars() const {
	return ScalarIds;
}

const IdVector &BoneStmt::memVerif() const {
	return MemVerifIds;
}

const IdVector &BoneStmt::boundVerif() const {
	return BoundVerifIds;
}

const IdVector &BoneStmt::scalarVerif() const {
	return ScalarVerifIds;
}

IdVector BoneStmt::allAccesses() const {
	const IdVector &Loads = this->loads();
	const IdVector &Stores = this->stores();
	IdVector All;
	All.insert(All.end(), Loads.begin(), Loads.end());
	All.insert(All.end(), Stores.begin(), Stores.end());
	std::sort(All.begin(), All.end());
	return All;
}

bool BoneStmt::isLoop() const {
	return false;
}

bool BoneStmt::accessMemory() const {
	return !this->loads().empty() || !this->stores().empty();
}

bool BoneStmt::isComposed() const {
	return false;
}

bool BoneStmt::isVerification() const {
	return !this->memVerif().empty() || !this->boundVerif().empty()
			|| !this->scalarVerif().empty();
}

bool BoneStmt::isVerificationOnly() const {
	return this->isVerification() && this->stores().empty();
}

bool BoneStmt::verifiesScalarInitialziation() const {
	return !this->scalarVerif().empty()
			&& (this->name().find("init") != std::string::npos);
}

int BoneStmt::lastMemoryAccess() const {
	assert(this->accessMemory());
	return this->allAccesses().back();
}

int BoneStmt::lastScalar() const {
	assert(!this->scalars().empty());
	return this->scalars().back();
}

void BoneStmt::dump(std::ostream &OS) const {
	OS << this->name() << " ";
	OutputIds(".loads", this->loads(), OS);
	OutputIds(".phis", this->scalars(), OS);
	OutputIds(".stores", this->stores(), OS);
	OutputIds(".mem_verif", this->memVerif(), OS);
	OutputIds(".bound_verif", this->boundVerif(), OS);
	OutputIds(".scalar_verif", this->scalarVerif(), OS);
}

std::vector<BoneLoop *> BoneStmt::loops() {
	return {};
}

std::vector<BoneStmt *> BoneStmt::stmts() {
	return {this};
}

} // end namespace codebones
} // end namespace apollo
