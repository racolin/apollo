//===--- BoneToAccess.cpp -------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesToScop/BoneToAccess.h"
#include "CodeBones/BonesToScop/BuildContext.h"
#include "Interpolation/Interpolation.h"

#include <string>

using apollo::codebones::BoneLoop;
using apollo::codebones::BoneStmt;

BoneToAccess::BoneToAccess(APOLLO_PROG *apolloProg_, ArrayAnalysis &RangeInfo_,
                           std::set<BoneStmt *> &RestrictTo_)
  : memToArray(&RangeInfo_), RestrictTo(RestrictTo_) {
  apolloProg = apolloProg_;
}

BoneToAccess::RetTy BoneToAccess::visitStmt(BoneStmt *Stmt, int) {

  if (!RestrictTo.count(Stmt)) {
    return RetTy();
  }

  RetTy::mapped_type Accesses = nullptr;
  const int NumIterators = Stmt->depth();
  for (int mem_id : Stmt->allAccesses()) {
    const auto &MemPred = apolloProg->PredictionModel->getMemPrediction(mem_id);
    const bool IsSolved = MemPred.isLinear();
    const bool IsTube = PredictionModel::isTube(MemPred.PreType);
    if (IsSolved) {
      std::vector<ArrayAnalysis::AccessInfo> AccessInformation =
          memToArray->MemToAccess[mem_id];
      ArrayAnalysis::ArrayInfo &Array =
          *memToArray->MemToArray.at(mem_id).first;
      const StaticMemInfo &StmtInfo = apolloProg->StaticInfo->Mem[mem_id];
      const int AccessType = StmtInfo.IsWrite ? OSL_TYPE_WRITE : OSL_TYPE_READ;
      for (auto &single_access : AccessInformation) {
        const int ArrayDimentions = Array.Dimentions;
        assert(single_access.Access.size() == (unsigned)ArrayDimentions);
        osl_relation_p Access = osl_relation_pmalloc(
            OSL_PRECISION, 1 + ArrayDimentions,
            2 + 1 + ArrayDimentions + NumIterators + NumParams);
        osl_relation_set_attributes(Access, 1 + ArrayDimentions, NumIterators,
                                    0, NumParams);
        osl_relation_set_type(Access, AccessType);
        osl_int_p ArrayRow = Access->m[0];
        osl_int_set_si(OSL_PRECISION, &ArrayRow[1], -1);
        osl_int_set_si(
            OSL_PRECISION,
            &ArrayRow[2 + ArrayDimentions + NumIterators + NumParams],
            Array.Id);

        for (int i = 0; i < ArrayDimentions; ++i) {
          osl_int_p IndexRow = Access->m[i + 1];
          osl_int_set_si(OSL_PRECISION, &IndexRow[2 + i], -1);
          const ArrayAnalysis::AccessInfo::LinearFunctionTy &LF =
              single_access.Access[i];

          for (unsigned coef_idx = 0; coef_idx < LF.size() - 1; ++coef_idx) {
            osl_int_set_si(OSL_PRECISION,
                           &IndexRow[2 + ArrayDimentions + coef_idx],
                           LF[coef_idx]);
          }
          osl_int_set_si(
              OSL_PRECISION,
              &IndexRow[2 + ArrayDimentions + NumIterators + NumParams],
              LF[LF.size() - 1]);
        }
        osl_relation_list_add(&Accesses, osl_relation_list_node(Access));
        osl_relation_free(Access);
      }
    }
    if (IsTube) {
      std::vector<ArrayAnalysis::AccessInfo> AccessInformation =
          memToArray->MemToAccess[mem_id];
      ArrayAnalysis::ArrayInfo &Array =
          *memToArray->MemToArray.at(mem_id).first;
      const StaticMemInfo &StmtInfo = apolloProg->StaticInfo->Mem[mem_id];
      int AccessType = StmtInfo.IsWrite ? OSL_TYPE_WRITE : OSL_TYPE_READ;
      for (auto &single_access : AccessInformation) {
        const int ArrayDimentions = Array.Dimentions;
        assert(single_access.Access.size() == (unsigned)ArrayDimentions);
        osl_relation_p Access = osl_relation_pmalloc(
            OSL_PRECISION, 3,
            2 + 1 + ArrayDimentions + NumIterators + NumParams);
        osl_relation_set_attributes(Access, 1 + ArrayDimentions, NumIterators,
                                    0, NumParams);
        osl_relation_set_type(Access, AccessType);

        osl_int_set_si(OSL_PRECISION, &Access->m[0][1], -1);
        osl_int_set_si(
            OSL_PRECISION,
            &Access->m[0][2 + ArrayDimentions + NumIterators + NumParams],
            Array.Id);

        osl_int_set_si(OSL_PRECISION, &Access->m[1][0], 1);
        osl_int_set_si(OSL_PRECISION, &Access->m[2][0], 1);

        osl_int_set_si(OSL_PRECISION, &Access->m[1][2], -1);
        osl_int_set_si(OSL_PRECISION, &Access->m[2][2], 1);

        const ArrayAnalysis::AccessInfo::LinearFunctionTy &LF =
            single_access.Access[0];
        for (unsigned coef_idx = 0; coef_idx < LF.size() - 1; ++coef_idx) {
          const long Coef = LF[coef_idx];
          osl_int_set_si(OSL_PRECISION, &Access->m[1][3 + coef_idx], Coef);
          osl_int_set_si(OSL_PRECISION, &Access->m[2][3 + coef_idx], -Coef);
        }
        const long ConstantCoef = LF.back();
        osl_int_set_si(OSL_PRECISION, &Access->m[1][Access->nb_columns - 1],
                       ConstantCoef + single_access.TubeWidth);
        osl_int_set_si(OSL_PRECISION, &Access->m[2][Access->nb_columns - 1],
                       -ConstantCoef + single_access.TubeWidth);
        osl_relation_list_add(&Accesses, osl_relation_list_node(Access));
        osl_relation_free(Access);
      }
    }
  }
  RetTy StmtSca;
  StmtSca[Stmt] = Accesses;
  return StmtSca;
}

BoneToAccess::RetTy BoneToAccess::visitLoop(BoneLoop *Loop, int) {
  RetTy StmtsToAccess;
  for (auto child : Loop->children()) {
    const RetTy ChildStmts = this->visit(child, 0);
    for (auto kv : ChildStmts) {
      StmtsToAccess[kv.first] = kv.second;
    }
  }
  return StmtsToAccess;
}

std::set<int> BoneToAccess::getArrayIds() {
  std::set<int> Ids;
  for (auto &array_info : memToArray->Arrays) {
    Ids.insert(array_info.Id);
  }
  return Ids;
}

static std::string getArrayName(const int Array) {
  return std::to_string(Array);
}

osl_generic_p BoneToAccess::buildArrays() {
  const std::string ArrayPrefix = "Mem_";
  osl_arrays_p Arrays = osl_arrays_malloc();
  const std::set<int> ArrayIds = std::move(this->getArrayIds());
  Arrays->nb_names = ArrayIds.size();
  Arrays->id = (int *)malloc(sizeof(int) * ArrayIds.size());
  Arrays->names = (char **)malloc(sizeof(char *) * ArrayIds.size());

  int ArrayIdx = 0;
  for (int array_id : ArrayIds) {
    std::string Name = getArrayName(array_id);
    std::string ArrayName = ArrayPrefix;
    ArrayName.append(Name.begin(), Name.end());

    Arrays->id[ArrayIdx] = array_id;
    Arrays->names[ArrayIdx] = strdup(ArrayName.c_str());
    ArrayIdx++;
  }
  osl_generic_p ArraysExtension = osl_generic_malloc();
  ArraysExtension->interface = osl_arrays_interface();
  ArraysExtension->data = Arrays;
  return ArraysExtension;
}

