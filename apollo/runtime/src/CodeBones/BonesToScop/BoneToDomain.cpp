//===--- BoneToDomain.cpp -------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesToScop/BoneToDomain.h"
#include "CodeBones/BonesToScop/BoneToBody.h"
#include "CodeBones/BonesToScop/BuildContext.h"
#include "Interpolation/Interpolation.h"

using apollo::codebones::BoneLoop;
using apollo::codebones::BoneStmt;

static bool hasInvariantLevel(const APOLLO_PROG *apolloProg,
                              ArrayAnalysis &RangeInfo,
                              apollo::codebones::BoneStmt *Stmt,
                              const unsigned LoopIdx) {

  if (!Stmt->stores().empty()) {
    return false;
  }
  // check if for one level the memory access remains linear.
  if (Stmt->memVerif().size() == 1 && Stmt->boundVerif().empty() &&
      Stmt->scalarVerif().empty()) {
    const int MemVerifId = Stmt->memVerif().front();
    const StaticMemInfo &MemInfo = apolloProg->StaticInfo->Mem[MemVerifId];
    if (MemInfo.Parents.size() <= LoopIdx)
      return true;
    // in this case it's linear
    if (!MemInfo.NonLinearLoop)
      return true;
    const int NonLinearLoopId = MemInfo.NonLinearLoop->Id;
    const int LoopId = MemInfo.Parents[LoopIdx]->Id;
    // for loops after the nonlinear level, we know that the iterator values
    // participate in a linear computation.
    if (NonLinearLoopId < LoopId)
      return true;
  }
  // check if for one level the inputs remain constant.
  const auto &LoadIds = Stmt->loads();
  const auto &PhiIds = Stmt->scalars();

  for (int id : LoadIds) {
    const auto &MemPred = apolloProg->PredictionModel->getMemPrediction(id);
    const bool IsLinear = MemPred.isLinear();
    if (!IsLinear)
      return false;
    // if someone reads, it may carry a dependency
    if (!RangeInfo.MemToArray[id].first->ReadOnly)
      return false;
    // check the coefficient to be 0
    const auto &LF = MemPred.LF;
    if (LoopIdx < LF.size() - 1) {
      const long coef = LF[LoopIdx];
      if (coef != 0)
        return false;
    }
  }

  for (int id : PhiIds) {
    const auto &ScalarPred =
        apolloProg->PredictionModel->getScalarPrediction(id);
    const bool IsLinear = ScalarPred.isLinear();
    if (!IsLinear)
      return false;
    const auto &LF = ScalarPred.LF;
    if (LoopIdx < LF.size() - 1) {
      const long coef = LF[LoopIdx];
      if (coef != 0)
        return false;
    }
  }
  // if we reach this point, no matter the value of the iterator at loop_idx,
  // the result of the code_bone will be the same.
  return true;
}

BoneToDomain::BoneToDomain(APOLLO_PROG *apolloProg_, ArrayAnalysis &RangeInfo_,
                           std::set<BoneStmt *> &RestrictTo_,
                           bool ReduceIterationSpace_)
  : MemToArray(&RangeInfo_), RestrictTo(RestrictTo_) {
  apolloProg = apolloProg_;
  ReduceIterationSpace = ReduceIterationSpace_;
}

BoneToDomain::RetTy BoneToDomain::visitStmt(BoneStmt *Stmt, int) {

  if (!RestrictTo.count(Stmt)) {
    return RetTy();
  }

  const int ParentLoopId = Stmt->getParent()->getId();
  const StaticLoopInfo &LoopInfo = apolloProg->StaticInfo->Loops[ParentLoopId];
  const int NumIterators = LoopInfo.depth();
  bool VerifiesScalarFinalization = !Stmt->scalarVerif().empty() && 
                                    !Stmt->verifiesScalarInitialziation();
  const bool VerifiesLoopBound = !Stmt->boundVerif().empty();
  osl_relation_p Domain = osl_relation_pmalloc(OSL_PRECISION, 2 * NumIterators,
                                               2 + NumIterators + NumParams);
  osl_relation_set_attributes(Domain, NumIterators, 0, 0, NumParams);
  osl_relation_set_type(Domain, OSL_TYPE_DOMAIN);
  // add loop bound constraints.
  for (int iter_idx = 0; iter_idx < NumIterators; ++iter_idx) {
    const int IterId = LoopInfo.Parents[iter_idx]->Id;
    osl_int_p Lower = Domain->m[2 * iter_idx];
    osl_int_p Upper = Domain->m[2 * iter_idx + 1];
    const bool ExecuteOnlyFirstIteration =
              ReduceIterationSpace &&
              hasInvariantLevel(apolloProg, *MemToArray, Stmt, iter_idx);
    const bool ExecuteOnlyLastIteration =
              VerifiesLoopBound && Stmt->boundVerif().front() < IterId;

    if (!ExecuteOnlyFirstIteration && !ExecuteOnlyLastIteration) {
      osl_int_set_si(OSL_PRECISION, &Lower[0], 1);
      osl_int_set_si(OSL_PRECISION, &Upper[0], 1);
      osl_int_set_si(OSL_PRECISION, &Lower[1 + iter_idx], 1);
      osl_int_set_si(OSL_PRECISION, &Upper[1 + iter_idx], -1);

      if (IterId == 0) {
        // cl <= i <= cu
        osl_int_set_si(OSL_PRECISION, &Lower[1 + NumIterators + CLParam], -1);
        osl_int_set_si(OSL_PRECISION, &Upper[1 + NumIterators + CUParam], 1);
      } else {
        const auto &LF =
            apolloProg->PredictionModel->getBoundPrediction(IterId).LF;

        for (unsigned i = 0; i < LF.size() - 1; ++i) {
          osl_int_set_si(OSL_PRECISION, &Upper[1 + i], LF[i]);
        }
        long ConstantCoef = LF[LF.size() - 1] - 1;
        if (iter_idx == (NumIterators - 1) && VerifiesScalarFinalization)
          ConstantCoef -= 1;
        osl_int_set_si(OSL_PRECISION, &Upper[1 + NumIterators + NumParams],
                       ConstantCoef);
      }
    } else {
      osl_int_set_si(OSL_PRECISION, &Lower[1 + iter_idx], 1);
      if (ExecuteOnlyLastIteration) {
        // j == chunk_upper or j == a*i+b
        if (IterId == 0)
          osl_int_set_si(OSL_PRECISION, &Lower[1 + NumIterators + CUParam], -1);
        else {
          const auto &BoundPred =
              apolloProg->PredictionModel->getBoundPrediction(IterId);
          const auto &LF = BoundPred.LF;
          for (unsigned i = 0; i < LF.size() - 1; ++i) {
            osl_int_set_si(OSL_PRECISION, &Lower[1 + i], -LF[i]);
          }
          const long ConstantCoef = LF[LF.size() - 1] - 1;
          osl_int_set_si(OSL_PRECISION, &Lower[1 + NumIterators + NumParams],
                         -ConstantCoef);
        }
      } else if (ExecuteOnlyFirstIteration) {
        // j == chunk_lower or j == 0
        if (IterId == 0)
          osl_int_set_si(OSL_PRECISION, &Lower[1 + NumIterators + CLParam], -1);
        else
          osl_int_set_si(OSL_PRECISION, &Lower[1 + NumIterators + NumParams],
                         0);
      }
    }
  }
  RetTy StmtDom;
  StmtDom[Stmt] = Domain;
  return StmtDom;
}

BoneToDomain::RetTy BoneToDomain::visitLoop(BoneLoop *Loop, int) {
  RetTy StmtsToDomains;
  for (auto child : Loop->children()) {
    const RetTy ChildStmts = this->visit(child, 0);
    for (auto kv : ChildStmts) {
      StmtsToDomains[kv.first] = kv.second;
    }
  }
  return StmtsToDomains;
}

