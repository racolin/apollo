//===--- BuildContext.cpp -------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/BonesToScop/BuildContext.h"
#include "Interpolation/Interpolation.h"
#include "RuntimeConfig.h"

#include <string>

extern RuntimeConfig apolloRuntimeConfig;

osl_relation_p buildContext(APOLLO_PROG *apolloProg) {
  const int ConstraintNumber = 4;
  const int MaxBound = std::min<long>(apolloProg->State->maxChunkUpper(),
                                      std::numeric_limits<int>::max());
  assert(0 < MaxBound);
  osl_relation_p Context =
      osl_relation_pmalloc(OSL_PRECISION, ConstraintNumber, 2 + NumParams);
  osl_relation_set_attributes(Context, 0, 0, 0, NumParams);
  osl_relation_set_type(Context, OSL_TYPE_CONTEXT);
  // cur_iteration <= cl
  osl_int_set_si(OSL_PRECISION, &Context->m[0][0], 1);
  osl_int_set_si(OSL_PRECISION, &Context->m[0][1 + NumParams],
                 -apolloProg->State->chunkLower());
  osl_int_set_si(OSL_PRECISION, &Context->m[0][1 + CLParam], 1);
  // cl+1 <= cu
  osl_int_set_si(OSL_PRECISION, &Context->m[1][0], 1);
  osl_int_set_si(OSL_PRECISION, &Context->m[1][1 + CLParam], -1);
  osl_int_set_si(OSL_PRECISION, &Context->m[1][1 + CUParam], 1);
  osl_int_set_si(OSL_PRECISION, &Context->m[1][1 + NumParams], -1);
  // cu - cl <= opt_size
  osl_int_set_si(OSL_PRECISION, &Context->m[2][0], 1);
  osl_int_set_si(OSL_PRECISION, &Context->m[2][1 + CLParam], 1);
  osl_int_set_si(OSL_PRECISION, &Context->m[2][1 + CUParam], -1);
  osl_int_set_si(OSL_PRECISION, &Context->m[2][1 + NumParams],
                 apolloRuntimeConfig.OptimizedChunkSize);
  // cu <= max_chunk_upper or cu <= max_int
  osl_int_set_si(OSL_PRECISION, &Context->m[3][0], 1);
  osl_int_set_si(OSL_PRECISION, &Context->m[3][1 + CUParam], -1);
  osl_int_set_si(OSL_PRECISION, &Context->m[3][1 + NumParams], MaxBound - 1);
  return Context;
}

