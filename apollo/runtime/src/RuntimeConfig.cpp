//===--- RuntimeConfig.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include <ProfilingPlugin/LinearPhasesDetector.h>
#include "RuntimeConfig.h"
#include "Backdoor/Backdoor.h"
#include "ProfilingPlugin/ProfilingPluginBase.h"
#include <cstdlib>
#include <iostream>
#include <omp.h>
#include <ProfilingPlugin/ReIndexPhasesDetector.h>

/// \brief Sets an integer value from env (if found)
/// \param Name
/// \param Var
void RuntimeConfig::setIntFromEnv(const char *Name, int &Var) {
	const char *Env = getenv(Name);
	if (Env)
		Var = atoi(Env);
}

/// \brief Sets an bool value from env (if found)
///        The value is set as true if the env variable is not zero
/// \param Name
/// \param Var
void RuntimeConfig::setBoolFromEnv(const char *Name, bool &Var) {
	const char *Env = getenv(Name);
	if (Env)
		Var = atoi(Env) != 0;
}

/// \brief Sets an string value from env (if found)
/// \param Name
/// \param Var
void RuntimeConfig::setStringFromEnv(const char *Name, std::string &Var) {
	const char *Env = getenv(Name);
	if (Env)
		Var = Env;
}

// constructor
RuntimeConfig::RuntimeConfig() {
	setIntFromEnv("APOLLO_INSTR_CHUNK_SIZE", InstrumentationChunkSize);
	setIntFromEnv("APOLLO_OPT_CHUNK_SIZE", OptimizedChunkSize);
	InstrumentationSample = InstrumentationChunkSize;
	setIntFromEnv("APOLLO_INSTR_SAMPLE", InstrumentationSample);
	NumberOfThreads = omp_get_max_threads();

	predictAll = backdoorutils::getEnvBool("APOLLO_PREDICT_ALL", "");

	// Profiling mode
	setIntFromEnv("APOLLO_PROFILING_FREQUENCY", ProfilingFrequency);
	profilingMode = backdoorutils::getEnvBool("APOLLO_PROFILING", "");
	if (backdoorutils::getEnvBool("APOLLO_PROFILING", "--reindex_polyhedral_phases_detector", true)) {
		ProfilingPlugins.push_back(new ReIndexPhasesDetector(false));
	}
	if (backdoorutils::getEnvBool("APOLLO_PROFILING", "--reindex_polyhedral_phases_detector_full", true)) {
		ProfilingPlugins.push_back(new ReIndexPhasesDetector(true));
	}
	if (backdoorutils::getEnvBool("APOLLO_PROFILING", "--polyhedral_phases_detector", true)) {
		ProfilingPlugins.push_back(new LinearPhasesDetector(false));
	}
	if (backdoorutils::getEnvBool("APOLLO_PROFILING", "--polyhedral_phases_detector_full", true)) {
		ProfilingPlugins.push_back(new LinearPhasesDetector(true));
	}
	if (profilingMode && ProfilingPlugins.size() == 0) {
		std::cerr << "APOLLO_PROFILING defined to a wrong value" << std::endl;
		std::cerr << "  current value is: " << getenv("APOLLO_PROFILING")
				<< std::endl;
		std::cerr
				<< "  expected value is one of: --reindex_phases_detector, --reindex_phases_detector_full, --polyhedral_phases_detector, --polyhedral_phases_detector_full"
				<< std::endl;
		exit(-1);
	}
}
