//===--- Regression.cpp ---------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Regression/Regression.h"
#include "Backdoor/Backdoor.h"
#include "APOLLO_PROG.h"

#include <iostream>
#include <limits>
#include <cmath>

extern APOLLO_PROG *apolloProg;
using EventListTy = std::vector<InstrumentationResult::InstrumentationEvent>;

namespace Regression {

void fixRegressionPlane(std::vector<double> &RegressionPlane,
                        const int RoundTo) {
  for (double &coefAsDouble : RegressionPlane) {
    const long Coef = static_cast<long>(coefAsDouble);
    const long CoefAbs = std::abs(Coef);
    const int Sign = (Coef == 0 ? 0 : (Coef > 0 ? 1 : -1));
    if (CoefAbs <= RoundTo) {
      if (CoefAbs >= (RoundTo / 2))
        coefAsDouble = Sign * RoundTo;
      else
        coefAsDouble = 0;
    }
  }
}

/// \brief Solve the linear equation using regression
///        This is the y = b1 * i + b2 * j + ... + b0
/// \return the coefficients of independent variables the constant
std::vector<double> computeRegressionLine(const SystemOfEquations &AllValues) {

  const size_t NumEvents = AllValues.VirtualIterators.size();
  const size_t NumVIS = AllValues.VirtualIterators.front().size();

  arma::mat VirtualIterators(NumEvents, NumVIS + 1);
  arma::mat Accesses(NumEvents, 1);

  for (size_t i = 0; i < NumEvents; i++) {
    for (size_t j = 0; j < NumVIS; j++) {
      VirtualIterators.at(i, j) = AllValues.VirtualIterators[i][j];
    }
    VirtualIterators.at(i, NumVIS) = 1;
    Accesses.at(i, 0) = static_cast<double>(AllValues.Values[i]);
  }

  std::ostream &oldstream = arma::get_cerr_stream();
  std::ostream nullstream(0);
  arma::set_cerr_stream(nullstream);

  std::vector<double> Result;
  try {
    arma::mat ResultFromArmadillo = arma::solve(VirtualIterators, Accesses);
    for (unsigned int i = 0; i < ResultFromArmadillo.n_rows; ++i) {
      Result.push_back(std::round(ResultFromArmadillo.at(i, 0)));
    }
  } catch (...) {
    Info("computeRegressionLine: could not find the system of equations.");
  }

  // reset stream
  arma::set_cerr_stream(oldstream);

  return Result;
}

/// \brief Finds the maximum and minimum distance of the points from the plane
///        and the correlation coefficient for the regression plane.
/// \return: the distances of each point from the regression line
void populateRegressionQuality(const std::vector<double> &RegressionPlane,
                               const SystemOfEquations &SystemOfEq,
                               long &MaxPosError, 
                               long &MaxNegError, 
                               double &RC) {

  const size_t NumVIS = SystemOfEq.VirtualIterators.front().size();
  const size_t NEvents = SystemOfEq.VirtualIterators.size();

  long MaxDeviation = 0;
  long MinDeviation = 0;
  std::vector<double> AllPredictions;

  MaxPosError = MaxNegError = 0;

  for (size_t event = 0; event < NEvents; ++event) {
    const std::vector<long> &VirtualIterators =
        SystemOfEq.VirtualIterators[event];

    long Prediction = 0;
    for (size_t vi = 0; vi < NumVIS; ++vi) {
      Prediction +=
          static_cast<long>(VirtualIterators[vi] * RegressionPlane[vi]);
    }
    Prediction += RegressionPlane[NumVIS];
    AllPredictions.push_back(Prediction);

    const long Deviation = Prediction - SystemOfEq.Values[event];

    MaxDeviation = std::max<long>(Deviation, MaxDeviation);
    MinDeviation = std::min<long>(Deviation, MinDeviation);
  }
  RC = Regression::computeCorrelationCoefficient(SystemOfEq, AllPredictions);
  MaxPosError = MaxDeviation;
  MaxNegError = MinDeviation;
}

static SystemOfEquations buildSystemOfEquiations(EventListTy EventList,
                                                 const int NumIterators, 
                                                 int RoundTo = 1,
                                                 long *Stabilize = nullptr) {

  const size_t NumEvents = EventList.size();
  SystemOfEquations System(NumIterators, NumEvents);

  long Min = std::numeric_limits<long>::max();
  long Max = std::numeric_limits<long>::min();

  for (size_t i = 0; i < NumEvents; ++i) {
    const auto &Event = EventList[i];
    // get maximum and minimum address
    if (Stabilize) {
      Min = std::min(Max, Event.Value);
      Max = std::max(Max, Event.Value);
    }

    System.Values[i] = Event.Value;
    std::copy(Event.Coefs, Event.Coefs + NumIterators, 
              System.VirtualIterators[i].begin());
  }

  // fix numerical problem for regression correlation
  if (Stabilize) {
    long Middle = (Min / 2) + (Max / 2);
    Middle = (Middle - (Middle % RoundTo));
    for (auto &Value : System.Values) {
      Value = Value - Middle;
    }
    *Stabilize = Middle;
  }
  return System;
}

void nonAffineBoundAnalysis(std::vector<long> &Plane, 
                            long &Max,
                            double &RC, 
                            const InstrumentationResult &IR,
                            const int NonLinearLoopID, 
                            bool &IsTube) {

  const auto LoopInfo = apolloProg->StaticInfo->Loops[NonLinearLoopID];
  const size_t NumIterators = LoopInfo.Parents.size() - 1;

  const EventListTy &EventList = IR.getBoundEvents(NonLinearLoopID);

  SystemOfEquations System = std::move(buildSystemOfEquiations(EventList, 
                                                               NumIterators));

  auto RegressionPlane = std::move(computeRegressionLine(System));
  const bool Success = RegressionPlane.size() > 0;

  if (Success) {
    fixRegressionPlane(RegressionPlane, 1);

    Plane.clear();
    Plane.insert(Plane.end(), RegressionPlane.begin(), RegressionPlane.end());

    long MaxPosError = 0;
    long MaxNegError = 0;
    populateRegressionQuality(RegressionPlane, System,
                              MaxPosError, MaxNegError, RC);

    // the regression plane goes through the minimum iterated - 1.
    Plane.back() += (MaxNegError - 1);
    Max = Plane.back() + MaxPosError;

    if (RC < 0.9) {
      IsTube = false;
    }
  }
}

void nonAffineMemoryAnalysis(std::vector<long> &Plane, 
                             long &TubeWidth,
                             double &RC, 
							 const std::vector<InstrumentationResult::InstrumentationEvent> &EventList,
                             const int NonLinearMemoryID, 
                             bool &IsTube) {

  const auto StmtInfo = apolloProg->StaticInfo->Mem[NonLinearMemoryID];
  const size_t NumIterators = StmtInfo.Parents.size();
  const int WordSize = StmtInfo.SizeInBytes;

  long Stabilize = 0;
  SystemOfEquations System = std::move(buildSystemOfEquiations(EventList, 
                                                               NumIterators, 
                                                               WordSize, 
                                                               &Stabilize));

  auto RegressionPlane = std::move(computeRegressionLine(System));
  const bool Success = RegressionPlane.size() > 0;

  if (Success) {
    fixRegressionPlane(RegressionPlane, WordSize);

    Plane.clear();
    Plane.insert(Plane.end(), RegressionPlane.begin(), RegressionPlane.end());
    Plane.back() = Plane.back() + Stabilize;

    long MaxPosError = 0;
    long MaxNegError = 0;
    populateRegressionQuality(RegressionPlane, System,
                              MaxPosError, MaxNegError, RC);
    TubeWidth = std::max(abs(MaxPosError), abs(MaxNegError)) *
               backdoorutils::getEnvFloat("APOLLO_BONES", "--tubewidth", 1.0);

    // Tube width should be multiple of word_size
    if (!(TubeWidth % WordSize)) {
      long Lower = TubeWidth - (TubeWidth % WordSize);
      Lower = Lower + WordSize;
      TubeWidth = Lower;
    }

    // Check correlation of Regression Tube
    if (RC < 0.9) {
      IsTube = false;
    }
  }
}

static double getAverage(const std::vector<double> &V) {
  double Avg = 0;
  const double Size = V.size();
  for (double d : V) {
    Avg += d / Size;
  }
  return Avg;
}

static double getDeviation(const std::vector<double> &V, double Avg) {
  double Var = 0.0;
  const double Size = V.size() - 1;
  for (double d : V) {
    Var += std::pow(d - Avg, 2.0) / Size;
  }
  return std::sqrt(Var);
}

static double getCovariance(const std::vector<double> &ObservedValue,
                            const std::vector<double> &PredictedValue,
                            const double AverageObservation,
                            const double AveragePrediction) {
  const size_t NObservations = ObservedValue.size();
  const double Size = NObservations;
  double Covariance = 0;
  for (size_t i = 0; i < NObservations; ++i) {
    Covariance += ((ObservedValue[i] - AverageObservation) *
                   (PredictedValue[i] - AveragePrediction)) /
                   Size;
  }
  return Covariance;
}

// multiple correlation coefficient
double computeCorrelationCoefficient(const SystemOfEquations &SystemOfEq,
                               const std::vector<double> &PredictedValues) {

  const double AverageObservation = getAverage(SystemOfEq.Values);
  const double AveragePrediction = getAverage(PredictedValues);

  const double Covariance = getCovariance(SystemOfEq.Values, PredictedValues,
                                    AverageObservation, AveragePrediction);

  const double DeviationObservation =
              getDeviation(SystemOfEq.Values, AverageObservation);
  const double DeviationPrediction = getDeviation(PredictedValues, 
                                                  AveragePrediction);

  return Covariance / (DeviationObservation * DeviationPrediction);
}

} // end namespace Regression
