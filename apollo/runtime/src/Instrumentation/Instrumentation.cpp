//===--- Instrumentation.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Instrumentation/Instrumentation.h"
#include "InstrumentationConfig.h"

#include <cstdarg>

/// \brief This functions register a new instrumentation event.
/// The registered event can be either static or dynamic. Static
/// events are all registered at the beginning of the execution of
/// the instrumentation skeleton loop while dynamic ones are recorded
/// during the loop execution.
///
/// \param Result the object where to record the event
/// \param Type the type of the event to record
/// \param Key the identifier of the event (the memory access, scalara or bound id)
/// \param Val the value of the event (e.g. the adress of memory access), only used for dynamic events
/// \param N the number of varargs
/// \param ... represents coefficients for static events and vi values for dynamic ones
extern "C" void apollo_register_event(InstrumentationResult *Result,
                                      const EventType Type,
                                      const long Key, 
                                      const long Val,
                                      const long N, ...) {

  va_list Value;
  va_start(Value, N);
  long Params[N];

  for (int i = 0; i < N; i++) {
    Params[i] = va_arg(Value, long);
  }

  va_end(Value);

  if (!Result) {
	assert(false);
    return;
  }
  auto &InstrumentationResults = *Result;

  switch (Type) {
  case MemDyn:
    InstrumentationResults.registerDynamicMemAccess(Key, (void *)Val, Params, N);
    break;
  case ScaDyn:
    InstrumentationResults.registerDynamicScalar(Key, Val, Params, N);
    break;
  case BoundDyn:
    InstrumentationResults.registerDynamicBound(Key, Val, Params, N);
    break;
  case MemSta:
    InstrumentationResults.registerStaticMemAccess(Key, Params, N);
    break;
  case ScaSta:
    InstrumentationResults.registerStaticScalar(Key, Params, N);
    break;
  case BoundSta:
    InstrumentationResults.registerStaticBound(Key, Params, N);
    break;
  case FuncCall:
    InstrumentationResults.registerFunCall();
    break;
  default:
	assert(false);
    break;
  }
}
