//===--- SwitchingState.cpp -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SwitchingState/SwitchingState.h"
#include "Backdoor/Backdoor.h"
#include "RuntimeConfig.h"

#include <cstdint>
#include <cassert>
#include <limits>

extern RuntimeConfig apolloRuntimeConfig;

SwitchingState::SwitchingState(bool EnableOptimized_) {
  PrevState = CurState = SwitchingState::ApolloStateTypes::None;
  InstSize = apolloRuntimeConfig.InstrumentationChunkSize;
  OptSize = apolloRuntimeConfig.OptimizedChunkSize;
  profilingMode = apolloRuntimeConfig.profilingMode;
  profilingFrequency = apolloRuntimeConfig.ProfilingFrequency;
  Lower = 0;
  Upper = 0;
  MaxBound = std::numeric_limits<long>::max() - 1;
  EnableOptimized = EnableOptimized_;
}

void SwitchingState::registerMaxUpperBound(const long Bound) {
  MaxBound = Bound;
}

long SwitchingState::maxChunkUpper() const {
  return MaxBound;
}

long SwitchingState::chunkLower() const {
  return Lower;
}

long SwitchingState::chunkUpper() const {
  return Upper;
}

long SwitchingState::chunkSize() const {
    return chunkUpper() - chunkLower();
}

static std::string stateToString(const SwitchingState::ApolloStateTypes &ST) {
  if (ST == SwitchingState::ApolloStateTypes::None)
    return "none";
  else if (ST == SwitchingState::ApolloStateTypes::Instrument)
         return "instrument";
  else if (ST == SwitchingState::ApolloStateTypes::Original)
         return "original";
  else if (ST == SwitchingState::ApolloStateTypes::Optimized)
         return "optimized";
  else if (ST == SwitchingState::ApolloStateTypes::TxSelection)
         return "tx_selection";
  assert(false);
  return "";
}

static std::string resultToString(const ChunkResult &Res) {
  if (Res == ChunkResult::Missprediction)
    return "missprediction";
  if (Res == ChunkResult::EarlyMissprediction)
    return "early_missprediction";
  if (Res == ChunkResult::RangeAnalysisFailed)
    return "range_analysis_failed";
  if (Res == ChunkResult::Success)
    return "success";
  if (Res == ChunkResult::TransformationNotFound)
    return "transformation_not_found";
  assert(false);
  return "";
}

void SwitchingState::dump(std::ostream &OS) {
  const std::string StateStr = stateToString(CurState);
  const std::string PrevStateStr = stateToString(PrevState);
  OS << "{chunk = " << Lower << " - ";
  if (CurState == ApolloStateTypes::TxSelection)
    OS << "... ";
  else
    OS << Upper;
  OS << ", " << PrevStateStr << "->" << StateStr << " }.";
}

SwitchingState::ApolloStateTypes SwitchingState::getCurState() const {
  return CurState;
}

SwitchingState::ApolloStateTypes SwitchingState::getPrevState() const {
  return PrevState;
}

void SwitchingState::advanceChunk(ApolloStateTypes NextState, long NextChunkSize) {
    PrevState = CurState;
    CurState = NextState;
    Lower = Upper;
    Upper = Lower + NextChunkSize;
}

void SwitchingState::retryChunk(ApolloStateTypes NextState, long RetryChunkSize) {
    CurState = NextState;
    Upper = Lower + RetryChunkSize;
}

SwitchingState::ApolloStateTypes SwitchingState::nextState(
		const ChunkResult &LastChunkResult, const long ExecutedUntil) {
	traceEnter(
			backdoorutils::concat("SwitchingState::next_state(",
					stateToString(CurState), ", ",
					resultToString(LastChunkResult), ")"));

	switch (CurState) {

	case ApolloStateTypes::None: {
		advanceChunk(ApolloStateTypes::Instrument, InstSize);
		break;
	}

	case ApolloStateTypes::Original: {
		advanceChunk(ApolloStateTypes::Instrument, InstSize);
		break;
	}

	case ApolloStateTypes::Instrument: {
		advanceChunk(ApolloStateTypes::TxSelection, OptSize);
		break;
	}

	case ApolloStateTypes::TxSelection: {
		Upper = ExecutedUntil;
		PrevState = CurState;

		// In profiling mode we go back either
		// to original or to instrument
		if (profilingMode) {
			if (profilingFrequency == -1) {
				advanceChunk(ApolloStateTypes::Instrument, InstSize);
			} else {
				advanceChunk(ApolloStateTypes::Original,
						profilingFrequency - InstSize);
			}
			break;
		}

		// In normal mode we go to optimize only if we found
		// a valid transformation
		if (LastChunkResult == ChunkResult::TransformationNotFound) {
			advanceChunk(ApolloStateTypes::Original, OptSize);
		} else if (LastChunkResult == ChunkResult::Success) {
			advanceChunk(ApolloStateTypes::Optimized,
					std::min<long>(OptSize, MaxBound - 1 - Upper));
		} else {
			assert(false);
		}
		break;
	}

	case ApolloStateTypes::Optimized: {
		if (LastChunkResult == ChunkResult::Missprediction) {
			retryChunk(ApolloStateTypes::Original, chunkSize());
		} else if (LastChunkResult == ChunkResult::RangeAnalysisFailed) {
			if (chunkSize() > 128)
				retryChunk(ApolloStateTypes::Optimized, chunkSize() / 2);
			else
				retryChunk(ApolloStateTypes::Original, chunkSize());
		} else if (LastChunkResult == ChunkResult::EarlyMissprediction) {
			bool AlreadySuccessfulBefore = PrevState
					== ApolloStateTypes::Optimized;
			if (AlreadySuccessfulBefore && chunkSize() > 128) {
				retryChunk(ApolloStateTypes::Optimized, chunkSize() / 2);
			} else {
				retryChunk(ApolloStateTypes::Original, chunkSize());
			}
		} else if (LastChunkResult == ChunkResult::Success) {
			advanceChunk(ApolloStateTypes::Optimized,
					std::min<long>(chunkSize() + OptSize,
							MaxBound - 1 - Upper));
		} else
			assert(false);
		break;
	}

	default:
		assert(false);
	} // switch

	if (MaxBound <= (Lower + InstSize)) {

		// We must keep transformation selection to give info to plugins
	    if (profilingMode && CurState == ApolloStateTypes::TxSelection) {

		}

	    // In profiling mode, we only run code in instrumentation version
	    else if (profilingMode) {
			retryChunk(ApolloStateTypes::Instrument, MaxBound - Lower);
		}

	    // In normal mode, we always finish by original code
	    else {
			retryChunk(ApolloStateTypes::Original, MaxBound - Lower);
		}
	}

	// Updates upper
	Upper = std::min<long>(Upper, MaxBound);

	// If APOLLO is not ran with optimized support
	if (!EnableOptimized && CurState == ApolloStateTypes::Optimized)
		CurState = ApolloStateTypes::Original;

	// Dumps next state
	std::stringstream Output;
	this->dump(Output);
	traceExit(
			backdoorutils::concat("SwitchingState::nextState= ", Output.str()));
	waitForUser();

	// check for consistency
	assert(Lower <= Upper);
	assert(Upper <= MaxBound + 1);
	return CurState;
}
