//===--- JitCompiler.cpp --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "JitCompiler/JitCompiler.h"
#include "JitCompiler/CreatePasses.h"
#include "Backdoor/Backdoor.h"

#include "llvm-c/Core.h"
#include "llvm/Bitcode/BitcodeReader.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/Analysis/Passes.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "llvm/ExecutionEngine/MCJIT.h"
#include "llvm/Transforms/IPO.h"
#include "llvm/Transforms/IPO/AlwaysInliner.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Vectorize.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/InitializePasses.h"
#include "llvm/Analysis/BasicAliasAnalysis.h"
#include "llvm/Analysis/CFLAndersAliasAnalysis.h"
#include "llvm/Analysis/ScopedNoAliasAA.h"
#include "llvm/Analysis/TypeBasedAliasAnalysis.h"
#include "llvm/Analysis/TargetTransformInfo.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/ErrorOr.h"

#include <sys/time.h>
#include <regex>

const char *JitPrefix = "skeleton_0";

using namespace llvm;
using namespace apollo;

namespace {

class InitializeLLVMPasses {
public:
  InitializeLLVMPasses() {
    llvm::PassRegistry &PR = *llvm::PassRegistry::getPassRegistry();
    llvm::initializeCore(PR);
    llvm::initializeTransformUtils(PR);
    llvm::initializeScalarOpts(PR);
    llvm::initializeVectorization(PR);
    llvm::initializeIPO(PR);
    llvm::initializeAnalysis(PR);
    llvm::initializeCodeGen(PR);
    llvm::initializeTarget(PR);
  }
};

InitializeLLVMPasses initialize;

void registerO3Passes(legacy::PassManager &MPM, bool Aggressive) {
  MPM.add(createVerifierPass());
  if (Aggressive) {
    MPM.add(createIPSCCPPass());
    MPM.add(createGlobalOptimizerPass());
    MPM.add(createDeadArgEliminationPass());
  }

  MPM.add(createInstructionCombiningPass());
  MPM.add(createCFGSimplificationPass());
  MPM.add(createPruneEHPass());
  MPM.add(createArgumentPromotionPass());
  // MPM.add(createScalarReplAggregatesPass(-1, true));
  MPM.add(createEarlyCSEPass());
  MPM.add(createJumpThreadingPass());
  MPM.add(createCorrelatedValuePropagationPass());
  MPM.add(createCFGSimplificationPass());
  MPM.add(createInstructionCombiningPass());

  if (Aggressive) {
    MPM.add(createTailCallEliminationPass());
    MPM.add(createCFGSimplificationPass());
    MPM.add(createReassociatePass());
  }

  MPM.add(createLoopSimplifyPass());

  if (Aggressive)
    MPM.add(createLCSSAPass());

  MPM.add(createLoopRotatePass());
  MPM.add(createLICMPass());

  if (Aggressive) {
    MPM.add(createLoopUnswitchPass(false));
    MPM.add(createInstructionCombiningPass());
    MPM.add(createLoopSimplifyPass());
    MPM.add(createLCSSAPass());
    MPM.add(createIndVarSimplifyPass());
    MPM.add(createLoopIdiomPass());
    MPM.add(createLoopDeletionPass());
    MPM.add(createSimpleLoopUnrollPass());
    MPM.add(createMergedLoadStoreMotionPass());
    MPM.add(createGVNPass());
    MPM.add(createMemCpyOptPass());
    MPM.add(createSCCPPass());
    MPM.add(createInstructionCombiningPass());
    MPM.add(createJumpThreadingPass());
    MPM.add(createCorrelatedValuePropagationPass());
    MPM.add(createDeadStoreEliminationPass());
    MPM.add(createAggressiveDCEPass());
    MPM.add(createCFGSimplificationPass());
    MPM.add(createInstructionCombiningPass());
    MPM.add(createBarrierNoopPass());
    MPM.add(createLoopSimplifyPass());
    MPM.add(createLCSSAPass());
    MPM.add(createPromoteMemoryToRegisterPass());
    MPM.add(createLoopVectorizePass(false, true));
    MPM.add(createInstructionCombiningPass());
    MPM.add(createCFGSimplificationPass());
    MPM.add(createInstructionCombiningPass());
    MPM.add(createLoopSimplifyPass());
    MPM.add(createLCSSAPass());
    MPM.add(createLoopUnrollPass(-1, -1, 1, 1));
  }

  MPM.add(createInstructionCombiningPass());
  MPM.add(createLoopVectorizePass(false, false));
  MPM.add(createSLPVectorizerPass());
  MPM.add(createCFGSimplificationPass());
  MPM.add(createInstructionCombiningPass());
  MPM.add(createAlignmentFromAssumptionsPass());
  MPM.add(createGlobalDCEPass());
  MPM.add(createConstantMergePass());
  MPM.add(createCFGSimplificationPass());

  // Only for debug.
  MPM.add(createVerifierPass());
}

void registerAliasAnalysis(legacy::PassManager &MPM) {
  // Alias analysis passes, including the apollo pass.
  MPM.add(createCFLAndersAAWrapperPass());
  MPM.add(createTypeBasedAAWrapperPass());
  MPM.add(createScopedNoAliasAAWrapperPass());
}

} // end anonymous namespace

namespace jit {

std::unique_ptr<llvm::Module> loadModule(char *Str, const size_t Size) {
  traceEnter("loadModule()");
  llvm::StringRef BytecodeAsString(Str, Size - 1);
  llvm::LLVMContext &Context = *unwrap(LLVMGetGlobalContext());
  std::unique_ptr<llvm::MemoryBuffer> MemoryBuffer(
      llvm::MemoryBuffer::getMemBuffer(BytecodeAsString));
  llvm::Expected<std::unique_ptr<llvm::Module>> ModuleOrErr =
      llvm::parseBitcodeFile(MemoryBuffer.get()->getMemBufferRef(), Context);

  if (!ModuleOrErr) {
    errorToErrorCodeAndEmitErrors(Context, ModuleOrErr.takeError());
    assert(false && "loading the module failed.");
  }

  traceExit("loadModule()");
  return std::move(*ModuleOrErr);
}

std::unique_ptr<llvm::Module>
getPatchedModule(std::unique_ptr<llvm::Module> &Original,
                 NestParamsStruc &Params,
                 std::shared_ptr<PredictionModel> &PM) {

  traceEnter("getPatchedModule()");

  assert(Original);
  std::unique_ptr<llvm::Module> cloned(CloneModule(Original.get()));
  std::unique_ptr<llvm::legacy::PassManager> pass_manager(
      new llvm::legacy::PassManager());

  // these passes replace known value by constants
  pass_manager->add(createPatchLoopParamsPass(Params));

  // this pass replaces all calls to get_coefficient
  // method by the constant value they return.
  // we can do this now because coefficient are known
  // thank to the instrumentation stuff
  pass_manager->add(createPatchLoopCoefsPass(PM.get()));

  // these passes optimize
  pass_manager->add(createSCCPPass());
  pass_manager->add(createInstructionCombiningPass());
  pass_manager->add(createJumpThreadingPass());
  pass_manager->add(createCorrelatedValuePropagationPass());
  pass_manager->add(createAggressiveDCEPass());
  pass_manager->add(createCFGSimplificationPass());
  pass_manager->add(createConstantInlinePass());

  traceEnter("pass_manager->run()");
  pass_manager->run(*cloned.get());
  traceExit("pass_manager->run()");

  traceExit("getPatchedModule()");

  return cloned;
}

std::unique_ptr<llvm::ExecutionEngine>
getExecutionEngine(std::unique_ptr<llvm::Module> &Patched,
                   std::shared_ptr<ScanTypeStruct> &Scan) {

  traceEnter("get_execution_engine()");
  assert(Scan);
  assert(Patched);

  // used by the pass_manager later, if not don't have any way to reference
  // it again.
  Module *PatchedPtr = Patched.get();
  EngineBuilder ebuilder(std::move(Patched));
  std::string eeError;
  std::unique_ptr<llvm::ExecutionEngine> EE(
      ebuilder.setErrorStr(&eeError)
          .setMCPU(sys::getHostCPUName())
          .setEngineKind(EngineKind::JIT)
          .setOptLevel(llvm::CodeGenOpt::Level::Aggressive)
          .create());

  TargetMachine *TM = EE->getTargetMachine();

  // register and run the passes.
  std::unique_ptr<llvm::legacy::PassManager> passManager(
      new llvm::legacy::PassManager());

  if (TM) {
    PatchedPtr->setDataLayout(EE->getTargetMachine()->createDataLayout());
    TM->setOptLevel(llvm::CodeGenOpt::Aggressive);
    passManager->add(
        createTargetTransformInfoWrapperPass(TM->getTargetIRAnalysis()));
  }

  registerAliasAnalysis(*passManager);

  passManager->add(createCodeBonesComposedPass(Scan.get()));
  passManager->add(createPromoteMemoryToRegisterPass());
  passManager->add(createCodeBonesCodeGenerationPass(Scan.get(), true, true));
  passManager->add(createAlwaysInlinerLegacyPass());
  passManager->add(createCodeBonesMemoryIndependantPass());

  registerO3Passes(*passManager, false);

  // pass_manager->add(create_print_ir_pass("", "/tmp/jit_result"));

  traceEnter("pass_manager->run()");
  passManager->run(*PatchedPtr);
  traceExit("pass_manager->run()");

  traceExit("get_execution_engine()");
  return EE;
}

std::pair<void *, void *>
getFunctionPtr(std::unique_ptr<llvm::ExecutionEngine> &EE) {
  assert(EE);
  std::pair<void *, void *> FunctionPointers;
  FunctionPointers.first = (void *)EE->getFunctionAddress("apollo_loop_verif");
  FunctionPointers.second = (void *)EE->getFunctionAddress("apollo_loop");
  return FunctionPointers;
}

} // end namespace jit
