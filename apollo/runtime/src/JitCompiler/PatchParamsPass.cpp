//===--- PatchParamsPass.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backdoor/Backdoor.h"
#include "JitCompiler/CreatePasses.h"

#include "llvm/Pass.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h"

#include <cstdint>

using namespace llvm;
using namespace apollo;

struct PatchLoopParamsPass : public FunctionPass {
  static char ID;
  char *Params;
  size_t Size;

  PatchLoopParamsPass() : FunctionPass(ID) {
    this->Params = nullptr;
    this->Size = 0;
  }

  PatchLoopParamsPass(ParamTy Params_, size_t Size_) : FunctionPass(ID) {
    this->Params = (char *)Params_;
    this->Size = Size_;
  }

  void getAnalysisUsage(AnalysisUsage &AU) const {}
  bool runOnFunction(Function &F);
};

char PatchLoopParamsPass::ID = 0;
static RegisterPass<PatchLoopParamsPass>
    X("Apollo_JIT_params", "Apollo_JIT_params Pass", false, false);

/// \brief hasConstantOffsetGep returns true if the gep indices are constant
/// \param gep a gep instruction
/// \return true if the geo indices are constant
static bool hasConstantOffsetGep(GetElementPtrInst *Gep) {
  for (auto idx = Gep->idx_begin(), en = Gep->idx_end(); idx != en; ++idx) {
    if (!isa<ConstantInt>(*idx))
      return false;
  }
  return true;
}

/// \brief getGepIndexes returns the gep indices in a vector
/// \param gep a gep with constant offsets
/// \return a vector with the indices
static std::vector<unsigned> getGepIndexes(GetElementPtrInst *Gep) {
  assert(hasConstantOffsetGep(Gep));
  std::vector<unsigned> Idxs;
  for (auto idx = Gep->idx_begin(), en = Gep->idx_end(); idx != en; ++idx) {
    ConstantInt *Const = cast<ConstantInt>(*idx);
    Idxs.push_back(Const->getSExtValue());
  }
  return Idxs;
}

/// \brief getGepLoads return all the loads that use a gep
/// \param gep a gep
/// \return a vector of loadinst
static std::vector<LoadInst *> getGepLoads(GetElementPtrInst *Gep) {
  std::vector<LoadInst *> Loads;
  for (User *user : Gep->users()) {
	if (!isa<LoadInst>(user)) {
		continue;
	}
    LoadInst *Load = dyn_cast<LoadInst>(user);
    assert(Load);
    Loads.push_back(Load);
  }
  return Loads;
}

/// \brief Create the pass for patching the param values in the jit
/// \param params
/// \return The pass
llvm::FunctionPass *apollo::createPatchLoopParamsPass(NestParamsStruc Params) {
  return new PatchLoopParamsPass(Params.Param, Params.SizeInBytes);
}

llvm::Value *getArgumentByName(Function *F, const std::string &Name) {
  for (auto arg = F->arg_begin(); arg != F->arg_end(); ++arg) {
    if (arg->getName().compare(Name) == 0)
      return (&*arg);
  }
  assert(false);
  return nullptr;
}

bool PatchLoopParamsPass::runOnFunction(llvm::Function &F) {
  traceEnter(backdoorutils::concat("PatchLoopParamsPass::runOnFunction ", F.getName().str()));
  bool modified = false;
  if (this->Params && this->Size) {
    llvm::Value *ParamsArg = getArgumentByName(&F, "param");
    if (ParamsArg) {
      LLVMContext &Context = F.getContext();
      const DataLayout &DL = F.getParent()->getDataLayout();
      StructType *ParamsType =
          cast<StructType>(ParamsArg->getType()->getContainedType(0));
      const StructLayout *ParamsLayout = DL.getStructLayout(ParamsType);
      Type *i64Ty = Type::getInt64Ty(Context);
      Type *i32Ty = Type::getInt32Ty(Context);
      Type *i16Ty = Type::getInt16Ty(Context);
      Type *i8Ty = Type::getInt8Ty(Context);
      // collect all the uses.
      std::vector<User *> Users;
      for (User *user : ParamsArg->users()) {
        Users.push_back(user);
      }
      // scan the uses looking for gep.
      for (User *user : Users) {
        GetElementPtrInst *Gep = dyn_cast<GetElementPtrInst>(user);
        if (Gep) {
          const bool ConstantIdxs = hasConstantOffsetGep(Gep);
          // the indexes must be constants
          if (ConstantIdxs) {
            std::vector<LoadInst *> Loads = std::move(getGepLoads(Gep));
            std::vector<unsigned> Indexes = std::move(getGepIndexes(Gep));
            Type *GepTy = Gep->getType()->getContainedType(0);
            // the first index must be 0, and the second one the offset
            if (Indexes[0] == 0 && Indexes.size() == 2 && !Loads.empty()) {
              unsigned Offset = ParamsLayout->getElementOffset(Indexes[1]);
              Constant *ToReplace = nullptr;
              // switch between the types, for floats there is no big
              // difference.
              if (GepTy->isPointerTy()) {
                long *DataPtr = (long *)(this->Params + Offset);
                long Ptr = *DataPtr;
                Constant *AsInt = ConstantInt::get(i64Ty, Ptr);
                ToReplace = ConstantExpr::getIntToPtr(AsInt, GepTy);
              } else if (GepTy->isIntegerTy()) {
                // only handle the most typicall cases.
                if (GepTy == i64Ty)
                  ToReplace = ConstantInt::get(
                      i64Ty, *(int64_t *)(this->Params + Offset));
                else if (GepTy == i32Ty)
                  ToReplace = ConstantInt::get(
                      i32Ty, *(int32_t *)(this->Params + Offset));
                else if (GepTy == i16Ty)
                  ToReplace = ConstantInt::get(
                      i64Ty, *(int16_t *)(this->Params + Offset));
                else if (GepTy == i8Ty)
                  ToReplace = ConstantInt::get(
                      i64Ty, *(int8_t *)(this->Params + Offset));
              }

              if (ToReplace) {
                for (LoadInst *load : Loads) {
                  load->replaceAllUsesWith(ToReplace);
                  modified = true;
                }
              }
            }
          }
        }
      }
    }
  }
  traceExit(backdoorutils::concat("PatchLoopParamsPass::runOnFunction ",
		  F.getName().str(), " ", (modified ? "true" : "false")));
  return modified;
}
