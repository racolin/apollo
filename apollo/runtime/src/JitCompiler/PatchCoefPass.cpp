//===--- PatchCoefPass.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backdoor/Backdoor.h"
#include "Interpolation/Interpolation.h"
#include "JitCompiler/PassUtils.h"
#include "JitCompiler/CreatePasses.h"

#include "llvm/Pass.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Bitcode/BitcodeReader.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO.h"

using namespace llvm;
using namespace apollo;

namespace apollo {
struct PatchLoopCoefsPass : public ModulePass {
  static char ID;
  PredictionModel *PM;
  PatchLoopCoefsPass() : ModulePass(ID), PM(0x0) {}
  PatchLoopCoefsPass(PredictionModel *PM_) : ModulePass(ID), PM(PM_) {}

  bool runOnModule(Module &M);
};

} // end namespace apollo

char PatchLoopCoefsPass::ID = 0;

Pass *apollo::createPatchLoopCoefsPass(PredictionModel *PM) {
  return new PatchLoopCoefsPass(PM);
}

bool PatchLoopCoefsPass::runOnModule(Module &M) {
  traceEnter("PatchLoopCoefsPass::runOnModule ");
  bool modified = false;
  Function *GetCoefFun = M.getFunction("get_coefficient");
  if (GetCoefFun) {
    // collect uses
    std::vector<CallInst *> Calls;
    for (User *use : GetCoefFun->users()) {
      if (isa<CallInst>(use))
        Calls.push_back(cast<CallInst>(use));
    }
    // patch
    for (CallInst *call : Calls) {
      ConstantInt *Type = dyn_cast<ConstantInt>(call->getArgOperand(1));
      ConstantInt *Id = dyn_cast<ConstantInt>(call->getArgOperand(2));
      ConstantInt *Num = dyn_cast<ConstantInt>(call->getArgOperand(3));
      if (Type && Id && Num) {
        const long Coefficient = get_coefficient(PM, Type->getSExtValue(), 
                                                 Id->getSExtValue(), 
                                                 Num->getSExtValue());
        Constant *PatchWith = ConstantInt::get(call->getType(), Coefficient);
        call->replaceAllUsesWith(PatchWith);
        call->eraseFromParent();
        modified = true;
      }
    }
  }
  traceExit(backdoorutils::concat("PatchLoopCoefsPass::runOnModule "
		  , (modified ? "true" : "false")));
  return modified;
}
