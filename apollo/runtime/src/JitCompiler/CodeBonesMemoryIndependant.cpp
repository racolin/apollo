//===--- CodeBonesMemoryIndependent.cpp -----------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backdoor/Backdoor.h"
#include "JitCompiler/PassUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Pass.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopInfo.h"

using namespace llvm;

// Pass to mark loops as vectorizable.
struct CodeBonesMemoryIndependant : public LoopPass {
  static char ID;
  CodeBonesMemoryIndependant() : LoopPass(ID) {}
  virtual bool runOnLoop(Loop *L, LPPassManager &LPM);
};

bool CodeBonesMemoryIndependant::runOnLoop(Loop *L, LPPassManager &LPM) {
  traceEnter("CodeBonesMemoryIndependant::runOnLoop");
  BasicBlock *Latch = L->getLoopLatch();
  if (Latch) {
    TerminatorInst *LatchTerm = Latch->getTerminator();
    MDNode *LoopMD = LatchTerm->getMetadata("llvm.loop");
    if (LoopMD) {
      LLVMContext &Context = LatchTerm->getContext();
      unsigned MemParallelMD =
          Context.getMDKindID("llvm.mem.parallel_loop_access");
      for (BasicBlock *block : L->getBlocks()) {
        for (Instruction &inst : *block) {
          if (isStatement(&inst)) {
            inst.setMetadata(MemParallelMD, {LoopMD});
          }
        }
      }
    }
  }
  traceExit("CodeBonesMemoryIndependant::runOnLoop");
  return true;
}

char CodeBonesMemoryIndependant::ID = 0;

namespace apollo {
Pass *createCodeBonesMemoryIndependantPass() {
  return new CodeBonesMemoryIndependant();
}
} // end namespace apollo
