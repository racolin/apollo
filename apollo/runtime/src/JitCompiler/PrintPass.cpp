//===--- PrintPass.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "JitCompiler/CreatePasses.h"
#include "JitCompiler/PassUtils.h"

#include "llvm/Pass.h"
#include "llvm/Bitcode/BitcodeReader.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Analysis/CFGPrinter.h"

#include <string>

using namespace llvm;

namespace llvm {

struct PrintIRPass : public ModulePass {
  static char ID;
  bool CFG;
  bool Dump;

  bool PrintTxt;
  std::string Txt;

  bool ToFile;
  std::string File;

  PrintIRPass() : ModulePass(ID) {}
  void getAnalysisUsage(AnalysisUsage &AU) { AU.setPreservesAll(); }
  /// \brief Create pass for dumping information
  /// \param Txt Text to print when apply this pass
  /// \param CFG Did we show the CFG?
  /// \param Dump Did we dump the module?
  /// \param File The file to dump the entire module
  /// \return The pass
  PrintIRPass(std::string Txt, bool CFG, bool Dump, std::string File)
  : ModulePass(ID) {
      if (Txt.size() != 0)
        this->PrintTxt = true;
      else
        this->PrintTxt = false;
      this->Txt = Txt;
      this->CFG = CFG;
      this->Dump = Dump;

      if (File.size() != 0)
        this->ToFile = true;
      else
        this->ToFile = false;
      this->File = File;
  }
  virtual bool runOnModule(llvm::Module &);
};

} // end namespace llvm

char PrintIRPass::ID = 0;

/// \brief Create a pass to write the entire module to a file.
/// \param File The file where you want to print the module
/// \param Txt Text to print when apply this pass
/// \return The pass
ModulePass *apollo::createPrintIRPass(std::string File, std::string Txt) {
  return new PrintIRPass(Txt, false, false, File);
}

bool PrintIRPass::runOnModule(Module &M) {
  static int Times = 0;
  for (Function &F : M) {
    if (PrintTxt)
      errs() << "print_ir_pass: " << F.getName() << "\n";
    if (CFG)
      F.viewCFG();
    if (Dump)
      F.dump();
  }

  std::error_code errore;
  std::string sfde_name = this->File + std::to_string(Times) + ".ll";
  raw_fd_ostream sfde(sfde_name, errore, llvm::sys::fs::OpenFlags::F_None);
  if (errore) {
    errs() << "Error while opening raw_fd_ofstream: " << errore.message()
           << "\n";
    assert(errore && "Error while opening raw_fd_ofstream.");
  }

  M.print(sfde, nullptr);
  sfde.flush();
  sfde.close();
  Times++;

  return false;
}
