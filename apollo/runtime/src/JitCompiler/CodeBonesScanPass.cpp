//===--- CodeBonesScanPass.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

// cloog
#define CLOOG_INT_GMP 1
#include "cloog/cloog.h"
#include "cloog/clast.h"

#include "CodeBones/CodeBones.h"
#include "JitCompiler/PassUtils.h"
#include "Backdoor/Backdoor.h"

#include "llvm/Pass.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"

#include <map>
#include <vector>
#include <string>
#include <limits>

#if (CLOOG_INT_GMP == 1)
#define cloog_int_get_si(expr) (mpz_get_si(expr))
#else
#define cloog_int_get_si(expr) (expr)
#endif

using namespace llvm;
using namespace apollo;

struct LoopCFG {
  BasicBlock *Preheader;
  BasicBlock *Header;
  BasicBlock *Inc;
  BasicBlock *Body;
  Value *Iterator;
};

using SymbolsTy = std::map<std::string, Value *>;

struct CodeBonesCodeGeneration : public ModulePass {
  static char ID;
  bool Computation;
  bool Verification;

  ScanTypeStruct *Scan;
  Value *Two;
  Value *One;
  Value *Zero;
  Value *True;
  Type *boolTy;
  Type *i64Ty;
  Type *voidTy;

  int Idx; // 0 verif, 1 computation
  std::set<std::pair<std::string, std::set<int>>> Parallel[2];
  std::set<std::pair<std::string, std::set<int>>> Vectorize[2];
  std::map<std::string, std::pair<bool, int>> Unroll[2];
  std::map<int, std::string> StmtIdToSymbolName[2];

  CodeBonesCodeGeneration() : ModulePass(ID) {}
  void getAnalysisUsage(AnalysisUsage &AU) {}

  CodeBonesCodeGeneration(ScanTypeStruct *Scan_, bool Comp_, bool Verif_)
  : ModulePass(ID) {
      Scan = Scan_;
      this->Computation = Comp_;
      this->Verification = Verif_;
  }

  virtual bool runOnModule(Module &M);

private:
  bool isLoopParallel(struct clast_for *);
  bool isLoopVectorize(struct clast_for *);

  BasicBlock *visitClast(SymbolsTy &ST, BasicBlock *BB, struct clast_stmt *,
                         Value *, bool);

  Value *getReductionAcumulation(BasicBlock *InsertAt, Value *Acum, Value *Val,
                                 clast_red_type Ty);
  Value *getBinaryOperation(BasicBlock *InsertAt, Value *LHS, Value *RHS,
                            clast_bin_type Ty);
  Value *visitClastExpr(SymbolsTy &ST, BasicBlock *BB, struct clast_expr *);

  BasicBlock *visitClastGuard(SymbolsTy &ST, BasicBlock *BB,
                              struct clast_guard *, Value *Verif, bool);

  Value *visitClastAssig(SymbolsTy &ST, BasicBlock *BB,
                         struct clast_assignment *);

  void visitClastUser(SymbolsTy &ST, BasicBlock *BB, struct clast_user_stmt *,
                      Value *V);

  LoopCFG createLoop(std::string Iter, Value *Lower, Value *Upper, Value *Step,
                     BasicBlock *Entry, BasicBlock *Exit);

  BasicBlock *visitClastSequentialFor(SymbolsTy &ST, BasicBlock *BB,
                                      struct clast_for *, Value *, bool);
  BasicBlock *visitClastUnrollFor(SymbolsTy &ST, BasicBlock *BB,
                                  struct clast_for *, int, Value *, bool);
  BasicBlock *visitClastParallelFor(SymbolsTy &ST, BasicBlock *BB,
                                    struct clast_for *);
  BasicBlock *visitClastFor(SymbolsTy &ST, BasicBlock *BB, struct clast_for *,
                            Value *V, bool);

  Function *defineParallelFun(Module *M);
  StructType *getSymbolsStruct(SymbolsTy &ST);
  Function *createBonesParallelFunction(Module *M, SymbolsTy &UsedSymbols,
                                        struct clast_for *ForLoop);

  bool hasStatementsOfType(struct clast_stmt *,
                           bool /*computation=true or verification=false*/);

  std::map<std::string, Function *> getBones(Module &M);
  Function *getSkeletonDeclaration(Module &M, Function *F, std::string suffix);
  void alwaysInlineBones(std::map<std::string, Function *> &Bones);
  Function *getRollbackFunction(Module *M);
};

char CodeBonesCodeGeneration::ID = 0;

Function *CodeBonesCodeGeneration::getRollbackFunction(Module *M) {
  Function *Rollback = M->getFunction("apollo_run_rollback");
  if (!Rollback) {
    FunctionType *RolTy = FunctionType::get(voidTy, {i64Ty}, false);
    Rollback = Function::Create(RolTy, Function::ExternalLinkage,
                                "apollo_run_rollback", M);
  }
  return Rollback;
}

Value *CodeBonesCodeGeneration::getReductionAcumulation(BasicBlock *InsertAt,
                                                        Value *Acum, Value *Val,
                                                        clast_red_type Ty) {

  TerminatorInst *InsertBefore = InsertAt->getTerminator();
  if (Ty == clast_red_sum)
    return BinaryOperator::CreateAdd(Acum, Val, "", InsertBefore);

  ICmpInst::Predicate Predicate =
      (Ty == clast_red_min) ? ICmpInst::ICMP_SLT : ICmpInst::ICMP_SGT;
  ICmpInst *Cmp = new ICmpInst(InsertBefore, Predicate, Acum, Val, "");
  const char *Name = (Ty == clast_red_min) ? "min" : "max";
  Value *Select = SelectInst::Create(Cmp, Acum, Val, Name, InsertBefore);
  return Select;
}

Value *CodeBonesCodeGeneration::getBinaryOperation(BasicBlock *InsertAt,
                                                   Value *LHS, Value *RHS,
                                                   clast_bin_type Ty) {
  IRBuilder<> Builder(InsertAt->getTerminator());
  if (Ty == clast_bin_div)
    return Builder.CreateSDiv(LHS, RHS);
  if (Ty == clast_bin_mod)
    return Builder.CreateSRem(LHS, RHS);

  Value *Div = Builder.CreateSDiv(LHS, RHS);
  Value *Rem = Builder.CreateSRem(LHS, RHS);
  Value *IsNegative = Builder.CreateXor(Builder.CreateICmpSLT(LHS, Zero),
                                        Builder.CreateICmpSLT(RHS, Zero));
  Value *Cmp = Builder.CreateICmpNE(Rem, Zero);

  if (Ty == clast_bin_cdiv) {
    Value *CmpCast = Builder.CreateZExt(
        Builder.CreateAnd(Cmp, Builder.CreateNot(IsNegative)), Div->getType());
    return Builder.CreateAdd(Div, CmpCast, "ceild");
  }
  if (Ty == clast_bin_fdiv) {
    Value *CmpCast =
        Builder.CreateZExt(Builder.CreateAnd(Cmp, IsNegative), Div->getType());
    return Builder.CreateSub(Div, CmpCast, "floord");
  }
  assert(false);
  return nullptr;
}

Value *CodeBonesCodeGeneration::visitClastExpr(SymbolsTy &Symbols,
                                               BasicBlock *InsertAt,
                                               struct clast_expr *Expr) {
  assert(Expr);
  switch (Expr->type) {
  case clast_expr_name: {
    struct clast_name *AsName = (struct clast_name *)Expr;
    if (!Symbols.count(AsName->name)) {
      std::cerr << "CodeBonesCodeGeneration::visit_clast_expr: symbol \""
              << AsName->name << "\" not defined in clast!\n";
      return nullptr;
    }
    return Symbols[AsName->name];
  }
  case clast_expr_term: {
    struct clast_term *AsTerm = (struct clast_term *)Expr;
    long Value = cloog_int_get_si(AsTerm->val);
    llvm::Value *Val = ConstantInt::get(i64Ty, Value);

    if (!AsTerm->var)
      return Val;
    llvm::Value *Var = visitClastExpr(Symbols, InsertAt, AsTerm->var);
    if (Value == 1)
      return Var;
    return BinaryOperator::CreateMul(Val, Var, "", InsertAt->getTerminator());
  }
  case clast_expr_bin: {
    struct clast_binary *AsBin = (struct clast_binary *)Expr;
    Value *LHS = visitClastExpr(Symbols, InsertAt, AsBin->LHS);
    Value *RHS = ConstantInt::get(i64Ty, cloog_int_get_si(AsBin->RHS));
    return getBinaryOperation(InsertAt, LHS, RHS, AsBin->type);
  }
  case clast_expr_red: {
    struct clast_reduction *AsRed = (struct clast_reduction *)Expr;
    assert(AsRed->n > 0);
    llvm::Value *Acum = visitClastExpr(Symbols, InsertAt, AsRed->elts[0]);
    for (int i = 1; i < AsRed->n; ++i) {
      llvm::Value *Val = visitClastExpr(Symbols, InsertAt, AsRed->elts[i]);
      Acum = getReductionAcumulation(InsertAt, Acum, Val, AsRed->type);
    }
    return Acum;
  }
  }
  std::cerr << "Unreacheable. expr->type = " << Expr->type << "\n";
  assert(false && "unknown expression type");
  return nullptr;
}

LoopCFG
CodeBonesCodeGeneration::createLoop(std::string Iter, llvm::Value *Lower,
                                    llvm::Value *Upper, llvm::Value *Step,
                                    BasicBlock *Entry, BasicBlock *Exit) {
  LoopCFG Loop;
  LLVMContext &Context = Entry->getContext();
  Function *Parent = Entry->getParent();
  BasicBlock *Preheader = BasicBlock::Create(
      Context, concat("for.", Iter, ".preheader"), Parent, Exit);
  BasicBlock *Header = BasicBlock::Create(
      Context, concat("for.", Iter, ".header"), Parent, Exit);
  BasicBlock *Inc =
      BasicBlock::Create(Context, concat("for.", Iter, ".inc"), Parent, Exit);
  BasicBlock *Body =
      BasicBlock::Create(Context, concat("for.", Iter, ".body"), Parent, Exit);
  Loop.Preheader = Preheader;
  Loop.Header = Header;
  Loop.Inc = Inc;
  Loop.Body = Body;
  Entry->getTerminator()->replaceUsesOfWith(Exit, Preheader);
  BranchInst::Create(Header, Preheader);
  BranchInst::Create(Header, Inc);
  BranchInst::Create(Inc, Body);
  // Create the iterator.
  PHINode *VirtualIterator =
      PHINode::Create(i64Ty, 2, concat("iterator.", Iter), Header);
  VirtualIterator->addIncoming(Lower, Preheader);
  // Create the step
  llvm::Value *IteratorInc = BinaryOperator::CreateAdd(
      VirtualIterator, Step, concat("iterator.", Iter, ".inc"),
      Inc->getTerminator());
  VirtualIterator->addIncoming(IteratorInc, Inc);
  Loop.Iterator = VirtualIterator;
  // Create conditions.
  llvm::Value *Cmp =
      new ICmpInst(*Header, ICmpInst::ICMP_SLT, VirtualIterator, Upper);
  BranchInst::Create(Body, Exit, Cmp, Header);
  return Loop;
}

BasicBlock *CodeBonesCodeGeneration::visitClastSequentialFor(SymbolsTy &Symbols, 
                                                      BasicBlock *ParentBlock, 
                                                      struct clast_for *ForLoop,
                                                      llvm::Value *Verif, 
                                                      bool ParallelParent) {

  BasicBlock *ParentPre = ParentBlock;
  BasicBlock *ParentPost = ParentBlock->splitBasicBlock(
      ParentBlock->getTerminator(), ParentBlock->getName() + ".c");
  long Stride = cloog_int_get_si(ForLoop->stride);
  // obtain the lower and upper bounds*/
  llvm::Value *Lower = visitClastExpr(Symbols, ParentPre, ForLoop->LB);
  llvm::Value *Upper = visitClastExpr(Symbols, ParentPre, ForLoop->UB);
  llvm::Value *Step = ConstantInt::get(i64Ty, Stride);
  llvm::Value *UpperPlusOne =
      BinaryOperator::CreateAdd(Upper, One, "", ParentPre->getTerminator());
  LoopCFG Loop = createLoop(ForLoop->iterator, Lower, UpperPlusOne, Step,
                            ParentPre, ParentPost);
  // Visit the body
  SymbolsTy SymbolsCopy = Symbols;
  SymbolsCopy[ForLoop->iterator] = Loop.Iterator;
  visitClast(SymbolsCopy, Loop.Body, ForLoop->body, Verif, ParallelParent);
  bool IsVectorizable = isLoopVectorize(ForLoop);

  if (IsVectorizable) {
    LLVMContext &Context = ParentBlock->getContext();
    unsigned LoopMD = Context.getMDKindID("llvm.loop");
    TerminatorInst *LoopBackedge = Loop.Inc->getTerminator();
    MDNode *LoopMDNode =
        MDNode::get(Context, std::vector<llvm::Metadata *>({nullptr}));
    LoopMDNode->replaceOperandWith(0, LoopMDNode);
    LoopBackedge->setMetadata(LoopMD, LoopMDNode);
  }
  return ParentPost;
}

BasicBlock *CodeBonesCodeGeneration::visitClastUnrollFor(SymbolsTy &Symbols, 
                                                      BasicBlock *ParentBlock, 
                                                      struct clast_for *ForLoop,
                                                      int UnrollFactor, 
                                                      llvm::Value *Verif, 
                                                      bool ParallelParent) {

  Function *Parent = ParentBlock->getParent();
  LLVMContext &Context = Parent->getContext();
  BasicBlock *ParentPre = ParentBlock;
  BasicBlock *ParentPost = ParentBlock->splitBasicBlock(
      ParentBlock->getTerminator(), ParentBlock->getName() + ".c");
  BasicBlock *PreUnroll =
      BasicBlock::Create(Context, concat("for.", ForLoop->iterator, ".unroll"),
                         Parent, ParentPost);
  ParentPost->replaceAllUsesWith(PreUnroll);
  BranchInst::Create(ParentPost, PreUnroll);
  llvm::Value *UnrollFactorConstant = ConstantInt::get(i64Ty, UnrollFactor);
  long Stride = cloog_int_get_si(ForLoop->stride);
  // obtain the lower and upper bounds*/
  llvm::Value *Lower = visitClastExpr(Symbols, ParentPre, ForLoop->LB);
  llvm::Value *Upper = visitClastExpr(Symbols, ParentPre, ForLoop->UB);
  llvm::Value *Step = ConstantInt::get(i64Ty, Stride);
  llvm::Value *UnrollStep = ConstantInt::get(i64Ty, Stride * UnrollFactor);
  IRBuilder<> Builder(ParentPre->getTerminator());
  llvm::Value *UpperPlusOne = Builder.CreateAdd(Upper, One);
  llvm::Value *UpperMinusLower = Builder.CreateSub(UpperPlusOne, Lower);
  llvm::Value *Reminder =
      Builder.CreateSRem(UpperMinusLower, UnrollFactorConstant);
  llvm::Value *UnrolledBound = Builder.CreateSub(UpperMinusLower, Reminder);
  LoopCFG L = createLoop(ForLoop->iterator, Zero, UnrolledBound, UnrollStep,
                         ParentPre, PreUnroll);
  Builder.SetInsertPoint(L.Header->getTerminator());

  // Visit the body
  for (int u = 0; u < UnrollFactor; ++u) {
    llvm::Value *UConstInt = ConstantInt::get(i64Ty, u);
    SymbolsTy SymbolsCopy = Symbols;
    SymbolsCopy[ForLoop->iterator] =
        Builder.CreateAdd(Builder.CreateAdd(L.Iterator, Lower), UConstInt);
    BasicBlock *ContinueAt =
        visitClast(SymbolsCopy, L.Body, ForLoop->body, Verif, ParallelParent);
    L.Body = ContinueAt;
  }

  // build the unroll loop
  LoopCFG UnrollLoop = createLoop(concat(ForLoop->iterator, ".unroll"),
                                  Builder.CreateAdd(UnrolledBound, Lower),
                                  UpperPlusOne, Step, PreUnroll, ParentPost);
  SymbolsTy SymbolsCopy = Symbols;
  SymbolsCopy[ForLoop->iterator] = UnrollLoop.Iterator;
  visitClast(SymbolsCopy, UnrollLoop.Body, ForLoop->body, Verif,
             ParallelParent);
  return ParentPost;
}

Function *CodeBonesCodeGeneration::defineParallelFun(Module *M) {
  Function *F = M->getFunction("apollo_run_code_bones_dispatcher");
  if (!F) {
    LLVMContext &Context = M->getContext();
    Type *voidPtr = Type::getInt8PtrTy(Context);
    // function, symbols, lower, upper, step
    std::vector<Type *> Arguments = {voidPtr, voidPtr, i64Ty, i64Ty, i64Ty};
    FunctionType *FunTy = FunctionType::get(voidTy, Arguments, false);
    return Function::Create(FunTy, Function::ExternalLinkage,
                            "apollo_run_code_bones_dispatcher", M);
  }
  return F;
}

StructType *CodeBonesCodeGeneration::getSymbolsStruct(SymbolsTy &UsedSymbols) {
  std::vector<Type *> Fields;
  for (auto &symbol : UsedSymbols) {
    if (!isa<Function>(symbol.second))
      Fields.push_back(symbol.second->getType());
  }
  return StructType::get(Fields[0]->getContext(), Fields, true);
}

Function *CodeBonesCodeGeneration::createBonesParallelFunction(
    Module *M, SymbolsTy &UsedSymbols, struct clast_for *ForLoop) {

  LLVMContext &Context = M->getContext();
  StructType *SymbolsStructTy = getSymbolsStruct(UsedSymbols);
  std::vector<Type *> Arguments = {PointerType::get(SymbolsStructTy, 0), i64Ty,
                                   i64Ty};
  FunctionType *FunTy = FunctionType::get(voidTy, Arguments, false);
  // Create the basic structure of the subfunction.
  Function *BoneFun =
      Function::Create(FunTy, Function::ExternalLinkage,
                       concat("for.", ForLoop->iterator, ".loop_function"), M);
  BasicBlock *Entry = BasicBlock::Create(Context, "entry", BoneFun);
  BasicBlock *VerifCheck = BasicBlock::Create(Context, "verif_check", BoneFun);
  BasicBlock *Rollback = BasicBlock::Create(Context, "rollback", BoneFun);
  BasicBlock *Exit = BasicBlock::Create(Context, "exit", BoneFun);
  BranchInst::Create(VerifCheck, Entry);
  Function *RollbackFun = getRollbackFunction(M);
  CallInst::Create(RollbackFun, {this->Zero}, "", Rollback);
  BranchInst::Create(Exit, Rollback);
  ReturnInst::Create(Context, Exit);
  const auto DL = M->getDataLayout();
  AllocaInst *Verif = new AllocaInst(boolTy, DL.getAllocaAddrSpace(),
				     "verification_flag",
                                     Entry->getTerminator());
  new StoreInst(True, Verif, Entry->getTerminator());
  LoadInst *VerificationCondition =
      new LoadInst(Verif, "verification", VerifCheck);
  BranchInst::Create(Exit, Rollback, VerificationCondition, VerifCheck);
  // Get the arguments
  llvm::Argument *symbols;
  llvm::Argument *lower;
  llvm::Argument *upper;
  auto argument_it = BoneFun->arg_begin();
  symbols = &*argument_it++;
  lower = &*argument_it++;
  upper = &*argument_it;
  symbols->setName("symbols");
  lower->setName("lower");
  upper->setName("upper");
  // Reload the symbols
  SymbolsTy NewSymbols;
  IRBuilder<> Builder(Entry->getTerminator());
  int SymbolIdx = 0;
  for (auto &symbol : UsedSymbols) {
    if (!isa<Function>(symbol.second)) {
      llvm::Value *Ptr = Builder.CreateStructGEP(
          cast<PointerType>(symbols->getType()->getScalarType())
              ->getElementType(),
          symbols, SymbolIdx);
      NewSymbols[symbol.first] = Builder.CreateLoad(Ptr, symbol.first);
      SymbolIdx++;
    } else {
      NewSymbols[symbol.first] = symbol.second;
    }
  }

  // Create the loop.
  llvm::Value *Step =
      ConstantInt::get(i64Ty, cloog_int_get_si(ForLoop->stride));
  LoopCFG L =
      createLoop(ForLoop->iterator, lower, upper, Step, Entry, VerifCheck);
  NewSymbols[ForLoop->iterator] = L.Iterator;
  visitClast(NewSymbols, L.Body, ForLoop->body, Verif, true);
  // For enabling vectorization:
  unsigned LoopMD = Context.getMDKindID("llvm.loop");
  TerminatorInst *LoopBackedge = L.Inc->getTerminator();
  MDNode *LoopMDNode =
      MDNode::get(Context, std::vector<llvm::Metadata *>({nullptr}));
  LoopMDNode->replaceOperandWith(0, LoopMDNode);
  LoopBackedge->setMetadata(LoopMD, LoopMDNode);
  return BoneFun;
}

BasicBlock *CodeBonesCodeGeneration::visitClastParallelFor(SymbolsTy &Symbols, 
                                                    BasicBlock *ParentBlock, 
                                                    struct clast_for *ForLoop) {

  Function *ParallelFun =
      defineParallelFun(ParentBlock->getParent()->getParent());
  Function *BoneFun =
      createBonesParallelFunction(ParallelFun->getParent(), Symbols, ForLoop);
  StructType *SymbolsStructTy = cast<StructType>(
      (BoneFun->getFunctionType()->getParamType(0))->getContainedType(0));
  IRBuilder<> Builder(ParentBlock->getTerminator());
  // Pack the arguments and call the loop in parallel.
  llvm::Value *Lower = visitClastExpr(Symbols, ParentBlock, ForLoop->LB);
  llvm::Value *Upper = visitClastExpr(Symbols, ParentBlock, ForLoop->UB);
  const auto DL = ParentBlock->getParent()->getParent()->getDataLayout();
  AllocaInst *StructAlloc =
    new AllocaInst(SymbolsStructTy, DL.getAllocaAddrSpace(), "packed_symbols",
		   &*(ParentBlock->getParent()->getEntryBlock().begin()));
  int SymbolIdx = 0;
  for (auto &symbol : Symbols) {
    llvm::Value *SymbolValue = symbol.second;
    if (!isa<Function>(SymbolValue)) {
      llvm::Value *StructField = Builder.CreateStructGEP(
          cast<PointerType>(StructAlloc->getType()->getScalarType())
              ->getElementType(),
          StructAlloc, SymbolIdx);
      Builder.CreateStore(SymbolValue, StructField);
      SymbolIdx++;
    }
  }

  llvm::Value *BoneCast = Builder.CreateBitCast(
      BoneFun, ParallelFun->getFunctionType()->getParamType(0));
  llvm::Value *StructCast = Builder.CreateBitCast(
      StructAlloc, ParallelFun->getFunctionType()->getParamType(1));
  // increase upper by one.
  Upper = Builder.CreateAdd(Upper, One);
  llvm::Value *Step =
      ConstantInt::get(i64Ty, cloog_int_get_si(ForLoop->stride));
  Builder.CreateCall(ParallelFun, {BoneCast, StructCast, Lower, Upper, Step});
  return ParentBlock;
}

std::set<int> getStmtIds(struct clast_stmt *S, bool GoToNext = false) {
  std::set<int> StmtIds;
  if (S) {
    if (CLAST_STMT_IS_A(S, stmt_user)) {
      clast_user_stmt *AsUser = (clast_user_stmt *)S;
      CloogStatement *CloogStmt = AsUser->statement;
      StmtIds.insert(CloogStmt->number);
    } else if (CLAST_STMT_IS_A(S, stmt_for)) {
             clast_for *AsFor = (clast_for *)S;
             StmtIds = getStmtIds(AsFor->body, true);
           } else if (CLAST_STMT_IS_A(S, stmt_guard)) {
                    clast_guard *AsGuard = (clast_guard *)S;
                    StmtIds = getStmtIds(AsGuard->then, true);
                  } else if (CLAST_STMT_IS_A(S, stmt_block)) {
                           clast_block *AsBlock = (clast_block *)S;
                           StmtIds = getStmtIds(AsBlock->body, true);
                         }

    if (GoToNext) {
      std::set<int> NextStmtIds = getStmtIds(S->next, true);
      StmtIds.insert(NextStmtIds.begin(), NextStmtIds.end());
    }
  }
  return StmtIds;
}

bool CodeBonesCodeGeneration::isLoopVectorize(struct clast_for *Loop) {
  // parallel loops are also vectorizable
  if (isLoopParallel(Loop))
    return true;
  std::string Iter = Loop->iterator;
  std::set<int> StmtIds = std::move(getStmtIds(Loop->body, true));
  for (const std::pair<std::string, std::set<int>> &IterStmtsPair :
       this->Vectorize[this->Idx]) {
    if (IterStmtsPair.first == Iter) {
      bool AllStmtsIncluded = true;
      for (auto stmt_id : StmtIds) {
        AllStmtsIncluded =
            AllStmtsIncluded && IterStmtsPair.second.count(stmt_id) > 0;
      }
      if (AllStmtsIncluded)
        return true;
    }
  }
  return false;
}

bool CodeBonesCodeGeneration::isLoopParallel(struct clast_for *Loop) {
  // on the verification every loop is parallel
  if (this->Idx == 0)
    return true;
  std::string Iter = Loop->iterator;
  std::set<int> StmtIds = std::move(getStmtIds(Loop->body, true));
  for (const std::pair<std::string, std::set<int>> &iter_stmts_pair :
       this->Parallel[this->Idx]) {
    if (iter_stmts_pair.first == Iter) {
      bool AllStmtsIncluded = true;
      for (auto stmt_id : StmtIds) {
        AllStmtsIncluded =
            AllStmtsIncluded && iter_stmts_pair.second.count(stmt_id) > 0;
      }
      if (AllStmtsIncluded)
        return true;
    }
  }
  return false;
}

BasicBlock *CodeBonesCodeGeneration::visitClastFor(SymbolsTy &Symbols,
                                                   BasicBlock *ParentBlock,
                                                   struct clast_for *ForLoop,
                                                   llvm::Value *Verif,
                                                   bool ParallelParent) {

  const bool IsParallel = !ParallelParent && isLoopParallel(ForLoop);
  if (IsParallel)
    return visitClastParallelFor(Symbols, ParentBlock, ForLoop);
  else {
    auto DoUnroll = Unroll[this->Idx].find(ForLoop->iterator);
    if (DoUnroll != Unroll[this->Idx].end())
      return visitClastUnrollFor(Symbols, ParentBlock, ForLoop,
                                 DoUnroll->second.second, Verif,
                                 ParallelParent);
    else
      return visitClastSequentialFor(Symbols, ParentBlock, ForLoop, Verif,
                                     ParallelParent);
  }
}

llvm::Value *
CodeBonesCodeGeneration::visitClastAssig(SymbolsTy &Symbols,
                                         BasicBlock *ParentBlock,
                                         struct clast_assignment *Assign) {

  llvm::Value *RHS = visitClastExpr(Symbols, ParentBlock, Assign->RHS);
  if (Assign->LHS) {
    std::string Name = Assign->LHS;
    Symbols[Name] = RHS;
  }
  return RHS;
}

void CodeBonesCodeGeneration::visitClastUser(SymbolsTy &Symbols,
                                             BasicBlock *ParentBlock,
                                             struct clast_user_stmt *User,
                                             llvm::Value *Verif) {

  CloogStatement *CloogStmt = User->statement;
  std::string StmtName = "";
  if (CloogStmt->name)
    StmtName = CloogStmt->name;
  if (Symbols.count(StmtName) == 0)
    StmtName = this->StmtIdToSymbolName[this->Idx][CloogStmt->number];

  llvm::Value *Bone = Symbols[StmtName];
  llvm::Value *Params = Symbols["param"];
  llvm::Value *PhiState = Symbols["phi_state"];
  llvm::Value *Coefs = Symbols["coefficients"];
  std::vector<clast_assignment *> Iterators;

  // to map iterators to original iterators
  clast_stmt *Substitutions = User->substitutions;
  while (Substitutions != nullptr) {
    assert(CLAST_STMT_IS_A(Substitutions, stmt_ass));
    Iterators.push_back((clast_assignment *)Substitutions);
    Substitutions = Substitutions->next;
  }

  // hack: I need to know the number of iterators.
  // Pluto tiles the domain, not the scattering.
  // The last iterators are the original ones.
  const int OriginalIterators =
            cast<Function>(Bone)->getFunctionType()->getNumParams() - 3;
  const int TileIterators = (Iterators.size() - OriginalIterators);

  std::vector<llvm::Value *> Arguments;
  Arguments.push_back(Params);
  Arguments.push_back(PhiState);
  Arguments.push_back(Coefs);
  for (int i = 0; i < OriginalIterators; ++i) {
    Arguments.push_back(
        visitClastAssig(Symbols, ParentBlock, Iterators[TileIterators + i]));
  }
  llvm::Value *CallResult =
      CallInst::Create(Bone, Arguments, "", ParentBlock->getTerminator());
  if (CallResult->getType() == boolTy) {
    LoadInst *FlagLoad = new LoadInst(Verif, "", ParentBlock->getTerminator());
    Instruction *NewFlagValue = BinaryOperator::CreateAnd(
        FlagLoad, CallResult, "", ParentBlock->getTerminator());
    new StoreInst(NewFlagValue, Verif, ParentBlock->getTerminator());
  }
}

BasicBlock *CodeBonesCodeGeneration::visitClastGuard(SymbolsTy &Symbols,
                                                     BasicBlock *ParentBlock,
                                                     struct clast_guard *Guard,
                                                     llvm::Value *Verif,
                                                     bool ParallelParent) {

  BasicBlock *ParentCont = ParentBlock->splitBasicBlock(
      ParentBlock->getTerminator(), ParentBlock->getName() + ".c");
  TerminatorInst *InsertAt = ParentBlock->getTerminator();
  clast_equation *EQ = &Guard->eq[0];
  llvm::Value *EqRHS;
  llvm::Value *EqLHS;
  ICmpInst::Predicate Predicate;

  EqRHS = visitClastExpr(Symbols, ParentBlock, EQ->RHS);
  EqLHS = visitClastExpr(Symbols, ParentBlock, EQ->LHS);
  Predicate = ((EQ->sign == 0) ? ICmpInst::ICMP_EQ
                               : ((EQ->sign < 0) ? ICmpInst::ICMP_SLE
                                                 : ICmpInst::ICMP_SGE));

  llvm::Value *Condition;
  if (EqRHS && EqLHS)
    Condition = new ICmpInst(InsertAt, Predicate, EqLHS, EqRHS, "");
  else
    Condition = ConstantInt::getTrue(InsertAt->getContext());
  for (int i = 1; i < Guard->n; ++i) {
    EQ = &Guard->eq[i];
    EqRHS = visitClastExpr(Symbols, ParentBlock, EQ->RHS);
    EqLHS = visitClastExpr(Symbols, ParentBlock, EQ->LHS);
    Predicate = ((EQ->sign == 0) ? ICmpInst::ICMP_EQ
                                 : ((EQ->sign < 0) ? ICmpInst::ICMP_SLE
                                                   : ICmpInst::ICMP_SGE));

    llvm::Value *NewCondition;
    if (EqRHS && EqLHS)
      NewCondition = new ICmpInst(InsertAt, Predicate, EqLHS, EqRHS, "");
    else
      NewCondition = ConstantInt::getTrue(InsertAt->getContext());
    Condition = BinaryOperator::CreateAnd(Condition, NewCondition, "", InsertAt);
  }

  BasicBlock *ThenBlock = BasicBlock::Create(
      ParentBlock->getContext(), ParentBlock->getName() + ".then",
      ParentBlock->getParent(), ParentCont);
  InsertAt->eraseFromParent();
  BranchInst::Create(ThenBlock, ParentCont, Condition, ParentBlock);
  BranchInst::Create(ParentCont, ThenBlock);

  SymbolsTy SymbolsCopy = Symbols;
  visitClast(SymbolsCopy, ThenBlock, Guard->then, Verif, ParallelParent);

  return ParentCont;
}

bool CodeBonesCodeGeneration::hasStatementsOfType(struct clast_stmt *S, 
                                               bool ComputationOrVerification) {

  std::set<int> StmtIds = std::move(getStmtIds(S));
  for (int stmt_id : StmtIds) {
    const std::string StmtName = this->StmtIdToSymbolName[this->Idx][stmt_id];
    const bool IsVerification = StmtName.find(".stmt") == std::string::npos;
    if (IsVerification == !ComputationOrVerification)
      return true;
  }
  return false;
}

BasicBlock *CodeBonesCodeGeneration::visitClast(SymbolsTy &Symbols,
                                                BasicBlock *ParentBlock,
                                                struct clast_stmt *S,
                                                llvm::Value *Verif,
                                                bool ParallelParent) {
  if (!S)
    return ParentBlock;

  bool HasComputationStmts = hasStatementsOfType(S, true);
  bool HasVerificationStmts = hasStatementsOfType(S, false);
  bool HasStatementsToCompute = (Computation && HasComputationStmts) ||
                                (Verification && HasVerificationStmts);

  if (CLAST_STMT_IS_A(S, stmt_ass))
    visitClastAssig(Symbols, ParentBlock, (clast_assignment *)S);

  if (HasStatementsToCompute) {
    if (CLAST_STMT_IS_A(S, stmt_user))
      visitClastUser(Symbols, ParentBlock, (clast_user_stmt *)S, Verif);
    else if (CLAST_STMT_IS_A(S, stmt_for))
      ParentBlock = visitClastFor(Symbols, ParentBlock, (clast_for *)S, Verif,
                                  ParallelParent);
    else if (CLAST_STMT_IS_A(S, stmt_guard))
      ParentBlock = visitClastGuard(Symbols, ParentBlock, (clast_guard *)S,
                                    Verif, ParallelParent);
    else if (CLAST_STMT_IS_A(S, stmt_block)) {
      clast_block *AsBlock = (clast_block *)S;
      SymbolsTy SymbolsCopy = Symbols;
      ParentBlock = visitClast(SymbolsCopy, ParentBlock, AsBlock->body, Verif,
                               ParallelParent);
    }
  }
  return visitClast(Symbols, ParentBlock, S->next, Verif, ParallelParent);
}

std::map<std::string, Function *> CodeBonesCodeGeneration::getBones(Module &M) {
  std::map<std::string, Function *> Bones;
  for (Function &F : M) {
    const bool IsDeclarationOnly = F.getBasicBlockList().empty();
    if (!IsDeclarationOnly) {
      Bones[F.getName()] = &F;
    }
  }
  return Bones;
}

Function *CodeBonesCodeGeneration::getSkeletonDeclaration(Module &M,
                                                          Function *Bone,
                                                          std::string suffix) {

  Type *ParamType = Bone->getFunctionType()->getParamType(0);
  Type *PhiType = Bone->getFunctionType()->getParamType(1);
  Type *CoefsType = Bone->getFunctionType()->getParamType(2);
  std::vector<Type *> ArgsType = {ParamType, PhiType, CoefsType, i64Ty, i64Ty};
  FunctionType *FunTy = FunctionType::get(voidTy, ArgsType, false);
  Function *F = Function::Create(FunTy, Function::ExternalLinkage,
                                 std::string("apollo_loop") + suffix, &M);
  auto arg = F->arg_begin();
  arg->setName("param");
  arg++;
  arg->setName("phi_state");
  arg++;
  arg->setName("coefs");
  arg++;
  arg->setName("chunk_lower");
  arg++;
  arg->setName("chunk_upper");
  return F;
}

void CodeBonesCodeGeneration::alwaysInlineBones(
    std::map<std::string, Function *> &Bones) {
  for (auto &bone : Bones) {
    bone.second->setLinkage(Function::PrivateLinkage);
    bone.second->addFnAttr(Attribute::AttrKind::AlwaysInline);
  }
}

bool CodeBonesCodeGeneration::runOnModule(Module &M) {
  traceEnter("CodeBonesCodeGeneration::runOnModule");
  LLVMContext &Context = M.getContext();
  this->True = ConstantInt::getTrue(Context);
  this->boolTy = this->True->getType();
  this->i64Ty = Type::getInt64Ty(Context);
  this->voidTy = Type::getVoidTy(Context);
  this->Zero = ConstantInt::get(i64Ty, 0);
  this->One = ConstantInt::get(i64Ty, 1);
  this->Two = ConstantInt::get(i64Ty, 2);

  if (Scan) {
    for (int i = 0; i < 2; ++i) {
      this->Parallel[i] = Scan->Parallel[i];
      this->Vectorize[i] = Scan->Vectorize[i];
      this->Unroll[i] = Scan->Unroll[i];
      this->StmtIdToSymbolName[i] = Scan->StmtIdToName[i];
    }
  }

  std::map<std::string, Function *> Bones = std::move(getBones(M));
  alwaysInlineBones(Bones);
  assert(!Bones.empty());

  Function *ABone = Bones.begin()->second;

  Function *RollbackFun = getRollbackFunction(&M);
  Function *VerifOnlyFun = getSkeletonDeclaration(M, ABone, "_verif");
  Function *SkeletonFun = getSkeletonDeclaration(M, ABone, "");

  Function *Skeletons[] = {VerifOnlyFun, SkeletonFun};

  for (this->Idx = 0; this->Idx < 2; ++this->Idx) {
    Function *Skeleton = Skeletons[this->Idx];

    BasicBlock *Entry = BasicBlock::Create(Context, "entry", Skeleton);
    BasicBlock *VerifCheck =
        BasicBlock::Create(Context, "verif_check", Skeleton);
    BasicBlock *Rollback = BasicBlock::Create(Context, "rollback", Skeleton);
    BasicBlock *Exit = BasicBlock::Create(Context, "exit", Skeleton);

    ReturnInst::Create(Context, Exit);
    BranchInst::Create(VerifCheck, Entry);

    const auto DL = M.getDataLayout();
    AllocaInst *Verif = new AllocaInst(boolTy, DL.getAllocaAddrSpace(),
				       "verification_flag",
                                       Entry->getTerminator());

    new StoreInst(True, Verif, Entry->getTerminator());

    CallInst::Create(RollbackFun, {this->Zero}, "", Rollback);
    BranchInst::Create(Exit, Rollback);

    LoadInst *VerificationCondition =
        new LoadInst(Verif, "verification_condition", VerifCheck);
    BranchInst::Create(Exit, Rollback, VerificationCondition, VerifCheck);

    // map the arguments and bones to symbols
    SymbolsTy Symbols; // mappings from cloog names to llvm
    auto arg_iterator = Skeleton->arg_begin();
    Symbols["param"] = &*arg_iterator++;
    Symbols["phi_state"] = &*arg_iterator++;
    Symbols["coefficients"] = &*arg_iterator++;
    Symbols["chunk_lower"] = &*arg_iterator++;
    Symbols["chunk_upper"] = BinaryOperator::CreateSub(
        &*arg_iterator, One, "chunk_upper_m1", Entry->getTerminator());

    for (auto &bone : Bones) {
      Symbols[bone.first] = bone.second;
    }

    if (this->Idx == 0 && this->Verification)
      visitClast(Symbols, Entry, Scan->ClastVerification, Verif, false);

    if (this->Idx == 1 && this->Computation)
      visitClast(Symbols, Entry, Scan->ClastComputation, Verif, false);
  }

  traceExit("CodeBonesCodeGeneration::runOnModule");
  return true;
}

namespace apollo {
Pass *createCodeBonesCodeGenerationPass(ScanTypeStruct *S, bool Comp,
                                        bool Verif) {
  return new CodeBonesCodeGeneration(S, Comp, Verif);
}
} // end namespace apollo
