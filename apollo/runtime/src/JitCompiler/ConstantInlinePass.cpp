//===--- ConstantInlinePass.cpp -------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "JitCompiler/CreatePasses.h"
#include "Backdoor/Backdoor.h"

#include "llvm/Pass.h"
#include "llvm/Bitcode/BitcodeReader.h"
#include "llvm/Bitcode/BitcodeWriter.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO.h"


extern APOLLO_PROG *apolloProg;

using namespace llvm;
using namespace apollo;

namespace apollo {

struct ConstantInlinePass : public llvm::FunctionPass {
  static char ID;
  ConstantInlinePass() : FunctionPass(ID) {}
  bool runOnFunction(llvm::Function &);
  void *getConstantValuePointer(Constant *OperandValue, bool &Success);
  Constant *getConstantValueOfType(llvm::Function &F, llvm::Type *InstTy,
                                   void *Ptr);
};

} // end namespace apollo

char ConstantInlinePass::ID = 0;
llvm::FunctionPass *apollo::createConstantInlinePass() {
  return new ConstantInlinePass();
}

void *ConstantInlinePass::getConstantValuePointer(Constant *OperandValue,
                                                  bool &Success) {
  ConstantExpr *CI = dyn_cast<llvm::ConstantExpr>(OperandValue);
  if (CI && CI->getOpcode() == Instruction::IntToPtr) {
    return getConstantValuePointer(CI->getOperand(0), Success);
  }
  ConstantInt *AsInt = dyn_cast<ConstantInt>(OperandValue);
  if (AsInt) {
    Success = true;
    void *Ptr = (void *)(AsInt->getSExtValue());
    return Ptr;
  }

  // TODO We should handle more cases.
  // llvm::errs() << "Now we cannot handle this case\n";
  Success = false;
  return nullptr;
}

Constant *ConstantInlinePass::getConstantValueOfType(llvm::Function &F,
                                                     llvm::Type *InstTy,
                                                     void *Ptr) {

  Constant *Value = nullptr;
  LLVMContext &Context = F.getContext();

  Type *i8Ty = Type::getInt8Ty(Context);
  Type *i16Ty = Type::getInt16Ty(Context);
  Type *i32Ty = Type::getInt32Ty(Context);
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *floatTy = Type::getFloatTy(Context);
  Type *doubleTy = Type::getDoubleTy(Context);
  // Pointer type
  if (InstTy->isPointerTy()) {
    long *PtrData = (long *)(Ptr);
    Constant *ConstValue = ConstantInt::get(i64Ty, (*PtrData));
    Value = ConstantExpr::getIntToPtr(ConstValue, InstTy);
  }
  // Integer types
  if (InstTy->isIntegerTy()) {
    if (InstTy == i64Ty)
      Value = ConstantInt::get(i64Ty, *(int64_t *)(Ptr));
    else if (InstTy == i32Ty)
      Value = ConstantInt::get(i32Ty, *(int32_t *)(Ptr));
    else if (InstTy == i16Ty)
      Value = ConstantInt::get(i16Ty, *(int16_t *)(Ptr));
    else if (InstTy == i8Ty)
      Value = ConstantInt::get(i8Ty, *(int8_t *)(Ptr));
  }
  // Float type
  if (InstTy->isFloatTy()) {
    Value = ConstantFP::get(floatTy, *(float *)(Ptr));
  }
  // Double type
  if (InstTy->isDoubleTy()) {
    Value = ConstantFP::get(doubleTy, *(double *)(Ptr));
  }
  return Value;
}

bool isReadOnly(const uintptr_t &OroginalAddress) {
  for (StaticMemInfo *stmt_info : apolloProg->StaticInfo->WriteMem) {
    const int StmtId = stmt_info->Id;
    const auto &MemRangeList = apolloProg->ExtremeMemRanges;
    const auto &Found = MemRangeList[StmtId];
    if ((Found.first <= OroginalAddress && Found.second >= OroginalAddress))
      return false;
  }
  return true;
}

bool ConstantInlinePass::runOnFunction(llvm::Function &F) {
  traceEnter(backdoorutils::concat("ConstantInlinePass::runOnFunction ", F.getName().str()));
  bool modified = false;
  std::set<Instruction *> WorkList;
  for (BasicBlock &Block : F) {
    for (Instruction &I : Block) {
      if (isa<LoadInst>(I) && isa<Constant>(I.getOperand(0)))
        WorkList.insert(&I);
    }
  }
  while (!WorkList.empty()) {
    Instruction *I = *WorkList.begin();
    WorkList.erase(WorkList.begin());
    Constant *OperandValue = cast<Constant>((*I).getOperand(0));
    bool Success = false;
    void *Ptr = (void *)getConstantValuePointer(OperandValue, Success);

    const uintptr_t OroginalAddress = (uintptr_t)Ptr;
    if (!Success || !isReadOnly(OroginalAddress))
      continue;
    if (Constant *ConstantValue =
            getConstantValueOfType(F, I->getType(), Ptr)) {
      I->replaceAllUsesWith(ConstantValue);
      WorkList.erase(I);
      I->eraseFromParent();
      modified = true;
    }
  }
  traceExit(backdoorutils::concat("ConstantInlinePass::runOnFunction ",
		  F.getName().str(), " ", (modified ? "true" : "false")));
  return modified;
}
