//===--- CodeBonesComposedPass.cpp ----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "CodeBones/CodeBones.h"
#include "CodeBones/BonesInfo/BoneAst.h"
#include "CodeBones/BonesInfo/BoneAstDump.h"
#include "CodeBones/BonesInfo/BoneComposedStmt.h"
#include "CodeBones/BonesInfo/BoneLoop.h"
#include "CodeBones/BonesInfo/BoneStmt.h"
#include "JitCompiler/PassUtils.h"
#include "Backdoor/Backdoor.h"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Constant.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/ValueMapper.h"

using namespace llvm;
using namespace apollo::codebones;

static Type *getReturnTy(LLVMContext &Context, bool Verif) {
  if (Verif)
    return Type::getInt1Ty(Context);
  return Type::getVoidTy(Context);
}

static Function *getComposedBoneDeclaration(BoneComposedStmt *Stmt, Module *M) {
  LLVMContext &Context = M->getContext();
  std::vector<BoneStmt *> Bones = Stmt->getBones();
  std::vector<Function *> BoneFuns = {
      cast<Function>(M->getFunction(Bones[0]->name())),
      cast<Function>(M->getFunction(Bones[1]->name()))};

  Type *RetTy = getReturnTy(Context, Stmt->isVerification());
  std::vector<Type *> ArgTypes;

  const int WithMaxNumArgs = (BoneFuns[0]->getFunctionType()->getNumParams() >
                              BoneFuns[1]->getFunctionType()->getNumParams())
                             ? 0
                             : 1;

  for (auto arg_it = BoneFuns[WithMaxNumArgs]->arg_begin();
       arg_it != BoneFuns[WithMaxNumArgs]->arg_end(); ++arg_it) {
    ArgTypes.push_back(arg_it->getType());
  }

  FunctionType *ComposedBoneTy = FunctionType::get(RetTy, ArgTypes, false);
  Function *ComposedBoneFun = Function::Create(ComposedBoneTy, 
                                               BoneFuns[0]->getLinkage(), 
                                               Stmt->name(), M);

  for (auto arg_it = BoneFuns[WithMaxNumArgs]->arg_begin(),
            comp_arg_it = ComposedBoneFun->arg_begin();
       arg_it != BoneFuns[WithMaxNumArgs]->arg_end(); ++arg_it) {
    comp_arg_it->setName(arg_it->getName());
  }
  return ComposedBoneFun;
}

static void generateCodeForGuarded(BoneComposedStmt *Composed,
                                   Function *ComposedBoneFun,
                                   std::vector<Value *> &Args, Module *M) {

  std::vector<BoneStmt *> Bones = Composed->getBones();
  std::vector<Function *> BoneFuns = {
      cast<Function>(M->getFunction(Bones[0]->name())),
      cast<Function>(M->getFunction(Bones[1]->name()))};

  LLVMContext &Context = M->getContext();
  BasicBlock *Entry = BasicBlock::Create(Context, "entry", ComposedBoneFun);
  BasicBlock *Second = BasicBlock::Create(Context, "second", ComposedBoneFun);
  BasicBlock *Exit = BasicBlock::Create(Context, "exit", ComposedBoneFun);

  AllocaInst *VerificationResult =
      new AllocaInst(Type::getInt1Ty(Context), 0, "verification_result", Entry);
  new StoreInst(ConstantInt::getTrue(Context), VerificationResult, Entry);

  LoadInst *Verification = new LoadInst(
      VerificationResult, VerificationResult->getName() + ".load", Exit);
  ReturnInst::Create(Context, Verification, Exit);

  std::vector<Value *> ArgsForFirst(
      Args.begin(),
      Args.begin() + BoneFuns[0]->getFunctionType()->getNumParams());
  std::vector<Value *> ArgsForSecond(
      Args.begin(),
      Args.begin() + BoneFuns[1]->getFunctionType()->getNumParams());

  CallInst *FirstBoneResult =
      CallInst::Create(BoneFuns[0], ArgsForFirst, "", Entry);
  CallInst *SecondBoneResult =
      CallInst::Create(BoneFuns[1], ArgsForSecond, "", Second);

  Value *VerifVal = new LoadInst(
      VerificationResult, VerificationResult->getName() + ".load", Entry);
  VerifVal = BinaryOperator::CreateAnd(VerifVal, FirstBoneResult, "", Entry);
  new StoreInst(VerifVal, VerificationResult, Entry);

  if (Bones[1]->isVerification()) {
    Value *VerifVal = new LoadInst(
        VerificationResult, VerificationResult->getName() + ".load", Second);
    VerifVal =
        BinaryOperator::CreateAnd(VerifVal, SecondBoneResult, "", Second);
    new StoreInst(VerifVal, VerificationResult, Second);
  }
  BranchInst::Create(Exit, Second);
  BranchInst::Create(Second, Exit, FirstBoneResult, Entry);
}

static void generateCodeForSequential(BoneComposedStmt *Composed,
                                      Function *ComposedBoneFun,
                                      std::vector<Value *> &Args, Module *M) {
  std::vector<BoneStmt *> Bones = Composed->getBones();
  std::vector<Function *> BoneFuns = {
      cast<Function>(M->getFunction(Bones[0]->name())),
      cast<Function>(M->getFunction(Bones[1]->name()))};

  LLVMContext &Context = M->getContext();
  BasicBlock *Entry = BasicBlock::Create(Context, "entry", ComposedBoneFun);
  BasicBlock *Exit = BasicBlock::Create(Context, "exit", ComposedBoneFun);

  AllocaInst *VerificationResult = nullptr;
  if (Composed->isVerification()) {
    // start with true in the verification result
    VerificationResult = new AllocaInst(Type::getInt1Ty(Context), 0,
                                        "verification_result", Entry);
    new StoreInst(ConstantInt::getTrue(Context), VerificationResult, Entry);
    // return the last value of the verification result
    LoadInst *Verification = new LoadInst(
        VerificationResult, VerificationResult->getName() + ".load", Exit);
    ReturnInst::Create(Context, Verification, Exit);
  } else {
    ReturnInst::Create(Context, Exit);
  }
  CallInst *FirstBoneResult = CallInst::Create(BoneFuns[0], Args, "", Entry);
  CallInst *SecondBoneResult = CallInst::Create(BoneFuns[1], Args, "", Entry);

  if (Bones[0]->isVerification()) {
    Value *VerifVal = new LoadInst(
        VerificationResult, VerificationResult->getName() + ".load", Entry);
    VerifVal = BinaryOperator::CreateAnd(VerifVal, FirstBoneResult, "", Entry);
    new StoreInst(VerifVal, VerificationResult, Entry);
  }

  if (Bones[1]->isVerification()) {
    Value *VerifVal = new LoadInst(
        VerificationResult, VerificationResult->getName() + ".load", Entry);
    VerifVal = BinaryOperator::CreateAnd(VerifVal, SecondBoneResult, "", Entry);
    new StoreInst(VerifVal, VerificationResult, Entry);
  }
  BranchInst::Create(Exit, Entry);
}

static void generateCodeForConcurrent(BoneComposedStmt *Composed,
                                      Function *ComposedBoneFun,
                                      std::vector<Value *> &Args, Module *M) {
  errs() << "concurrent execution not yet supported.\n";
  generateCodeForSequential(Composed, ComposedBoneFun, Args, M);
}

class BoneComposeGenerateCode : public BoneAstVisitor<bool, Module *> {
public:
  virtual bool visitStmt(BoneStmt *Stmt, Module *M) {
    if (!Stmt->isComposed())
      return false;
    BoneComposedStmt *Composed = (BoneComposedStmt *)Stmt;
    // generate the bones for the child(maybe one is also composed)
    for (BoneStmt *child_stmt : Composed->getBones()) {
      this->visit(child_stmt, M);
    }
    Function *ComposedBoneFun = getComposedBoneDeclaration(Composed, M);
    std::vector<Value *> Args;
    for (auto arg_it = ComposedBoneFun->arg_begin();
         arg_it != ComposedBoneFun->arg_end(); ++arg_it) {
      Args.push_back(&*arg_it);
    }
    if (Composed->isGuarded())
      generateCodeForGuarded(Composed, ComposedBoneFun, Args, M);
    else if (Composed->executesSequentially())
           generateCodeForSequential(Composed, ComposedBoneFun, Args, M);
         else
           generateCodeForConcurrent(Composed, ComposedBoneFun, Args, M);

    return true;
  }

  virtual bool visitLoop(BoneLoop *Loop, Module *M) {
    bool Composed = false;
    for (BoneAst *child : Loop->children()) {
      if (this->visit(child, M))
        Composed = true;
    }
    return Composed;
  }
};

// Pass to generate the composed statements.
struct CodeBonesComposed : public ModulePass {
  static char ID;
  ScanTypeStruct *Scan;
  CodeBonesComposed(ScanTypeStruct *S) : ModulePass(ID) { 
    this->Scan = S; 
  }
  virtual bool runOnModule(Module &M) {
    if (Scan && Scan->Loop) {
      BoneComposeGenerateCode gc;
      return gc.visit(Scan->Loop.get(), &M);
    }
    return true;
  }
};

char CodeBonesComposed::ID = 0;

namespace apollo {
Pass *createCodeBonesComposedPass(ScanTypeStruct *S) {
  return new CodeBonesComposed(S);
}
} // end namespace apollo
