//===--- MemProtection.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backup/MemProtection.h"
#include "Backup/Pool.h"
#include "APOLLO_PROG.h"

#include <algorithm>
#include <limits>
#include <cstring>

MemoryProtection *MemoryProtection::MemProtect = nullptr;
MemPool Pool8(1024 * 256, 8);
MemPool Pool16(1024 * 64, 16);

static bool rangeCmp(const MemRangeTy &A, const MemRangeTy &B) {
  if (A.first < B.first)
    return true;
  if (A.first == B.first)
    return B.second < A.second;
  return false;
}

static size_t rangeDistance(const MemRangeTy &A, const MemRangeTy &B) {
  if (rangeOverlap(A, B))
    return 0;
  if (A.first > B.second)
    return A.first - B.second;
  return B.first - A.second;
}

static MemRangeTy rangeMerge(const MemRangeTy &A, const MemRangeTy &B) {
  MemRangeTy Merged;
  Merged.first = std::min<uintptr_t>(A.first, B.first);
  Merged.second = std::max<uintptr_t>(A.second, B.second);
  return Merged;
}

void MemoryProtection::registerRange(const size_t Size, void *Ptr) {
  MemRangeTy Range;
  Range.first = (uintptr_t)Ptr;
  Range.second = (uintptr_t)Ptr + Size - 1;

  while (!Mtx.try_lock());
  // This object locks and unlocks the mutex automatically.
  std::lock_guard<std::mutex> ScopeLock(Mtx, std::adopt_lock);
  // Check if overlapping and merge
  for (size_t i = 0; i < this->NumRanges; ++i) {
    MemRangeTy ExpandedRange = Ranges[i];
    ExpandedRange.first -= MergeThreashold;
    ExpandedRange.second += MergeThreashold;
    if (rangeOverlap(Range, ExpandedRange)) {
      Ranges[i] = rangeMerge(Ranges[i], Range);
      return;
    }
  }

  Ranges[this->NumRanges] = Range;
  std::sort(this->Ranges, this->Ranges + this->NumRanges + 1, rangeCmp);
  // if there is space just increase the number of ranges.
  if (this->NumRanges != MaxRanges)
    this->NumRanges++;
  else {
    // IMPORTANT: This part of the code should not be executed many times.
    // we need to minimize the distance.
    size_t MinimumDistance = rangeDistance(Ranges[0], Ranges[1]);
    size_t MinimumDistanceOfI = 0;
    for (size_t i = 1; i < this->NumRanges; ++i) {
      const size_t Distance = rangeDistance(Ranges[i], Ranges[i + 1]);
      if (Distance < MinimumDistance) {
        MinimumDistance = Distance;
        MinimumDistanceOfI = i;
      }
    }
    Ranges[MinimumDistanceOfI] = rangeMerge(Ranges[MinimumDistanceOfI], 
                                            Ranges[MinimumDistanceOfI + 1]);

    for (size_t i = MinimumDistanceOfI; i < this->NumRanges; ++i) {
      Ranges[i] = Ranges[i + 1];
    }
  }
  return;
}

bool MemoryProtection::overlapsApolloMemory(const MemRangeTy &A,
                                            MemRangeTy *OverlapsWith) {
  while (!Mtx.try_lock());
  // This object locks and unlocks the mutex automatically.
  std::lock_guard<std::mutex> ScopeLock(Mtx, std::adopt_lock);
  for (size_t i = 0; i < this->NumRanges; ++i) {
    if (rangeOverlap(A, Ranges[i])) {
      if (OverlapsWith)
        *OverlapsWith = Ranges[i];
      return true;
    }
  }
  return false;
}

// declare libc malloc.
extern "C" void *__libc_malloc(size_t);
extern "C" void *__libc_calloc(size_t, size_t);
extern "C" void *__libc_realloc(void *, size_t);
extern "C" void __libc_free(void *);

// to override the default malloc implementation.
void *malloc(const size_t Size) {
  if (Size == 0)
    return nullptr;
  MemoryProtection *MemProtect = MemoryProtection::MemProtect;
  const bool ProtectionEnabled = MemProtect != nullptr;

  if (!ProtectionEnabled)
    return __libc_malloc(Size);
  if (Size <= Pool8.UnitSize && !Pool8.isFull())
    return Pool8.allocUnit(Size);
  else if (Size <= Pool16.UnitSize && !Pool16.isFull())
    return Pool16.allocUnit(Size);

  void *Ptr = __libc_malloc(Size);
  MemoryProtection::MemProtect->registerRange(Size, Ptr);
  return Ptr;
}

void *calloc(const size_t NMemB, const size_t Size) {
  const size_t Total = NMemB * Size;
  void *Ptr = malloc(Total);
  if (!Ptr)
    return nullptr;
  return memset(Ptr, 0, Total);
}

void *realloc(void *Ptr, const size_t Size) {
  if (!Ptr)
    return malloc(Size);
  if (Size == 0) {
    free(Ptr);
    return nullptr;
  }
  void *NewPtr = nullptr;
  if (Pool8.contains(Ptr)) {
    if (Size <= Pool8.UnitSize)
      return Ptr;
    NewPtr = __libc_malloc(Size);
    memcpy(NewPtr, Ptr, Pool8.UnitSize);
    Pool8.freeUnit(Ptr);
  } else if (Pool16.contains(Ptr)) {
    if (Size <= Pool16.UnitSize)
      return Ptr;
    NewPtr = __libc_malloc(Size);
    memcpy(NewPtr, Ptr, Pool16.UnitSize);
    Pool16.freeUnit(Ptr);
  } else
    NewPtr = __libc_realloc(Ptr, Size);

  if (MemoryProtection::MemProtect != nullptr)
    MemoryProtection::MemProtect->registerRange(Size, NewPtr);
  return NewPtr;
}

void free(void *Ptr) {
  if (Pool8.contains(Ptr)) {
    Pool8.freeUnit(Ptr);
    return;
  }
  if (Pool16.contains(Ptr)) {
    Pool16.freeUnit(Ptr);
    return;
  }
  __libc_free(Ptr);
  return;
}
