//===--- BackupContainer.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backup/BackupContainer.h"
#include "RuntimeConfig.h"

#include <sys/mman.h>

#include <cassert>
#include <iostream>
#include <algorithm>
#include <cstring>

extern RuntimeConfig apolloRuntimeConfig;

template <int VecSize>
void singleVecCopy(const char *__restrict__ Src, char *Dest) {
  char *DestAlign = (char *)__builtin_assume_aligned(&Dest, 16);
  char Buffer[VecSize];
  for (size_t j = 0; j < VecSize; j++) {
    Buffer[j] = Src[j];
  }
  for (size_t j = 0; j < VecSize; j++) {
    DestAlign[j] = Buffer[j];
  }
  return;
}

/// \brief Apollo implementation of memcpy for efficiency
/// \param in_dest : Target
/// \param in_src : Source
/// \param n_bytes : Number of bytes to be copied
static void apolloMemcpy(char *__restrict__ InDest,
                         const char *__restrict__ InSrc,
                         size_t UnsignedNbytes) {

  const long VecSize = (sizeof(char) * 16) * 4;
  const long AlignTo = 16;
  // cast to long to not have problems with the sign.
  const long NBytes = UnsignedNbytes;
  char *Dest = (char *)InDest;
  const char *Src = (char *)InSrc;
  // first, we iterate until we align memory accesses to the source.
  long IAlign = 0;
  for (IAlign = 0; (IAlign < std::min<long>(AlignTo, NBytes)); ++IAlign) {
    if (((MemTy)&Dest[IAlign]) % AlignTo == 0)
      break;
  }

  if (IAlign)
    std::memcpy(&Dest[0], &Src[0], IAlign);
  #pragma omp parallel for
  for (long i = IAlign; i < (NBytes - VecSize); i += VecSize) {
    singleVecCopy<VecSize>(&Src[i], &Dest[i]);
  }

  const size_t BytesLeft = std::max<long>(NBytes - VecSize, IAlign);
  std::memcpy(&Dest[BytesLeft], &Src[BytesLeft], NBytes - BytesLeft);
}

static bool memRangeCmp(const MemRangeTy &A, const MemRangeTy &B) {
  return A.first < B.first || (A.first == B.first && A.second < B.second);
}

static MemRangeTy expandedRange(const MemRangeTy &A) {
  MemRangeTy B = A;
  B.second++;
  B.first--;
  return B;
}

BackupContainer::BackupContainer() {
  Buffer = nullptr;
  BufferSize = 0;
}

BackupContainer::~BackupContainer() {
  delete[] Buffer;
}

void BackupContainer::addRange(const uint64_t Lower, const uint64_t Upper) {
  assert(this->Backup.empty());
  assert(Lower < Upper);
  this->Ranges.push_back(MemRangeTy(Lower, Upper));
}

bool BackupContainer::doBackup() {
  std::sort(Ranges.begin(), Ranges.end(), memRangeCmp);
  if (!Ranges.empty()) {
    BackupInfo First;
    First.Range = Ranges[0];
    Backup.push_back(First);
    // get the ranges to backup
    const size_t NumRanges = Ranges.size();
    for (size_t i = 1; i < NumRanges; ++i) {
      MemRangeTy &CurRange = Ranges[i];
      BackupInfo &LastRange = Backup.back();
      if (rangeOverlap(LastRange.Range, expandedRange(CurRange))) {
        LastRange.Range.second = std::max<MemTy>(LastRange.Range.second, 
                                                 CurRange.second);
      } else {
        BackupInfo NewRange;
        NewRange.Range = CurRange;
        Backup.push_back(NewRange);
      }
    }

    Ranges.clear();
    // compute the total size, and each size.
    size_t BackupSize = 0;
    for (BackupInfo &info : Backup) {
      info.Len = info.Range.second - info.Range.first + 1;
      BackupSize += info.Len;
    }

    // allocate memory.
    if (BufferSize < BackupSize) {
      if (Buffer)
        delete[] Buffer;

      try {
          Buffer = new char[BackupSize];
      } catch(std::bad_alloc &) {
          Buffer = nullptr;
          BufferSize = 0;
          return false;
      }

      BufferSize = BackupSize;
    }

    // compute where to store each range.
    size_t PartialSize = 0;
    for (BackupInfo &info : Backup) {
      info.At = &Buffer[PartialSize];
      BackupSize += info.Len;
    }
    #pragma omp parallel for
    for (size_t i = 0; i < Backup.size(); ++i) {
      BackupInfo &BackIn = Backup[i];
      apolloMemcpy(BackIn.At, (char *)BackIn.Range.first, BackIn.Len);
    }
  }
  return true;
}

void BackupContainer::rollback() {
  #pragma omp parallel for
  for (size_t i = 0; i < Backup.size(); ++i) {
    BackupInfo &BackIn = Backup[i];
    apolloMemcpy((char *)BackIn.Range.first, BackIn.At, BackIn.Len);
  }
  clear();
}

void BackupContainer::clear() {
  Ranges.clear();
  Backup.clear();
}
