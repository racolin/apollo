//===--- Pool.cpp ---------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backup/Pool.h"

#include <iostream>
#include <cstdlib>
#include <limits>
#include <algorithm>

// declare libc malloc.
extern "C" void *__libc_malloc(size_t);
extern "C" void __libc_free(void *);

bool MemPool::contains(void *Ptr) const {
  const intptr_t BlockLower = (intptr_t) this->MemoryBlock;
  const intptr_t BlockUpper = ((intptr_t) this->MemoryBlock) + BlockSize;
  const intptr_t PtrAsInt = (intptr_t)Ptr;
  if (PtrAsInt < BlockLower || PtrAsInt > BlockUpper)
    return false;
  return true;
}

bool MemPool::isFull() const { 
  return this->FreeUnitsCount == 0; 
}

MemPool::MemPool(IndexTy UnitNumberVal, IndexTy UnitSizeVal_) {
  while (!Mtx.try_lock());
  // This object locks and unlocks the mutex automatically.
  std::lock_guard<std::mutex> scope_lock(Mtx, std::adopt_lock);
  UnitNumber = UnitNumberVal;
  UnitSize = std::max(sizeof(IndexTy), (size_t)UnitSizeVal_);
  NextFreeUnit = -1;
  FreeUnitsCount = 0;
  BlockSize = UnitNumber * UnitSize;
  MemoryBlock = __libc_malloc(BlockSize);
  assert(nullptr != MemoryBlock);
  // init the indexes of the next free block.
  for (IndexTy i = 0; i < (IndexTy)UnitNumber - 1; i++) {
    char *CurrElemAddr = getAbsoluteAddress<char>(i * UnitSize);
    IndexTy *CurrElemCast = (IndexTy *)CurrElemAddr;
    *CurrElemCast = i + 1;
  }

  char *LastElemAddr = getAbsoluteAddress<char>(UnitSize * (UnitNumber - 1));
  IndexTy *LastElemCast = (IndexTy *)LastElemAddr;
  *LastElemCast = -1;
  NextFreeUnit = 0;
  FreeUnitsCount = UnitNumber;
}

MemPool::~MemPool() {
  while (!Mtx.try_lock());
  // This object locks and unlocks the mutex automatically.
  std::lock_guard<std::mutex> ScopeLock(Mtx, std::adopt_lock);
  assert(nullptr != MemoryBlock);
  __libc_free(MemoryBlock);
}

void *MemPool::allocUnit(const size_t Size) {
  while (!Mtx.try_lock());
  // This object locks and unlocks the mutex automatically.
  std::lock_guard<std::mutex> ScopeLock(Mtx, std::adopt_lock);
  assert(MemoryBlock);
  assert(Size <= UnitSize);
  assert(!this->isFull());
  // we calculate the address of the next free unit and then update the
  // rest of the members
  char *NewUnit = getAbsoluteAddress<char>(UnitSize * NextFreeUnit);
  FreeUnitsCount--;
  if (FreeUnitsCount == 0)
    NextFreeUnit = -1;
  else
    NextFreeUnit = *((IndexTy *)NewUnit);
  return (void *)NewUnit;
}

void MemPool::freeUnit(void *Ptr) {
  while (!Mtx.try_lock());
  // This object locks and unlocks the mutex automatically.
  std::lock_guard<std::mutex> ScopeLock(Mtx, std::adopt_lock);
  assert(Ptr && this->contains(Ptr));
  uintptr_t PtrAsInt = (uintptr_t)Ptr;
  PtrAsInt = PtrAsInt - (uintptr_t) this->MemoryBlock;
  assert((PtrAsInt % UnitSize == 0) && "Pointer not aligned!");
  char *PtrCast = (char *)Ptr;
  IndexTy DistanceToBlock = PtrCast - (char *)MemoryBlock;
  // when a unit gets free it is immediately put as the first one in
  // the list of available units
  IndexTy *PtrCast2 = (IndexTy *)Ptr;
  *PtrCast2 = NextFreeUnit;
  IndexTy ElementOffset = DistanceToBlock / UnitSize;
  // possition in the memory_block of the unit being freed
  NextFreeUnit = ElementOffset;
  FreeUnitsCount++;
}
