//===--- AllocatedMemArrange.cpp ------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Backup/AllocatedMemArrange.h"
#include "Backup/Common.h"

#include <iostream>

void AllocatedMemoryArrange::meoryRange(uintptr_t Left, uintptr_t Right) {
  Left -= Left % PageSizeInBytes;
  Right += PageSizeInBytes - (Right % PageSizeInBytes) - 1;
  // auto => std::map<uintptr_t, uintptr_t>::iterator
  auto Lower = AllocatedMap.end();
  auto Upper = AllocatedMap.upper_bound(Left);
  if (AllocatedMap.size() && Upper != AllocatedMap.begin()) {
    Lower = Upper;
    Lower--;
  }
  if (Lower != AllocatedMap.end()) {
    if (Lower->second >= Left) {
      if (Lower->second <= Right) {
        Left = Lower->first;
        AllocatedMap.erase(Lower);
      } else {
        Left = Lower->first;
        Right = Lower->second;
        AllocatedMap.erase(Lower);
      }
    }
  }
  if (Upper != AllocatedMap.end()) {
    if (Upper->first <= Right) {
      if (Upper->second >= Right) {
        Right = Upper->second;
        AllocatedMap.erase(Upper);
      } else {
        AllocatedMap.erase(Upper);
      }
    }
  }
  AllocatedMap.insert(std::pair<uintptr_t, uintptr_t>(Left, Right));
}

bool AllocatedMemoryArrange::isAllocated(const uintptr_t Ptr) {
  // // auto => std::map<uintptr_t, uintptr_t>::iterator
  auto Upper = AllocatedMap.upper_bound(Ptr);
  if (AllocatedMap.size() && Upper != AllocatedMap.end() &&
      Upper != AllocatedMap.begin()) {
    Upper--; // now upper is lower bound of 'ptr'
  } else {
    // 'linear find' case
    for (auto it = AllocatedMap.begin(), E = AllocatedMap.end(); it != E;
         ++it) {
      if (it->first <= Ptr && it->second >= Ptr)
        return true;
    }
    return false;
  }
  if (Upper->first <= Ptr && Upper->second >= Ptr)
    return true;
  return false;
}

void AllocatedMemoryArrange::printAllocatedMemoryRanges() {
  for (auto it = AllocatedMap.begin(), E = AllocatedMap.end(); it != E; ++it) {
    std::cout << "[" << it->first << ", " << it->second << "]" << std::endl;
  }
}
