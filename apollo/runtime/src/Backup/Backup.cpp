//===--- Backup.cpp -------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "APOLLO_PROG.h"
#include "Backup/Backup.h"
#include "StaticInfo/StaticInfo.h"
#include "Backup/MemProtection.h"
#include "Interpolation/Interpolation.h"
#include "Backdoor/Backdoor.h"

#include <deque>
#include <omp.h>
#include <string.h>
#include <utility>
#include <algorithm>
#include <ucontext.h>
#include <setjmp.h>
#include <signal.h>

enum BackupStmtType : int { Read = 1, Write = 2 };

/// \brief GetMaxCoefIndex For a statement id,
///        it returns the position of the index with maximum step.
/// \param StmtPrediction
/// \return
static int getMaxCoefIndex(const PredictionModel::MemoryPrediction &MemPred) {
  int MaxCoeficientPos = 0;
  double ActualCoef = 0.0;
  double MaxCoefficient = 0.0;

  if (MemPred.isLinearOrTube()) {
    const auto &LinearFun = MemPred.LF;
    MaxCoefficient = abs(LinearFun[0]);
    for (unsigned int i = 1; i < LinearFun.size() - 1; ++i) {
      ActualCoef = abs(LinearFun[i]);
      if (MaxCoefficient < ActualCoef) {
        MaxCoefficient = ActualCoef;
        MaxCoeficientPos = i;
      }
    }
  }

  return MaxCoeficientPos;
}

/// \brief Finds the min and max address accessed by the statement
///        for a given chunk
/// \param StmtId : The statement id
/// \param MinMaxIterator : The table containing the min and max values of
///        each iterator for the given chuck
/// \param apolloProg : The apollo program
/// \param IgnoredLoopId : To ignore one loop level, by default is set to
///        not ignore (-1)

static MemRangeTy getStmtRange(const uint64_t StmtId,
                               const std::vector<BoundRangeTy> &MinMaxIterator,
                               const APOLLO_PROG *apolloProg, 
                               const int IgnoredLoopId) {

  const auto &StmtPrediction =
      apolloProg->PredictionModel->getMemPrediction(StmtId);
  const auto &LF = apolloProg->PredictionModel->getMemPrediction(StmtId).LF;

  const StaticMemInfo &StmtInfo = apolloProg->StaticInfo->Mem[StmtId];
  const auto &StmtParents = StmtInfo.Parents;

  MemTy MinAddr = LF.at(StmtParents.size()) - StmtPrediction.TubeWidth;
  MemTy MaxAddr = LF.at(StmtParents.size()) + (StmtInfo.SizeInBytes) - 1 +
                  StmtPrediction.TubeWidth;

  for (size_t parent_idx = 0; parent_idx < StmtParents.size(); parent_idx++) {
    const int ParentId = StmtParents[parent_idx]->Id;
    const bool IgnoreLoop = ParentId == IgnoredLoopId;
    if (!IgnoreLoop) {
      const BoundRangeTy &MinMaxIterBounds = MinMaxIterator[ParentId];
      const auto Coefficient = LF.at(parent_idx);
      if (Coefficient >= 0) {
        MinAddr += Coefficient * (MinMaxIterBounds.first);
        MaxAddr += Coefficient * (MinMaxIterBounds.second);
      } else {
        MinAddr += Coefficient * (MinMaxIterBounds.second);
        MaxAddr += Coefficient * (MinMaxIterBounds.first);
      }
    }
  }
  MemRangeTy Range(MinAddr, MaxAddr);
  return Range;
}

/// \brief Finds the ranges of address accessed by the statement for a given
///        chunk. Calculates the ranges for each step of iterator with higher
///        coefficient of the stmt linear function. For the rest just get the
///        upper and lower value.
/// \param apolloProg : The apollo program
/// \param StmtId : The statement id
/// \param lb : lower bound of the outer-most iterator
/// \param ub : upper bound of the outer-most iterator
/// \return a vector with the ranges that are accessed
static std::vector<MemRangeTy> getBackupRanges(const APOLLO_PROG *apolloProg,
                                               const uint64_t StmtId) {

  const auto &StmtPrediction =
      apolloProg->PredictionModel->getMemPrediction(StmtId);
  const int MaxCoefficientPos = getMaxCoefIndex(StmtPrediction);
  const int64_t MaxCoefficient = StmtPrediction.LF[MaxCoefficientPos];

  const StaticMemInfo &StmtInfo = apolloProg->StaticInfo->Mem[StmtId];
  const uint64_t MaxCoefficientLoopId = StmtInfo.Parents[MaxCoefficientPos]->Id;
  const auto Iterbounds = apolloProg->ExtremeBounds;
  const MemRangeTy MemRange =
         getStmtRange(StmtId, Iterbounds, apolloProg, MaxCoefficientLoopId);
  // max coef loop bounds
  std::pair<int64_t, int64_t> Bounds = Iterbounds[MaxCoefficientLoopId];
  const int64_t Lower = Bounds.first;
  const int64_t Upper = Bounds.second;
  // distance between ranges of memory bigger than page_size
  bool HeuristicCondition = false;
  if (MaxCoefficient > 0) {
    HeuristicCondition = (MemRange.second + PageSizeInBytes) < 
                         (MemRange.first + MaxCoefficient);
  } else {
    HeuristicCondition = (MemRange.first - PageSizeInBytes) > 
                         (MemRange.second + MaxCoefficient);
  }
  std::vector<MemRangeTy> Ranges;
  const MemRangeTy ExactRange = apolloProg->ExtremeMemRanges.at(StmtId);
  if (HeuristicCondition) {
    for (long iterator = Lower; iterator < Upper; ++iterator) {
      const int64_t Step = MaxCoefficient * iterator;
      MemRangeTy MemRangeToBackup = std::make_pair<MemTy, MemTy>(
          MemRange.first + Step, MemRange.second + Step);
      MemRangeToBackup.first =
          std::min<int64_t>(std::max<int64_t>(MemRange.first, ExactRange.first),
                            ExactRange.second);
      MemRangeToBackup.second = std::min<int64_t>(
          std::max<int64_t>(MemRange.second, ExactRange.first),
          ExactRange.second);
      
      if (MemRangeToBackup.first != MemRangeToBackup.second)
        Ranges.push_back(MemRangeToBackup);
    }
  } else {
    Ranges.push_back(ExactRange);
  }
  return Ranges;
}

static jmp_buf Context;

static void sigHandler(int Id) {
  // avoid infinite loop
  longjmp(Context, 1);
}

static bool apolloRunBackupCheck(const std::vector<MemTy> &Inputs,
                                 bool IsWrite) {
  struct sigaction Sa;
  memset(&Sa, 0, sizeof(struct sigaction));
  Sa.sa_handler = sigHandler;
  Sa.sa_flags = SA_RESTART;
  sigaction(SIGSEGV, &Sa, NULL);
  if (setjmp(Context)) {
    // If this one was on setjmp's block, it would need to be volatile,
    // to make sure the compiler reloads it.
    sigset_t Ss;
    //  Make sure to unblock SIGSEGV, according to POSIX it gets blocked
    //  when calling its signal handler. sigsetjmp()/siglongjmp would
    //  make this unnecessary. */
    sigemptyset(&Ss);
    sigaddset(&Ss, SIGSEGV);
    sigprocmask(SIG_UNBLOCK, &Ss, NULL);
    // reset the signal handler
    Sa.sa_handler = SIG_DFL;
    sigaction(SIGSEGV, &Sa, NULL);
    return false;
  } else {
    // A hack: When backing up memory:-
    // Assume that the predicted upper bound address was allocated as a read
    // only area. The memcpy will succeed as we are only trying to read from
    // that area now. But in the original code or in restore it will cause a
    // segfault, when we try to write to that location.

    // To prevent this we are just trying to write to the upper bound location,
    // the value of the upper bound location (yes it is x = x). In order to
    // prevent optimizations which eliminate the useless load and store we are
    // declaring the hack_write variable as volatile.

    // The same with the lower bound
    for (auto input : Inputs) {
      volatile int Hack = 0;
      char *PtrHack = (char *)input + Hack;
      char *Ptr = (char *)input;
      if (IsWrite)
        *Ptr = *PtrHack;
      else {
        // this if will never execute. This stmt is just
        // to ensure that the compiler wont optimize the read stmts above
        if (*Ptr != *PtrHack)
          return false;
      }
    }
  }
  // reset the signal handler
  Sa.sa_handler = SIG_DFL;
  sigaction(SIGSEGV, &Sa, NULL);
  return true;
}

/// \brief Find all the (id of) statements which needs to be backed up
/// \param apolloProg : The apollo program
/// \param isWrite: a flag indicating if is write statements or read statements
/// \return a vector containing the stmt ids.(sorted)
static std::vector<uint64_t> getStmtsToBackup(const APOLLO_PROG *apolloProg, 
                                              const int Ty) {
  const bool Write = (Ty & BackupStmtType::Write) != 0;
  const bool Read = (Ty & BackupStmtType::Read) != 0;
  std::vector<uint64_t> IdsStmts;
  if (Write) {
    for (StaticMemInfo *stmt_info : apolloProg->StaticInfo->WriteMem) {
      const auto &Prediction =
          apolloProg->PredictionModel->getMemPrediction(stmt_info->Id);
      const bool IsSolved = Prediction.isLinear();
      if (IsSolved)
        IdsStmts.push_back(stmt_info->Id);
      const bool IsTube = PredictionModel::isTube(Prediction.PreType);
      if (IsTube)
        IdsStmts.push_back(stmt_info->Id);
    }
  }

  if (Read) {
    for (StaticMemInfo *stmt_info : apolloProg->StaticInfo->ReadMem) {
      const auto &Prediction =
          apolloProg->PredictionModel->getMemPrediction(stmt_info->Id);
      const bool IsSolved = Prediction.isLinear();
      if (IsSolved)
        IdsStmts.push_back(stmt_info->Id);
      const bool IsTube = PredictionModel::isTube(Prediction.PreType);
      if (IsTube)
        IdsStmts.push_back(stmt_info->Id);
    }
  }
  return IdsStmts;
}

bool checkMemoryExtremes(const APOLLO_PROG *apolloProg) {
  traceEnter("check_memory_extremes");
  const std::vector<uint64_t> IdsBackupStmts = std::move(getStmtsToBackup(
      apolloProg, BackupStmtType::Read | BackupStmtType::Write));

  for (auto stmt_id : IdsBackupStmts) {
    const MemRangeTy Range = apolloProg->ExtremeMemRanges[stmt_id];
    // MemRangeTy OverlapsWith.
    if (MemoryProtection::MemProtect->overlapsApolloMemory(Range)) {
      traceExit(backdoorutils::concat("check_memory_extremes = fail(stmt=",
                                      stmt_id,
                                      " overlaps with apollo memory)"));
      return false;
    }
    if (!apolloRunBackupCheck({Range.first, Range.second},
                              apolloProg->StaticInfo->Mem[stmt_id].IsWrite)) {
      traceExit(backdoorutils::concat("check_memory_extremes = fail(stmt=",
                                      stmt_id, " segfaults)"));
      return false;
    }
    // TODO we should use this after improving 'memory_runtime_verify'
    // apolloProg->AllocatedMemory.meoryRange(range.first, range.second);
  }
  traceExit("check_memory_extremes = success");
  return true;
}

bool doBackup(APOLLO_PROG *apolloProg) {
  traceEnter("do_backup");
  eventStart("backup");
  apolloProg->BackupContainerObj.clear();
  const std::vector<uint64_t> IdsWriteStmts =
        std::move(getStmtsToBackup(apolloProg, BackupStmtType::Write));

  // for each write find the range of memory it will access and call backup
  for (auto stmt_id : IdsWriteStmts) {
    const std::vector<MemRangeTy> Ranges =
          std::move(getBackupRanges(apolloProg, stmt_id));
    for (auto &range : Ranges) {
      apolloProg->BackupContainerObj.addRange(range.first, range.second);
    }
  }
  const bool Succeed = apolloProg->BackupContainerObj.doBackup();

  eventEnd("backup");
  traceExit("do_backup");

  return Succeed;
}

void clearBackup(APOLLO_PROG *apolloProg) {
  traceEnter("clear_backup");
  if (apolloProg->ShouldRollback) {
    traceEnter("rollback");
    eventStart("rollback");
    apolloProg->BackupContainerObj.rollback();
    apolloProg->ShouldRollback = false;
    eventEnd("rollback");
    traceExit("rollback");
  }
  apolloProg->BackupContainerObj.clear();
  traceExit("clear_backup");
}
