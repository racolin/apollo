//===--- Remapper.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//     Manuel Selva                  <manuel.selva@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include <ProfilingPlugin/ReIndexPhasesDetector.h>
#include "Backdoor/Backdoor.h"

ReIndexPhasesDetector::ReIndexPhasesDetector(bool full) :
		full(full), CurrentPhaseId(0), CurrentPhase(
		NULL), idxMap() {
}

std::shared_ptr<PredictionModel> ReIndexPhasesDetector::buildNewPredictionModel(
		const InstrumentationResult& Result, unsigned int LowerBound,
		unsigned int UpperBound, bool& reIndexed) {

	traceEnter(
			backdoorutils::concat(
					"ProfilingPluginLinearize::buildNewPredictionModel(",
					LowerBound, ":", (UpperBound - 1), ")"));

	// Build prediction model in the standard APOLLO way
	std::shared_ptr<PredictionModel> PM =
			PredictionModel::buildFromInstrumentation(Result, false);

	// Try to re index if the standard APOLLO way failed
	idxMap.clear();
	std::stringstream out;
	if (PM->hasNonPredictedEntities()) {
		PM->reIndexMemAccesses(Result, idxMap);
		reIndexed = true;
		if (PM->hasNonPredictedMemStatements()) {
			PM = NULL;
		}
	}

	traceExit(
			backdoorutils::concat(
					"ProfilingPluginLinearize::buildNewPredictionModel(",
					LowerBound, ":", (UpperBound - 1), ") -> ", out.str()));
	return PM;
}

void ReIndexPhasesDetector::InstrumentationChunkCompleted(
		unsigned int LowerBound, unsigned int UpperBound,
		const InstrumentationResult &Result) {

	traceEnter(
			backdoorutils::concat(
					"ProfilingPluginLinearize::InstrumentationChunkCompleted(",
					LowerBound, ":", (UpperBound - 1), ") with ",
					Result.getEventsCount(), " events"));

	// Start a new phase if we don't have one yet
	if (CurrentPhase == NULL) {
		bool reIndexed = false;
		std::shared_ptr<PredictionModel> PM = buildNewPredictionModel(Result,
				LowerBound, UpperBound, reIndexed);
		CurrentPhase = new ProfilingPhase(CurrentPhaseId++, LowerBound,
				UpperBound, PM, reIndexed);
	}

	// Else
	else {

		// If we are in a phase non linear, try to build a new linear model
		if (CurrentPhase->PM == NULL) {
			bool reIndexed = false;
			std::shared_ptr<PredictionModel> PM = buildNewPredictionModel(
					Result, LowerBound, UpperBound, reIndexed);
			if (PM == NULL) {
				CurrentPhase->upper = UpperBound;
			} else {
				ProfilingPhase* NewPhase = new ProfilingPhase(CurrentPhaseId++,
						LowerBound, UpperBound, PM, reIndexed);
				CurrentPhase->dump(full);
				delete (CurrentPhase);
				CurrentPhase = NewPhase;
			}
		}

		// Else we check if the current phase is still valid
		else {
			bool stillValid = CurrentPhase->PM->isStillValid(Result, idxMap);
			if (stillValid) {
				CurrentPhase->upper = UpperBound;
			} else {
				bool reIndexed = false;
				std::shared_ptr<PredictionModel> PM = buildNewPredictionModel(
						Result, LowerBound, UpperBound, reIndexed);
				ProfilingPhase * NewPhase = new ProfilingPhase(CurrentPhaseId++,
						LowerBound, UpperBound, PM, reIndexed);
				CurrentPhase->dump(full);
				delete (CurrentPhase);
				CurrentPhase = NewPhase;
			}
		}
	}

//	// If PM is NULL we build a prediction model
//	std::stringstream out;
//	if (PM == NULL) {
//		out << "failed to keep previous empty model. ";
//		buildNewPredictionModel(Result, LowerBound, UpperBound);
//		if (PM == NULL) {
//			out
//					<< "failed to build a new linear or tube model even after linearization";
//		} else {
//			out << "succeeded to build " << PM->GetMemStmtsStatus() << " model";
//		}
//	}
//
//	// Else we check if the prediction model is still valid for the new events
//	else {
//		bool stillValid = PM->isStillValid(Result, ReindexMaps);
//		if (stillValid) {
//			out << "succeeded to keep previous " << PM->GetMemStmtsStatus()
//					<< " model";
//		} else {
//			out << "failed to keep previous " << PM->GetMemStmtsStatus()
//					<< " model. ";
//			buildNewPredictionModel(Result, LowerBound, UpperBound);
//			if (PM == NULL) {
//				out
//						<< "failed to build a new linear or tube model even after linearization";
//			} else {
//				out << "succeeded to build " << PM->GetMemStmtsStatus()
//						<< " model";
//			}
//		}
//	}

	traceExit(
			backdoorutils::concat(
					"ProfilingPluginLinearize::InstrumentationChunkCompleted(",
					LowerBound, ":", (UpperBound - 1), ")"));
}

void ReIndexPhasesDetector::PlutoCompleted(apollo::ScopPtrTy origScop,
		apollo::ScopPtrTy txScop) {
	if (CurrentPhase->origScop == NULL) {
		CurrentPhase->origScop = osl_scop_clone(origScop->Computation);
		CurrentPhase->txScop = osl_scop_clone(txScop->Computation);
	}
}

void ReIndexPhasesDetector::LoopCompleted() {
	if (CurrentPhase != NULL) {
		CurrentPhase->dump(full);
		CurrentPhaseId = 0;
		CurrentPhase = NULL;
	}
}

