//===--- LinearPhasesDetector.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//     Manuel Selva                  <manuel.selva@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ProfilingPlugin/ProfilingPhase.h"
#include <osl/osl.h>
#include <iomanip>

ProfilingPhase::ProfilingPhase(unsigned long int id_, unsigned long int lower_,
		unsigned long int upper_, std::shared_ptr<PredictionModel> PM_,
		bool reIndexed_) :
		id(id_), lower(lower_), upper(upper_), PM(PM_), origScop(NULL), txScop(
		NULL), reIndexed(reIndexed_) {
}

void ProfilingPhase::dump(bool full) {
	std::cout << (full ? "Begin " : "") << "phase " << std::setw(6) << std::left
			<< this->id << ": from " << std::setw(10) << std::left
			<< this->lower << " to " << std::setw(10) << std::left
			<< this->upper << ": ";
	if (this->PM == NULL) {
		std::cout << "non linear " << (reIndexed ? "after re-indexing" : "")
				<< std::endl;
		if (!full) {
			return;
		}
	} else {
		std::cout << "linear ";
		if (this->PM->hasTubePredictions()) {
			int i = 0;
			std::cout << "with tubes (for ";
			if (this->PM->hasTubeMemStatements()) {
				std::cout << "memory accesses";
				i = 1;
			}
			if (this->PM->hasTubeBounds()) {
				if (i != 0) {
					std::cout << " and ";
				}
				std::cout << "bounds";
				i = 2;
			}
			if (this->PM->hasTubeScalars()) {
				if (i != 0) {
					std::cout << " and ";
				}
				std::cout << "scalars";
			}
			std::cout << ") ";
		}
		std::cout << (reIndexed ? "after re-indexing" : "") << std::endl;

		// Dump parallelism information if available
		// It can be NULL only with ReIndex plugin
		if (txScop != NULL) {
			osl_loop_p Loop = (osl_loop_p) osl_generic_lookup(txScop->extension,
					OSL_URI_LOOP);
			while (Loop) {
				int Directive = Loop->directive;
				bool Vectorize = Directive == OSL_LOOP_DIRECTIVE_VECTOR;
				bool Parallel = Directive == OSL_LOOP_DIRECTIVE_PARALLEL;
				std::string oldName = std::string(Loop->iter);
				size_t last_index = oldName.find_last_not_of("0123456789");
				std::string newName = "loop " + oldName.substr(last_index + 1);
				if (!Vectorize && !Parallel) {
					std::cout << newName
							<< " is neither vectorized nor parallelized"
							<< std::endl;
				}
				if (Vectorize && !Parallel) {
					std::cout << newName << " is vectorized" << std::endl;
				}
				if (!Vectorize && Parallel) {
					std::cout << newName << " is parallelized" << std::endl;
				}
				if (Vectorize && Parallel) {
					std::cout << newName << " is vectorized and parallelized"
							<< std::endl;
				}
				Loop = Loop->next;
			}

			if (full) {

				// Dump scops
				std::cout << std::endl << "Scop before optimization: "
						<< std::endl;
				osl_scop_print(stdout, origScop);
				std::cout << std::endl << "Scop after optimization: "
						<< std::endl;
				osl_scop_print(stdout, txScop);
			}
		}
	}
	if (full) {
		std::cout << "End phase " << std::endl;
	}
}

