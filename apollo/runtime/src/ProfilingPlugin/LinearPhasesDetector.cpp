//===--- LinearPhasesDetector.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//     Manuel Selva                  <manuel.selva@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "ProfilingPlugin/LinearPhasesDetector.h"
#include "Backdoor/Backdoor.h"
# include <osl/osl.h>

LinearPhasesDetector::LinearPhasesDetector(bool full) :
		full(full), CurrentPhaseId(0), CurrentPhase(
		NULL) {
}

LinearPhasesDetector::~LinearPhasesDetector() {
	CurrentPhase->dump(full);
}

void LinearPhasesDetector::InstrumentationChunkCompleted(
		unsigned int LowerBound, unsigned int UpperBound,
		const InstrumentationResult &Result) {

	traceEnter(
			backdoorutils::concat(
					"LinearPhasesDetector::InstrumentationChunkCompleted(",
					LowerBound, ":", UpperBound));
	std::string exitString;

	// Start a new phase if we don't have one yet
	if (CurrentPhase == NULL) {
		std::shared_ptr<PredictionModel> PM =
				PredictionModel::buildFromInstrumentation(Result, false);
		if (PM->hasNonPredictedEntities()) {
			PM = NULL;
		}
		CurrentPhase = new ProfilingPhase(CurrentPhaseId++, LowerBound,
				UpperBound, PM);
		exitString = "first phase created";
	}

	// Else
	else {

		// If we are in a phase non linear, try to build a new linear model
		if (CurrentPhase->PM == NULL) {
			std::shared_ptr<PredictionModel> PM =
					PredictionModel::buildFromInstrumentation(Result, false);
			if (PM->hasNonPredictedEntities()) {
				CurrentPhase->upper = UpperBound;
				exitString = "still in non linear phase";
			} else {
				ProfilingPhase* NewPhase = new ProfilingPhase(CurrentPhaseId++,
						LowerBound, UpperBound, PM);
				CurrentPhase->dump(full);
				delete (CurrentPhase);
				CurrentPhase = NewPhase;
				exitString = "left non linear phase to a new linear one";
			}
		}

		// Else we check if the current phase is still valid
		else {
			bool stillValid = CurrentPhase->PM->isStillValid(Result);
			if (stillValid) {
				CurrentPhase->upper = UpperBound;
				exitString = "still in same linear phase";
			} else {
				std::shared_ptr<PredictionModel> PM =
						PredictionModel::buildFromInstrumentation(Result,
								false);
				if (PM->hasNonPredictedEntities()) {
					PM = NULL;
					exitString = "left linear phase to a new linear one";
				} else {
					exitString = "left linear phase to a new non linear one";
				}

				ProfilingPhase * NewPhase = new ProfilingPhase(CurrentPhaseId++,
						LowerBound, UpperBound, PM);
				CurrentPhase->dump(full);
				delete (CurrentPhase);
				CurrentPhase = NewPhase;
			}
		}
	}

	traceExit(
			backdoorutils::concat(
					"LinearPhasesDetector::InstrumentationChunkCompleted(",
					LowerBound, ":", UpperBound, " -> ", exitString));
}

void LinearPhasesDetector::PlutoCompleted(apollo::ScopPtrTy origScop,
		apollo::ScopPtrTy txScop) {
	if (CurrentPhase->origScop == NULL) {
		CurrentPhase->origScop = osl_scop_clone(origScop->Computation);
		CurrentPhase->txScop = osl_scop_clone(txScop->Computation);
	}
}

void LinearPhasesDetector::LoopCompleted() {
	if (CurrentPhase != NULL) {
		CurrentPhase->dump(full);
		CurrentPhaseId = 0;
		CurrentPhase = NULL;
	}
}

