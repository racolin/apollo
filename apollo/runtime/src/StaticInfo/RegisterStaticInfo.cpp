//===--- RegisterStatic.cpp -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "StaticInfo/StaticInfo.h"
#include "StaticInfoConfig.h"

#include <cstdarg>
#include <cassert>
#include <iostream>

extern "C" void apollo_register_static_info(StaticNestInfo *Info,
                                            StaticInfoType Type, 
                                            const long Id, 
                                            const long Num,
                                            ... /*data*/) {
  if (!Info)
    return;

  va_list ValueList;
  va_start(ValueList, Num);
  long Data[Num];
  for (long i = 0; i < Num; i++) {
    Data[i] = va_arg(ValueList, long);
  }
  va_end(ValueList);

  switch (Type) {
  case StaticInfoType::Init: {
    assert(Num == 3);
    Info->registerInit(Data[0], Data[1], Data[2]);
    return;
  }
  case StaticInfoType::LoopParents: {
    Info->registerLoopParents(Id, Num, Data);
    return;
  }
  case StaticInfoType::ScalarParents: {
    assert(Num == 1);
    Info->registerScalarParents(Id, Data);
    return;
  }
  case StaticInfoType::StmtInfo: {
    assert(Num == 4);
    Info->registerMemInfo(Id, Data);
    return;
  }
  case StaticInfoType::StmtNoAlias: {
    Info->registerMemNoAlias(Id, Num, Data);
    return;
  }
  case StaticInfoType::ParamsSize: {
    assert(Num == 1);
    Info->registerParamsSize(Data[0]);
    return;
  }
  default:
    std::cerr << "WARNING! apolloRegisterStaticInfo:"
             << "unkown static info type!\n";
    return;
  }
  return;
}

