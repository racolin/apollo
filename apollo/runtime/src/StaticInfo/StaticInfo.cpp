//===--- StaticInfo.cpp ---------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "StaticInfo/StaticInfo.h"
#include "Backdoor/Backdoor.h"

#include <iostream>

void StaticNestInfo::registerInit(const long NumLoops, 
                                  const long NumScalars,
                                  const long NumStmts) {

  traceEnter("StaticNestInfo::register_init()");

  for (int i = 0; i < NumLoops; ++i) {
    Loops.emplace_back(i);
  }
  for (StaticLoopInfo &loop : Loops) {
    InnermostLoops.insert(&loop); // all loops start as innermost.
  }
  for (int i = 0; i < NumScalars; ++i) {
    Scalars.emplace_back(i);
  }
  for (int i = 0; i < NumStmts; ++i) {
    Mem.emplace_back(i);
  }

  traceExit("StaticNestInfo::register_init()");
}

void StaticNestInfo::registerLoopParents(const long LoopId, 
                                         const long Num, 
                                         long *Parents) {

  auto &LoopParents = Loops[LoopId].Parents;
  for (int parent_idx = 0; parent_idx < Num; ++parent_idx) {
    StaticLoopInfo &ParentLoop = Loops[Parents[parent_idx]];
    LoopParents.push_back(&ParentLoop);
    if (parent_idx < Num - 1)
      InnermostLoops.erase(&ParentLoop);
  }

  MaxLoopDepth = std::max<int>(MaxLoopDepth, Loops[LoopId].depth());
  if (LoopId != 0) {
    const int InmediateParent = Parents[Num - 2];
    Loops[InmediateParent].Children.push_back(&Loops[LoopId]);
  }
}

void StaticNestInfo::registerScalarParents(const long ScalarId, long *Parents) {
  const int ParentLoop = Parents[0];
  auto &ScalarParents = Scalars[ScalarId].Parents;
  ScalarParents = Loops[ParentLoop].Parents;
  Loops[ParentLoop].Scalars.push_back(&Scalars[ScalarId]);
}

void StaticNestInfo::registerMemInfo(const long StmtId, long *Data) {
  const int Size = Data[0];
  const bool IsWrite = Data[1];
  const int ParentLoopId = Data[2];
  const int NonLinearLevel = Data[3];

  StaticMemInfo &Stmt = Mem[StmtId];
  Stmt.Parents = Loops[ParentLoopId].Parents;
  Stmt.IsWrite = IsWrite;
  Stmt.SizeInBytes = Size;

  if (NonLinearLevel != -1)
    Stmt.NonLinearLoop = &Loops[NonLinearLevel];

  StaticLoopInfo *ParentLoop = Stmt.Parents.back();
  ParentLoop->Stmts.push_back(&Stmt);

  if (ParentLoop->isInnermost()) {
    for (StaticLoopInfo *a_parent_loop : Stmt.Parents) {
      if (a_parent_loop->InnermostMemId == -1)
        a_parent_loop->InnermostMemId = Stmt.Id;
    }
  }

  if (IsWrite)
    WriteMem.push_back(&Stmt);
  else
    ReadMem.push_back(&Stmt);
}

void StaticNestInfo::registerMemNoAlias(const long StmtId, const long Num, 
                                        long *Data) {
  StaticMemInfo &Stmt = Mem[StmtId];
  for (int i = 0; i < Num; ++i) {
    const int NoAliasStmtId = Data[i];
    Stmt.NoAlias.insert(&Mem[NoAliasStmtId]);
  }
}

void StaticNestInfo::registerParamsSize(const long Size) { 
  this->ParamsSize = Size; 
}

void StaticNestInfo::dump(std::ostream &OS) {
  OS << "StaticNestInfo [\n";
  for (StaticLoopInfo &loop : Loops) {
    OS << "\tloop " << loop.Id << " : .parents=< ";
    for (StaticLoopInfo *parent_loop : loop.Parents) {
      OS << parent_loop->Id << " ";
    }
    OS << "> .an_innermost_stmt=" << loop.InnermostMemId << "\n";
  }

  for (StaticScalarInfo &scalar : Scalars) {
    OS << "\tscalar " << scalar.Id << " : .parent=" << scalar.getParentId()
      << "\n";
  }

  for (StaticMemInfo &stmt : Mem) {
    OS << "\tstmt[" << (stmt.IsWrite ? "write" : "read") << "] " << stmt.Id
      << " : .parent=" << stmt.getParentId() << " .size=" << stmt.SizeInBytes
       << " ";

    if (!stmt.NonLinearLoop)
      OS << "affine ";
    else
      OS << "non-affine at loop " << stmt.NonLinearLoop->Id << " ";
    OS << ".no_alias=< ";

    for (StaticMemInfo *no_alias : stmt.NoAlias) {
      OS << no_alias->Id << " ";
    }
    OS << ">\n";
  }
  OS << "\tparams_size = " << ParamsSize << "\n";
  OS << "]";
}

