//===--- Instrumentation.h ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef INSTRUMENTATION_H
#define INSTRUMENTATION_H

#include "StaticInfo/StaticInfo.h"

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cassert>
#include <cmath>

/// \brief This class contains the collected values from dynamic
/// instrumentation and information collected statically.
///
/// Statically collected information comes from:
///
/// - apollo_register_static_info functions calls inserted by static passes
///   in the skeleton_0 (static_info) function called by runtime when
///   instrumentation chunks starts
///
/// - apollo_register_event function calls (with static parameters)
///   inserted by static passes in the skeleton_1 (instrumentation)
///   function. Before starting to loop with instrumentation on,
///   the skeleton_1 function register static events computed statically.
///
/// Dynamically collected information comes from:
///
/// - apollo_register_event function calls (with dynamic parameters)
///   inserted by static passes in the skeleton_1 (instrumentation)
///   function. Compared to static events, these calls are made all
///   along the run of the loop with instrumentation. The frequency
///   of these calls compare to the real events (memory accesses, ...)
///   depends on the sampling frequency parameters.
///
class InstrumentationResult {
public:
  struct InstrumentationEvent {
    const static size_t MaxLoopDepth = 6;
    enum class InstrumentationEventType : char {
      Memory,
      Scalar,
      Bound,
      FunctionCall
    };
    enum class InstrumentationType : char { StaticEvent, DynamicEvent };

    unsigned short Id;
    InstrumentationEventType EventType;
    InstrumentationType InstrType;

    // for dynamic events it stores the vi-values
    // for static the coefficients
    long Coefs[MaxLoopDepth];

    // for dynamic events it stores the effective value of the access, scalar, bound
    // for static it is always 0
    long Value;

    std::string typeToString() const {
      if (InstrType == InstrumentationType::StaticEvent)
        return "static";
      if (InstrType == InstrumentationType::DynamicEvent)
        return "dynamic";
      assert(false);
      return "";
    }

    std::string eventTypeToString() const {
      if (EventType == InstrumentationEventType::Memory)
        return "memory";
      if (EventType == InstrumentationEventType::Scalar)
        return "scalar";
      if (EventType == InstrumentationEventType::Bound)
        return "bound";
      if (EventType == InstrumentationEventType::FunctionCall)
        return "function_call";
      assert(false);
      return "";
    }

    void dump(std::ostream &OS) const {
      OS << "{ id=" << Id << ", value=" << Value
        << ", type=" << typeToString() << "-"
         << eventTypeToString() << ", < ";

      for (size_t v = 0;
           v < InstrumentationEvent::MaxLoopDepth && Coefs[v] != -1; ++v) {
        OS << Coefs[v] << " ";
      }
      OS << "> }";
    }
  };

private:
  InstrumentationResult(){}; // no default constructor
  //std::vector<const InstrumentationEvent *> Events;
  std::vector<std::vector<InstrumentationEvent>> Mems;
  std::vector<std::vector<InstrumentationEvent>> Scalars;
  std::vector<std::vector<InstrumentationEvent>> Bounds;
  std::vector<InstrumentationEvent> FunCalls;

public:
  InstrumentationResult(StaticNestInfo *Nest, int InstChunkSize) {
    //size_t MaxEventNum = 0;

    std::vector<InstrumentationEvent> EmptyVector;
    Mems = std::vector<std::vector<InstrumentationEvent>>(Nest->Mem.size(),
                                                          EmptyVector);
    Scalars = std::vector<std::vector<InstrumentationEvent>>(
        Nest->Scalars.size(), EmptyVector);
    Bounds = std::vector<std::vector<InstrumentationEvent>>(Nest->Loops.size(),
                                                            EmptyVector);
//    for (StaticMemInfo &mem : Nest->Mem) {
//      std::vector<InstrumentationEvent> &EventVec = Mems[mem.Id];
//      const size_t MaxEvents = std::pow(InstChunkSize, mem.Parents.size()) + 1;
//      //MaxEventNum += MaxEvents;
//      EventVec.reserve(MaxEvents);
//    }
//
//    for (StaticScalarInfo &scalar : Nest->Scalars) {
//      std::vector<InstrumentationEvent> &EventVec = Scalars[scalar.Id];
//      const size_t MaxEvents = std::pow(InstChunkSize, scalar.Parents.size()) + 1;
//      //MaxEventNum += MaxEvents;
//      EventVec.reserve(MaxEvents);
//    }
//
//    for (StaticLoopInfo &loop : Nest->Loops) {
//      std::vector<InstrumentationEvent> &EventVec = Bounds[loop.Id];
//      const size_t MaxEvents = std::pow(InstChunkSize, loop.Parents.size() - 1) + 1;
//      //MaxEventNum += MaxEvents;
//      EventVec.reserve(MaxEvents);
//    }
//    //Events.reserve(MaxEventNum);
  }

  void dump(std::ostream &OS) const {
    OS << "IntrumentationResult : dump() [\n";
//    for (size_t i = 0; i < Events.size(); ++i) {
//      auto Ev = Events[i];
//      OS << "\t" << i << " : ";
//      Ev->dump(OS);
//      OS << "\n";
//    }
    int j = 0;
    for (size_t i = 0; i < Mems.size(); ++i) {
    	for (InstrumentationEvent Ev : Mems[i]) {
			OS << "\t" << j++ << " : ";
			Ev.dump(OS);
			OS << "\n";
    	}
      }
    for (size_t i = 0; i < Scalars.size(); ++i) {
    	for (InstrumentationEvent Ev : Scalars[i]) {
			OS << "\t" << j++ << " : ";
			Ev.dump(OS);
			OS << "\n";
    	}
      }
    for (size_t i = 0; i < Bounds.size(); ++i) {
    	for (InstrumentationEvent Ev : Bounds[i]) {
			OS << "\t" << j++ << " : ";
			Ev.dump(OS);
			OS << "\n";
    	}
      }
    for (size_t i = 0; i < FunCalls.size(); ++i) {
        auto Ev = FunCalls[i];
        OS << "\t" << j++ << " : ";
        Ev.dump(OS);
        OS << "\n";
      }


    OS << "]";
  }

  const InstrumentationEvent
  createEvent(const int Id, 
                InstrumentationEvent::InstrumentationType InstrType,
                InstrumentationEvent::InstrumentationEventType EventType,
                const long Value, long *Vis, const int nVis) {
	if (EventType != InstrumentationEvent::InstrumentationEventType::FunctionCall) {
		assert(Vis);
	}
    assert(0 <= nVis);
    assert((size_t)nVis <= InstrumentationEvent::MaxLoopDepth);

    InstrumentationEvent Event;
    Event.Id = Id;
    Event.InstrType = InstrType;
    Event.EventType = EventType;
    Event.Value = Value;
    std::copy(Vis, Vis + nVis, Event.Coefs);
    std::fill(Event.Coefs + nVis,
              Event.Coefs + InstrumentationEvent::MaxLoopDepth, -1);

    return Event;
  }

  void registerFunCall() {
    FunCalls.emplace_back(this->createEvent(
        0, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::FunctionCall, 0,
        nullptr, 0));
    //Events.push_back(&FunCalls.back());
  }

  void registerDynamicMemAccess(const int Id, void *Addr, long *Vis, const int nVis) {
    Mems[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::Memory, (long)Addr, Vis,
        nVis));
    //Events.push_back(&Mems[Id].back());
  }

  void registerDynamicScalar(const int Id, const long Value, 
                      long *Vis, const int nVis) {
    Scalars[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::Scalar, Value, Vis,
        nVis));
    //Events.push_back(&Scalars[Id].back());
  }

  void registerDynamicBound(const int Id, const long Iters, 
                     long *Vis, const int nVis) {
    Bounds[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::DynamicEvent,
        InstrumentationEvent::InstrumentationEventType::Bound, Iters, Vis,
        nVis));
    //Events.push_back(&Bounds[Id].back());
  }

  void registerStaticMemAccess(const int Id, long *Coefs, const int nCoef) {
    Mems[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::StaticEvent,
        InstrumentationEvent::InstrumentationEventType::Memory, 0, Coefs,
        nCoef));
    //Events.push_back(&Mems[Id].back());
  }

  void registerStaticScalar(const int Id, long *Coefs, const int nCoef) {
    Scalars[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::StaticEvent,
        InstrumentationEvent::InstrumentationEventType::Scalar, 0, Coefs,
        nCoef));
    //Events.push_back(&Scalars[Id].back());
  }

  void registerStaticBound(const int Id, long *Coefs, const int nCoef) {
    Bounds[Id].emplace_back(this->createEvent(
        Id, InstrumentationEvent::InstrumentationType::StaticEvent,
        InstrumentationEvent::InstrumentationEventType::Bound, 0, Coefs,
        nCoef));
    //Events.push_back(&Bounds[Id].back());
  }

//  const std::vector<const InstrumentationEvent *> &getEvents() const {
//    return Events;
//  }

  const long getEventsCount() const {
	  	long count = 0;
		for (size_t i = 0; i < Mems.size(); ++i) {
			count += Mems[i].size();
		}
		for (size_t i = 0; i < Scalars.size(); ++i) {
			count += Scalars[i].size();
		}
		for (size_t i = 0; i < Bounds.size(); ++i) {
			count += Bounds[i].size();
		}
		count += FunCalls.size();
		return count;
	}

  const long getMemEventsCount() const {
	  	long count = 0;
		for (size_t i = 0; i < Mems.size(); ++i) {
			count += Mems[i].size();
		}
		return count;
	}

  const std::vector<InstrumentationEvent> &getMemEvents(const int Id) const {
    return Mems[Id];
  }

  const std::vector<InstrumentationEvent> &getScalarEvents(const int Id) const {
    return Scalars[Id];
  }

  const std::vector<InstrumentationEvent> &getBoundEvents(const int Id) const {
    return Bounds[Id];
  }

  const std::vector<InstrumentationEvent> &getFunCallsEvents() const {
    return FunCalls;
  }
};

#endif

