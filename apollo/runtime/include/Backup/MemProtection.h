//===--- MemProtection.h --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef MEMORY_PROTECTION_H
#define MEMORY_PROTECTION_H

#include "Common.h"
#include "Backup/Pool.h"

#include <cassert>
#include <mutex>
#include <iostream>

class MemPool;
extern MemPool Pool8;
extern MemPool Pool16;

class MemoryProtection {
  std::mutex Mtx;
  static const size_t MaxRanges = 512;
  const size_t MergeThreashold = 512;
  size_t NumRanges;
  MemRangeTy Ranges[MaxRanges + 1];

public:
  static MemoryProtection *MemProtect;
  void registerRange(const size_t Size, void *Ptr);
  bool overlapsApolloMemory(const intptr_t First, const uintptr_t Second) {
    MemRangeTy Range;
    Range.first = First;
    Range.second = Second;
    return this->overlapsApolloMemory(Range);
  }

  bool overlapsApolloMemory(const MemRangeTy &A,
                            MemRangeTy *OverlapsWith = nullptr);

  template <typename T> 
  void registerObject(T &Ty) {
    this->registerRange(sizeof(T), &Ty);
  }

  MemoryProtection() {
    assert(MemoryProtection::MemProtect == nullptr);
    this->NumRanges = 0;
    MemoryProtection::MemProtect = this;
    // register itself
    this->registerObject(MemoryProtection::MemProtect);
    this->registerObject(*this);
    this->registerObject(Pool8);
    this->registerObject(Pool16);
    // register the memory pools
    this->registerRange(Pool8.BlockSize, Pool8.MemoryBlock);
    this->registerRange(Pool16.BlockSize, Pool16.MemoryBlock);
  }

  ~MemoryProtection() { 
    MemoryProtection::MemProtect = nullptr; 
  }
};

#endif
