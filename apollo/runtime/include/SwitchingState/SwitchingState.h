//===--- SwitchingState.h -------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef SWITCHING_STATE_H
#define SWITCHING_STATE_H

#include <iostream>

enum class ChunkResult {
  Success,
  RangeAnalysisFailed,
  TransformationNotFound,
  EarlyMissprediction,
  Missprediction
};

class SwitchingState {
public:
  enum class ApolloStateTypes {
    None,
    Original,
    Instrument,
    TxSelection,
    Optimized
  };

  using IterationCostTy = double;

private:
  long OptSize;
  long InstSize;
  long Lower;
  long Upper;
  long MaxBound;
  bool profilingMode;
  long profilingFrequency;

  ApolloStateTypes CurState;
  ApolloStateTypes PrevState;

  bool EnableOptimized; // to allow instrumentation only

public:
  SwitchingState(bool EnableOptimized_ = true);
  void registerMaxUpperBound(const long Bound);

  long maxChunkUpper() const;
  long chunkLower() const;
  long chunkUpper() const;
  long chunkSize() const;

  void retryChunk(ApolloStateTypes NextState, long RetryChunkSize);

  ApolloStateTypes getCurState() const;
  ApolloStateTypes getPrevState() const;
  ApolloStateTypes
  nextState(const ChunkResult &LastChunkResult = ChunkResult::Success,
            const long ExecutedUntil = -1);
  void dump(std::ostream &OS);

private:
  void advanceChunk(ApolloStateTypes NextState, long NextChunkSize);
};

#endif
