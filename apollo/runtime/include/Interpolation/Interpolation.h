//===--- Interpolation.h --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef INSTRUMENTATION_SOLVER_H
#define INSTRUMENTATION_SOLVER_H

#include <ProfilingPlugin/ReIndexMap.h>
#include "Instrumentation/Instrumentation.h"
#include "armadillo"

#include <memory>

struct APOLLO_PROG;

class PredictionModel {
public:
	using CoefTy = int64_t;
	using LinearFunctionTy = std::vector<CoefTy>;

	enum PredictionType {
		NotExecuted,
		NonLinear,
		Tube,
		DynamicLinear,
		ScalarSemiLinear,
		StaticLinear,
		DynamicReIndexed,
		TubeReIndexed
	};

	struct MemoryPrediction {
		PredictionType PreType;

		// The coefficients of the linear function if linear or tube
		// for a 3 depth loop nest i j k the address is computed by
		// @= LF[0]*i + LF[1]*j + LF[2]*k + LF[3]
		LinearFunctionTy LF;

		long TubeWidth = 0;
		double RC = 0.0;

		bool isConstant() const {
			return PredictionModel::isLinear(PreType)
					&& (0 == std::accumulate(++LF.rbegin(), LF.rend(), 0));
		}
		bool isLinear() const {
			return PredictionModel::isLinear(PreType);
		}
		bool isLinearAfterReIndexing() const {
			return PredictionModel::isLinearAfterReIndexing(PreType);
		}
		bool isTube() const {
			return PredictionModel::isTube(PreType);
		}
		bool isTubeAfterReIndexing() const {
			return PredictionModel::isTubeAfterReIndexing(PreType);
		}
		bool isLinearOrTube() const {
			return this->isLinear() || this->isTube();
		}
		bool isLinearOrTubeAfterReIndexing() const {
			return this->isLinearAfterReIndexing()
					|| this->isTubeAfterReIndexing();
		}
	};

	struct ScalarPrediction {
		PredictionType PreType;

		LinearFunctionTy LF;

		bool isLinear() const {
			return PredictionModel::isLinear(PreType);
		}
	};

	struct BoundPrediction {
		PredictionType PreType;
		LinearFunctionTy LF;

		long Max = 0;
		double RC = 0.0;

		bool isLinear() const {
			return PredictionModel::isLinear(PreType);
		}
		bool isTube() const {
			return PredictionModel::isTube(PreType);
		}
		bool isPredicted() const {
			return this->isLinear() || this->isTube();
		}
	};

private:
	std::vector<MemoryPrediction> Memory;
	std::vector<ScalarPrediction> Scalars;
	std::vector<BoundPrediction> Bounds;
	bool UnsafeCallDetected;
	bool UnpredictedDetected;

public:
	const MemoryPrediction &getMemPrediction(const int Idx) const {
		return Memory.at(Idx);
	}

	const ScalarPrediction &getScalarPrediction(const int Idx) const {
		return Scalars.at(Idx);
	}

	const BoundPrediction &getBoundPrediction(const int Idx) const {
		return Bounds.at(Idx);
	}

	bool anyUnpredicted() const {
		return UnpredictedDetected;
	}

	bool anyUnsafeCall() const {
		return UnsafeCallDetected;
	}

	static std::string predictionTypeToString(const PredictionType &PrTy) {
		if (PrTy == NotExecuted)
			return "not executed";
		if (PrTy == NonLinear)
			return "non linear";
		if (PrTy == DynamicLinear)
			return "dynamic linear";
		if (PrTy == DynamicReIndexed)
			return "linear after re-indexing";
		if (PrTy == ScalarSemiLinear)
			return "scalar semi linear";
		if (PrTy == StaticLinear)
			return "static linear";
		if (PrTy == Tube)
			return "tube";
		if (PrTy == TubeReIndexed)
			return "tube after re-indexing";
		assert(false);
		return "";
	}

	static bool isLinear(const PredictionType &PrTy) {
		return PrTy == DynamicLinear || PrTy == StaticLinear;
	}

	static bool isLinearAfterReIndexing(const PredictionType &PrTy) {
		return PrTy == DynamicReIndexed;
	}

	static bool isTube(const PredictionType &PrTy) {
		return PrTy == Tube;
	}

	static bool isTubeAfterReIndexing(const PredictionType &PrTy) {
		return PrTy == TubeReIndexed;
	}

	bool hasNonPredictedEntities() const {
		return this->anyUnsafeCall() || this->anyUnpredicted();
	}

	bool hasNonPredictedMemStatements() const {
		for (MemoryPrediction pred : Memory) {
			if (!(pred.isLinearOrTube() 
			        || pred.isLinearOrTubeAfterReIndexing())) {
				return true;
			}
		}
		return false;
	}

	bool hasTubeMemStatements() const {
		for (MemoryPrediction pred : Memory) {
			if (pred.isTube()) {
				return true;
			}
			if (pred.isTubeAfterReIndexing()) {
				return true;
			}
		}
		return false;
	}

	bool hasTubeBounds() const {
		for (BoundPrediction pred : Bounds) {
			if (pred.isTube()) {
				return true;
			}
		}
		return false;
	}

	bool hasTubeScalars() const {
		return false;
	}

	bool hasTubePredictions() const {
		return hasTubeMemStatements() || hasTubeBounds() || hasTubeScalars();
	}

	bool isStaticLinear() const {
		for (MemoryPrediction pred : Memory) {
			if (pred.PreType != PredictionModel::StaticLinear) {
				return false;
			}
		}
		return true;
	}

	bool isDynamicLinear() const {
		for (MemoryPrediction pred : Memory) {
			if (pred.PreType != PredictionModel::StaticLinear
					&& pred.PreType != PredictionModel::DynamicLinear) {
				return false;
			}
		}
		return true;
	}

	bool isTube() const {
		for (MemoryPrediction pred : Memory) {
			if (pred.PreType != PredictionModel::StaticLinear
					&& pred.PreType != PredictionModel::DynamicLinear
					&& pred.PreType != PredictionModel::Tube) {
				return false;
			}
		}
		return true;
	}

	bool isDynamicLinearAfterReIndexing() const {
		for (MemoryPrediction pred : Memory) {
			if (pred.PreType != PredictionModel::StaticLinear
					&& pred.PreType != PredictionModel::DynamicLinear
					&& pred.PreType != PredictionModel::Tube
					&& pred.PreType != PredictionModel::DynamicReIndexed) {
				return false;
			}
		}
		return true;
	}

	bool isTubeAfterReIndexing() const {
		for (MemoryPrediction pred : Memory) {
			if (pred.PreType != PredictionModel::StaticLinear
					&& pred.PreType != PredictionModel::DynamicLinear
					&& pred.PreType != PredictionModel::Tube
					&& pred.PreType != PredictionModel::DynamicReIndexed
					&& pred.PreType != PredictionModel::TubeReIndexed) {
				return false;
			}
		}
		return true;
	}

	std::string GetMemStmtsStatus() {
		if (isStaticLinear()) {
			return "static-linear";
		} else if (isDynamicLinear()) {
			return "dynamic-linear";
		} else if (isTube()) {
			return "tube";
		} else if (isDynamicLinearAfterReIndexing()) {
			return "dynamic-linear-after-re-indexing";
		} else if (isTubeAfterReIndexing()) {
			return "tube-linear-after-re-indexing";
		} else {
			assert(false);
			return "Invalid state in PredictionModel::GetMemStmtsStatus()";
		}
	}

	void reIndexMemAccesses(const InstrumentationResult &IR,
			ReIndexMap& ReindexMap);

	bool isStillValid(const InstrumentationResult &IR, ReIndexMap& ReindexMaps);

	bool isStillValid(const InstrumentationResult &IR);

	void dump(std::ostream &OS);

	static LinearFunctionTy tryBuildLinearFunction(
			const std::vector<InstrumentationResult::InstrumentationEvent> &Events,
			const int NumParents, PredictionType &Ty, bool IsScalar = false);

	static std::shared_ptr<PredictionModel>
	buildFromInstrumentation(const InstrumentationResult &IR, bool EarlyStop);

};

extern "C" long get_coefficient(PredictionModel *PM, const long Ty,
		const long Id, const long Cf);

extern "C" bool memory_runtime_verify(const long StmtId, void *MemAddress,
		const int NumberOfViIt, ...);

#endif
