//===--- BoneComposedStmt.h -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef BONES_COMPOSED_STMT_H
#define BONES_COMPOSED_STMT_H

#include <JitCompiler/CodeGenerator.h>
#include "StaticInfo/StaticInfo.h"
#include "CodeBones/BonesInfo/BoneStmt.h"

namespace apollo {
namespace codebones {

class BoneComposedStmt : public BoneStmt {
public:
  enum class CompositionType { Guarded, Sequential, Concurrent };

private:
  BoneStmt *First;
  BoneStmt *Second;
  CompositionType Composition;

public:
  BoneComposedStmt(BoneStmt *A, 
                   BoneStmt *B, 
                   const CompositionType &CompTy);
  virtual bool isComposed() const;
  bool executesSequentially() const;
  bool isGuarded() const;
  std::vector<BoneStmt *> getBones();

  static bool canCompose(BoneStmt *A, 
                         BoneStmt *B, 
                         bool TryReverse = true);
  static BoneComposedStmt *compose(BoneStmt *A, 
                                   BoneStmt *B,
                                   const CompositionType &Ty);
};

} // end namsepace codebones
} // end namsepace apollo

#endif
