//===--- ArrayInfo.h ------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef ARRAY_INFO_H
#define ARRAY_INFO_H

#include "APOLLO_PROG.h"
#include "Backup/Common.h"

#include <vector>
#include <map>
#include <string>
#include <set>
#include <vector>

class ArrayAnalysis {
public:
  enum class DelinearizeOption { NoDelinearize, Delinearize, ForceDelinearize };

  // Information about a reconstructed array
  // and its memory accesses
  struct ArrayInfo {
    int Id;
    MemRangeTy Range;

    int WordSizeInBytes;
    int Dimentions;

    bool ReadOnly;
    bool ContainsNonLinearAccess;

    std::set<int> MemIds;
  };

  // Information about a single memory access
  struct AccessInfo {
    using LinearFunctionTy = std::vector<long>;
    std::vector<LinearFunctionTy> Access;
    ArrayInfo *Array;
    int MemId;
    long TubeWidth = 0;
  };

  struct DimensionInfo {
    int Dimension;            // dimension size
    std::vector<int> CoefIdx; // coefficients involved
    AccessInfo::LinearFunctionTy Access;
  };

private:
  DelinearizeOption Delinearize;
  struct APOLLO_PROG *apolloProg;

  // Contains the IDs of all linear and tube memory accesses
  std::set<int> SolvedMemoryIds;

  // Predicted loop bounds for each loop
  std::vector<BoundRangeTy> LoopBounds;

  // Predicted memory range for each memory access
  std::vector<MemRangeTy> MemToRange;

public:

  // List of reconstructed arrays with:
  //  - range
  //  - word size
  //  - dimensions
  //  - IDs of their associated accesses
  std::vector<ArrayInfo> Arrays;

  // Map from memory accesses IDs to reconstructed arrays
  // and offset
  std::map<int, std::pair<ArrayInfo *, long>> MemToArray;

  // Map from memory accesses IDs to accesses information
  std::map<int, std::vector<AccessInfo>> MemToAccess;

private:
  void filterUnsolvedIds();
  long wordSizeForId(const int Id);
  std::vector<ArrayInfo> initArrays();

  bool arrayOverlap(const ArrayInfo &ArrayA, const ArrayInfo &ArrayB);
  bool arrayAlias(const ArrayInfo &ArrayA, const ArrayInfo &ArrayB);

  bool canMerge(const ArrayInfo &ArrayA, const ArrayInfo &ArrayB);
  void mergeArrays(ArrayInfo *ArrayA, const ArrayInfo &ArrayB);

  void computeArrayWordSize(std::vector<ArrayInfo> &Arrays);
  void
  computeMemToArray(std::map<int, std::pair<ArrayInfo *, long>> &Mem2Array);
  void computeAccesses(std::map<int, std::vector<AccessInfo>> &MemToAccess);

  using ArrayToAccessTy = std::map<ArrayInfo *, std::vector<AccessInfo *>>;
  using AccessToDimentionsTy = std::map<AccessInfo *, 
                                        std::vector<DimensionInfo>>;

  ArrayToAccessTy mapArrayToAccess();
  void delinearizeAccesses();
  AccessToDimentionsTy getAccessDimentions(ArrayToAccessTy &Arr2Acc);

  // fix for some illegal cases
  bool fixDelinearizedDimentions(std::vector<AccessInfo *> &Accesses,
                                 AccessToDimentionsTy &Acc2Dim);
  bool fixLowerExtremes(std::vector<AccessInfo *> &Accesses,
                        AccessToDimentionsTy &Acc2Dim);
  bool fixUpperExtremes(std::vector<AccessInfo *> &Accesses,
                        AccessToDimentionsTy &Acc2Dim);

  void proposeDelinearizedAccessFunctions(AccessToDimentionsTy &Acc2Dim);
  void updateDelinearizedAccessFunctions(ArrayInfo *Array,
                                         std::vector<AccessInfo *> &Accesses,
                                         AccessToDimentionsTy &Acc2Dim);

  void normalizeArraysIDs(std::vector<ArrayInfo> &Arrays);
  void mergeArrays(std::vector<ArrayInfo> &Arrays);
  void computeArrayInfo();

  DelinearizeOption getDelinearizeOption();

  // Utilities
  void dump(std::ostream &OS) const;

public:
  ArrayAnalysis(APOLLO_PROG *apolloProg);
};

#endif
