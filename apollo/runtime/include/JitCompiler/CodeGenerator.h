//===--- Skeletons.h ------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef SKELETONS_H
#define SKELETONS_H

#include "JitCompiler/JitCompiler.h"
#include "JitCompiler/PhiState.h"
#include "Interpolation/Interpolation.h"
#include "Params.h"

#include <future>
#include <memory>

namespace apollo {

class CodeGenerator {
public:
  struct StaticSkeletonInfo {
    struct JitBytecode {
      char *Ptr;
      long Size;
    };
    long Id;
    void *FunctionPtr;
    JitBytecode Bytecode;
  };

  struct CodeBonesDescriptor {
    std::string Name;
    std::vector<int> Stores;
    std::vector<int> Loads;
    std::vector<int> Scalars;
    std::vector<int> MemVerifIds;
    std::vector<int> BoundVerifIds;
    std::vector<int> ScalarVerifIds;
  };

private:
  using InstrumentationFunTy = long (*)(void *, ParamTy, PhiStateTy, 
                                        long, long, long, long);
  using BasicScalarPredictFunTy = long (*)(PhiStateTy, void *, long);
  using CodeBonesFunTy = long (*)(ParamTy, PhiStateTy, void *, long, long);
  using StaticInfoInitFunTy = long (*)(struct StaticNestInfo *);

  struct JitState {
    std::unique_ptr<llvm::Module> PatchedModule;
    std::unique_ptr<llvm::ExecutionEngine> ExecutionEngine;
    CodeBonesFunTy VerificationFunction;
    CodeBonesFunTy OptimizedFunction;
  };

  // binary skeletons
  StaticInfoInitFunTy RegisterStaticInfo;
  InstrumentationFunTy Instrument;
  BasicScalarPredictFunTy BasicScalarPredict;

  // Pointer to string variable containing LLVM bytecode of a module
  // containing the code bones. Each code bone is a function
  // inside this module.
  StaticSkeletonInfo::JitBytecode CodeBonesBytecode;

  // LLVM in memory module loaded from the pointer
  // above (CodeBonesBytecode.ptr)
  std::unique_ptr<llvm::Module> OriginalCodeBonesModule;

  // Copy of the bone LLVM module where we can perform
  // modifications
  std::unique_ptr<JitState> JitStateObj;

  CodeBonesFunTy Verification;
  CodeBonesFunTy Optimized;

  /// \brief Load the code bones module from the string variable
  /// containing its bytecode
  llvm::Module *loadCodeBonesModule();

public:
  CodeGenerator(struct StaticSkeletonInfo *Skeletons, unsigned NumSkeletons);
  ~CodeGenerator();

  // jit compiler functions
  std::vector<CodeBonesDescriptor> getCodeBonesDescriptors();

  void patchModules(NestParamsStruc &Params,
                    std::shared_ptr<PredictionModel> &PM);

  void generateOptimizedCode(std::shared_ptr<ScanTypeStruct> &Scan);

  void callRegisterStaticInfo(struct StaticNestInfo *StaticInfo);

  long callOriginal(NestParamsStruc &Param, PhiState &State, 
                    const long ChunkLower,
                    const long ChunkUpper);

  long callInstrumentation(NestParamsStruc &Param, PhiState &State,
                           InstrumentationResult *IR, 
                           const long ChunkLower,
                           const long ChunkUpper, 
                           const long InstOuter, 
                           const long InstInner);

  PhiState predictPhiState(PhiState &State,
                           std::shared_ptr<PredictionModel> &PM,
                           const long iteration);

  void callOptimized(NestParamsStruc &Params, PhiState &PhiStateObj,
                     std::shared_ptr<PredictionModel> &PM, 
                     const long ChunkLower,
                     const long ChunkUpper);

  void callVerificationOnly(NestParamsStruc &Params, PhiState &PhiStateObj,
                            std::shared_ptr<PredictionModel> &PM,
                            const long ChunkLower, 
                            const long ChunkUpper);
};

} // end namespace apollo

#endif
