//===--- ApolloPhiState.cpp -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Debug.h"

#include "SkeletonConfig.h"
#include "Transformations/Instrumentation/ApolloPhiState.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief Inserts the condition for initializing the phi-state.
///        (check if the lower bound is 0)
/// \param The loop preheader.
/// \return The created condition.
Instruction *insertInitCondition(BasicBlock *Preheader) {
	Function *Parent = Preheader->getParent();
	Instruction *InsertAt = &*(Preheader->begin());
	Argument *ChunkLower = function::getArgumentByName(Parent, "apollo_lower");
	Type *ZeroType = ChunkLower->getType();
	Constant *Zero = ConstantInt::get(ZeroType, 0);
	Instruction *Condition = new ICmpInst(InsertAt, ICmpInst::ICMP_SLE,
			ChunkLower, Zero, "phi_state_init_cond");
	return Condition;
}

/// \brief Creates a block which loads the phi-node state.
/// \param the basicblock in which the new block will continue.
/// \param the phi-node initialization store.
BasicBlock *createPhiFromState(BasicBlock *ContinueAt, StoreInst *Store) {
	Function *Parent = ContinueAt->getParent();
	BasicBlock *PhiFromState = BasicBlock::Create(Parent->getContext(),
			"loadPhiState", Parent);
	AllocaInst *Alloc = cast<AllocaInst>(Store->getPointerOperand());
	Constant *GepOffset = cast<Constant>(
			metadata::getMetadataOperand(Alloc, mdkind::ApolloPhiStateField,
					0));
	Constant *Zero = ConstantInt::get(GepOffset->getType(), 0);
	Argument *PhiState = function::getArgumentByName(Parent, "phi_state");
	Type *T =
			cast<PointerType>(PhiState->getType()->getScalarType())->getElementType();
	Instruction *Gep = GetElementPtrInst::Create(T, PhiState,
			std::vector<Value *>( { Zero, GepOffset }), "phi_state_gep",
			PhiFromState);
	LoadInst *Load = new LoadInst(Gep, "phi_state_load", PhiFromState);
	new StoreInst(Load, Alloc, PhiFromState);
	BranchInst::Create(ContinueAt, PhiFromState);
	return PhiFromState;
}

/// \brief Collects all the stores that initialize the phi-nodes of the
///        outermost loop.
/// \param The outermost loop.
/// \return A vector with all the stores that initialize the phi-nodes.
std::vector<StoreInst *> collectOutermostPhiInitialization(Loop *L) {
	BasicBlock *PhiInitBlock = 0x0;
	Function *Parent = loop::getLoopParentFunction(L);
	Function::iterator block_it;
	for (BasicBlock &block : *Parent) {
		const bool isOutsideLoopNest = !L->contains(&block);
		if (isOutsideLoopNest) {
			for (Instruction &inst : block) {
				StoreInst *Store = dyn_cast<StoreInst>(&inst);
				if (Store) {
					AllocaInst *Ptr = dyn_cast<AllocaInst>(
							Store->getPointerOperand());
					const bool IsPhiInitializationBlock = Ptr
							&& metadata::hasMetadataKind(Ptr,
									mdkind::ApolloPhiId);
					if (IsPhiInitializationBlock) {
						PhiInitBlock = &block;
						break;
					}
				}
			}
			if (PhiInitBlock)
				break;
		}
	}

	std::vector<StoreInst *> Stores;
	if (PhiInitBlock) {
		for (Instruction &inst : *PhiInitBlock) {
			StoreInst *Store = dyn_cast<StoreInst>(&inst);
			if (Store) {
				AllocaInst *Ptr = dyn_cast<AllocaInst>(
						Store->getPointerOperand());
				if (Ptr && metadata::hasMetadataKind(Ptr, mdkind::ApolloPhiId))
					Stores.push_back(Store);
			}
		}
	}
	return Stores;
}

/// \brief At each loop nest exit, store the phi-node values in the state.
/// \param A vector with all the stores that initialize the phi-nodes.
/// \param The outermost loop.
void commitScalarsAtExit(std::vector<StoreInst *> &Stores, Loop *L) {
	std::set<BasicBlock *> Exits = std::move(loop::getLoopExitBlocks(L));
	Argument *PhiState = function::getArgumentByName(
			loop::getLoopParentFunction(L), "phi_state");

	// insert the code at the exit for saving the phi-node state.
	for (StoreInst *store : Stores) {
		AllocaInst *Ptr = cast<AllocaInst>(store->getPointerOperand());
		Constant *GepOffset = cast<Constant>(
				metadata::getMetadataOperand(Ptr, mdkind::ApolloPhiStateField,
						0));
		Constant *Zero = ConstantInt::get(GepOffset->getType(), 0);
		for (BasicBlock *exit : Exits) {
			Instruction *InsertAt = exit->getTerminator();
			Type *T =
					cast<PointerType>(PhiState->getType()->getScalarType())->getElementType();
			Instruction *Gep = GetElementPtrInst::Create(T, PhiState,
					std::vector<Value *>( { Zero, GepOffset }), "phi_state_gep",
					InsertAt);
			LoadInst *Load = new LoadInst(Ptr, "phi_load_before_commit",
					InsertAt);
			new StoreInst(Load, Gep, InsertAt);
		}
	}
}

/// \brief Insert the phi-state initialization. It can be initialized with the
///        original code or initialized from phi-state depending on a condition.
///  \param A vector with the initialization stores of the outermost phi-nodes.
void guardInitialization(std::vector<StoreInst *> &Stores) {
	BasicBlock *PhiBlock = Stores[0]->getParent();
	Instruction *Condition = insertInitCondition(PhiBlock);
	BasicBlock *StoreAt = PhiBlock;
	for (StoreInst *store : Stores) {
		BasicBlock::iterator after(store);
		++after;
		BasicBlock *PhiInitializationBlock = StoreAt->splitBasicBlock(store,
				"phiStateInitialization");
		BasicBlock *Cont = PhiInitializationBlock->splitBasicBlock(after,
				"cont");
		BasicBlock *PhiFromState = createPhiFromState(Cont, store);
		StoreAt->getTerminator()->eraseFromParent();
		BranchInst::Create(PhiInitializationBlock, PhiFromState, Condition,
				StoreAt);
		StoreAt = Cont;
	}
}

} // end anonymous namespace

namespace apollo {

ApolloInstrumentPhiState::ApolloInstrumentPhiState() :
		ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(),
				"ApolloInstrumentPhiState") {
}

void ApolloInstrumentPhiState::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloInstrumentPhiState::runOnLoop(Loop *L) {
	std::vector<StoreInst *> Stores = std::move(
			collectOutermostPhiInitialization(L));
	if (!Stores.empty()) {
		DEBUG_WITH_TYPE("apollo-pass-instrument-phi-state",
				dbgs() << "function before: \n";
				L->getHeader()->getParent()->dump(); dbgs() << "\n\n");
		guardInitialization(Stores);
		commitScalarsAtExit(Stores, L);
		DEBUG_WITH_TYPE("apollo-pass-instrument-phi-state",
						dbgs() << "function after: \n";
						L->getHeader()->getParent()->dump());
		return true;
	}
	return false;
}

char ApolloInstrumentPhiState::ID = 0;
DefinePassCreator(ApolloInstrumentPhiState);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloInstrumentPhiState);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloInstrumentPhiState, "APOLLO_INSTRUMENT_PHI_STATE",
		"APOLLO_INSTRUMENT_PHI_STATE", false, false)
	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
	INITIALIZE_PASS_END(ApolloInstrumentPhiState, "APOLLO_INSTRUMENT_PHI_STATE",
			"APOLLO_INSTRUMENT_PHI_STATE", false, false)
