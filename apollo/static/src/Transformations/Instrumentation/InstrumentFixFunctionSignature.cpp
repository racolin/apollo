//===--- InstrumentFixFunctionSignature.cpp -------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/Instrumentation/InstrumentFixFunctionSignature.h"
#include "Utils/FunctionUtils.h"
#include "Transformations/Preparation/ApolloCloneLoopFunction.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {

using NameTypePair = std::pair<std::string, llvm::Type *>;

/// \brief Adds arguments to the apollo_loop function.
Function *changeFunctionDefinition(Function *OldFunction) {
  Type *i64Ty = Type::getInt64Ty(OldFunction->getContext());
  Type *i8PtrTy = Type::getInt8PtrTy(OldFunction->getContext());
  std::vector<NameTypePair> NewParameters = {
      NameTypePair("instrumentation_result", i8PtrTy),
      NameTypePair("apollo_lower", i64Ty), NameTypePair("apollo_upper", i64Ty),
      NameTypePair("apollo_outer_instrument_bound", i64Ty),
      NameTypePair("apollo_inner_instrument_bound", i64Ty)};
  return ApolloCloneLoopFunction::addParameters(OldFunction, NewParameters);
}

} // end namespace anonymous

namespace apollo {
InstrumentFixFunctionSignature::InstrumentFixFunctionSignature()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(), "InstrumentFixFunctionSignature") {
}

void InstrumentFixFunctionSignature::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool InstrumentFixFunctionSignature::runOnApollo(Function *F) {
  changeFunctionDefinition(F);
  return true;
}

char InstrumentFixFunctionSignature::ID = 0;
DefinePassCreator(InstrumentFixFunctionSignature);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(InstrumentFixFunctionSignature);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(InstrumentFixFunctionSignature,
                      "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE",
                      "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(InstrumentFixFunctionSignature,
                    "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE",
                    "APOLLO_INSTRUMENT_FIX_FUNCTION_SIGNATURE", false, false)
