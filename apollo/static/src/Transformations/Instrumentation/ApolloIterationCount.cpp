//===--- ApolloIterationCount.cpp -----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "Transformations/Instrumentation/ApolloIterationCount.h"
#include "Transformations/Instrumentation/InstrumentationUtils.h"
#include "Transformations/Instrumentation/ApolloLinearEquation.h"
#include "Transformations/Instrumentation/ApolloOnCondition.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/Utils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief  Creates the block which calls the function to register the
///         loop bounds.
/// \param  VoopId the loop id.
/// \param  ViLoads a vector with the vi values of all the virtual iterators.
/// \param  RegisterFunction the function that registers the loop bound.
/// \param  ExitSplit the block were the register block should continue.
/// \return The new block which calls the runtime function to register
///         the bounds.
BasicBlock *createRegistrationBlock(Value *LoopId,
		std::vector<LoadInst *> &ViLoads, Function *RegisterFunction,
		BasicBlock *ExitSplit) {
	const unsigned Depth = ViLoads.size();
	LoadInst *ViLoad = ViLoads[Depth - 1];
	Function *ParentFunc = ViLoad->getParent()->getParent();
	BasicBlock *RegisterBlock = BasicBlock::Create(ParentFunc->getContext(),
			"register_iteration_count_block", ParentFunc);
	ir::ApolloIRBuilder Builder(RegisterBlock);
	Constant *One = ConstantInt::get(ViLoad->getType(), 1);
	Constant *NumberOfParents = ConstantInt::get(ViLoad->getType(), Depth - 1);
	Constant *EventId = ConstantInt::get(ViLoad->getType(),
			EventType::BoundDyn);
	Value *InstrumentationResult = function::getArgumentByName(ParentFunc,
			"instrumentation_result");
	std::vector<Value *> Args = { InstrumentationResult, EventId, LoopId,
			Builder.CreateAdd(ViLoad, One), NumberOfParents };
	Args.insert(Args.end(), ViLoads.begin(),
			ViLoads.begin() + ViLoads.size() - 1);
	Builder.CreateCall(RegisterFunction, Args);
	Builder.CreateBr(ExitSplit);
	return RegisterBlock;
}

/// \brief Inserts recursively the registration code for the loop trip counts.
/// \param L a Loop.
/// \param registerFunction the function which registers the iteration count.
void insertIterationCountInstrumentation(Loop *L, Function *RegisterFunction,
		std::vector<LoadInst *> ViLoads = std::vector<LoadInst *>()) {

	// Insert dynamic bounds registration for all loops in the nest
	// that are dynamic and excluding the outer most loop because it
	// is executed by chunks.
	bool IsOutermost = loop::isOutermostLoop(L);
	BasicBlock *Header = L->getHeader();
	AllocaInst *ViAlloc = vi::getLoopVi(L);
	LoadInst *ViLoad = new LoadInst(ViAlloc, ViAlloc->getName() + "_load",
				&*(Header->begin()));
	ViLoads.push_back(ViLoad);
	if (!IsOutermost
			&& !ApolloInstrumentLinearEquation::hasLineq(
					L->getHeader()->getParent(), loop::getLoopId(L))) {
		LLVMContext &Context = Header->getContext();
		Value *InstrumentCondition =
				ApolloInstrumentOnCondition::getInstrumentationCondition(
						L->getParentLoop());
		Constant *LoopId = constant::getInt64Constant(Context,
				loop::getLoopId(L));

		// For each exit block insert the registration code.
		std::set<BasicBlock *> ExitBlocks = std::move(
				loop::getLoopExitBlocks(L));
		for (BasicBlock *exitBlock : ExitBlocks) {
			BasicBlock *ExitSplit = exitBlock->splitBasicBlock(
					exitBlock->begin());
			ExitSplit->setName("original_loop_exit");
			pred_iterator pit = pred_begin(exitBlock), pend = pred_end(
					exitBlock);
			while (pit != pend) {
				BasicBlock *Predecesor = *pit;
				if (!L->contains(Predecesor)) {
					Predecesor->getTerminator()->replaceUsesOfWith(exitBlock,
							ExitSplit);
					pit = pred_begin(exitBlock);
				} else
					pit++;
			}
			// Delete the exit block terminator
			TerminatorInst *ExitTerminator = exitBlock->getTerminator();
			ExitTerminator->eraseFromParent();
			BasicBlock *RegisterBlock = createRegistrationBlock(LoopId, ViLoads,
					RegisterFunction, ExitSplit);
			BranchInst::Create(RegisterBlock, ExitSplit, InstrumentCondition,
					exitBlock);
		}
	}

	// Recursive call
	for (Loop *subloop : *L) {
		insertIterationCountInstrumentation(subloop, RegisterFunction, ViLoads);
	}
}

} // end anonymous namespace

namespace apollo {

ApolloInstrumentIterationCount::ApolloInstrumentIterationCount() :
		ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(),
				"ApolloInstrumentIterationCount") {
}

void ApolloInstrumentIterationCount::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloInstrumentIterationCount::runOnLoop(Loop *L) {
	Module *M = loop::getLoopParentModule(L);
	insertIterationCountInstrumentation(L, getApolloRegisterEvent(M));
	return true;
}

char ApolloInstrumentIterationCount::ID = 0;
DefinePassCreator(ApolloInstrumentIterationCount);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloInstrumentIterationCount);
}

INITIALIZE_PASS_BEGIN(ApolloInstrumentIterationCount,
		"APOLLO_INSTRUMENT_ITERATION_COUNT",
		"APOLLO_INSTRUMENT_ITERATION_COUNT", false, false)
	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
	INITIALIZE_PASS_END(ApolloInstrumentIterationCount,
			"APOLLO_INSTRUMENT_ITERATION_COUNT",
			"APOLLO_INSTRUMENT_ITERATION_COUNT", false, false)
