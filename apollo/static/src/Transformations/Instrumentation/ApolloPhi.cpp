//===--- ApolloPhi.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "Transformations/Instrumentation/ApolloPhi.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"
#include "Transformations/Instrumentation/InstrumentationUtils.h"
#include "Transformations/Instrumentation/ApolloOnCondition.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief This function collects the values for the vi of the parent loops.
/// \param L a loop.
/// \return a vector with the values of the vi.
std::vector<Value *> getParentViValues(Loop *L) {
  LoadInst *ViLoad = new LoadInst(vi::getLoopVi(L), "vi_load",
                                  L->getHeader()->getTerminator());
  if (loop::isOutermostLoop(L))
    return {ViLoad};
  std::vector<Value *> ViValues = getParentViValues(L->getParentLoop());
  ViValues.push_back(ViLoad);
  return ViValues;
}

/// \brief This function collects the phi-nodes to work with them.
/// \param F an apollo_loop_%id_instrument function.
/// \return a vector with the alloc of the phi-nodes.
std::vector<AllocaInst *> collectPhiToInstrument(Function *F) {
  BasicBlock &Entry = F->getEntryBlock();
  std::vector<AllocaInst *> Phis;
  for (Instruction &inst : Entry) {
    AllocaInst *Alloc = dyn_cast<AllocaInst>(&inst);
    // only visit phi-nodes alloca instructions
    // if it has an id assigned it must be instrumented,
    // if not its a reduction.
    const bool IsPhiAlloc =
          Alloc && metadata::hasMetadataKind(Alloc, mdkind::ApolloPhiId);
    if (IsPhiAlloc) {
      const bool IsLinear = metadata::hasMetadataKind(Alloc, mdkind::LinEq);
      const bool IsFloating =
            Alloc->getType()->getContainedType(0)->isFloatingPointTy();
      if (IsPhiAlloc && !IsLinear && !IsFloating)
        Phis.push_back(Alloc);
    }
  }
  return Phis;
}

/// \brief Returns the unique load of a phi-alloc.
/// \param phiAlloc a phi node alloc.
/// \param LoopInfo LI analysis.
/// \return the unique load of a phi.
LoadInst *getPhiLoad(AllocaInst *PhiAlloc, LoopInfo *LI) {
  for (User *user : PhiAlloc->users()) {
    LoadInst *PhiLoad = dyn_cast<LoadInst>(user);
    // there should be only one load inside the loop nest!
    if (PhiLoad && LI->getLoopFor(PhiLoad->getParent()))
      return PhiLoad;
  }
  assert(false && "phiLoad: Phi LoadInst not found!");
  return 0x0;
}

/// \brief This function collects and then inserts the instrumentation code
///        for the phi-nodes of the header of the loop.
/// \param F an apollo_loop_%id_instrument function.
/// \param LI LoopInfo analysis.
void insertPhiNodeInstrumentation(Function *F, LoopInfo *LI,
                                  Function *RegisterFunction) {
  std::vector<AllocaInst *> Phis = std::move(collectPhiToInstrument(F));
  LLVMContext &Context = RegisterFunction->getContext();
  for (AllocaInst *alloc : Phis) {
    Value *PhiId = metadata::getMetadataOperand(alloc, mdkind::ApolloPhiId);
    LoadInst *PhiLoad = getPhiLoad(alloc, LI);
    BasicBlock *PhiLoadBlock = PhiLoad->getParent();
    Loop *L = LI->getLoopFor(PhiLoadBlock);
    Value *InstrumentCondition =
        ApolloInstrumentOnCondition::getInstrumentationCondition(L);
    BasicBlock::iterator afterPhiLoad(PhiLoad);
    ++afterPhiLoad;
    BasicBlock *SplitBlock = PhiLoadBlock->splitBasicBlock(
        afterPhiLoad, PhiLoadBlock->getName() + ".split");
    BasicBlock *RegisterBlock =
        BasicBlock::Create(Context, "instrument_phi", F, SplitBlock);
    TerminatorInst *RegTerm = BranchInst::Create(SplitBlock, RegisterBlock);
    PhiLoadBlock->getTerminator()->eraseFromParent();
    BranchInst::Create(RegisterBlock, SplitBlock, InstrumentCondition,
                       PhiLoadBlock);
    Value *PhiCast = PhiLoad;
    Type *PhiType = PhiLoad->getType(), *i64ty = Type::getInt64Ty(Context);
    if (!PhiType->isIntegerTy(64)) {
      if (PhiType->isPointerTy()) {
        PhiCast = CastInst::CreatePointerCast(
            PhiLoad, i64ty, PhiLoad->getName() + "_cast", RegTerm);
      } else {
        // if the type is an integer
        PhiCast = CastInst::CreateZExtOrBitCast(
            PhiLoad, i64ty, PhiLoad->getName() + "_cast", RegTerm);
      }
    }
    Constant *EventId = constant::getInt64Constant(PhiLoad->getContext(), EventType::ScaDyn);
    auto ParentsViValues = std::move(getParentViValues(L));
    Value *InstrRes = function::getArgumentByName(F, "instrumentation_result");
    std::vector<Value *> Args = { InstrRes, EventId, PhiId, PhiCast,
                       constant::getInt64Constant(Context, L->getLoopDepth())};
    Args.insert(Args.end(), ParentsViValues.begin(), ParentsViValues.end());
    L->addBasicBlockToLoop(SplitBlock, *LI);
    L->addBasicBlockToLoop(RegisterBlock, *LI);
    CallInst::Create(RegisterFunction, Args, "", RegTerm);
  }
}

} // end anonymous namespace

namespace apollo {

ApolloInstrumentPhi::ApolloInstrumentPhi()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(), "ApolloInstrumentPhi") {
}

void ApolloInstrumentPhi::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloInstrumentPhi::runOnApollo(Function *F) {
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  Function *RegisterFunction = getApolloRegisterEvent(F->getParent());
  insertPhiNodeInstrumentation(F, &LI, RegisterFunction);
  return true;
}

char ApolloInstrumentPhi::ID = 0;
DefinePassCreator(ApolloInstrumentPhi);

} // ens namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloInstrumentPhi);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloInstrumentPhi, "APOLLO_INSTRUMENT_PHI",
                      "APOLLO_INSTRUMENT_PHI", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloInstrumentPhi, "APOLLO_INSTRUMENT_PHI",
                    "APOLLO_INSTRUMENT_PHI", false, false)
