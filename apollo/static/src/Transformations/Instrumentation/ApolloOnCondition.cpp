//===--- ApolloOnCondition.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/Instrumentation/ApolloOnCondition.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {
/// \brief This function inserts recursively on each loop the condition for
///        instrumentation.
/// \param L a loop.
/// \param parentCondition the condition of the parent loop.
///        For the outermost is the constant true.
void insertInstrumentationCondition(Loop *L, Value *ParentCondition) {
  const bool IsOutermost = loop::isOutermostLoop(L);
  BasicBlock *Header = L->getHeader();
  Function *ParentFunction = Header->getParent();
  AllocaInst *ViAlloc = vi::getLoopVi(L);
  Instruction *HeaderBegin = &*(Header->begin());
  LoadInst *ViLoad =
      new LoadInst(ViAlloc, ViAlloc->getName() + "_load", HeaderBegin);
  const std::string BoundName = IsOutermost 
                                ? "apollo_outer_instrument_bound"
                                : "apollo_inner_instrument_bound";
  Value *Bound = function::getArgumentByName(ParentFunction, BoundName);
  if (IsOutermost) {
    Value *LowerChunk =
        function::getArgumentByName(ParentFunction, "apollo_lower");
    Bound =
        BinaryOperator::CreateAdd(LowerChunk, Bound, "upper_instrument_bound",
                                  &*(ParentFunction->getEntryBlock().begin()));
  }
  ICmpInst *ViLower = new ICmpInst(HeaderBegin, ICmpInst::ICMP_SLT, ViLoad,
                                   Bound, "vi_lower_than_bound");
  Instruction *Condition = BinaryOperator::Create(
      BinaryOperator::And, ParentCondition, ViLower,
      Header->getName() + "_instrument_condition", HeaderBegin);
  metadata::attachMetadata(Condition, mdkind::ApolloInstrumentationCondition,
                           loop::getLoopPathConstants(L));
  for (Loop *subloop : *L) {
    insertInstrumentationCondition(subloop, Condition);
  }
}

/// \brief Inserts the base instrumentation condition.
///        Compares the outer instrument bound with zero.
/// \param apolloInstrument The instrumentation skeleton function.
/// \return The new condition inserted at the entry.
Instruction *baseInstrumentCondition(Function *apolloInstrument) {
  Value *apolloInstrumentBound = function::getArgumentByName(
      apolloInstrument, "apollo_outer_instrument_bound");
  Constant *Zero = constant::zero64(apolloInstrument->getContext());
  return new ICmpInst(&*(apolloInstrument->getEntryBlock().begin()),
                      ICmpInst::ICMP_NE, apolloInstrumentBound, Zero,
                      "instrument_base_cond");
}

} // end anonymous namespace

namespace apollo {

ApolloInstrumentOnCondition::ApolloInstrumentOnCondition()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(), "ApolloInstrumentOnCondition") {
}

void ApolloInstrumentOnCondition::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloInstrumentOnCondition::runOnLoop(Loop *L) {
  Instruction *BaseCondition =
      baseInstrumentCondition(loop::getLoopParentFunction(L));
  insertInstrumentationCondition(L, BaseCondition);
  return true;
}

Instruction *ApolloInstrumentOnCondition::getInstrumentationCondition(Loop *L) {
  const auto LoopPath = std::move(loop::getLoopPathConstants(L));
  for (BasicBlock &block : *loop::getLoopParentFunction(L)) {
    for (Instruction &inst : block) {
      if (metadata::hasMetadataKind(&inst,
                                    mdkind::ApolloInstrumentationCondition)) {
        bool SamePath = LoopPath.size() ==
                         metadata::getMetadataNumOperands(
                           &inst, mdkind::ApolloInstrumentationCondition);
        for (unsigned i = 0;
             i < metadata::getMetadataNumOperands(
                     &inst, mdkind::ApolloInstrumentationCondition) && SamePath;
             i++) {
          Value *MdVal = metadata::getMetadataOperand(
              &inst, mdkind::ApolloInstrumentationCondition, i);
          SamePath = MdVal == LoopPath[i];
        }
        if (SamePath)
          return &inst;
      }
    }
  }
  assert(false &&
         "getInstrumentationCondition: Instrumentation condition not found!");
  return 0x0;
}

char ApolloInstrumentOnCondition::ID = 0;
DefinePassCreator(ApolloInstrumentOnCondition);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloInstrumentOnCondition);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloInstrumentOnCondition,
                      "APOLLO_INSTRUMENT_ON_CONDITION",
                      "APOLLO_INSTRUMENT_ON_CONDITION", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloInstrumentOnCondition,
                    "APOLLO_INSTRUMENT_ON_CONDITION",
                    "APOLLO_INSTRUMENT_ON_CONDITION", false, false)
