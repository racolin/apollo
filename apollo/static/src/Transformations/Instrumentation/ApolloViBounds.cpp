//===--- ApolloViBounds.cpp -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Instrumentation/ApolloViBounds.h"
#include "SkeletonConfig.h"
#include "Utils/MetadataHandler.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief Inserts the vi initialization for each loop. For the innermost is 0.
///        For the outermost is the 'lower' param.
/// \param L a loop, with a preheader.
void insertInitialization(Loop *L) {
  BasicBlock *Preheader = L->getLoopPreheader();
  AllocaInst *ViAlloc = vi::getLoopVi(L);
  Value *InitialValue;
  if (loop::isOutermostLoop(L)) {
    Argument *Lower =
        function::getArgumentByName(Preheader->getParent(), "apollo_lower");
    InitialValue = Lower;
  } else
    InitialValue = constant::zero64(Preheader->getContext());

  new StoreInst(InitialValue, ViAlloc, Preheader->getTerminator());
  for (Loop *subloop : *L) {
    insertInitialization(subloop);
  }
}

/// \brief Insert the loop exit on virtual iterator condition for the outermost
///        loop. It keeps the LoopInfo analysis updated.
/// \param L a loop.
/// \param LI LoopInfo analysis.
void insertOutermostBound(Loop *L, LoopInfo *LI) {
  AllocaInst *ViAlloc = vi::getLoopVi(L);
  BasicBlock *Header = L->getHeader(), *NewHeader, *ViExitBlock;
  Function *Parent = Header->getParent();
  LLVMContext &Context = Parent->getContext();
  NewHeader = BasicBlock::Create(Context, Header->getName() + "_new_header",
                                 Parent, Header);
  Header->replaceAllUsesWith(NewHeader);
  Argument *Upper = function::getArgumentByName(Parent, "apollo_upper");
  LoadInst *ViLoad =
      new LoadInst(ViAlloc, ViAlloc->getName() + "_load", NewHeader);
  ICmpInst *CiCmp =
      new ICmpInst(*NewHeader, ICmpInst::ICMP_SLT, ViLoad, Upper, "vi_exit");
  ViExitBlock = BasicBlock::Create(Context, "vi_exit", Parent);
  Type *i64Ty = Type::getInt64Ty(Context);
  Constant *Zero = ConstantInt::get(i64Ty, 0);
  ReturnInst::Create(Context, Zero, ViExitBlock);
  BranchInst::Create(Header, ViExitBlock, CiCmp, NewHeader);
  L->addBasicBlockToLoop(NewHeader, *LI);
  L->moveToHeader(NewHeader);
}

} // end anonymous namespace

namespace apollo {

ApolloInstrumentViBounds::ApolloInstrumentViBounds()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::instrumentation>(), "ApolloInstrumentViBounds") {
}

void ApolloInstrumentViBounds::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloInstrumentViBounds::runOnLoopLI(Loop *L, LoopInfo *LI) {
  insertOutermostBound(L, LI);
  insertInitialization(L);
  return true;
}

char ApolloInstrumentViBounds::ID = 0;
DefinePassCreator(ApolloInstrumentViBounds);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloInstrumentViBounds);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloInstrumentViBounds,
                      "APOLLO_INSTRUMENT_VIRTUAL_ITERATOR_BOUNDS",
                      "APOLLO_INSTRUMENT_VIRTUAL_ITERATOR_BOUNDS", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloInstrumentViBounds,
                    "APOLLO_INSTRUMENT_VIRTUAL_ITERATOR_BOUNDS",
                    "APOLLO_INSTRUMENT_VIRTUAL_ITERATOR_BOUNDS", false, false)
