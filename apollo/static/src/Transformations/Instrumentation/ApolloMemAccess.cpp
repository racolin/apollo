//===--- ApolloMemAccess.cpp ----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "InstrumentationConfig.h"
#include "MetadataKind.h"
#include "Transformations/Instrumentation/ApolloMemAccess.h"
#include "Transformations/Instrumentation/InstrumentationUtils.h"
#include "Transformations/Instrumentation/ApolloLinearEquation.h"
#include "Transformations/Instrumentation/ApolloOnCondition.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/Utils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Metadata.h"

using namespace llvm;
using namespace apollo;

namespace {

/// \brief CreateRegisterBlock Creates the block in which the stmt registration
///        will go.
/// \param Block
/// \param ParentLoop
/// \return The newly created block for the statement registration.
BasicBlock *createRegisterBlock(BasicBlock &BB, Loop *ParentLoop) {
	BasicBlock *BlockCont = BB.splitBasicBlock(BB.getTerminator(),
			BB.getName() + ".cont");
	BasicBlock *RegisterBlock = BasicBlock::Create(BB.getContext(),
			BB.getName() + ".instrument", BB.getParent(), BlockCont);
	Instruction *InstrumentCondition =
			ApolloInstrumentOnCondition::getInstrumentationCondition(
					ParentLoop);
	BB.getTerminator()->eraseFromParent();
	BranchInst::Create(RegisterBlock, BlockCont, InstrumentCondition, &BB);
	BranchInst::Create(BlockCont, RegisterBlock);
	return RegisterBlock;
}

/// \brief InstrumentSingleAccess Creates the register function for one stmt.
void instrumentSingleAccess(Instruction &Stmt, Loop *ParentLoop,
		IRBuilder<> &Builder) {
	std::vector<LoadInst *> Vis = std::move(vi::getNestViLoad(ParentLoop));
	Type *i64Ty = Type::getInt64Ty(Stmt.getContext());
	Value *Ptr = Builder.CreatePtrToInt(statement::getPointerOperand(&Stmt),
			i64Ty);
	Value *EventTy = constant::getInt64Constant(Stmt.getContext(),
			EventType::MemDyn);
	Value *InstrumentationResult = apollo::function::getArgumentByName(
			ParentLoop->getHeader()->getParent(), "instrumentation_result");
	std::vector<Value *> Params;
	Params.push_back(InstrumentationResult);
	Params.push_back(EventTy);
	Params.push_back(
			metadata::getMetadataOperand(&Stmt, mdkind::ApolloStatementId));
	Params.push_back(Ptr);
	Params.push_back(
			constant::getInt64Constant(Stmt.getContext(),
					ParentLoop->getLoopDepth()));
	Params.insert(Params.end(), Vis.begin(), Vis.end());
	Module *M = Stmt.getParent()->getParent()->getParent();
	Function *RegFun = getApolloRegisterEvent(M);
	Builder.CreateCall(RegFun, Params);
}

/// \brief InstrumentAccessesInBlock This function creates a registration block
///        and registers all the memory accesses for 'block' in it.
void instrumentAccessesInBlock(BasicBlock &BB, LoopInfo &LI) {
	std::vector<Instruction *> Stmts;
	for (Instruction &stmt : BB) {
		if (statement::isStatement(&stmt)) {
			Stmts.push_back(&stmt);
		}
	}
	if (!Stmts.empty()) {
		Loop *ParentLoop = LI.getLoopFor(&BB);
		BasicBlock *RegisterBlock = createRegisterBlock(BB, ParentLoop);
		IRBuilder<> Builder(RegisterBlock->getTerminator());
		for (Instruction *stmt : Stmts) {
			instrumentSingleAccess(*stmt, ParentLoop, Builder);
		}
	}
}

} // end anonymous namespace

namespace apollo {

ApolloInstrumentMemAccess::ApolloInstrumentMemAccess() :
		pass::ApolloPassOnSkeleton(ID,
				skeleton::sk_name<skeleton::instrumentation>(),
				"ApolloInstrumentMemAccess") {
}

void ApolloInstrumentMemAccess::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloInstrumentMemAccess::runOnLoopLI(Loop *L, LoopInfo *LI) {
	std::vector<BasicBlock *> LoopBlocks = L->getBlocks();
	for (BasicBlock *block : LoopBlocks) {
		instrumentAccessesInBlock(*block, *LI);
	}
	return true;
}

char ApolloInstrumentMemAccess::ID = 0;
DefinePassCreator(ApolloInstrumentMemAccess);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloInstrumentMemAccess);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloInstrumentMemAccess, "APOLLO_INSTRUMENT_MEM_ACCESS",
		"APOLLO_INSTRUMENT_MEM_ACCESS", false, false)
	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
	INITIALIZE_PASS_END(ApolloInstrumentMemAccess,
			"APOLLO_INSTRUMENT_MEM_ACCESS", "APOLLO_INSTRUMENT_MEM_ACCESS",
			false, false)
