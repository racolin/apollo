//===--- SwitchingCall.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Utils/Utils.h"
#include "Utils/LoopUtils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/TypeUtils.h"
#include "Transformations/FinalPasses/SwitchingCall.h"
#include "Utils/ViUtils.h"
#include "SkeletonConfig.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Pass.h"
#include "llvm/Analysis/LoopPass.h"

#include <set>
#include <vector>
#include <cctype>

using namespace llvm;
using namespace apollo;

namespace {

struct SkeletonDescriptor {
  unsigned ID;
  Constant *skeleton;
  Constant *jit_ptr;
  Constant *jit_ptr_size;
};

/// \brief get the skeleton id of a skeleton function.
unsigned getSkeletonId(Function *CurFunctionIterator) {
  const std::string Name = CurFunctionIterator->getName();
  unsigned Base = 1;
  unsigned Acum = 0;
  for (unsigned pos = Name.size() - 1; isdigit(Name[pos]); --pos) {
    Acum += (Name[pos] - '0') * Base;
    Base *= 10;
  }
  return Acum;
}

/// \brief Takes two functions, one is an 'apollo_loop' function.
///        It checks if the second function is a skeleton generated for the
///        'apollo_loop' function.
/// \param apolloLoopFunction an 'apollo_loop' function.
/// \param F a function to check if it is a code skeleton function.
/// \return True if the second function is a skeleton generated from the
///         'apollo_loop' function.
bool isSkeletonForApolloLoopFunction(Function *apolloLoopFunction,
                                     Function *F) {
  const StringRef FunctionName = F->getName();
  const std::string SkeletonPrefix =
      utils::concat(apolloLoopFunction->getName(), "_skeleton");
  return FunctionName.startswith(SkeletonPrefix);
}

/// \brief Compare between two functions by name.
/// \param a a Function.
/// \param b another Function.
/// \return name of a < name of b.
bool skeletonCmp(const SkeletonDescriptor &A, const SkeletonDescriptor &B) {
  return A.ID < B.ID;
}

/// \brief Sorts a vector of functions lexicographically.
/// \param FunctionVector A vector of functions.
void sortFunctionsByName(std::vector<SkeletonDescriptor> &FunctionVector) {
  std::sort(FunctionVector.begin(), FunctionVector.end(), skeletonCmp);
}

/// \brief for an 'apollo_loop' collects all the code skeletons.
/// \param apolloLoopFunction an 'apollo_loop' function.
/// \return A vector with all the code skeletons for a given
///         'apollo_loop' function.
std::vector<SkeletonDescriptor>
collectSkeletonsFor(Function *apolloLoopFunction) {
  std::vector<SkeletonDescriptor> CollectedSkeletons;
  Module *ParentModule = apolloLoopFunction->getParent();
  const std::string JitPrefix = 
                    utils::concat(apolloLoopFunction->getName(), "_jit_");
  for (Function &curFunctionIterator : *ParentModule) {
    if (isSkeletonForApolloLoopFunction(apolloLoopFunction,
                                        &curFunctionIterator)) {

      const unsigned SkeletonId = getSkeletonId(&curFunctionIterator);
      // get the IR for the jit. If not available, leave a 0x0.
      Constant *JitPtr =
          ParentModule->getNamedGlobal(utils::concat(JitPrefix, SkeletonId));
      Constant *JitSize = 0x0;
      if (JitPtr == 0x0) {
        Constant *Zero = constant::zero64(ParentModule->getContext());
        JitSize = Zero;
        JitPtr = ConstantExpr::getIntToPtr(
            Zero, Type::getInt8PtrTy(ParentModule->getContext()));
      } else {
        const unsigned JitArraySize =
            dyn_cast<ArrayType>(JitPtr->getType()->getContainedType(0))
                ->getNumElements();
        JitSize = constant::getInt64Constant(ParentModule->getContext(),
                                             JitArraySize);
        JitPtr = ConstantExpr::getBitCast(
            JitPtr, Type::getInt8PtrTy(ParentModule->getContext()));
      }
      Constant *BinarySkeleton = &curFunctionIterator;
      if (!skeleton::hasBinary(SkeletonId)) {
        BinarySkeleton = ConstantExpr::getIntToPtr(
            constant::zero64(ParentModule->getContext()),
            BinarySkeleton->getType());
      }
      CollectedSkeletons.push_back(
          {SkeletonId, BinarySkeleton, JitPtr, JitSize});
    }
  }
  sortFunctionsByName(CollectedSkeletons);
  return CollectedSkeletons;
}

/// \brief it creates a new global variable with pointers to the skeleton
///        functions.
/// \param apolloFunction an apollo loop function.
/// \return the number of skeletons.
unsigned generateSkeletonArray(Function *apolloFunction) {
  Module *M = apolloFunction->getParent();
  std::vector<SkeletonDescriptor> Skeletons =
      std::move(collectSkeletonsFor(apolloFunction));
  Type *i8Ty = Type::getInt8Ty(M->getContext());
  Type *i64Ty = Type::getInt64Ty(M->getContext());
  Type *voidPtr = PointerType::get(i8Ty, 0);
  StructType *JitStructTy =
      StructType::create(M->getContext(), std::vector<Type *>({voidPtr, i64Ty}),
                         "apollo.jit_bitecode");
  StructType *SkeletonDescriptorTy = StructType::create(
      M->getContext(), std::vector<Type *>({i64Ty, voidPtr, JitStructTy}),
      "apollo.skeleton_descriptor");
  ArrayType *ArrayTy = ArrayType::get(SkeletonDescriptorTy, Skeletons.size());
  std::vector<Constant *> SkeletonPtr;
  for (auto &skeleton : Skeletons) {
    Constant *Desc = ConstantStruct::get(
        SkeletonDescriptorTy,
        std::vector<Constant *>(
            {constant::getInt64Constant(M->getContext(), skeleton.ID),
             ConstantExpr::getBitCast(skeleton.skeleton, voidPtr),
             ConstantStruct::get(JitStructTy, std::vector<Constant *>(
                                                  {skeleton.jit_ptr,
                                                   skeleton.jit_ptr_size}))}));
    SkeletonPtr.push_back(Desc);
  }
  Constant *ConstArr = ConstantArray::get(ArrayTy, SkeletonPtr);
  const std::string GlobalName =
      utils::concat(apolloFunction->getName(), "_skeletons");
  GlobalVariable *GlobalVar =
      cast<GlobalVariable>(M->getOrInsertGlobal(GlobalName, ArrayTy));
  GlobalVar->setConstant(true);
  GlobalVar->setLinkage(GlobalVariable::PrivateLinkage);
  GlobalVar->setInitializer(ConstArr);
  return Skeletons.size();
}

/// \brief Create a function that initializes all the parameters required by the
///        runtime and calls the switching mechanism.
/// \param The apollo_loop function used for identify all the parameters
///        and skeletons.
/// \param The result of the APOLLO_statement_analyzer analysis result.
/// \param DataLayout analysis to know the size of the phi-node state structure.
/// \param skeletonNumber number of available skeletons.
/// \return The new function.
Function *generateApolloRuntimeHookCall(Function *apolloFunction,
                                        const DataLayout &DL,
                                        unsigned SkeletonNumber) {
  LLVMContext &Context = apolloFunction->getContext();
  Module *M = apolloFunction->getParent();
  // Create the function
  Function *RuntimeHookPoint = Function::Create(
      apolloFunction->getFunctionType(), apolloFunction->getLinkage(),
      utils::concat(apolloFunction->getName(), "_", "start"), M);
  std::vector<Value *> Arguments =
      function::getArgumentVector(RuntimeHookPoint);
  const std::vector<std::string> ArgNames = {"params", "phi_state"};
  for (unsigned i = 0; i < Arguments.size(); ++i) {
    Arguments[i]->setName(ArgNames[i]);
  }

  Function *apolloRuntimeHook = M->getFunction("apollo_runtime_hook");
  GlobalVariable *Skeletons =
      M->getNamedGlobal(utils::concat(apolloFunction->getName(), "_skeletons"));
  Type *voidPtr = PointerType::get(Type::getInt8Ty(Context), 0);
  Type *PhiStateTy = Arguments[1]->getType()->getContainedType(0);
  const int PhiStateSize = DL.getTypeStoreSize(PhiStateTy);
  Constant *One = constant::one64(Context);
  Constant *ConstPhiStateSize =
      constant::getInt64Constant(Context, PhiStateSize);
  Constant *ConstNumSkeletons =
      constant::getInt64Constant(Context, SkeletonNumber);
  // fill the function
  BasicBlock *HookBlock = BasicBlock::Create(RuntimeHookPoint->getContext(),
                                             "hook_block", RuntimeHookPoint);
  CastInst *StatePtr = CastInst::CreateTruncOrBitCast(
      Arguments[0], voidPtr, Arguments[0]->getName() + "_cast", HookBlock);
  ir::ApolloIRBuilder Builder(HookBlock);
  Value *SkeletonsPtr = Builder.CreateBitCast(
      Builder.CreateConstGEP2_32(Skeletons->getType()->getContainedType(0),
                                 Skeletons, 0, 0),
      voidPtr);
  // use the skeleton pointer as an unique id for the scop.
  Value *RuntimeHookId =
      Builder.CreatePointerCast(SkeletonsPtr, Type::getInt64Ty(Context));
  // Call to the switching mechanism.
  const std::vector<Value *> HookArgs = {RuntimeHookId, SkeletonsPtr,
                                         ConstNumSkeletons, StatePtr,
                                         ConstPhiStateSize};
  CallInst *RuntimeHookResult =
      Builder.CreateCall(apolloRuntimeHook, HookArgs, "hook_call_result");
  Builder.CreateRet(Builder.CreateSub(RuntimeHookResult, One, "result"));
  return RuntimeHookPoint;
}

/// \@brief Generates the needed functions definitions for the switching
///         mechanism.
/// \param M a module.
void defineApolloSwitchRuntimeFunctions(Module *M) {
  // should define only once.
  const bool ShouldDefine = M->getNamedGlobal("apollo_runtime_hook") == 0x0;
  if (ShouldDefine) {
    LLVMContext &Context = M->getContext();
    Function::LinkageTypes ExternalLinkage = Function::ExternalLinkage;
    Type *i64Ty = Type::getInt64Ty(Context);
    Type *i8Ptr = PointerType::get(Type::getInt8Ty(Context), 0);
    FunctionType *FunTyRunSwitch = FunctionType::get(
        i64Ty, std::vector<Type *>({i64Ty, i8Ptr, i64Ty, i8Ptr, i64Ty}), false);
    Function::Create(FunTyRunSwitch, ExternalLinkage, "apollo_runtime_hook", M);
  }
}

} // end anonymous namespace

namespace apollo {

SwitchingCall::SwitchingCall() : ApolloPass(ID, "SwitchingCall") {}

void SwitchingCall::getAnalysisUsage(AnalysisUsage &AU) const {}

bool SwitchingCall::runOnApollo(Function *apolloFunction) {
  defineApolloSwitchRuntimeFunctions(apolloFunction->getParent());
  const DataLayout &DL = apolloFunction->getParent()->getDataLayout();
  const unsigned NumSkeletons = generateSkeletonArray(apolloFunction);
  Function *apolloRuntimeHookCall =
      generateApolloRuntimeHookCall(apolloFunction, DL, NumSkeletons);
  apolloFunction->replaceAllUsesWith(apolloRuntimeHookCall);
  apolloFunction->eraseFromParent();
  return true;
}

char SwitchingCall::ID = 0;
DefinePassCreator(SwitchingCall);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(SwitchingCall);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(SwitchingCall, "APOLLO_SWITCH_MECHANISM",
                      "APOLLO_SWITCH_MECHANISM", false, false)
INITIALIZE_PASS_END(SwitchingCall, "APOLLO_SWITCH_MECHANISM",
                    "APOLLO_SWITCH_MECHANISM", false, false)
