//===--- ApolloPhiHandling.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"
#include "Transformations/MetadataAdders/ApolloPhiHandling.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Analysis/LoopInfo.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief function to visit a single phi-node at the header of a loop.
/// \param Phi a PHINode.
/// \param PhiId used to assign an id to each phi-node.
void visitPhi(PHINode *Phi, Loop *L, unsigned &PhiId) {
	LLVMContext &Context = Phi->getContext();
	Constant *Id = constant::getInt64Constant(Context, PhiId);
	Constant *ParentLoopId = constant::getInt64Constant(Context,
			loop::getLoopId(L));
	metadata::attachMetadata(Phi, mdkind::ApolloPhiId, { Id });
	metadata::attachMetadata(Phi, mdkind::ParentLoop, { ParentLoopId });
	PhiId++;
}

/// \brief function to visit recursively all the loops.
/// \param L a loop.
/// \param phi_id used to assign an id to each phi-node.
void visitLoop(Loop *L, unsigned &PhiId) {
	BasicBlock *Header = L->getHeader();
	std::vector<PHINode *> PhisToVisit;
	for (Instruction &inst : *Header) {
		if (!isa<PHINode>(inst))
			break;
		PhisToVisit.push_back(&cast<PHINode>(inst));
	}
	for (auto phi : PhisToVisit) {
		visitPhi(phi, L, PhiId);
	}
	for (Loop *subloop : *L) {
		visitLoop(subloop, PhiId);
	}
}

} // end anonymous namespace

namespace apollo {

ApolloPhiHandling::ApolloPhiHandling() :
		ApolloPass(ID, "ApolloPhiHandling") {
}

void ApolloPhiHandling::getAnalysisUsage(AnalysisUsage &AU) const {
	ApolloPass::getAnalysisUsage(AU);
}

bool ApolloPhiHandling::runOnLoop(Loop *L) {
	Function *ParentFunction = loop::getLoopParentFunction(L);
	DEBUG_WITH_TYPE("apollo-pass-phi-handling",
			dbgs() << "function before: \n"; ParentFunction->dump(); dbgs() << "\n\n");
	LoopInfo &LI =
			getAnalysis<LoopInfoWrapperPass>(*ParentFunction).getLoopInfo();
	L = function::getOutermostLoopInFunction(ParentFunction, &LI);
	unsigned PhiId = 0;
	visitLoop(L, PhiId);
	DEBUG_WITH_TYPE("apollo-pass-phi-handling",
			dbgs() << "function after: \n"; ParentFunction->dump());
	return true;
}

char ApolloPhiHandling::ID = 0;
DefinePassCreator(ApolloPhiHandling);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloPhiHandling);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloPhiHandling, "APOLLO_PHI_HANDLING",
		"APOLLO_PHI_HANDLING", false, false)
	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
	INITIALIZE_PASS_END(ApolloPhiHandling, "APOLLO_PHI_HANDLING",
			"APOLLO_PHI_HANDLING", false, false)
