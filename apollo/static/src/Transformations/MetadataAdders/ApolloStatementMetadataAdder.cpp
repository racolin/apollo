//===--- ApolloStatementMetadataAdder.cpp ---------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/MetadataAdders/ApolloStatementMetadataAdder.h"
#include "MetadataKind.h"
#include "Utils/Utils.h"
#include "Utils/ViUtils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Metadata.h"
#include "llvm/IR/InstIterator.h"

#include <unordered_map>
#include <vector>
#include <string>

using namespace apollo;
using namespace llvm;

namespace {

unsigned getNumPredeccessors(const BasicBlock *BB) {
  unsigned Num = 0;
  for (auto it = llvm::pred_begin(BB); it != llvm::pred_end(BB); ++it) {
    Num++;
  }
  return Num;
}

void updateSuccessorsIncomming(BasicBlock *CurBlock, LoopInfo *LI,
                        std::unordered_map<BasicBlock *, unsigned> &Incomming) {

  for (auto succ_it = succ_begin(CurBlock); succ_it != succ_end(CurBlock);
       ++succ_it) {
    if (LI->getLoopFor(*succ_it)) // it must be inside the nest.
      Incomming[*succ_it] = Incomming[*succ_it] - 1;
  }
}

BasicBlock *getNextBlock(Loop *L,
             const std::unordered_map<BasicBlock *, unsigned> &Incomming,
             const std::set<BasicBlock *> &VisitedBlocks) {

  for (auto &block_incomming : Incomming) {
    const bool Visited = VisitedBlocks.count(block_incomming.first) != 0;
    const bool InsideLoop = L->contains(block_incomming.first);
    const bool IncommingVisited = block_incomming.second <= 0;
    if (!Visited && InsideLoop && IncommingVisited)
      return block_incomming.first;
  }
  return 0x0;
}

} // end anonymous namespace

namespace apollo {

ApolloStatementMetadataAdder::ApolloStatementMetadataAdder() : ApolloPass(ID, "ApolloStatementMetadataAdder") {
  IdCounter = 0;
}

void ApolloStatementMetadataAdder::getAnalysisUsage(AnalysisUsage &AU) const {
  ApolloPass::getAnalysisUsage(AU);
}

bool ApolloStatementMetadataAdder::runOnApollo(Function *F) {
  IdCounter = 0;
  DepthNbUsed.clear();
  Table.clear();
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  Loop *L = function::getOutermostLoopInFunction(F, &LI);
  // visit the loop and do it recursively on subloops
  std::unordered_map<BasicBlock *, unsigned> IncommingBlocks;
  for (BasicBlock *block : L->getBlocks()) {
    unsigned PredNum = getNumPredeccessors(block);
    const bool IsHeader = LI[block]->getHeader() == block;
    if (IsHeader) // to break the cycles and get a topological order
      PredNum--;
    IncommingBlocks[block] = PredNum;
  }
  IncommingBlocks[L->getHeader()] = 0;
  auto EmptyVisited = std::set<BasicBlock *>();
  statementAddIdRecursively(L, std::vector<unsigned>(), 0, IncommingBlocks,
                            EmptyVisited, &LI);
  return true;
}

void ApolloStatementMetadataAdder::statementAddIdBlock(
    BasicBlock *CurBlock, Loop *L, std::vector<unsigned> &Path, LoopInfo *LI) {
  LLVMContext &Context = CurBlock->getContext();
  for (Instruction &inst_iter : *CurBlock) {
    // add a metadata for store/load instruction
    if (statement::isMemAccess(&inst_iter)) {
      MapVectorUiType::iterator mit = DepthNbUsed.find(Path);
      // if there are ids in the same level increase the biggest.
      if (mit != DepthNbUsed.end())
        DepthNbUsed[Path] = DepthNbUsed[Path] + 1;
      else
        DepthNbUsed[Path] = 0;

      std::vector<Value *> MetadataNode;
      // first value is id.
      MetadataNode.push_back(constant::getInt64Constant(Context, IdCounter));
      // in the middle the loop path.
      for (unsigned p : Path) {
        MetadataNode.push_back(constant::getInt64Constant(Context, p));
      }
      metadata::attachMetadata(&inst_iter, mdkind::ApolloStatementId,
                               MetadataNode);
      metadata::attachMetadata(
          &inst_iter, mdkind::ParentLoop,
          {constant::getInt64Constant(Context, loop::getLoopId(L))});
      Table[IdCounter] = make_pair(Path, DepthNbUsed[Path]);
      ++IdCounter;
    }
  }
}

void ApolloStatementMetadataAdder::statementAddIdRecursively(
                           Loop *L, 
                           std::vector<unsigned int> Path, 
                           unsigned Position, 
                           std::unordered_map<BasicBlock *, unsigned> &Incomming,
                           std::set<BasicBlock *> &VisitedBlocks, 
                           LoopInfo *LI) {

  Path.push_back(Position);
  std::vector<Loop *> SubLoops = L->getSubLoops();
  BasicBlock *CurBlock;

  while ((CurBlock = getNextBlock(L, Incomming, VisitedBlocks)) != 0x0) {
    const bool isInThisLoop = LI->getLoopFor(CurBlock) == L;
    if (isInThisLoop) {
      statementAddIdBlock(CurBlock, L, Path, LI);
      updateSuccessorsIncomming(CurBlock, LI, Incomming);
      VisitedBlocks.insert(CurBlock);
    } else {
      // find the subloop that contains the block (it can be different from
      // LI->getLoopFor)
      Loop *ParentSubloop = 0x0;
      unsigned SubLoopPos = 0;
      for (SubLoopPos = 0; SubLoopPos < SubLoops.size() && ParentSubloop == 0x0;
           ++SubLoopPos) {
        if (SubLoops[SubLoopPos]->contains(CurBlock))
          ParentSubloop = SubLoops[SubLoopPos];
      }
      statementAddIdRecursively(ParentSubloop, Path, SubLoopPos - 1, Incomming,
                                VisitedBlocks, LI);
    }
  }
}

char ApolloStatementMetadataAdder::ID = 0;
DefinePassCreator(ApolloStatementMetadataAdder);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloStatementMetadataAdder);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloStatementMetadataAdder,
                      "APOLLO_STATEMENT_METADATA_ADDER",
                      "APOLLO_STATEMENT_METADATA_ADDER", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloStatementMetadataAdder,
                    "APOLLO_STATEMENT_METADATA_ADDER",
                    "APOLLO_STATEMENT_METADATA_ADDER", false, false)
