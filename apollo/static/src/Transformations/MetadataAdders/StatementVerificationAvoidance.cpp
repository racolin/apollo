//===--- StatementVerificationAvoidance.cpp -------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/MetadataAdders/StatementVerificationAvoidance.h"
#include "Utils/ViUtils.h"
#include "Utils/SCEVUtils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Metadata.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief collect all the statements that dominates a statement.
std::set<Instruction *> collectStatementsDominating(Instruction *Stmt,
		DominatorTree &DT) {
	BasicBlock *BB = Stmt->getParent();
	Function *F = BB->getParent();
	std::set<Instruction *> StmtsDom;
	for (BasicBlock &blockDom : *F) {
		// collect statements only if it dominates the stmt block
		if (DT.dominates(&blockDom, BB) && &blockDom != BB) {
			for (Instruction &stmtDom : blockDom) {
				if (statement::isStatement(&stmtDom)) {
					StmtsDom.insert(&stmtDom);
				}
			}
		}
	}
	// and collect inside the parent block
	for (Instruction &stmtDom : *BB) {
		if (Stmt == &stmtDom) {
			break;
		}
		if (statement::isStatement(&stmtDom)) {
			StmtsDom.insert(&stmtDom);
		}
	}
	return StmtsDom;
}

/// \brief collect all the statement pointers that dominates a statement.
std::map<Instruction *, Instruction *> collectStmtPointersDominating(
		Instruction *Stmt, DominatorTree &DT) {
	std::set<Instruction *> StatementsDom = std::move(
			collectStatementsDominating(Stmt, DT));
	std::map<Instruction *, Instruction *> StmtPointersDominating;
	for (Instruction *stmtDom : StatementsDom) {
		Value *Ptr = statement::getPointerOperand(stmtDom);
		Instruction *PointerInst = dyn_cast<Instruction>(Ptr);
		if (PointerInst) {
			StmtPointersDominating[PointerInst] = stmtDom;
		}
	}
	return StmtPointersDominating;
}

/// \brief collect all the SCEV objects for all the pointers values used by statements
/// that dominate the given statement.
std::map<const SCEV *, Instruction *> collectStmtPointersSCEVDominating(
		Instruction *Stmt, DominatorTree &DT, ScalarEvolution &SE) {

	std::map<Instruction *, Instruction *> StmtPointersDominating = std::move(
			collectStmtPointersDominating(Stmt, DT));
	std::map<const SCEV *, Instruction *> StmtPointersSCEVDominating;
	for (auto &ptr_stmt : StmtPointersDominating) {
		StmtPointersSCEVDominating[SE.getSCEV(ptr_stmt.first)] = ptr_stmt.second;
	}
	return StmtPointersSCEVDominating;
}

#define scev_case(_Type, _Scev) if (isa<_Type>(_Scev))

/// \brief return true if the scev is a linear transformation from a
///       set of scevs.
bool isLinearFrom(Loop *L, ScalarEvolution &SE, const SCEV *Scev,
		std::map<const SCEV *, Instruction *> &ScevsInst,
		std::set<Instruction *> &LinearFromThis) {

	// No dominators ? MANU
	if (SCEVUtils::isAffine(&SE, Scev, L)) {
		return true;
	}

	// A stmt that dominates itself, is it possible ?
	if (ScevsInst.count(Scev)) {
		LinearFromThis.insert(ScevsInst[Scev]);
		return true;
	}

	// every case != from phi node should be catched before.
	scev_case(SCEVUnknown, Scev)
		return false;
	scev_case(SCEVSignExtendExpr, Scev)
		return isLinearFrom(
			L, SE, cast<SCEVSignExtendExpr>(Scev)->getOperand(), ScevsInst, LinearFromThis);
	scev_case(SCEVZeroExtendExpr, Scev)
		return SE.isKnownNonNegative(Scev) &&
				isLinearFrom(L, SE, cast<SCEVZeroExtendExpr>(Scev)->getOperand(), ScevsInst, LinearFromThis);
	scev_case(SCEVAddExpr, Scev){
		const SCEVAddExpr *ScevAdd = cast<SCEVAddExpr>(Scev);
		for (unsigned scevOp = 0; scevOp < ScevAdd->getNumOperands(); ++scevOp) {
			if (!isLinearFrom(L, SE, ScevAdd->getOperand(scevOp), ScevsInst,
							LinearFromThis))
			return false;
		}
		return true;
	}
	scev_case(SCEVMulExpr, Scev) // all loop invariant but one
	{
		const SCEVMulExpr *ScevMul = cast<SCEVMulExpr>(Scev);
		unsigned Invariants = 0;
		for (unsigned scevOp = 0;
			scevOp < ScevMul->getNumOperands() && Invariants < 2; ++scevOp) {
			const SCEV *ScevOperand = ScevMul->getOperand(scevOp);
			if (!isLinearFrom(L, SE, ScevOperand, ScevsInst, LinearFromThis))
				return false;
			if (!SCEVUtils::isLoopInvariant(&SE, ScevOperand, L))
				++Invariants;
		}
		return Invariants < 2;
	}
	return false;
}

/// \brief This function attaches the IsAffineIterator metadata to all
/// iterators and scalars (i.e; all phi instructions) included
/// in the given loop that are affine. Affine here, means affine
/// regarding all the higher loops including the phi instruction
/// \param L the loop to start from
/// \param SE the scalar evolution pass' result
void attachIsAffineIteratorMetadata(Loop *L, ScalarEvolution &SE) {
	BasicBlock *Header = L->getHeader();
	BasicBlock::iterator instruction = Header->begin();
	for (; isa<PHINode>(instruction); ++instruction) {
		PHINode *Phi = cast<PHINode>(instruction);
		if (!Phi->getType()->isFloatingPointTy()) {
			const SCEV *Scev = SE.getSCEV(Phi);
			if (SCEVUtils::isAffine(&SE, Scev, loop::getOutermostLoop(L))) {
				metadata::attachMetadata(Phi, mdkind::IsAffineIterator,
						mdkind::IsAffineIterator);
			}
		}
	}
	for (Loop *subloop : *L) {
		attachIsAffineIteratorMetadata(subloop, SE);
	}
}

} // end anonymous namespace

namespace apollo {

StatementVerificationAvoidance::StatementVerificationAvoidance() :
		ApolloPass(ID, "StatementVerificationAvoidance") {
}

void StatementVerificationAvoidance::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<DominatorTreeWrapperPass>();
	AU.addRequired<LoopInfoWrapperPass>();
	AU.addRequired<ScalarEvolutionWrapperPass>();
}

bool StatementVerificationAvoidance::canAvoidVerification(Instruction *Stmt,
		Loop *L) {
	Value *Ptr = statement::getPointerOperand(Stmt);
	Instruction *PtrInst = dyn_cast<Instruction>(Ptr);
	// if it's not an instruction, is a constant->it's not neccesary to verify it.
	// if it's outside the loop don't verify.
	// if it's affine don't verif.
	const bool MustVerif = PtrInst && L->contains(PtrInst->getParent())
			&& !(metadata::hasMetadataKind(Stmt, mdkind::IsAffine));
	return !MustVerif;
}

bool StatementVerificationAvoidance::runOnApollo(Function *F) {

	// Get analyses
	DominatorTree &DT = getAnalysis<DominatorTreeWrapperPass>(*F).getDomTree();
	LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
	ScalarEvolutionWrapperPass &SeWrapper = getAnalysis<
			ScalarEvolutionWrapperPass>(*F);
	ScalarEvolution &SE = SeWrapper.getSE();

	// Attach IsAffineIterator metadata
	Loop *L = function::getOutermostLoopInFunction(F, &LI);
	attachIsAffineIteratorMetadata(L, SE);

	// Attach IsAffineFromVerified, IsAffine and NonLinearAtLevel to memory accesses
	for (BasicBlock *block : L->getBlocks()) {
		for (Instruction &stmt : *block) {
			if (statement::isStatement(&stmt)) {
				int NonLinearLoopId = -1;
				Value *Ptr = statement::getPointerOperand(&stmt);
				Instruction *PointerInst = dyn_cast<Instruction>(Ptr);

				// If the address used by the mem access is a pointer,
				// we try to identify if it's an affine form of loop coutners
				if (PointerInst) {

					// if its linear transformation from verified values, add metadata
					const SCEV *Scev = SE.getSCEV(PointerInst);
					std::map<const SCEV *, Instruction *> StmtPointersSCEVDominating =
							std::move(
									collectStmtPointersSCEVDominating(&stmt, DT, SE));
					std::set<Instruction *> VerifiedFromThese;
					bool LinearVerified = isLinearFrom(L, SE, Scev,
							StmtPointersSCEVDominating, VerifiedFromThese);
					if (LinearVerified) {

						// In this case, the stmt has dominators with pointer
						// and "they are all affine"
						if (!VerifiedFromThese.empty()) {
							std::vector<Value *> VerifIds;
							for (Instruction *stmt_ver : VerifiedFromThese) {
								Value *Id = metadata::getMetadataOperand(
										stmt_ver, mdkind::ApolloStatementId);
								VerifIds.push_back(Id);
							}
							metadata::attachMetadata(&stmt,
									mdkind::IsAffineFromVerified, VerifIds);
						}

						// In this case, the stmt has no dominators with pointer
						// and it is affine
						else {
							metadata::attachMetadata(&stmt, mdkind::IsAffine,
									mdkind::IsAffine);
						}
					}

					// Compute non linear loop level
					Loop *NonLinearLoop = SCEVUtils::getNonLinearLoopLevel(&SE,
							Scev, loop::getOutermostLoop(L),
							LI.getLoopFor(PointerInst->getParent()));
					if (NonLinearLoop) {
						NonLinearLoopId = loop::getLoopId(NonLinearLoop);
					}
				}

				// Else it's a constant access and thus affine
				else {
					metadata::attachMetadata(&stmt, mdkind::IsAffine,
							mdkind::IsAffine);
				}

				metadata::attachMetadata(&stmt, mdkind::NonLinearAtLevel,
						{ ConstantInt::get(Type::getInt64Ty(stmt.getContext()),
								NonLinearLoopId) });
			}
		}
	}

	SeWrapper.releaseMemory();
	return true;
}

char StatementVerificationAvoidance::ID = 0;
DefinePassCreator(StatementVerificationAvoidance);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(StatementVerificationAvoidance);
}

INITIALIZE_PASS_BEGIN(StatementVerificationAvoidance,
		"APOLLO_STATEMENT_VERIF_AVOIDANCE",
		"APOLLO_STATEMENT_VERIF_AVOIDANCE", false, false)
	INITIALIZE_PASS_DEPENDENCY(DominatorTreeWrapperPass)
	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
	INITIALIZE_PASS_DEPENDENCY(ScalarEvolutionWrapperPass)
	INITIALIZE_PASS_END(StatementVerificationAvoidance,
			"APOLLO_STATEMENT_VERIF_AVOIDANCE",
			"APOLLO_STATEMENT_VERIF_AVOIDANCE", false, false)
