//===--- FunctionCallParent.cpp -------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/MetadataAdders/FunctionCallParent.h"
#include "Utils/MetadataHandler.h"
#include "Utils/LoopUtils.h"
#include "Utils/ConstantUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief attachParentLoop attach the parent loop for a function call.
/// \param inst an instruction
/// \param loop_id parent loop id.
void attachParentLoop(Instruction *Inst, const unsigned LoopId) {
  metadata::attachMetadata(
      Inst, mdkind::ParentLoop,
      {constant::getInt64Constant(Inst->getContext(), LoopId)});
}

} // end anonymous namespace

namespace apollo {

FunctionCallParent::FunctionCallParent() : pass::ApolloPass(ID, "FunctionCallParent") {}

void FunctionCallParent::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool FunctionCallParent::runOnLoopLI(Loop *Outermost, LoopInfo *LI) {
  bool CallFound = false;
  for (BasicBlock *block : Outermost->getBlocks()) {
    const unsigned LoopId = loop::getLoopId(LI->getLoopFor(block));
    for (Instruction &call : *block) {
      if (isa<CallInst>(call)) {
        attachParentLoop(&call, LoopId);
        CallFound = true;
      }
    }
  }
  return CallFound;
}

char FunctionCallParent::ID = 0;
DefinePassCreator(FunctionCallParent);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(FunctionCallParent);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(FunctionCallParent, "APOLLO_FUNCTION_CALL_PARENTS",
                      "APOLLO_FUNCTION_CALL_PARENTS", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(FunctionCallParent, "APOLLO_FUNCTION_CALL_PARENTS",
                    "APOLLO_FUNCTION_CALL_PARENTS", false, false)
