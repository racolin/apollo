//===--- RegisterStaticInfo.cpp -------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "StaticInfoConfig.h"
#include "Transformations/Dumpers/RegisterStaticInfo.h"
#include "Utils/Utils.h"
#include "Utils/ViUtils.h"
#include "Utils/TypeUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/FunctionUtils.h"

#include "llvm/Analysis/TargetTransformInfo.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"

#include <vector>
#include <stack>

using namespace apollo;
using namespace llvm;

namespace {

void defineRegisterFunctions(Module *M) {

  LLVMContext &Context = M->getContext();
  Type *voidTy = Type::getVoidTy(Context);
  Type *i8Ty = Type::getInt8Ty(Context);
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *i8PtrTy = PointerType::get(i8Ty, 0);

  std::vector<std::string> Name = {"apollo_register_static_info"};
  std::vector<bool> VarArgs = {true};
  std::vector<Type *> returnTy = {voidTy};
  std::vector<Type *> RegisterStaticInfoArgs = {i8PtrTy, i64Ty, i64Ty, i64Ty};
  std::vector<std::vector<Type *>> ArgsTy{RegisterStaticInfoArgs};
  for (size_t i = 0; i < Name.size(); ++i) {
    if (!M->getFunction(Name[i])) {
      FunctionType *FunTy =
          FunctionType::get(returnTy[i], ArgsTy[i], VarArgs[i]);
      Function::Create(FunTy, GlobalVariable::ExternalLinkage, Name[i], M);
    }
  }
}

unsigned getNumScalars(Loop *L) {
  int Num = 0;
  for (BasicBlock &block : *L->getHeader()->getParent()) {
    for (Instruction &inst : block) {
      if (!isa<PHINode>(inst))
        break;
      if (metadata::hasMetadataKind(&inst, mdkind::ApolloPhiId))
        Num++;
    }
  }
  return Num;
}

unsigned getNumStmts(Loop *L) {
  int Num = 0;
  for (BasicBlock &block : *L->getHeader()->getParent()) {
    for (Instruction &inst : block) {
      if (statement::isStatement(&inst))
        Num++;
    }
  }
  return Num;
}

void registerInit(BasicBlock *BB, Function *F, Argument *Arg, Loop *Outermost) {
  const unsigned NumLoops = loop::getNumLoops(Outermost);
  const unsigned NumScalars = getNumScalars(Outermost);
  const unsigned NumStmts = getNumStmts(Outermost);
  LLVMContext &Context = BB->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  std::vector<Value *> Args;
  Args.push_back(Arg);
  Args.push_back(ConstantInt::get(i64Ty, StaticInfoType::Init));
  Args.push_back(ConstantInt::get(i64Ty, 0));
  Args.push_back(ConstantInt::get(i64Ty, 3));
  Args.push_back(ConstantInt::get(i64Ty, NumLoops));
  Args.push_back(ConstantInt::get(i64Ty, NumScalars));
  Args.push_back(ConstantInt::get(i64Ty, NumStmts));
  CallInst::Create(F, Args, "", BB->getTerminator());
}

void registerLoops(BasicBlock *BB, Function *F, Argument *Arg, Loop *L) {
  const auto Parents = loop::getLoopIdVector(L);
  LLVMContext &Context = BB->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  std::vector<Value *> Args;
  Args.push_back(Arg);
  Args.push_back(ConstantInt::get(i64Ty, StaticInfoType::LoopParents));
  Args.push_back(ConstantInt::get(i64Ty, Parents.back()));
  Args.push_back(ConstantInt::get(i64Ty, Parents.size()));
  for (auto parentId : Parents) {
    Args.push_back(ConstantInt::get(i64Ty, parentId));
  }
  CallInst::Create(F, Args, "", BB->getTerminator());
  for (Loop *subloop : *L) {
    registerLoops(BB, F, Arg, subloop);
  }
}

void registerScalars(BasicBlock *BB, Function *F, Argument *Arg, Loop *L) {
  const auto ParentId = std::move(loop::getLoopId(L));
  LLVMContext &Context = BB->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  std::vector<Value *> Args;
  Args.push_back(Arg);
  Args.push_back(ConstantInt::get(i64Ty, StaticInfoType::ScalarParents));
  Args.push_back(nullptr); // dummy for the scalar id.
  Args.push_back(ConstantInt::get(i64Ty, 1));
  Args.push_back(ConstantInt::get(i64Ty, ParentId));
  for (Instruction &inst : *L->getHeader()) {
    PHINode *Phi = dyn_cast<PHINode>(&inst);
    if (!Phi)
      break;
    if (basicscalar::isBasicScalar(Phi)) {
      const int ScalarId = basicscalar::getBasicScalarId(Phi);
      Args[2] = ConstantInt::get(i64Ty, ScalarId);
      CallInst::Create(F, Args, "", BB->getTerminator());
    }
  }
  for (Loop *subloop : *L) {
    registerScalars(BB, F, Arg, subloop);
  }
}

void registerStmts(BasicBlock *BB, Function *F, Argument *Arg, Loop *L,
                   LoopInfo &LI) {
  const auto ParentId = std::move(loop::getLoopId(L));
  Module *M = F->getParent();
  const DataLayout &DL = M->getDataLayout();
  LLVMContext &Context = BB->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  std::vector<Value *> Args;
  Args.push_back(Arg);
  Args.push_back(ConstantInt::get(i64Ty, StaticInfoType::StmtInfo));
  Args.push_back(nullptr); // dummy for the stmt id.
  Args.push_back(ConstantInt::get(i64Ty, 4));
  Args.push_back(nullptr); // size
  Args.push_back(nullptr); // is_write
  Args.push_back(ConstantInt::get(i64Ty, ParentId));
  Args.push_back(nullptr); // non linear level
  for (BasicBlock *block : L->getBlocks()) {
    if (LI.getLoopFor(block) == L) {
      for (Instruction &stmt : *block) {
        if (statement::isStatement(&stmt)) {
          Type *PtrTy = statement::getPointerOperand(&stmt)->getType();
          const int StmtId = statement::getStatementId(&stmt);
          const int Size = DL.getTypeStoreSize(PtrTy->getContainedType(0));
          const bool IsWrite = isa<StoreInst>(&stmt);
          int NonLinearLevel = -1;
          if (metadata::hasMetadataKind(&stmt, mdkind::NonLinearAtLevel)) {
            Value *NonLinearLevelVal =
                metadata::getMetadataOperand(&stmt, mdkind::NonLinearAtLevel);
            NonLinearLevel =
                cast<ConstantInt>(NonLinearLevelVal)->getSExtValue();
          }
          Args[2] = ConstantInt::get(i64Ty, StmtId);
          Args[4] = ConstantInt::get(i64Ty, Size);
          Args[5] = ConstantInt::get(i64Ty, IsWrite);
          Args[7] = ConstantInt::get(i64Ty, NonLinearLevel);
          CallInst::Create(F, Args, "", BB->getTerminator());
        }
      }
    }
  }
  for (Loop *subloop : *L) {
    registerStmts(BB, F, Arg, subloop, LI);
  }
}

void registerParamsSize(BasicBlock *RegBlock, Function *RegisterFun,
                        Argument *Arg, Function *F) {

  LLVMContext &Context = RegBlock->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *ParamsTy = F->arg_begin()->getType()->getContainedType(0);
  const size_t ParamsSize =
               F->getParent()->getDataLayout().getTypeStoreSize(ParamsTy);
  std::vector<Value *> Args;
  Args.push_back(Arg);
  Args.push_back(ConstantInt::get(i64Ty, StaticInfoType::ParamsSize));
  Args.push_back(ConstantInt::get(i64Ty, 0));
  Args.push_back(ConstantInt::get(i64Ty, 1));
  Args.push_back(ConstantInt::get(i64Ty, ParamsSize));
  CallInst::Create(RegisterFun, Args, "", RegBlock->getTerminator());
}

} // end anonymous namespace

namespace apollo {

RegisterStaticInfo::RegisterStaticInfo() : ApolloPass(ID, "RegisterStaticInfo") {}

void RegisterStaticInfo::getAnalysisUsage(AnalysisUsage &AU) const {
  ApolloPass::getAnalysisUsage(AU);
}

bool RegisterStaticInfo::runOnApollo(Function *F) {
  LLVMContext &Context = F->getContext();
  Module *M = F->getParent();
  defineRegisterFunctions(M);
  Type *voidTy = Type::getVoidTy(Context);
  Type *i8Ty = Type::getInt8Ty(Context);
  Type *i8PtrTy = PointerType::get(i8Ty, 0);
  FunctionType *RegisterInfoSkeletonTy =
      FunctionType::get(voidTy, {i8PtrTy}, false);
  Function *RegisterInfoSkeleton = Function::Create(
      RegisterInfoSkeletonTy, GlobalVariable::PrivateLinkage,
      utils::concat(F->getName(), "_skeleton_", skeleton::static_info::id), M);
  BasicBlock *Block =
      BasicBlock::Create(Context, "register", RegisterInfoSkeleton);
  ReturnInst::Create(Context, Block);
  Function *RegFun = M->getFunction("apollo_register_static_info");
  Argument *StaticInfo = &*(RegisterInfoSkeleton->arg_begin());
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  Loop *L = function::getOutermostLoopInFunction(F, &LI);
  registerInit(Block, RegFun, StaticInfo, L);
  registerLoops(Block, RegFun, StaticInfo, L);
  registerScalars(Block, RegFun, StaticInfo, L);
  registerStmts(Block, RegFun, StaticInfo, L, LI);
  registerParamsSize(Block, RegFun, StaticInfo, F);
  return true;
}

char RegisterStaticInfo::ID = 0;
DefinePassCreator(RegisterStaticInfo);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(RegisterStaticInfo);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(RegisterStaticInfo, "APOLLO_LOOP_INFO_DUMP",
                      "APOLLO_LOOP_INFO_DUMP", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(RegisterStaticInfo, "APOLLO_LOOP_INFO_DUMP",
                    "APOLLO_LOOP_INFO_DUMP", false, false)
