//===--- RegisterStmtAlias.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "StaticInfoConfig.h"
#include "Transformations/Dumpers/RegisterStmtAlias.h"
#include "Utils/MetadataHandler.h"
#include "Utils/ViUtils.h"
#include "Utils/Utils.h"

#include "llvm/Analysis/TargetTransformInfo.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Analysis/TypeBasedAliasAnalysis.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"

#include <map>

using namespace apollo;
using namespace llvm;

namespace {

MemoryLocation getMemoryLocation(Instruction *I) {
  if (isa<LoadInst>(I))
    return MemoryLocation::get(cast<LoadInst>(I));
  if (isa<StoreInst>(I))
    return MemoryLocation::get(cast<StoreInst>(I));
  assert(false);
  return MemoryLocation();
}

bool stmtIncompatibleTypes(Type *A, Type *B) {
  // HACK TO OVERCOME THAT THE LLVM AA STOPED WORKING.
  if (A == B)
    return false;
  const int AreTheyPointers = A->isPointerTy() + B->isPointerTy();
  if (AreTheyPointers == 1)
    return true;
  if (AreTheyPointers == 2)
    return stmtIncompatibleTypes(A->getContainedType(0),
                                 B->getContainedType(0));

  const int AreTheyFloats = A->isFloatingPointTy() + B->isFloatingPointTy();
  const int AreTheyIntegers = A->isIntegerTy() + B->isIntegerTy();

  // A float does not alias with a double, an int32 does not alias with an int64
  if (AreTheyFloats == 2 || AreTheyIntegers == 2)
    return A != B;

  // A float and an integer do not alias
  if (AreTheyFloats == 1 && AreTheyIntegers == 1)
    return true;
  return false;
}

template <typename AAType>
std::vector<int>
statementNoAlias(const int StmtId, 
                 std::map<int, Instruction *> &Stmts, AAType &AA) {
  std::vector<int> AliasResult;
  Instruction *Stmt = Stmts[StmtId];
  Type *StmtContainedType =
      statement::getPointerOperand(Stmt)->getType()->getContainedType(0);
  MemoryLocation StmtLocation = getMemoryLocation(Stmt);
  for (auto &other_stmt_pair : Stmts) {
    const int OtherStmtId = other_stmt_pair.first;
    Instruction *OtherStmt = other_stmt_pair.second;
    Type *OtherStmtContainedType =
        statement::getPointerOperand(OtherStmt)->getType()->getContainedType(0);
    MemoryLocation OtherStmtLocation = getMemoryLocation(OtherStmt);
    // FIXME: to overcome that the AA is not working
    const bool IncompatibeTypes =
        stmtIncompatibleTypes(StmtContainedType, OtherStmtContainedType);

    if (AA.isNoAlias(StmtLocation, OtherStmtLocation) || IncompatibeTypes) {
      AliasResult.push_back(OtherStmtId);
    }
  }
  return AliasResult;
}

template <typename AAType>
void registerAA(BasicBlock *BB, Function *F, Argument *Arg,
                std::map<int, Instruction *> &Stmts, AAType &AA) {

  LLVMContext &Context = BB->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  for (auto &stmt : Stmts) {
    const int StmtId = stmt.first;
    auto NoAlias = statementNoAlias<AAType>(StmtId, Stmts, AA);
    if (!NoAlias.empty()) {
      std::vector<Value *> Args;
      Args.push_back(Arg);
      Args.push_back(ConstantInt::get(i64Ty, StaticInfoType::StmtNoAlias));
      Args.push_back(ConstantInt::get(i64Ty, StmtId));
      Args.push_back(ConstantInt::get(i64Ty, NoAlias.size()));
      for (auto &no_alias_stmt_id : NoAlias) {
        Args.push_back(ConstantInt::get(i64Ty, no_alias_stmt_id));
      }
      CallInst::Create(F, Args, "", BB->getTerminator());
    }
  }
}

} // end anonymous namespace

namespace apollo {

RegisterStmtAlias::RegisterStmtAlias() : ApolloPass(ID, "RegisterStmtAlias") {}

void RegisterStmtAlias::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<AAResultsWrapperPass>();
}

bool RegisterStmtAlias::runOnApollo(Function *F) {
  Module *M = F->getParent();
  Function *RegisterInfoSkeleton = M->getFunction(
      utils::concat(F->getName(), "_skeleton_", skeleton::static_info::id));
  BasicBlock *Block = &RegisterInfoSkeleton->getEntryBlock();
  Function *RegFun = M->getFunction("apollo_register_static_info");
  Argument *StaticInfo = &*(RegisterInfoSkeleton->arg_begin());
  std::map<int, Instruction *> Stmts;
  for (BasicBlock &block : *F) {
    for (Instruction &inst : block) {
      if (statement::isStatement(&inst)) {
        const int id = statement::getStatementId(&inst);
        Stmts[id] = &inst;
      }
    }
  }
  AAResultsWrapperPass &AaWrapper = getAnalysis<AAResultsWrapperPass>(*F);
  AliasAnalysis &AA = AaWrapper.getAAResults();
  registerAA<AliasAnalysis>(Block, RegFun, StaticInfo, Stmts, AA);
  return true;
}

char RegisterStmtAlias::ID = 0;
DefinePassCreator(RegisterStmtAlias);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(RegisterStmtAlias);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(RegisterStmtAlias, "APOLLO_AA", "APOLLO_AA", false, false)
INITIALIZE_PASS_DEPENDENCY(AAResultsWrapperPass)
INITIALIZE_PASS_END(RegisterStmtAlias, "APOLLO_AA", "APOLLO_AA", false, false)
