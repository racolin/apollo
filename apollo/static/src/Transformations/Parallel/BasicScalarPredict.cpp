//===--- BasicScalarPredict.cpp -------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/Parallel/BasicScalarPredict.h"
#include "Transformations/CodeBones/CodeBonesUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/Utils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {

Function *DeclarePredFunction(Loop *L) {
  Function *OriginalFun = loop::getLoopParentFunction(L);
  Module *M = OriginalFun->getParent();
  FunctionType *OriginalFunTy = OriginalFun->getFunctionType();
  Type *PhiStateTy = OriginalFunTy->getParamType(1);
  Type *CoefTy = Type::getInt8PtrTy(M->getContext());
  Type *i64Ty = Type::getInt64Ty(M->getContext());
  Type *voidTy = Type::getVoidTy(M->getContext());
  std::vector<Type *> ArgTypes = {PhiStateTy, CoefTy, i64Ty};
  FunctionType *PredictionFunTy = FunctionType::get(voidTy, ArgTypes, false);
  const std::string PredictionFuncName =
      utils::concat(OriginalFun->getName(), "_",
                    skeleton::sk_name<skeleton::basic_scalar_predict>());

  return Function::Create(PredictionFunTy, Function::PrivateLinkage,
                          PredictionFuncName, M);
}

Function *DeclareGetCoefFunction(Module *M) {
  Function *F = M->getFunction("get_coefficient");
  if (F)
    return F;
  Type *i64Ty = Type::getInt64Ty(M->getContext());
  Type *CoefTy = Type::getInt8PtrTy(M->getContext());
  std::vector<Type *> ArgTypes = {CoefTy, i64Ty, i64Ty, i64Ty};
  FunctionType *FunTy =
      FunctionType::get(i64Ty, ArgTypes, false);
  Function *GetCoef =
      Function::Create(FunTy, Function::ExternalLinkage, "get_coefficient", M);
  GetCoef->addFnAttr(Attribute::ReadNone);
  return GetCoef;
}

Function *DeclareMemoryRuntimeVerifyFunction(Module *M) {
  Function *F = M->getFunction("memory_runtime_verify");
  if (F)
    return F;
  LLVMContext &Context = M->getContext();
  Type *retTy = Type::getInt1Ty(Context);
  Type *StmtId = Type::getInt64Ty(Context);
  Type *voidPtr = Type::getInt8PtrTy(Context);
  Type *NumberOfViIter = Type::getInt32Ty(Context);
  std::vector<Type *> ArgTypes = {StmtId, voidPtr, NumberOfViIter};
  FunctionType *FunTy =
      FunctionType::get(retTy, ArgTypes, true);
  Function *MemoryVerify = Function::Create(FunTy, Function::ExternalLinkage,
                                            "memory_runtime_verify", M);
  MemoryVerify->addFnAttr(Attribute::ReadNone);
  return MemoryVerify;
}

} // end anonymous namespace

namespace apollo {

BasicScalarPredict::BasicScalarPredict() : ApolloPass(ID, "BasicScalarPredict") {}

void BasicScalarPredict::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool BasicScalarPredict::runOnLoop(Loop *L) {
  Function *GetCoef =
      DeclareGetCoefFunction(L->getHeader()->getParent()->getParent());
  Function *PredictionFunction = DeclarePredFunction(L);
  Function *MemoryRuntimeVerify = DeclareMemoryRuntimeVerifyFunction(
      L->getHeader()->getParent()->getParent());

  BasicBlock *Header = L->getHeader();

  LLVMContext &Context = PredictionFunction->getContext();
  BasicBlock *Entry = BasicBlock::Create(Context, "entry", PredictionFunction);
  ReturnInst::Create(Context, Entry);

  auto params_it = PredictionFunction->arg_begin();
  Argument &PhiState = *params_it;
  params_it++;
  Argument &CoefStruct = *params_it;
  params_it++;
  Argument &ViValue = *params_it;
  params_it++;

  PhiState.setName("phi_state");
  CoefStruct.setName("coefs");
  ViValue.setName("vi_0");

  IRBuilder<> Builder(Entry->getTerminator());

  Type *i64Ty = Type::getInt64Ty(Context);
  Constant *Zero = ConstantInt::get(i64Ty, 0);
  Constant *One = ConstantInt::get(i64Ty, 1);
  for (Instruction &phi : *Header) {
    if (!isa<PHINode>(phi))
      break;
    Type *PhiTy = phi.getType();
    if (PhiTy->isIntegerTy() || PhiTy->isPointerTy()) {
      Constant *PhiId = ConstantInt::get(
          i64Ty, cast<ConstantInt>(
                     metadata::getMetadataOperand(&phi, mdkind::ApolloPhiId))
                     ->getSExtValue());
      ConstantInt *PhiStateField = cast<ConstantInt>(
          metadata::getMetadataOperand(&phi, mdkind::ApolloPhiStateField));
      Type *PhiStateFieldPtrTy =
          cast<PointerType>(PhiState.getType()->getScalarType())
              ->getElementType();

      Value *PhiStateFieldPtr = Builder.CreateStructGEP(
          PhiStateFieldPtrTy, &PhiState, PhiStateField->getSExtValue());
      Value *ViCoef = Builder.CreateCall(
          GetCoef, std::vector<Value *>({&CoefStruct, One, PhiId, Zero}));
      Value *ConsCoef = Builder.CreateCall(
          GetCoef, std::vector<Value *>({&CoefStruct, One, PhiId, One}));

      Value *Prediction =
          Builder.CreateAdd(Builder.CreateMul(ViCoef, &ViValue), ConsCoef);
      if (PhiTy->isIntegerTy())
        Prediction = Builder.CreateTruncOrBitCast(Prediction, PhiTy);
      else if (PhiTy->isPointerTy())
        Prediction = Builder.CreateIntToPtr(Prediction, PhiTy);
      else
        assert(false && "Unexpected type");
      Builder.CreateStore(Prediction, PhiStateFieldPtr);
    }
  }
  return true;
}

char BasicScalarPredict::ID = 0;
DefinePassCreator(BasicScalarPredict);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(BasicScalarPredict);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(BasicScalarPredict, "BASIC_SCALAR_PREDICT",
                      "BASIC_SCALAR_PREDICT", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(BasicScalarPredict, "BASIC_SCALAR_PREDICT",
                    "BASIC_SCALAR_PREDICT", false, false)
