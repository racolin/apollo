//===--- ApolloReg2Mem.cpp ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Preparation/ApolloReg2Mem.h"
#include "Utils/Utils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/ValueMap.h"
#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"

#include <unordered_map>

using namespace llvm;
using namespace apollo;

namespace {

/// \brief breaks a phi-node into memory accesses. It inserts the alloc at the
///        function entry point.
/// \param phi a phi-node
/// \return the new phi-node alloc.
AllocaInst *demotePHIToStack(PHINode *Phi, Loop *L) {
  Function *F = Phi->getParent()->getParent();
  AllocaInst *Slot =
      new AllocaInst(Phi->getType(), 0, Phi->getName() + ".reg2mem",
                     &*(F->getEntryBlock().begin()));
  for (unsigned i = 0, e = Phi->getNumIncomingValues(); i < e; ++i) {
    StoreInst *Store = new StoreInst(Phi->getIncomingValue(i), Slot,
                                     Phi->getIncomingBlock(i)->getTerminator());

    if (L) {
      if (L->contains(Phi->getIncomingBlock(i)))
        metadata::attachMetadata(
            Store, mdkind::BasicScalarFinal,
            {constant::getInt64Constant(F->getContext(), loop::getLoopId(L))});
      else
        metadata::attachMetadata(
            Store, mdkind::BasicScalarInitial,
            {constant::getInt64Constant(F->getContext(), loop::getLoopId(L))});
    }
  }

  BasicBlock::iterator insert_ptr(Phi);
  for (; isa<PHINode>(insert_ptr) || isa<LandingPadInst>(insert_ptr);
       ++insert_ptr);

  Value *V = new LoadInst(Slot, Phi->getName() + ".reload", &*insert_ptr);

  Phi->replaceAllUsesWith(V);
  SmallVector<std::pair<unsigned, MDNode *>, 8> metadata;
  Phi->getAllMetadata(metadata);

  for (unsigned i = 0; i < metadata.size(); ++i) {
    Slot->setMetadata(metadata[i].first, metadata[i].second);
  }
  Phi->eraseFromParent();
  return Slot;
}

/// \brief It maps the phi-nodes from the first function with phis from the
///        second function.
/// \param skeleton a function.
/// \param original map phis from this function.
/// \return a std::map of phinodes.
std::unordered_map<PHINode *, PHINode *> mapPhis(Function *SkeletonFunction,
                                                 Function *OriginalFunction) {
  std::unordered_map<PHINode *, PHINode *> PhiMap;
  std::map<BasicBlock *, BasicBlock *> BBMap;
  for (BasicBlock &skeleton_block : *SkeletonFunction) {
    BBMap[&skeleton_block] = 0x0;
    for (BasicBlock &mapped : *OriginalFunction) {
      if (mapped.getName() == skeleton_block.getName())
        BBMap[&skeleton_block] = &mapped;
    }
  }

  for (auto &bpair : BBMap) {
    for (BasicBlock::iterator sk_phi = bpair.first->begin();
         isa<PHINode>(sk_phi); ++sk_phi) {
      PhiMap[cast<PHINode>(sk_phi)] = 0x0;
      if (bpair.second) {
        for (BasicBlock::iterator orig_phi = bpair.second->begin();
             isa<PHINode>(orig_phi); ++orig_phi) {
          if (orig_phi->getName() == sk_phi->getName())
            PhiMap[cast<PHINode>(sk_phi)] = cast<PHINode>(orig_phi);
        }
      }
    }
  }
  return PhiMap;
}

/// \brief It breaks the phi-nodes of a skeleton function,
///        but keeping a reference to the original phi-node in the apollo
///        function as metadata.
/// \param skeletonFunction a code skeleton function.
/// \param apolloFunction the corresponding apollo loop function.
void breakPhiNodes(Function *SkeletonFunction, Function *apolloFunction,
                   LoopInfo *LI) {

  std::unordered_map<PHINode *, PHINode *> PhiMap =
      std::move(mapPhis(SkeletonFunction, apolloFunction));

  for (const std::pair<PHINode *, PHINode *> &phiMapping : PhiMap) {
    PHINode *PhiToBreak = phiMapping.first;
    PHINode *OriginalPhi = phiMapping.second;
    Loop *L = LI->getLoopFor(PhiToBreak->getParent());
    AllocaInst *phiAlloc = demotePHIToStack(PhiToBreak, L);
    if (OriginalPhi) {
      SmallVector<std::pair<unsigned, MDNode *>, 8> mdVector;
      OriginalPhi->getAllMetadata(mdVector);
      for (auto &mdPair : mdVector) {
        phiAlloc->setMetadata(mdPair.first, mdPair.second);
      }
    }
  }
}

} // end anonymous namespace

namespace apollo {

ApolloReg2Mem::ApolloReg2Mem() : ApolloPass(ID, "ApolloReg2Mem") {}

ApolloReg2Mem::ApolloReg2Mem(std::string SkeletonName) : ApolloPass(ID, "ApolloReg2Mem") {
  this->SkelName = SkeletonName;
}

void ApolloReg2Mem::getAnalysisUsage(AnalysisUsage &AU) const {
  ApolloPass::getAnalysisUsage(AU);
}

bool ApolloReg2Mem::runOnApollo(Function *F) {
  Module *M = F->getParent();
  Function *SkeletonFunction =
      M->getFunction(utils::concat(F->getName(), "_", SkelName));
  if (SkeletonFunction) {
    LoopInfo &LI =
        getAnalysis<LoopInfoWrapperPass>(*SkeletonFunction).getLoopInfo();
    breakPhiNodes(SkeletonFunction, F, &LI);
    return true;
  }
  return false;
}

char ApolloReg2Mem::ID = 0;

DefinePassCreator_1(ApolloReg2Mem, std::string);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloReg2Mem);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloReg2Mem, "APOLLO_REG2MEM", "Apollo Reg2Mem.", false,
                      false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloReg2Mem, "APOLLO_REG2MEM", "Apollo Reg2Mem.", false,
                    false)
