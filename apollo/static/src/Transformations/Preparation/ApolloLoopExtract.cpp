//===--- ApolloLoopExtract.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Preparation/ApolloLoopExtract.h"
#include "Utils/FunctionUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/LoopUtils.h"
#include "Utils/Utils.h"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Dominators.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Support/Debug.h"

#include <set>

using namespace llvm;
using namespace apollo;

namespace {

/// \brief Erase the apollo_scop metadata from the instruction.
/// \return true if it removed metadata, false if not.
bool removePragmaInstr(Instruction *I) {
  const bool HasMetadata = metadata::hasMetadataKind(I, mdkind::apollo);
  if (HasMetadata)
    I->setMetadata(mdkind::apollo, 0x0);
  return HasMetadata;
}

/// \brief instructionHasPragmaDcop returns true if the instruction has a
///        pragma.
/// \param I
/// \return true if the Instruction has a pragma
bool instructionHasPragmaDcop(Instruction &I) {
  if (metadata::hasMetadataKind(&I, mdkind::apollo)) {
    const unsigned NumMdOperands =
        metadata::getMetadataNumOperands(&I, mdkind::apollo);
    for (unsigned i = 0; i < NumMdOperands; ++i) {
      MDNode *MdNode = I.getMetadata(mdkind::apollo);
      Metadata *MD = MdNode->getOperand(i);
      MDString *MdStr = dyn_cast<MDString>(MD);
      if (MdStr && MdStr->getString() == "dcop")
        return true;
    }
  }
  return false;
}

/// \brief Identifies if the loop is tagged as an apollo loop.
/// \param L a loop.
/// \param LI
/// \return true if the loop is tagged as an apollo loop.
bool loopHasPragma(Loop *L, LoopInfo *LI) {
  for (BasicBlock *block : L->getBlocks()) {
    if (LI->getLoopFor(block) == L) {
      const bool HasPragma = std::find_if(block->begin(), block->end(),
                                      instructionHasPragmaDcop) != block->end();
      if (!HasPragma)
        return false;
    }
  }
  return true;
}

/// \brief Identifies if the loop is tagged as an apollo loop.
/// \param L a loop.
/// \return true if the loop is tagged as an apollo loop.
bool loopHasAnyPragma(Loop *L) {
  for (BasicBlock *block : L->getBlocks()) {
    const bool HasPragma = std::find_if(block->begin(), block->end(),
                                      instructionHasPragmaDcop) != block->end();
    if (HasPragma)
      return true;
  }
  return false;
}

/// \brief Move to the top loop which has apollo pragma
/// \param L The current loop in BB lives
/// \param LI
/// \return The outermost loop with pragma metadata.
Loop *moveToTopApolloLoop(Loop *L, LoopInfo *LI) {
  Loop *ParentLoop = L->getParentLoop();
  while (ParentLoop) {
    const bool ParentHasPragma = loopHasPragma(ParentLoop, LI);
    if (ParentHasPragma)
      L = ParentLoop;
    else
      break;
    ParentLoop = L->getParentLoop();
  }
  return L;
}

/// \brief Extracts a loop into a new apollo_loop function.
/// \param L a Loop.
/// \param DT the dominator tree of the parent function of L.
/// \return A function containing the loop or NULL if the loop can't be
///		extracted.
Function *extractApolloLoop(Loop *L, DominatorTree &DT) {
  static unsigned apolloLoopId = 0;
  CodeExtractor Extractor(DT, *L);
  Function *Extracted = Extractor.extractCodeRegion();
  if (!Extracted)
    return nullptr;
  // apollo loops are internal and not inlinable.
  assert(Extracted && "code extraction failed");
  Extracted->setName(utils::concat("apollo_loop_", apolloLoopId));
  Extracted->removeFnAttr(Attribute::AlwaysInline);
  Extracted->addFnAttr(Attribute::NoInline);
  Extracted->setLinkage(Function::PrivateLinkage);
  apolloLoopId++;
  return Extracted;
}

/// \brief For a function, obtain the outermost loop of a nest marked
///        with the pragma metadata.
/// \param F a function.
/// \param LI Loop info.
/// \return NULL if no loop found. If found return the outermost loop marked
///         with metadata.
Loop *getTopApolloLoop(Function *F, LoopInfo *LI) {
  Loop *AnyApolloLoop = 0x0;
  // First try to find a loop that has apollo pragma
  for (BasicBlock &aBlock : *F) {
    Loop *L = LI->getLoopFor(&aBlock);
    const bool IsApolloLoop = L && loopHasAnyPragma(L);
    if (IsApolloLoop) {
      AnyApolloLoop = L;
      break;
    }
  }

  // now move to the outermost loop
  if (AnyApolloLoop)
    return moveToTopApolloLoop(AnyApolloLoop, LI);
  // if not found, return 0x0
  return 0x0;
}

/// \brief For a function, erase the metadata from instructions outside any loop
/// \param F a function.
/// \param LI LoopInfo from the function.
void cleanMetadataOutsideApolloLoop(Function *F, LoopInfo *LI) {
  for (BasicBlock &block : *F) {
    Loop *L = LI->getLoopFor(&block);
    const bool IsInsideApolloLoop = L && loopHasPragma(L, LI);
    if (!IsInsideApolloLoop) {
      for (Instruction &i : block) {
        removePragmaInstr(&i);
      }
    }
  }
}

/// \brief For a loop, erase the metadata from instructions inside the loop.
/// \param L a loop.
void cleanMetadataLoop(Loop *L) {
  for (BasicBlock *block : L->getBlocks()) {
    for (Instruction &i : *block) {
      removePragmaInstr(&i);
    }
  }
}

/// \brief For a function, erase the metadata from instructions inside it.
/// \param F a Function.
void cleanMetadataFunction(Function *F) {
  for (BasicBlock &block : *F) {
    for (Instruction &i : block) {
      removePragmaInstr(&i);
    }
  }
}

/// \brief Fix the function entry block. Split the entry block if it belongs
///        to the loop.
/// \param L Current Loop
void fixEntryBlock(Loop *L) {
  Function *LoopParentFunction = loop::getLoopParentFunction(L);
  BasicBlock &EntryBlock = LoopParentFunction->getEntryBlock();
  if (L->contains(&EntryBlock)) {
    BasicBlock *NewEntry =
        BasicBlock::Create(LoopParentFunction->getContext(), "new_entry",
                           LoopParentFunction, &EntryBlock);
    BranchInst::Create(&EntryBlock, NewEntry);
  }
}

/// \brief Fix the loop exit blocks. Split them if they are return statements.
/// \param L Current Loop
void fixReturnBlocks(Loop *L) {
  // If any of the exit blocks is a return block, split it.
  std::set<BasicBlock *> ExitBlocks = std::move(loop::getLoopExitBlocks(L));
  for (BasicBlock *exitBlock : ExitBlocks) {
    TerminatorInst *exitBlockTerminator = exitBlock->getTerminator();
    if (isa<ReturnInst>(exitBlockTerminator))
      exitBlock->splitBasicBlock(exitBlockTerminator,
                                 exitBlock->getName() + ".return");
  }
}

/// \brief Check if the entry terminator is a branch to the header.
/// \param L Current Loop
bool entryBranchToLoopHeader(Loop *L) {
  Function *LoopParentFunction = loop::getLoopParentFunction(L);
  BasicBlock &EntryBlock = LoopParentFunction->getEntryBlock();
  TerminatorInst *EntryTerminator = EntryBlock.getTerminator();
  return isa<BranchInst>(EntryTerminator) &&
         cast<BranchInst>(EntryTerminator)->isUnconditional() &&
         EntryTerminator->getSuccessor(0) == L->getHeader();
}

/// \brief Fix the loop for function extraction.
///        llvm loop extract may crash without this fix
/// \param L Current Loop
void apolloFixLoop(Loop *L) {
  if (!entryBranchToLoopHeader(L)) {
    fixEntryBlock(L);
    fixReturnBlocks(L);
  }
}

/// \brief we can extract the loop if this function returns true.
///        Check LLVM LoopExtractor at line 118 for more info.
bool checkLandingpad(Loop *L) {
  // For an explanation about this check llvm LoopExtractor source
  // http://llvm.org/docs/doxygen/html/LoopExtractor_8cpp_source.html
  std::set<BasicBlock *> ExitBlocks = std::move(loop::getLoopExitBlocks(L));
  for (BasicBlock *exitBlock : ExitBlocks) {
    if (exitBlock->isLandingPad()) {
      errs() << "WARNING: Can't extract the loop. Loop exit is landingpad.\n";
      return false;
    }
  }
  return true;
}

/// \brief Check that it is safe to extract the loop blocks
///
/// This mirrors the private check in
/// llvm::CodeExtractor::buildExtractionBlockSet (see
/// llvm/lib/Transforms/Utils/CodeExtractor.cpp). It is needed to avoid
/// extracting an ineligible loop.
/// \p L The loop to check
/// \p DT The Dominator tree for the loop
/// \returns true iff it is safe to call CodeExtractor::extractCodeRegion the
/// loop on the loop.
bool canExtractLoopCode(Loop* L, DominatorTree *DT)
{
  const bool allow_var_args = false;
  ArrayRef<BasicBlock*> blocks = L->getBlocks();
  SetVector<BasicBlock*> region;

  // Collect the blocks in the region
  for (BasicBlock* block: L->getBlocks()) {
    if (!DT->isReachableFromEntry(block))
      continue;
    if (!region.insert(block))
      return false;
    if (!CodeExtractor::isBlockValidForExtraction(*block, allow_var_args))
      return true;
  }

  // Check all required blocks are present
  const auto& end_itr = region.end();
  for (auto block_itr = std::next(region.begin());
       block_itr != end_itr; ++block_itr) {
    // All preceding blocks must be in the region
    for (auto pred_itr = pred_begin(*block_itr);
	 pred_itr != pred_end(*block_itr);
	 ++pred_itr) {
      const auto& predecessor = *pred_itr;
      if (region.count(predecessor) == 0)
	return false;
    }
  }
  return true;
}

/// \brief This function is used to check if Apollo can handle a loop.
/// \return True if Apollo can handle the loop.
bool apolloCanHandle(Loop *L, DominatorTree* DT) {
  // Add restrictions of apollo in this function.
  // Inherited from llvm loop extractor code. Related with loops with
  // exceptions.
  if(!checkLandingpad(L))
    return false;

  if (!canExtractLoopCode(L, DT))
    return false;

  return true;
}

} // end anonymous namespace

namespace apollo {

ApolloLoopExtract::ApolloLoopExtract() : ModulePass(ID) {}

void ApolloLoopExtract::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<DominatorTreeWrapperPass>();
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloLoopExtract::runOnModule(Module &M) {
  DEBUG_WITH_TYPE("apollo-pass-0",
			dbgs() << "Running pass on module: ApolloLoopExtract\n");
  bool Modify = false;
  std::vector<Function *> Work;
  for (Function &F : M) {
    if (!F.getBasicBlockList().empty())
      Work.push_back(&F);
  }
  for (Function *F : Work) {
    DominatorTree &DT = getAnalysis<DominatorTreeWrapperPass>(*F).getDomTree();
    LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
    if (runOnFunction(*F, DT, LI))
      Modify = true;
  }
  return Modify;
}

bool ApolloLoopExtract::runOnFunction(Function &F, DominatorTree &DT,
                                      LoopInfo &LI) {
  DEBUG_WITH_TYPE("apollo-pass-1",
			dbgs() << "   function:" << F.getName().str() << "\n");
  bool apolloLoopExtracted = false;
  Loop *apolloLoop = getTopApolloLoop(&F, &LI);
  // If there is no apolloLoop this will exit the loop.
  while (apolloLoop) {
    apolloFixLoop(apolloLoop);

    Function *Extracted = nullptr;
    if (apolloCanHandle(apolloLoop, &DT))
      Extracted = extractApolloLoop(apolloLoop, DT);

    if(Extracted) {
      // Process the extracted loop
      cleanMetadataFunction(Extracted);
      Extracted->addFnAttr(Attribute::NoInline);
      apolloLoopExtracted = true;
    } else {
      // Clean up loops which we cannot handle.
      cleanMetadataLoop(apolloLoop);
    }

    apolloLoop = getTopApolloLoop(&F, &LI);
  }

  cleanMetadataOutsideApolloLoop(&F, &LI);
  return apolloLoopExtracted;
}

char ApolloLoopExtract::ID = 0;
DefinePassCreator(ApolloLoopExtract);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloLoopExtract);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloLoopExtract, "APOLLO_LOOP_EXTRACT",
                      "Apollo loop extract.", false, false)
INITIALIZE_PASS_DEPENDENCY(DominatorTreeWrapperPass)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloLoopExtract, "APOLLO_LOOP_EXTRACT",
                    "Apollo loop extract.", false, false)
