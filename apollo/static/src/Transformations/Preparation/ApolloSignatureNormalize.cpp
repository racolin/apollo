//===--- ApolloSignatureNormalize.cpp -------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Preparation/ApolloSignatureNormalize.h"
#include "Transformations/Preparation/ApolloCloneLoopFunction.h"
#include "BasicPass/ApolloPass.h"
#include "Utils/MetadataHandler.h"
#include "Utils/ConstantUtils.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Transforms/Utils/CodeExtractor.h"
#include "llvm/Transforms/Utils/Cloning.h"

#include <set>

using namespace llvm;
using namespace apollo;

namespace {

/// \brief it allocates a structure for the parameters and initialize it with
///        the parameters values in the fields.
/// \param F an apollo loop function.
/// \param callerInst The call instruction.
/// \param paramStruct the structure type.
/// \return the pointer to the stack allocated structure.
AllocaInst *getParamAlloc(Function *F, CallInst *CI, StructType *ParamStruct) {
  LLVMContext &Context = F->getContext();
  Type *i32Ty = Type::getInt32Ty(Context);
  Constant *Zero = ConstantInt::get(i32Ty, 0);
  AllocaInst *Alloc = new AllocaInst(
      ParamStruct, 0, utils::concat(ParamStruct->getName(), "_alloc"),
      &*(CI->getParent()->getParent()->getEntryBlock().begin()));
  metadata::attachMetadata(Alloc, mdkind::ApolloParamAlloc, F->getName());
  for (unsigned arg_num = 0; arg_num < CI->getNumArgOperands(); ++arg_num) {
    Value *ArgVal = CI->getArgOperand(arg_num);
    Constant *FieldIdx = ConstantInt::get(i32Ty, arg_num);
    std::vector<Value *> FieldIdxs = {Zero, FieldIdx};
    GetElementPtrInst *FieldPtr = GetElementPtrInst::CreateInBounds(
        Alloc, FieldIdxs, utils::concat("arg_", arg_num, "_gep"), CI);
    new StoreInst(ArgVal, FieldPtr, CI);
  }
  return Alloc;
}

/// \brief It returns a structure type which can contain the outermost
///        loop phi-nodes values.
/// \param F an apollo loop function.
/// \param afu function Analysis.
/// \return A structure type which has as body the outermost loop phi-node types
StructType *getPhiStateStruct(Function *F, LoopInfo &LI) {
  Loop *L = function::getOutermostLoopInFunction(F, &LI);
  BasicBlock *Header = L->getHeader();
  std::vector<Type *> StructBody;
  Type *i32Ty = Type::getInt32Ty(Header->getContext());
  int Field = 0;
  for (Instruction &inst : *Header) {
    if (!isa<PHINode>(inst))
      break;
    Constant *FieldConstant = ConstantInt::get(i32Ty, Field);
    metadata::attachMetadata(&inst, mdkind::ApolloPhiStateField,
                             {FieldConstant});
    Field++;
    StructBody.push_back(inst.getType());
  }
  if (StructBody.empty()) {
    // in case of emtpy body create a dummy field.
    StructBody.push_back(Type::getInt8Ty(F->getContext()));
  }
  StructType *PhiStateStruct = StructType::create(
      StructBody, utils::concat("apollo.", F->getName(), "_phi_state"));
  return PhiStateStruct;
}

/// \brief Given a function, it returns a structure type which can contain all
///        its arguments.
/// \param F a function.
/// \return A structure type, which has as body the argument types.
StructType *getParamStruct(Function *F) {
  std::vector<Value *> Args = function::getArgumentVector(F);
  std::vector<Type *> StructBody;
  for (Value *arg : Args) {
    StructBody.push_back(arg->getType());
  }
  if (StructBody.empty()) {
    // in case of emtpy body create a dummy field.
    StructBody.push_back(Type::getInt8Ty(F->getContext()));
  }
  StructType *ParamStruct = StructType::create(
      StructBody, utils::concat("apollo.", F->getName(), "_param"));
  return ParamStruct;
}

/// \brief Defines the new function with the signature changed.
/// \param F The original function.
/// \param paramStruct The type of the structure that will hold all the
///        parameters.
/// \param phiStateStruct The type of the structure that will hold the
///        phi-node state.
/// \return The new function definition.
Function *defineSignChange(Function *F, StructType *ParamStruct,
                           StructType *PhiStateStruct) {
  const std::string FuntionName = F->getName();
  F->setName(FuntionName + "_old");
  PointerType *ParamStructPtr = PointerType::get(ParamStruct, 0),
              *phiStateStructPtr = PointerType::get(PhiStateStruct, 0);
  Type *i64Ty = Type::getInt64Ty(F->getContext());
  std::vector<Type *> ArgTypes = {ParamStructPtr, phiStateStructPtr};
  FunctionType *FunctionType = FunctionType::get(i64Ty, ArgTypes, false);
  Function *SignChange = Function::Create(FunctionType, F->getLinkage(),
                                          FuntionName, F->getParent());
  auto arg_param = SignChange->arg_begin();
  auto arg_state = SignChange->arg_begin();
  arg_state++;
  arg_param->setName("param");
  arg_state->setName("phi_state");
  return SignChange;
}

/// \brief This function maps the arguments in the old function with the load of
///        fields of the structure.
/// \param F the old function.
/// \param signChange the function with the signature changed.
/// \param param load inst : a vector to keep the instructions for the meantime.
/// \param valueMap a ValueToValueMapTy to store the mapping.
void mapArgumentsWithParamStruct(Function *F, Function *SignChange,
                                 std::vector<Instruction *> &ParamLoadInst,
                                 ValueToValueMapTy &ValueMap) {
  Type *i32Ty = Type::getInt32Ty(F->getContext());
  Constant *Zero = ConstantInt::get(i32Ty, 0);
  auto arg_param = SignChange->arg_begin();
  std::vector<Value *> OriginalArgs = function::getArgumentVector(F);
  for (unsigned arg_num = 0; arg_num < OriginalArgs.size(); ++arg_num) {
    const std::string ArgName = OriginalArgs[arg_num]->getName();
    Constant *FieldIdx = ConstantInt::get(i32Ty, arg_num);
    std::vector<Value *> FieldIdxs = {Zero, FieldIdx};
    GetElementPtrInst *FieldPtr = GetElementPtrInst::CreateInBounds(
        &*arg_param, FieldIdxs, ArgName + "_gep");
    LoadInst *ArgLoad = new LoadInst(FieldPtr, ArgName);
    ParamLoadInst.push_back(FieldPtr);
    ParamLoadInst.push_back(ArgLoad);
    ValueMap[OriginalArgs[arg_num]] = ArgLoad;
  }
}

/// \brief this function sets the return of each return statement to
///        "(int64)return_value + 1;
/// \param F a Function with the signature changed.
/// \param returnType the old return type.
void fixReturnTypes(Function *F, Type *ReturnType) {
  LLVMContext &Context = F->getContext();
  Type *i64Ty = Type::getInt64Ty(Context);
  Constant *One = ConstantInt::get(i64Ty, 1);
  for (BasicBlock &block : *F) {
    TerminatorInst *Term = block.getTerminator();
    if (isa<ReturnInst>(Term)) {
      if (ReturnType->isVoidTy()) {
        ReturnInst::Create(Context, One, &block);
        Term->eraseFromParent();
      } else {
        Value *ReturnValue = cast<ReturnInst>(Term)->getReturnValue();
        Term->eraseFromParent();
        if (!ReturnType->isIntegerTy(64))
          ReturnValue = CastInst::CreateZExtOrBitCast(ReturnValue, i64Ty,
                                                      "return_cast", &block);

        BinaryOperator *Inc =
            BinaryOperator::CreateAdd(ReturnValue, One, "return_inc", &block);
        ReturnInst::Create(Context, Inc, &block);
      }
    }
  }
}

/// \brief Given an apollo loop function, it will change the signature and fix
///        the parameters.
/// \param F a function.
/// \param paramStruct
/// \param phiStateStruct
/// \return The function with the signature changed.
Function *changeSignature(Function *F, StructType *ParamStruct,
                          StructType *PhiStateStruct) {
  // define the new function.
  Function *SignChange = defineSignChange(F, ParamStruct, PhiStateStruct);
  // map the arguments with the load from the struct.
  ValueToValueMapTy ValueMap;
  std::vector<Instruction *> ParamLoadInst;
  mapArgumentsWithParamStruct(F, SignChange, ParamLoadInst, ValueMap);
  // clone and remap all the values
  SmallVector<ReturnInst *, 8> Returns;
  CloneFunctionInto(SignChange, F, ValueMap, false, Returns, "", 0x0);
  ApolloCloneLoopFunction::fixInstructions(SignChange, ValueMap);
  // set the param load as the new entry point.
  BasicBlock *OldEntry = &SignChange->front();
  BasicBlock *ParamLoad = BasicBlock::Create(
      SignChange->getContext(), "param_load", SignChange, OldEntry);
  TerminatorInst *ParamLoadTerm = BranchInst::Create(OldEntry, ParamLoad);

  for (Instruction *inst : ParamLoadInst) {
    inst->insertBefore(ParamLoadTerm);
  }

  Constant *Zero = constant::getInt64Constant(F->getContext(), 0);
  Constant *ZeroPtr =
      ConstantExpr::getIntToPtr(Zero, PointerType::get(PhiStateStruct, 0));
  Type *OriginalReturnTy = F->getFunctionType()->getReturnType();
  // remove the old call and replace with the new one
  std::vector<CallInst *> Uses;
  for (User *user : F->users()) {
    CallInst *CI = cast<CallInst>(user);
    Uses.push_back(CI);
  }
  for (CallInst *caller : Uses) {
    AllocaInst *ParamAlloc = getParamAlloc(F, caller, ParamStruct);
    std::vector<Value *> Args = {ParamAlloc, ZeroPtr};
    CallInst *NewCall =
        CallInst::Create(SignChange, Args,
                         utils::concat(SignChange->getName(), "_call"), caller);

    if (!OriginalReturnTy->isVoidTy()) {
      CastInst *NewCallCast = CastInst::CreateTruncOrBitCast(
          NewCall, OriginalReturnTy, utils::concat(F->getName(), "_cast"),
          caller);
      caller->replaceAllUsesWith(NewCallCast);
    }
    caller->eraseFromParent();
  }
  // fix return types.
  fixReturnTypes(SignChange, OriginalReturnTy);
  // erase the old function
  F->eraseFromParent();
  return SignChange;
}

} // end anonymous namespace

namespace apollo {

ApolloSignatureNormalize::ApolloSignatureNormalize() : ApolloPass(ID, "ApolloSignatureNormalize") {}

void ApolloSignatureNormalize::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloSignatureNormalize::runOnApollo(Function *F) {
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  StructType *paramStruct = getParamStruct(F);
  StructType *phiStateStruct = getPhiStateStruct(F, LI);
  changeSignature(F, paramStruct, phiStateStruct);
  return true;
}

char ApolloSignatureNormalize::ID = 0;
DefinePassCreator(ApolloSignatureNormalize);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloSignatureNormalize);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloSignatureNormalize, "APOLLO_SIGNATURE_NORMALIZE",
                      "Apollo signature normalize.", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloSignatureNormalize, "APOLLO_SIGNATURE_NORMALIZE",
                    "Apollo signature normalize.", false, false)
