//===--- PromoteMemIntrinsics.cpp -----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Preparation/PromoteMemIntrinsics.h"
#include "BasicPass/ApolloPass.h"
#include "Utils/ConstantUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;
using namespace apollo;

namespace {

/// \brief if the number of iterations is lower than the unroll threashold,
///        don't create a loop, create the unrolled loop directly.
unsigned UnrollThreashold = 32;

/// \brief creates the body for the mem copy/move/set function.
///        The body creator is the function which gives the specific
///        functionality to the new created loop.
///        In case the number of iterations is known, and lower than a
///        threashold, it creates the unrolled version of the loop.
template <typename BodyCreator>
void memFunCreator(CallInst *CI, BodyCreator &Creator, bool IsLittleEndian) {
  BasicBlock *Parent = CI->getParent();
  BasicBlock *Cont =
      Parent->splitBasicBlock(CI, Parent->getName() + ".after.memfun");
  Value *Len = CI->getArgOperand(2);
  BasicBlock *Body = BasicBlock::Create(Parent->getContext(), "memfun.body",
                                        Parent->getParent(), Cont);
  Constant *Zero = ConstantInt::get(Len->getType(), 0);
  Constant *One = ConstantInt::get(Len->getType(), 1);
  ConstantInt *LenConstant = dyn_cast<ConstantInt>(Len);
  unsigned CopySize = IsLittleEndian ? 8 : 1;

  if (LenConstant &&
      (LenConstant->getSExtValue() / CopySize) < UnrollThreashold) {
    assert(LenConstant->getSExtValue() != 0x0);
    Parent->getTerminator()->replaceUsesOfWith(Cont, Body);
    BranchInst::Create(Cont, Body);
    unsigned IterStart = 0;
    unsigned BytesToCopy = LenConstant->getSExtValue();
    do {
      assert(IterStart % BytesToCopy == 0);
      for (unsigned i = IterStart; i < BytesToCopy; i += CopySize) {
        Creator(CI, constant::getInt32Constant(Body->getContext(), i),
                Body->getTerminator(), CopySize);
      }
      IterStart = BytesToCopy - BytesToCopy % CopySize;
      CopySize /= 2;
    } while (CopySize != 1);
  } else {
    // if len == zero then { goto after.memcpy } else { goto memcpy.body }
    Parent->getTerminator()->eraseFromParent();
    Value *LenZero = new ICmpInst(*Parent, ICmpInst::ICMP_EQ, Len, Zero);
    BranchInst::Create(Cont, Body, LenZero, Parent);
    // fill the body
    IRBuilder<> Builder(Body);
    PHINode *Iter = Builder.CreatePHI(Len->getType(), 2, "memfun.iter");
    Value *IterInc = Builder.CreateAdd(Iter, One);
    Value *Cond = Builder.CreateICmpEQ(IterInc, Len);
    Value *IterCast = Builder.CreateTruncOrBitCast(
        Iter, Type::getInt32Ty(Body->getContext()));
    Iter->addIncoming(Zero, Parent);
    Iter->addIncoming(IterInc, Body);
    Builder.CreateCondBr(Cond, Cont, Body);
    Creator(CI, IterCast, Body->getTerminator(), 1);
  }
  CI->eraseFromParent();
}

/// \brief creates the body for memcpy. If possible it will work moving bigger
///        words.
void memCpyCreator(CallInst *CI, Value *Iter, Instruction *InsertAt,
                   unsigned NumBytes) {
  Value *Dest = CI->getArgOperand(0);
  Value *Src = CI->getArgOperand(1);
  //*align = call->getArgOperand(3),
  //*is_volatile = call->getArgOperand(4);
  Type *nIntTy = Type::getIntNTy(CI->getContext(), NumBytes * 8);
  IRBuilder<> Builder(InsertAt);
  Value *DestPtr = Builder.CreatePointerCast(Builder.CreateGEP(Dest, Iter),
                                             PointerType::get(nIntTy, 0));
  Value *SrcPtr = Builder.CreatePointerCast(Builder.CreateGEP(Src, Iter),
                                            PointerType::get(nIntTy, 0));
  Builder.CreateStore(Builder.CreateLoad(SrcPtr), DestPtr);
}

/// \brief creates the body for a memmove
void memMoveCreator(CallInst *CI, Value *Iter, Instruction *InsertAt,
                    unsigned NumBytes) {
  Value *Dest = CI->getArgOperand(0);
  Value *Src = CI->getArgOperand(1);
  Value *Len = CI->getArgOperand(2);
  //*align = call->getArgOperand(3),
  //*is_volatile = call->getArgOperand(4);
  IRBuilder<> Builder(InsertAt);
  Constant *One = ConstantInt::get(Iter->getType(), 1);
  Constant *Mone = ConstantInt::get(Iter->getType(), -1);
  Constant *Zero = ConstantInt::get(Iter->getType(), 0);
  for (unsigned i = 0; i < NumBytes; ++i) {
    Value *LenCast = Builder.CreateTruncOrBitCast(Len, Iter->getType());
    Value *LenM1 = Builder.CreateAdd(LenCast, Mone);
    Value *CMP = Builder.CreateICmpULT(Dest, Src);
    Value *Op = Builder.CreateSelect(CMP, One, Mone);
    Value *Init = Builder.CreateSelect(CMP, Zero, LenM1);
    Value *Idx = Builder.CreateMul(Builder.CreateAdd(Iter, Init), Op);
    memCpyCreator(CI, Idx, InsertAt, 1);
    Iter = Builder.CreateAdd(Iter, One);
  }
}

/// \brief creates the body for memset. If possible it will work moving bigger
///        words.
void memSetCreator(CallInst *CI, Value *Iter, Instruction *InsertAt,
                   unsigned NumBytes) {
  Value *Dest = CI->getArgOperand(0);
  Value *Val = CI->getArgOperand(1);
  IRBuilder<> Builder(InsertAt);
  Type *nIntTy = Type::getIntNTy(CI->getContext(), NumBytes * 8);
  Type *i8Ty = Type::getInt8Ty(CI->getContext());
  Constant *Zero = ConstantInt::get(i8Ty, 0);
  Value *Vec = ConstantVector::get(std::vector<Constant *>(NumBytes, Zero));
  for (unsigned i = 0; i < NumBytes; ++i) {
    Builder.CreateInsertElement(
        Vec, Val, constant::getInt32Constant(CI->getContext(), i));
  }
  Vec = Builder.CreateBitCast(Vec, nIntTy);
  Value *DestPtr =
      Builder.CreatePointerCast(Builder.CreateGEP(Dest, Iter), nIntTy);
  Builder.CreateStore(Vec, DestPtr);
}

} // end anonymous namespace

namespace apollo {

PromoteMemIntrinsics::PromoteMemIntrinsics() : ApolloPass(ID, "PromoteMemIntrinsics") {}

void PromoteMemIntrinsics::getAnalysisUsage(AnalysisUsage &AU) const {}

bool PromoteMemIntrinsics::runOnApollo(Function *F) {
  bool Promoted = false;
  std::vector<CallInst *> calls;
  // collect calls
  for (BasicBlock &block : *F) {
    for (Instruction &call_inst : block) {
      CallInst *CI = dyn_cast<CallInst>(&call_inst);
      if (CI)
        calls.push_back(CI);
    }
  }
  const bool IsLittleEndian = F->getParent()->getDataLayout().isLittleEndian();
  // filter intrinsic calls
  for (CallInst *call : calls) {
    Function *F = call->getCalledFunction();
    #define match(foo, name) if (foo->getName().startswith(name))
    if (F) {
      match(F, "llvm.memcpy") {
        memFunCreator(call, memCpyCreator, IsLittleEndian);
        Promoted = true;
      }
      match(F, "llvm.memmove") {
        memFunCreator(call, memMoveCreator, IsLittleEndian);
        Promoted = true;
      }
      match(F, "llvm.memset") {
        memFunCreator(call, memSetCreator, IsLittleEndian);
        Promoted = true;
      }
    }
  }
  return Promoted;
}

char PromoteMemIntrinsics::ID = 0;
DefinePassCreator(PromoteMemIntrinsics);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(PromoteMemIntrinsics);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(PromoteMemIntrinsics, "APOLLO_MEM_INTRINSICS_PROMOTE",
                      "APOLLO_MEM_INTRINSICS_PROMOTE", false, false)
INITIALIZE_PASS_END(PromoteMemIntrinsics, "APOLLO_MEM_INTRINSICS_PROMOTE",
                    "APOLLO_MEM_INTRINSICS_PROMOTE", false, false)
