//===--- ApolloSimpleLoopLatch.cpp ----------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Preparation/ApolloSimpleLoopLatch.h"
#include "SkeletonConfig.h"
#include "Utils/Utils.h"

#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"

using namespace apollo;
using namespace llvm;

namespace {

/// \brief Insert one unique loop latch in to the loop.
/// \param l a Loop.
/// \return true if a new unique latch has been created
bool insertUniqueLatch(Loop *L) {

	// Get latches and preheaders blocks
	BasicBlock *Header = L->getHeader();
	std::vector<BasicBlock *> Latches;
	std::vector<BasicBlock *> PreHeaders;
	for (auto pred = pred_begin(Header), pred_en = pred_end(Header);
			pred != pred_en; pred++) {
		BasicBlock *PredBlock = *pred;
		if (L->contains(PredBlock))
			Latches.push_back(PredBlock);
		else
			PreHeaders.push_back(PredBlock);
	}

	// If L has a single latch, nothing to do
	const bool IsSimpleLatch = Latches.size() == 1 && Latches[0] != Header
			&& Latches[0]->getTerminator()->getNumSuccessors() == 1;
	if (IsSimpleLatch) {
		return false;
	}

	std::vector<PHINode *> PhNodes;
	for (auto phi = Header->begin(); isa<PHINode>(phi); ++phi)
		PhNodes.push_back(cast<PHINode>(phi));

	BasicBlock *UniqueLatch = BasicBlock::Create(Header->getContext(),
			"unique_latch", Header->getParent());
	BranchInst::Create(Header, UniqueLatch);
	for (BasicBlock *old_latch : Latches) {
		old_latch->getTerminator()->replaceUsesOfWith(Header, UniqueLatch);
	}
	if (Latches.size() == 1) {
		for (PHINode *phi : PhNodes) {
			for (unsigned i = 0; i < phi->getNumIncomingValues(); ++i) {
				if (phi->getIncomingBlock(i) == Latches[0])
					phi->setIncomingBlock(i, UniqueLatch);
			}
		}
	} else {
		for (PHINode *phi : PhNodes) {
			PHINode *PhiNodeAtHeader = PHINode::Create(phi->getType(),
					PreHeaders.size() + 1, phi->getName() + ".at_header", phi);
			PHINode *PhiNodeAtLatch = PHINode::Create(phi->getType(),
					Latches.size(), phi->getName() + ".at_latch",
					UniqueLatch->getTerminator());
			PhiNodeAtHeader->addIncoming(PhiNodeAtLatch, UniqueLatch);
			for (unsigned i = 0; i < PreHeaders.size(); ++i) {
				PhiNodeAtHeader->addIncoming(
						phi->getIncomingValueForBlock(PreHeaders[i]),
						PreHeaders[i]);
			}
			for (unsigned i = 0; i < Latches.size(); ++i) {
				PhiNodeAtLatch->addIncoming(
						phi->getIncomingValueForBlock(Latches[i]), Latches[i]);
			}
		}
	}
	for (Loop *subloop : *L) {
		insertUniqueLatch(subloop);
	}
	return true;
}

} // end anonymous namespace

namespace apollo {

ApolloSimpleLoopLatch::ApolloSimpleLoopLatch() :
		ApolloPassOnSkeleton(ID, "", "ApolloSimpleLoopLatch") {
}
ApolloSimpleLoopLatch::ApolloSimpleLoopLatch(std::string SK) :
		ApolloPassOnSkeleton(ID, SK, "ApolloSimpleLoopLatch") {
}

void ApolloSimpleLoopLatch::getAnalysisUsage(AnalysisUsage &AU) const {
	AU.addRequired<LoopInfoWrapperPass>();
}

bool ApolloSimpleLoopLatch::runOnLoop(Loop *L) {
	DEBUG_WITH_TYPE("apollo-pass-simple-loop-latch",
			dbgs() << "loop before: \n"; L->dump(); dbgs() << "\n\n"; L->getHeader()->getParent()->viewCFG());
	bool res = insertUniqueLatch(L);
	DEBUG_WITH_TYPE("apollo-pass-simple-loop-latch",
			dbgs() << "loop after: \n"; L->dump(); dbgs() << "\n\n"; L->getHeader()->getParent()->viewCFG());
	return res;
}

char ApolloSimpleLoopLatch::ID = 0;
DefinePassCreator_1(ApolloSimpleLoopLatch, std::string);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloSimpleLoopLatch);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloSimpleLoopLatch, "APOLLO_SIMPLE_LOOP_LATCH",
		"APOLLO_SIMPLE_LOOP_LATCH", false, false)
	INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
	INITIALIZE_PASS_END(ApolloSimpleLoopLatch, "APOLLO_SIMPLE_LOOP_LATCH",
			"APOLLO_SIMPLE_LOOP_LATCH", false, false)
