//===--- ApolloIgnoreEmpty.cpp --------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/Preparation/ApolloIgnoreEmpty.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"

#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/Analysis/LoopPass.h"

#include <set>
#include <vector>

using namespace llvm;
using namespace apollo;

namespace apollo {

ApolloIgnoreEmpty::ApolloIgnoreEmpty() : ApolloPass(ID, "ApolloIgnoreEmpty") {}

void ApolloIgnoreEmpty::getAnalysisUsage(AnalysisUsage &AU) const {
  ApolloPass::getAnalysisUsage(AU);
}

bool ApolloIgnoreEmpty::runOnApollo(Function *apolloFunction) {
  LoopInfo &LI =
      getAnalysis<LoopInfoWrapperPass>(*apolloFunction).getLoopInfo();
  const bool HasLoop = function::getOutermostLoopInFunction(apolloFunction, &LI) != 0x0;
  if (!HasLoop) {
    apolloFunction->removeFnAttr(Attribute::NoInline);
    apolloFunction->addFnAttr(Attribute::AlwaysInline);
    return true;
  }
  return false;
}

char ApolloIgnoreEmpty::ID = 0;
DefinePassCreator(ApolloIgnoreEmpty);

} // end namespace apollo

namespace llvm {
class PassRegistry;
DeclarePassInitializator(ApolloIgnoreEmpty);
} // end namespace llvm

INITIALIZE_PASS_BEGIN(ApolloIgnoreEmpty, "APOLLO_IGNORE_EMPTY",
                      "Apollo mark for reinline empty nests.", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(ApolloIgnoreEmpty, "APOLLO_IGNORE_EMPTY",
                    "Apollo mark for reinline empty nests.", false, false)
