//===--- BoundVerif.cpp ---------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/CodeBones/BoundVerif.h"
#include "Transformations/Parallel/BasicScalarPredict.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/SCEVUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Constants.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/Debug.h"

#include <stack>

using namespace apollo;
using namespace llvm;

// Enable debugging information for code-bones-extraction with
// '-debug' or '-debug=codebones-boundverif'
#define DEBUG_TYPE "codebones-boundverif"

namespace {

std::map<int, std::set<Instruction *>>
collectBoundConditions(Function *F, std::set<int> &Affine) {
  std::map<int, std::set<Instruction *>> LoopConditions;
  for (BasicBlock &BB : *F) {
    for (Instruction &I : BB) {
      if (metadata::hasMetadataKind(&I, mdkind::OriginalLoopBound)) {
        ConstantInt *LoopIdConstant = cast<ConstantInt>(
            metadata::getMetadataOperand(&I, mdkind::OriginalLoopBound));
        const int LoopId = LoopIdConstant->getValue().getSExtValue();
        if (!Affine.count(LoopId)) {
          std::set<Instruction *> BoundConditions;
          if (LoopConditions.count(LoopId)) {
            BoundConditions = LoopConditions[LoopId];
          }
          BoundConditions.insert(&I);
          LoopConditions[LoopId] = BoundConditions;
        }
      }
    }
  }
  return LoopConditions;
}

std::map<int, std::set<Instruction *>>
collectBoundVerifComputations(std::map<int, std::set<Instruction *>> &Bounds,
                              Loop *Outermost, cfghelper::cfg &PD,
                              cfghelper::cfg &DT) {

  std::map<int, std::set<Instruction *>> Computations;
  for (std::pair<const int, std::set<Instruction *>> &pair : Bounds) {
    Loop *L = loop::getLoopWithId(Outermost, pair.first);
    std::set<Value *> initial = {L->getLoopLatch()};
    for (Instruction *bound_condition : pair.second) {
      initial.insert(bound_condition);
    }
    Computations[pair.first] = std::move(collectComputations(initial, PD, DT));
  }
  return Computations;
}

Function *createBoneDefinition(Function *F, Loop *Outermost, 
                               const int MaxLoopId,
                               const int LoopId) {
  // create function signature.
  const int NestId = function::getNestId(F);
  Loop *ParentLoop = loop::getLoopWithId(Outermost, MaxLoopId);
  const int LoopDepth = ParentLoop->getLoopDepth();

  const std::string BoneName = utils::concat("apollo_bone.nest_", NestId, 
                                             ".verif_bound_", LoopId);

  LLVMContext &Context = F->getContext();
  Type *boolTy = Type::getInt1Ty(Context);
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *GlobalParamsTy = F->getFunctionType()->getParamType(0);
  Type *PhiStateTy = F->getFunctionType()->getParamType(1);
  Type *CoefTy = Type::getInt8PtrTy(Context, 0);

  std::vector<Type *> ParamTypes = std::vector<Type *>(LoopDepth + 3, i64Ty);
  ParamTypes[0] = GlobalParamsTy;
  ParamTypes[1] = PhiStateTy;
  ParamTypes[2] = CoefTy;

  FunctionType *BoneType = FunctionType::get(boolTy, ParamTypes, false);
  Function *Bone = Function::Create(BoneType, Function::PrivateLinkage,
                                    BoneName, F->getParent());

  // name the virtual iterators
  auto argIt = Bone->arg_begin();
  for (int i = 0; argIt != Bone->arg_end(); ++argIt, ++i) {
    if (i == 0)
      argIt->setName("param");
    else if (i == 1)
      argIt->setName("phi_state");
    else if (i == 2)
      argIt->setName("coefs");
    else if (i > 2)
      argIt->setName(utils::concat("vi.", i - 3));
  }
  return Bone;
}

void extractComputations(Function *Bone, Function *F, Loop *Outermost,
                         std::set<Instruction *> &Computations,
                         std::map<BasicBlock *, BasicBlock *> &bMap,
                         std::map<Value *, Value *> &iMap) {
  // Map the global params
  auto F_arg_itr = F->arg_begin();
  auto Bone_arg_itr = Bone->arg_begin();

  iMap[&*(F_arg_itr)] = &*(Bone_arg_itr);
  ++F_arg_itr;
  ++Bone_arg_itr;
  iMap[&*(F_arg_itr)] = &*(Bone_arg_itr);

  // Extract the code-bone from the function
  extractCodeBone(Bone, F, Outermost, Computations, bMap, iMap);
}

bool createBoundVerifBones(Function *F, Loop *Outermost,
			   std::map<int, std::set<Instruction *>> &BoundsCond,
			   std::map<int, std::set<Instruction *>> &BoundsComp,
			   std::map<int, Function *> &Bones) {

  LLVMContext &Context = F->getContext();
  Value *False = ConstantInt::getFalse(Context);
  Value *True = ConstantInt::getTrue(Context);

  for (std::pair<const int, std::set<Instruction *>> &pair : BoundsCond) {
    Loop *L = loop::getLoopWithId(Outermost, pair.first);
    Function *Bone = createBoneDefinition(F, Outermost, pair.first, pair.first);
    Bones[pair.first] = Bone;

    std::map<BasicBlock *, BasicBlock *> bMap;
    std::map<Value *, Value *> iMap;
    extractComputations(Bone, F, Outermost, BoundsComp[pair.first], bMap, iMap);
    if (Bone == nullptr) {
      // Failed to construct a valid code-bone
      DEBUG({
	  dbgs() << __FUNCTION__ << ": "
		 << "Failed to extract a boundverif code-bone\n"
		 << "------------------------------------------\n";
	});

      return false;
    }

    auto AllViValues = std::move(getAllViValues(Bone, Outermost, pair.first));

    if (!(basicScalarLinearFunction(Bone, Outermost, AllViValues, iMap)
	  && statementLinearFunctions(Bone, Outermost, AllViValues))) {
      // Failed to construct a valid code-bone
      DEBUG({
	  dbgs() << __FUNCTION__ << ": "
		 << "Failed to replace scalars or statements with"
		 << " their prediction, can't construct a boundverif"
		 << " code-bone\n"
		 << "------------------------------------------\n";
	});

      return false;
    }

    // make the returns equal to true.
    for (BasicBlock &block : *Bone) {
      ReturnInst *Term = dyn_cast<ReturnInst>(block.getTerminator());
      if (Term) {
        Term->eraseFromParent();
        ReturnInst::Create(Context, True, &block);
      }
    }

    // make the prediction.
    apollo::ir::ApolloIRBuilder Builder(&*(Bone->getEntryBlock().begin()));

    // load bound coefficients
    std::vector<Value *> Coefs =
        std::move(loadBoundCoefficients(pair.first, Outermost, Builder));
    std::vector<Value *> VirtualIterators = std::move(getBoneVi(Bone));
    std::vector<Value *> Vis;

    for (unsigned i = 0; i < Coefs.size() - 1; ++i) {
      Vis.push_back(VirtualIterators[i]);
    }

    Vis.push_back(Builder.getInt(1, 64));
    Value *ParentLoopVi = VirtualIterators[Coefs.size() - 1];
    Vis[Vis.size() - 1] = Builder.getInt(1, 64);
    Value *Bound = Builder.CreateSub(Builder.createDotProduct(Coefs, Vis),
                                     Builder.getInt(1, 64));
    Value *Prediction = Builder.CreateICmpSLT(
        ParentLoopVi, Bound,
        utils::concat("loop.", pair.first, ".bound.prediction"));
    // create a block that returns false
    BasicBlock *Fail = BasicBlock::Create(Context, "rollback", Bone);
    ReturnInst::Create(Context, False, Fail);

    // compare the prediction and the original condition.
    for (Instruction *orig_condition : pair.second) {
      Instruction *IriginalInstr = cast<Instruction>(iMap[orig_condition]);
      BasicBlock *ConditionParent = IriginalInstr->getParent();
      BasicBlock *ParentContinue =
          ConditionParent->splitBasicBlock(ConditionParent->getTerminator());
      ParentContinue->setName(ConditionParent->getName() + ".c");
      // [original->(predicted < bound)]. ssi [original && !(predicted < bound)]
      ConditionParent->getTerminator()->eraseFromParent();
      Builder.SetInsertPoint(ConditionParent);
      Value *Verification =
          Builder.CreateOr(Builder.CreateNot(IriginalInstr), Prediction);
      Builder.CreateCondBr(Verification, ParentContinue, Fail);
    }

    // set the latch condition
    BasicBlock *Latch = bMap[L->getLoopLatch()];
    TerminatorInst *LatchTerm = Latch->getTerminator();
    LatchTerm->eraseFromParent();
    Builder.SetInsertPoint(Latch);
    Builder.CreateRet(Prediction);

    DEBUG({
	dbgs() << __FUNCTION__ << ": "
	       << "Extracted boundverif code-bone " << Bone->getName();
	Bone->print(dbgs(), nullptr, false, true);
	dbgs() << "------------------------------------------\n";
      });
  }

 return true;
}

Function *createOutermostLoopBone(Function *F, Loop *Outermost,
                                  cfghelper::cfg &PD, cfghelper::cfg &DT) {
  std::set<Value *> Initial;
  auto OriginalExitBlocks = std::move(loop::getLoopExitBlocks(Outermost));
  for (BasicBlock *exit : OriginalExitBlocks) {
    Initial.insert(exit);
  }

  // collect the computations. The initial set is the exit blocks and
  // the predecesor termnators.
  std::set<Instruction *> Computations =
      std::move(collectComputations(Initial, PD, DT));
  Function *Bone = createBoneDefinition(F, Outermost, 0, 0);
  std::map<BasicBlock *, BasicBlock *> bMap;
  std::map<Value *, Value *> iMap;
  extractComputations(Bone, F, Outermost, Computations, bMap, iMap);
  if (Bone == nullptr)
    return nullptr;

  std::set<BasicBlock *> ExitBlocksInBones;

  for (BasicBlock *exit : OriginalExitBlocks) {
    ExitBlocksInBones.insert(bMap[exit]);
  }

  LLVMContext &Context = F->getContext();
  Value *False = ConstantInt::getFalse(Context);
  Value *True = ConstantInt::getTrue(Context);

  // if an exit block is reached it returns false. if not it returns true.
  for (BasicBlock &block : *Bone) {
    TerminatorInst *Term = block.getTerminator();
    const bool IsExit = ExitBlocksInBones.count(&block);
    if (isa<ReturnInst>(Term) || IsExit) {
      Term->eraseFromParent();
      Value *RetValue = IsExit ? False : True;
      ReturnInst::Create(Context, RetValue, &block);
    }
  }
  auto AllViValues = std::move(getAllViValues(Bone, Outermost, 0));
  if (!(basicScalarLinearFunction(Bone, Outermost, AllViValues, iMap)
	&& statementLinearFunctions(Bone, Outermost, AllViValues))) {
    return nullptr;
  }
  return Bone;
}

std::set<int> getAffineLoops(Loop *Outermost, Loop *L, ScalarEvolution &SE) {
  std::set<int> AffineLoops;
  for (Loop *subloop : *L) {
    std::set<int> AffineChildren = getAffineLoops(Outermost, subloop, SE);
    AffineLoops.insert(AffineChildren.begin(), AffineChildren.end());
  }
  const bool IsAffine = SE.hasLoopInvariantBackedgeTakenCount(L);
  /*
  if (!IsAffine) {
    const SCEV* BackedgeCount = SE.getBackedgeTakenCount(loop);
    if (!isa<SCEVCouldNotCompute>(BackedgeCount)) {
      IsAffine = SCEVUtils::isAffine(&SE, BackedgeCount, Outermost);
    }
  }
  */
  if (IsAffine)
    AffineLoops.insert(loop::getLoopId(L));
  return AffineLoops;
}

} // end anonymous namespace

namespace apollo {

DefinePassCreator(BoundVerifBones);
char BoundVerifBones::ID = 0;

BoundVerifBones::BoundVerifBones()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::code_bones>(),
			 "BoundVerifBones") {}

void BoundVerifBones::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
  AU.addRequired<ScalarEvolutionWrapperPass>();
}

bool BoundVerifBones::runOnApollo(Function *F) {
  DEBUG({
      dbgs() << __FUNCTION__ << ":\n"
	     << "Creating boundverif code-bones fom Function "
	     << F->getName() << ":\n";
      F->print(dbgs(), nullptr, false, true);
      dbgs() << "------------------------------------------\n";
    });

  const DataLayout &DL = F->getParent()->getDataLayout();
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  ScalarEvolutionWrapperPass &SeWrapper =
      getAnalysis<ScalarEvolutionWrapperPass>(*F);
  ScalarEvolution &SE = getAnalysis<ScalarEvolutionWrapperPass>(*F).getSE();
  Loop *Outermost = function::getOutermostLoopInFunction(F, &LI);
  cfghelper::cfg CFG(*F, LI, true);
  cfghelper::cfg Dom = CFG.dominator();
  cfghelper::cfg PostDom = CFG.postDominator();
  std::set<int> AffineLoopIds =
      std::move(getAffineLoops(Outermost, Outermost, SE));
  auto BoundConditions = std::move(collectBoundConditions(F, AffineLoopIds));
  auto VerifComputations = std::move(
      collectBoundVerifComputations(BoundConditions, Outermost, PostDom, Dom));
  std::map<int, Function *>  VerifBones;

  if (!createBoundVerifBones(F, Outermost, BoundConditions,
			     VerifComputations, VerifBones)) {
    // Something went wrong when creating the code-bones.
    // Undo the changes and back out
    for (auto itr = VerifBones.begin(); itr != VerifBones.end(); ++itr) {
      Function* Bone = itr->second;
      eraseCodeBone(Bone);
    }

    return true;
  }

  DEBUG(if (VerifBones.empty())
	  {
	    dbgs() << __FUNCTION__ << ": "
		   << "No boundverif code-bones created\n"
		   << "Number of bound conditions found: "
		   << BoundConditions.size()
		   << "\n"
		   << "------------------------------------------\n";
	  });


  if (!AffineLoopIds.count(0))
    VerifBones[0] = createOutermostLoopBone(F, Outermost, PostDom, Dom);
  for (auto bone : VerifBones) {
    apollo::optimize(bone.second, DL);
  }
  SeWrapper.releaseMemory();

#ifndef NDEBUG
  {
    bool debug_errors = false;
    assert(!verifyModule(*(F->getParent()), &errs(), &debug_errors) &&
	   "Malformed module created by apollo::BoundVerifBones");
  }
#endif // NDEBUG

  return true;
}

} // end namespace apollo

INITIALIZE_PASS_BEGIN(BoundVerifBones, "APOLLO_BOUNDVERIF_BONES",
                      "APOLLO_BOUNDVERIF_BONES", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_DEPENDENCY(ScalarEvolutionWrapperPass)
INITIALIZE_PASS_END(BoundVerifBones, "APOLLO_BOUNDVERIF_BONES",
                    "APOLLO_BOUNDVERIF_BONES", false, false)
