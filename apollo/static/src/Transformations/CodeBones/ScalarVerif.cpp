//===--- ScalarVerif.cpp --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016-2019. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/CodeBones/ScalarVerif.h"
#include "Transformations/Parallel/BasicScalarPredict.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/SCEVUtils.h"

#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Constants.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/Debug.h"

#include <stack>

using namespace apollo;
using namespace llvm;

// Enable debugging information for code-bones-extraction with
// '-debug' or '-debug=codebones-scalarverif'
#define DEBUG_TYPE "codebones-scalarverif"

namespace {

std::vector<PHINode *> collectPhis(Loop *L) {
  BasicBlock *Header = L->getHeader();
  std::vector<PHINode *> Phis;
  for (auto phi = Header->begin(); isa<PHINode>(*phi); ++phi) {
    Phis.push_back(&cast<PHINode>(*phi));
  }

  for (Loop *subloop : *L) {
    auto ChildPhis = collectPhis(subloop);
    Phis.insert(Phis.end(), ChildPhis.begin(), ChildPhis.end());
  }
  return Phis;
}

std::vector<std::pair<BasicBlock *, Value *>>
getPhiIncoming(PHINode *Phi, LoopInfo &LI, bool Initial) {
  Loop *ParentLoop = LI.getLoopFor(Phi->getParent());
  assert(ParentLoop);
  std::vector<std::pair<BasicBlock *, Value *>> incoming;
  for (unsigned i = 0; i < Phi->getNumIncomingValues(); ++i) {
    BasicBlock *BB = Phi->getIncomingBlock(i);
    if (ParentLoop->contains(BB) != Initial) {
      Value *V = Phi->getIncomingValue(i);
      incoming.push_back(std::pair<BasicBlock *, Value *>(BB, V));
    }
  }
  return incoming;
}

bool initialNeedsVerification(
    PHINode *Phi, std::vector<std::pair<BasicBlock *, Value *>> &Initial,
    Loop *Outermost, ScalarEvolution &SE) {

  // The initial value for the outermost phis is not verified.
  if (Phi->getParent() == Outermost->getHeader())
    return false;
  // I can avoid the verif if there is only one incomming value and it's affine.
  std::set<Value *> Values;
  for (auto &pair : Initial) {
    Values.insert(pair.second);
  }
  if (Values.size() == 1) {
    Value *UniqueValue = *Values.begin();
    const SCEV *Scev = SE.getSCEV(UniqueValue);
    // If it could not be computed, it needs verification.
    if (isa<SCEVCouldNotCompute>(Scev))
      return true;
    // If it's not affine, it needs verification.
    return !SCEVUtils::isAffine(&SE, Scev, Outermost);
  }
  return true;
}

Function *createBoneDefinition(Function *F, Loop *ParentLoop,
                               const std::string &BoneCategory, PHINode *Phi) {

  // create function signature.
  const int NestId = function::getNestId(F);
  const int PhiId = basicscalar::getBasicScalarId(Phi);
  const int LoopDepth = ParentLoop->getLoopDepth();
  const std::string BoneName = utils::concat(
      "apollo_bone.nest_", NestId, ".verif_scalar_", BoneCategory, "_", PhiId);

  LLVMContext &Context = F->getContext();
  Type *boolTy = Type::getInt1Ty(Context);
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *GlobalParamsTy = F->getFunctionType()->getParamType(0);
  Type *PhiStateTy = F->getFunctionType()->getParamType(1);
  Type *CoefTy = Type::getInt8PtrTy(Context, 0);

  std::vector<Type *> ParamTypes = std::vector<Type *>(LoopDepth + 3, i64Ty);
  ParamTypes[0] = GlobalParamsTy;
  ParamTypes[1] = PhiStateTy;
  ParamTypes[2] = CoefTy;

  FunctionType *BoneType = FunctionType::get(boolTy, ParamTypes, false);
  Function *Bone = Function::Create(BoneType, Function::PrivateLinkage,
                                    BoneName, F->getParent());
  // name the virtual iterators
  auto arg_it = Bone->arg_begin();
  for (int i = 0; arg_it != Bone->arg_end(); ++arg_it, ++i) {
    if (i == 0)
      arg_it->setName("param");
    else if (i == 1)
      arg_it->setName("phi_state");
    else if (i == 2)
      arg_it->setName("coefs");
    else if (i > 2)
      arg_it->setName(utils::concat("vi.", i - 3));
  }
  return Bone;
}

Function *createBoneInitDefinition(Function *F, LoopInfo &LI, PHINode *Phi) {
  // the parent loop of the initial value is the parent loop of the parent loop.
  Loop *ParentLoop = LI.getLoopFor(Phi->getParent())->getParentLoop();
  assert(ParentLoop);
  return createBoneDefinition(F, ParentLoop, "init", Phi);
}

Function *createBoneFinalDefinition(Function *F, LoopInfo &LI, PHINode *Phi) {
  Loop *ParentLoop = LI.getLoopFor(Phi->getParent());
  assert(ParentLoop);
  return createBoneDefinition(F, ParentLoop, "final", Phi);
}

bool extractComputations(Function *ParentFunction, Function *Bone,
                         Loop *Outermost, Loop *ParentLoop,
                         std::set<Instruction *> &Computations,
                         std::map<BasicBlock *, BasicBlock *> &bMap,
                         std::map<Value *, Value *> &iMap) {

  // Map the global params
  auto Parent_arg_itr = ParentFunction->arg_begin();
  auto Bone_arg_itr = Bone->arg_begin();

  iMap[&*(Parent_arg_itr)] = &*(Bone_arg_itr);
  ++Parent_arg_itr;
  ++Bone_arg_itr;
  iMap[&*(Parent_arg_itr)] = &*(Bone_arg_itr);

  // Extract the code-bone from the function
  extractCodeBone(Bone, ParentFunction, Outermost, Computations, bMap, iMap);
  auto AllViValues =
      std::move(getAllViValues(Bone, Outermost, loop::getLoopId(ParentLoop)));

  if (!(basicScalarLinearFunction(Bone, Outermost, AllViValues, iMap)
	&& statementLinearFunctions(Bone, Outermost, AllViValues))) {
    // Failed to create a valid code-bone
    return false;
  }

  LLVMContext &Context = ParentFunction->getContext();
  Value *True = ConstantInt::getTrue(Context);

  // All the exits return success
  for (BasicBlock &block : *Bone) {
    ReturnInst *Ret = dyn_cast<ReturnInst>(block.getTerminator());
    if (Ret) {
      Ret->eraseFromParent();
      ReturnInst::Create(Context, True, &block);
    }
  }

  return true;
}

void insertScalarVerification(
    PHINode *Phi, Function *Bone, BasicBlock *InsertAt, Loop *Outermost,
    std::vector<std::pair<BasicBlock *, Value *>> &Incoming,
    std::vector<Value *> &Vis, std::map<BasicBlock *, BasicBlock *> &bMap,
    std::map<Value *, Value *> &iMap) {

  Type *PhiType = Phi->getType();
  ir::ApolloIRBuilder Builder(InsertAt);
  Value *IncomingValue;
  if (Incoming.size() > 1) {
    PHINode *IncomingMerge =
        Builder.CreatePHI(PhiType, Incoming.size(), "phi.merge");
    for (unsigned i = 0; i < Incoming.size(); ++i) {
      Value *V = Incoming[i].second;
      if (isa<Instruction>(V))
        V = iMap[V];
      IncomingMerge->setIncomingBlock(i, bMap[Incoming[i].first]);
      IncomingMerge->setIncomingValue(i, V);
    }
    IncomingValue = IncomingMerge;
  } else {

    Value *V = Incoming[0].second;

    if (isa<Instruction>(V)) {
      assert(iMap.count(V) && "Instruction is used but not mapped!");
      V = iMap[V];
    }
    IncomingValue = V;
  }

  std::vector<Value *> Coefs =
      std::move(loadScalarCoefficients(Phi, Outermost, Builder));

  Vis.push_back(Builder.getInt(1, 64));
  Value *Prediction = Builder.createDotProduct(Coefs, Vis);

  if (PhiType->isIntegerTy())
    Prediction = Builder.CreateIntCast(
        Prediction, PhiType, cast<IntegerType>(PhiType)->getSignBit());
  else if (PhiType->isPointerTy())
    Prediction = Builder.CreateIntToPtr(Prediction, PhiType);
  else
    assert(false && "Not handled yet!");

  Prediction->setName(utils::concat(
      "scalar.", basicscalar::getBasicScalarId(Phi), ".prediction"));

  Builder.CreateRet(Builder.CreateICmpEQ(Prediction, IncomingValue));
}

Function *getVerifInitOrNull(PHINode *Phi, Loop *Outermost, LoopInfo &LI,
                             ScalarEvolution &SE, cfghelper::cfg &DT,
                             cfghelper::cfg &PD) {

  auto Initial = std::move(getPhiIncoming(Phi, LI, true));
  if (!initialNeedsVerification(Phi, Initial, Outermost, SE)) {
    DEBUG({
	dbgs() << __FUNCTION__ << ": "
	       << "No need to verify initial scalar:\n";
	Phi->print(dbgs(), true);
      });
    return nullptr;
  }

  BasicBlock *ParentBlock = Phi->getParent();
  Function *ParentFunction = ParentBlock->getParent();
  // Insert the initial values, the initial blocks, and the phi node parent.
  std::set<Value *> InitialValues;
  for (auto &pair : Initial) {
    InitialValues.insert(pair.first);
    InitialValues.insert(pair.second);
  }

  InitialValues.insert(ParentBlock);
  Function *Bone = createBoneInitDefinition(ParentFunction, LI, Phi);
  std::set<Instruction *> Computations =
      std::move(collectComputations(InitialValues, PD, DT));

  std::map<BasicBlock *, BasicBlock *> bMap;
  std::map<Value *, Value *> iMap;
  Loop *ParentLoop = LI.getLoopFor(Phi->getParent())->getParentLoop();
  if (!extractComputations(ParentFunction, Bone, Outermost, ParentLoop,
			   Computations, bMap, iMap)) {
    // Failed to create a valid code-bone
    DEBUG({
	dbgs() << __FUNCTION__ << ": "
	       << "Failed to construct an initial scalarverif code-bone\n"
	       << "------------------------------------------\n";
      });
    eraseCodeBone(Bone);
    return nullptr;
  }

  // Perform the verification in the phi parent block.
  BasicBlock *ParentBlockClone = bMap[ParentBlock];
  ParentBlockClone->getTerminator()->eraseFromParent();
  std::vector<Value *> VirtualIterators = std::move(getBoneVi(Bone));
  // Since it's verifying the first iteration
  VirtualIterators.push_back(
      ConstantInt::get(VirtualIterators[0]->getType(), 0));
  insertScalarVerification(Phi, Bone, ParentBlockClone, Outermost, Initial,
                           VirtualIterators, bMap, iMap);


  DEBUG({
      dbgs() << __FUNCTION__ << ": "
	     << "Extracted initial scalarverif code-bone " << Bone->getName();
      Bone->print(dbgs(), nullptr, false, true);
      dbgs() << "------------------------------------------\n";
    });

  return Bone;
}

bool finalNeedsVerification(
    std::vector<std::pair<BasicBlock *, Value *>> &Final, Loop *Outermost,
    ScalarEvolution &SE) {
  // I can avoid the verif if there is only one incomming value and
  // it's a constant step.
  std::set<Value *> Values;
  for (auto &pair : Final) {
    Values.insert(pair.second);
  }
  if (Values.size() == 1) {
    Value *UniqueValue = *Values.begin();
    const SCEV *Scev = SE.getSCEV(UniqueValue);
    // If it could not be computed, it needs verification.
    if (isa<SCEVCouldNotCompute>(Scev))
      return true;
    const SCEVAddRecExpr *AddRec = dyn_cast<SCEVAddRecExpr>(Scev);
    if (!AddRec)
      return true;
    // verify if the step is not loop invariant.
    const SCEV *Step = AddRec->getStepRecurrence(SE);
    return !SCEVUtils::isLoopInvariant(&SE, Step, Outermost);
  }
  return true;
}

Function *getVerifFinalOrNull(PHINode *Phi, Loop *Outermost, LoopInfo &LI,
                              ScalarEvolution &SE, cfghelper::cfg &DT,
                              cfghelper::cfg &PD) {

  auto Final = std::move(getPhiIncoming(Phi, LI, false));
  if (!finalNeedsVerification(Final, Outermost, SE)) {
    // Failed to create a valid code-bone
    DEBUG({
	dbgs() << __FUNCTION__ << ": "
	       << "No need to verify final scalar:\n";
	Phi->print(dbgs(), true);
      });
    return nullptr;
  }
  BasicBlock *ParentBlock = Phi->getParent();
  Function *ParentFunction = ParentBlock->getParent();
  // Insert the initial values, the initial blocks, and the phi node parent.
  std::set<Value *> InitialValues;
  for (auto &pair : Final) {
    InitialValues.insert(pair.first);
    InitialValues.insert(pair.second);
  }

  Function *Bone = createBoneFinalDefinition(ParentFunction, LI, Phi);
  std::set<Instruction *> Computations =
      std::move(collectComputations(InitialValues, PD, DT));
  std::map<BasicBlock *, BasicBlock *> bMap;
  std::map<Value *, Value *> iMap;
  Loop *ParentLoop = LI.getLoopFor(Phi->getParent());
  assert(ParentLoop);

  if (!extractComputations(ParentFunction, Bone, Outermost, ParentLoop,
			   Computations, bMap, iMap)) {
    // Failed to create a valid code-bone
    DEBUG({
	dbgs() << __FUNCTION__ << ": "
	       << "Failed to construct a final scalarverif code-bone\n"
	       << "------------------------------------------\n";
      });

    eraseCodeBone(Bone);
    return nullptr;
  }

  // Perform the verification in a new block.
  BasicBlock *VerifBlock = BasicBlock::Create(ParentFunction->getContext(),
                                              "phi_final_verification", Bone);

  for (auto &pair : Final) {
    BasicBlock *LatchClone = bMap[pair.first];
    LatchClone->getTerminator()->eraseFromParent();
    BranchInst::Create(VerifBlock, LatchClone);
  }

  std::vector<Value *> VirtualIterators = std::move(getBoneVi(Bone));
  // add one since it's verifying the next
  Value *ParentVi = VirtualIterators[VirtualIterators.size() - 1];
  Value *One = ConstantInt::get(ParentVi->getType(), 1);
  VirtualIterators[VirtualIterators.size() - 1] = BinaryOperator::CreateAdd(
      ParentVi, One, utils::concat(ParentVi->getName(), ".next"),
      Bone->getEntryBlock().getTerminator());
  insertScalarVerification(Phi, Bone, VerifBlock, Outermost, Final,
                           VirtualIterators, bMap, iMap);

  DEBUG({
      dbgs() << __FUNCTION__ << ": "
	     << "Extracted final scalarverif code-bone " << Bone->getName();
      Bone->print(dbgs(), nullptr, false, true);
      dbgs() << "------------------------------------------\n";
    });
  return Bone;
}

std::vector<Function *> getVerifBones(std::vector<PHINode *> &Phis,
                                      Loop *Outermost, LoopInfo &LI,
                                      ScalarEvolution &SE, cfghelper::cfg &DT,
                                      cfghelper::cfg &PD) {

  std::vector<Function *> Bones;
  for (PHINode *phi : Phis) {
    const bool IsReduction = phi->getType()->isFloatingPointTy();
    // if it is a floating point op, it's never predicted.
    if (!IsReduction) {
      Function *VerifInit;
      Function *VerifFinal;
      VerifInit = getVerifInitOrNull(phi, Outermost, LI, SE, DT, PD);
      if (VerifInit)
        Bones.push_back(VerifInit);
      VerifFinal = getVerifFinalOrNull(phi, Outermost, LI, SE, DT, PD);
      if (VerifFinal)
        Bones.push_back(VerifFinal);
    }
    DEBUG(if (IsReduction) {
	dbgs() << __FUNCTION__ << ": "
	       << "Skipping floating-point scalar:\n";
	phi->print(dbgs(), true);
      });
  }
  return Bones;
}

} // end anonymous namespace

namespace apollo {

DefinePassCreator(ScalarVerifBones);
char ScalarVerifBones::ID = 0;

ScalarVerifBones::ScalarVerifBones()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::code_bones>(), "ScalarVerifBones") {}

void ScalarVerifBones::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
  AU.addRequired<ScalarEvolutionWrapperPass>();
}

bool ScalarVerifBones::runOnApollo(Function *F) {
  DEBUG({
      dbgs() << __FUNCTION__ << ":\n"
	     << "Creating scalarverif code-bones fom Function "
	     << F->getName() << ":\n";
      F->print(dbgs(), nullptr, false, true);
      dbgs() << "------------------------------------------\n";
    });
  const DataLayout &DL = F->getParent()->getDataLayout();
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  ScalarEvolutionWrapperPass &SeWrapper =
      getAnalysis<ScalarEvolutionWrapperPass>(*F);
  ScalarEvolution &SE = getAnalysis<ScalarEvolutionWrapperPass>(*F).getSE();
  Loop *Outermost = function::getOutermostLoopInFunction(F, &LI);
  cfghelper::cfg CFG(*F, LI, true);
  cfghelper::cfg Dom = CFG.dominator();
  cfghelper::cfg PostDom = CFG.postDominator();
  std::vector<PHINode *> Phis = std::move(collectPhis(Outermost));
  std::vector<Function *> Bones =
      std::move(getVerifBones(Phis, Outermost, LI, SE, Dom, PostDom));

  DEBUG(if (Bones.empty())
	  {
	    dbgs() << __FUNCTION__ << ": "
		   << "No scalarverif code-bones created\n"
		   << "Number of scalars to verify: "
		   << Phis.size()
		   << "\n"
		   << "------------------------------------------\n";
	  });

  for (auto &bone : Bones) {
    apollo::optimize(bone, DL);
  }

#ifndef NDEBUG
  {
    bool debug_errors = false;
    assert(!verifyModule(*(F->getParent()), &errs(), &debug_errors) &&
	   "Malformed module created by apollo::ScalarVerifBones");
  }
#endif // NDEBUG

  SeWrapper.releaseMemory();
  return true;
}

} // end namespace apollo

INITIALIZE_PASS_BEGIN(ScalarVerifBones, "APOLLO_SCALARVERIF_BONES",
                      "APOLLO_SCALARVERIF_BONES", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_DEPENDENCY(ScalarEvolutionWrapperPass)
INITIALIZE_PASS_END(ScalarVerifBones, "APOLLO_SCALARVERIF_BONES",
                    "APOLLO_SCALARVERIF_BONES", false, false)
