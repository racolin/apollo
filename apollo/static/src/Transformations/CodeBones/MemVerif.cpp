//===--- MemVerif.cpp -----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "SkeletonConfig.h"
#include "Transformations/CodeBones/MemVerif.h"
#include "Transformations/Parallel/BasicScalarPredict.h"
#include "Utils/Utils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/LoopUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/MetadataHandler.h"
#include "Utils/SCEVUtils.h"

#include "llvm/IR/DataLayout.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/Debug.h"

#include <stack>

// Enable debugging information for code-bones-extraction with
// '-debug' or '-debug=codebones-memverif'
#define DEBUG_TYPE "codebones-memverif"

using namespace apollo;
using namespace llvm;

namespace {

bool stmtNeedsVerification(Instruction *I) {
  if (metadata::hasMetadataKind(I, mdkind::IsAffineFromVerified))
    return false;
  if (metadata::hasMetadataKind(I, mdkind::IsAffine))
    return false;
  return true;
}

std::vector<Instruction *> collectStatementsToVerify(Function *F) {
  std::vector<Instruction *> Stmts;
  for (BasicBlock &block : *F) {
    for (Instruction &inst : block) {
      if (statement::isStatement(&inst) && stmtNeedsVerification(&inst))
        Stmts.push_back(&inst);
    }
  }
  return Stmts;
}

std::vector<std::set<Instruction *>>
collectMemVerifComputations(std::vector<Instruction *> &Stmts,
                            cfghelper::cfg &PD, cfghelper::cfg &DT) {

  std::vector<std::set<Instruction *>> Computations;
  for (Instruction *stmt : Stmts) {
    std::set<Value *> Initial = {statement::getPointerOperand(stmt),
                                 stmt->getParent()};
    Computations.push_back(collectComputations(Initial, PD, DT));
  }
  return Computations;
}

Function *createBoneDefinition(Function *F, Loop *Outermost, int MaxLoopId,
                               Instruction *Stmt) {
  // create function signature.
  const int NestId = function::getNestId(F);
  const int StmtId = statement::getStatementId(Stmt);
  Loop *ParentLoop = loop::getLoopWithId(Outermost, MaxLoopId);
  const int LoopDepth = ParentLoop->getLoopDepth();

  const std::string BoneName =
           utils::concat("apollo_bone.nest_", NestId, ".verif_stmt_", StmtId);

  LLVMContext &Context = F->getContext();

  Type *boolTy = Type::getInt1Ty(Context);
  Type *i64Ty = Type::getInt64Ty(Context);
  Type *GlobalParamsTy = F->getFunctionType()->getParamType(0);
  Type *PhiStateTy = F->getFunctionType()->getParamType(1);
  Type *CoefTy = Type::getInt8PtrTy(Context, 0);

  std::vector<Type *> ParamTypes = std::vector<Type *>(LoopDepth + 3, i64Ty);
  ParamTypes[0] = GlobalParamsTy;
  ParamTypes[1] = PhiStateTy;
  ParamTypes[2] = CoefTy;

  FunctionType *BoneType = FunctionType::get(boolTy, ParamTypes, false);
  Function *Bone = Function::Create(BoneType, Function::PrivateLinkage,
                                    BoneName, F->getParent());

  // name the virtual iterators
  auto arg_it = Bone->arg_begin();
  for (int i = 0; arg_it != Bone->arg_end(); ++arg_it, ++i) {
    if (i == 0)
      arg_it->setName("param");
    else if (i == 1)
      arg_it->setName("phi_state");
    else if (i == 2)
      arg_it->setName("coefs");
    else if (i > 2)
      arg_it->setName(utils::concat("vi.", i - 3));
  }
  return Bone;
}

void extractComputations(Function *Bone, Function *F, Loop *Outermost,
                         std::set<Instruction *> &Computations,
                         std::map<BasicBlock *, BasicBlock *> &bMap,
                         std::map<Value *, Value *> &iMap) {
  // Map the global params
  auto F_arg_itr = F->arg_begin();
  auto Bone_arg_itr = Bone->arg_begin();

  iMap[&*(F_arg_itr)] = &*(Bone_arg_itr);
  ++F_arg_itr;
  ++Bone_arg_itr;
  iMap[&*(F_arg_itr)] = &*(Bone_arg_itr);

  // Extract the code-bone from the function
  extractCodeBone(Bone, F, Outermost, Computations, bMap, iMap);
}

bool
createMemVerifBones(Function *F, Loop *Outermost,
		    std::vector<Instruction *> &Stmts,
		    std::vector<std::set<Instruction *>> &VerifComputations,
		    std::vector<Function *>& Bones) {
  LLVMContext &Context = F->getContext();
  Function *GetVerify = F->getParent()->getFunction("memory_runtime_verify");

  Type* I32 = Type::getInt32Ty(Context);
  Constant *One = ConstantInt::get(I32, 1);
  Constant *Nine = ConstantInt::get(I32, 9);
  Value *True = ConstantInt::getTrue(Context);

  for (unsigned i = 0; i < Stmts.size(); ++i) {
    const int StmtLoopId = cast<ConstantInt>(metadata::getMetadataOperand(
                                             Stmts[i], mdkind::ParentLoop))
                                             ->getSExtValue();
    auto Bone = createBoneDefinition(F, Outermost, StmtLoopId, Stmts[i]);
    Bones.push_back(Bone);

    std::map<BasicBlock *, BasicBlock *> bMap;
    std::map<Value *, Value *> iMap;
    extractComputations(Bone, F, Outermost, VerifComputations[i], bMap,
                        iMap);
    if (Bone == nullptr) {

      // Failed to construct a valid code-bone
      DEBUG({
	  dbgs() << __FUNCTION__ << ": "
		 << "Failed to construct a memverif code-bone\n"
		 << "------------------------------------------\n";
	});

      return false; // Failed to create a valid code-bone
    }

    auto AllViValues = std::move(getAllViValues(Bone, Outermost, StmtLoopId));

    if (!(basicScalarLinearFunction(Bone, Outermost, AllViValues, iMap)
	  && statementLinearFunctions(Bone, Outermost, AllViValues))) {
      return false; // Failed to create a valid code-bone
    }

    Value *PtrToVerif = iMap[statement::getPointerOperand(Stmts[i])];

    std::vector<BasicBlock *> BlocksOfBone;
    for (BasicBlock &block : *Bones[i]) {
      BlocksOfBone.push_back(&block);
    }
    // compute the prediction and return accordingly,
    // in any other case return true.
    for (BasicBlock *block : BlocksOfBone) {
      ReturnInst *Ret = dyn_cast<ReturnInst>(block->getTerminator());
      const bool IsMemAccessBlock = bMap[Stmts[i]->getParent()] == block;
      if (IsMemAccessBlock) {
        block->getTerminator()->eraseFromParent();
        apollo::ir::ApolloIRBuilder Builder(block);

        // Prepare memory runtime verification function call
        const int StmtId = statement::getStatementId(Stmts[i]);
        Constant *StmtIdArg = Builder.getInt(StmtId, 64);

        const std::string FunctionName =
            utils::concat("get_mem_runtime_verify.", StmtId);
        Function *GetParentFun = Builder.GetInsertBlock()->getParent();

        Value *PonterArg = Builder.CreateBitCast(
            PtrToVerif, GetVerify->getFunctionType()->getParamType(1));

        std::vector<Value *> ArgsV({StmtIdArg, PonterArg});
        std::vector<Value *> Vit = std::move(getBoneVi(GetParentFun));
        Constant *NumberOfVi = Builder.getInt(Vit.size(), 32);

        ArgsV.push_back(NumberOfVi);
        ArgsV.insert(ArgsV.end(), Vit.begin(), Vit.end());

        // load the coefficients
        std::vector<Value *> TubeCallValue(2);
        std::vector<Value *> Coefs = std::move(loadStatementCoefficients(
            Stmts[i], Outermost, Builder, TubeCallValue, "verify"));
        std::vector<Value *> VirtualIterators = std::move(getBoneVi(Bones[i]));

        Value *TubeWidth = TubeCallValue[0];
        Value *IsTube = TubeCallValue[1];

        std::vector<Value *> Vis;
        for (unsigned i = 0; i < Coefs.size() - 1; ++i) {
          Vis.push_back(VirtualIterators[i]);
        }
        Vis.push_back(Builder.getInt(1, 64));

        Value *Prediction = Builder.createDotProduct(Coefs, Vis);
        Prediction = Builder.CreateIntToPtr(Prediction, PtrToVerif->getType());
        Prediction->setName(utils::concat(
            "stmt.", statement::getStatementId(Stmts[i]), ".prediction"));

        Value *rLine = Builder.CreatePtrToInt(
            Prediction, (Builder.getInt(-2, 64))->getType());
        rLine->setName(
            utils::concat("rLine.", statement::getStatementId(Stmts[i])));

        // Get lower
        Value *Lower = Builder.CreateSub(rLine, TubeWidth);
        Lower->setName(
            utils::concat("lower.", statement::getStatementId(Stmts[i])));

        // Get upper
        Value *Upper = Builder.CreateAdd(rLine, TubeWidth);
        Upper->setName(
            utils::concat("upper.", statement::getStatementId(Stmts[i])));

        Value *PredictionSucceeded =
            Builder.CreateICmpEQ(PtrToVerif, Prediction);
        Value *PtrNotNull = Builder.CreateICmpNE(
            PtrToVerif,
            ConstantPointerNull::get(cast<PointerType>(PtrToVerif->getType())));

        // check linear verification
        Value *LinearVerification =
            Builder.CreateAnd(PredictionSucceeded, PtrNotNull);
        LinearVerification->setName(utils::concat(
            "linear_verify.", statement::getStatementId(Stmts[i])));

        // Create BasicBlock for linera verificitain
        Function *Fun = block->getParent();
        BasicBlock *RetLinearVerify =
            BasicBlock::Create(Fun->getContext(), "ret_linear_verify", Fun);

        // Create BasicBlock for checking tube validation
        BasicBlock *CheckTubeValid =
            BasicBlock::Create(Fun->getContext(), "check_tube_valid", Fun);

        // if (IsTube != 2) then it is not tube
        Value *IsNotTube =
            Builder.CreateICmpNE(IsTube, Builder.getInt(2, 64), "is_not_tube");

        // if (isNotTube)
        //   go to the ret_linear_verify BasiBlock
        // else
        //   go to the check_tube_valid BasiBlock
        Builder.CreateCondBr(IsNotTube, RetLinearVerify, CheckTubeValid);

        // return linear_verification inside of ret_linear_verify BasiBlock
        Builder.SetInsertPoint(RetLinearVerify);
        Builder.CreateRet(LinearVerification);

        // Start puting instructions in check_tube_valid BasiBlock for
        // checking tube
        Builder.SetInsertPoint(CheckTubeValid);
        Value *OriginalAddress = Builder.CreatePtrToInt(
            PtrToVerif, (Builder.getInt(2, 64))->getType());

        // if (original_address >= lower)
        Value *LowerCheck = Builder.CreateICmpUGE(OriginalAddress, Lower);
        LowerCheck->setName(
            utils::concat("lower_check.", statement::getStatementId(Stmts[i])));

        // if (original_address <= upper)
        Value *UpperCheck = Builder.CreateICmpULE(OriginalAddress, Upper);
        UpperCheck->setName(
            utils::concat("upper_check.", statement::getStatementId(Stmts[i])));

        // Create BasicBlock ret_inside_tube, it returns true if
        // ID is inside of tube
        BasicBlock *RetInsideTube =
            BasicBlock::Create(Fun->getContext(), "ret_inside_tube", Fun);

        // Create BasicBlock ret_mem_runtime_verify, it calls memory runtime
        // checking function and return it
        BasicBlock *RetMemRuntimeVerify = BasicBlock::Create(
            Fun->getContext(), "ret_mem_runtime_verify", Fun);

        // inside_tube = (lower_check && upper_check)
        Value *InsideTube = Builder.CreateAnd(LowerCheck, UpperCheck);
        InsideTube->setName(
            utils::concat("inside_tube.", statement::getStatementId(Stmts[i])));

        // if (inside_tube)
        //   go to the ret_inside_tube BasicBlock
        // else
        //   go to the ret_mem_runtime_verify
        BranchInst* IfInsideTube =
          Builder.CreateCondBr(InsideTube, RetInsideTube, RetMemRuntimeVerify);

        //TODO set the branch weight metadata. The true branch is more probable.
        /*SmallVector<Metadata*, 3> BrWeights;
        BrWeights.push_back(MDString::get(Context, "branch_weights"));
        BrWeights.push_back(ConstantAsMetadata::get(Nine));
        BrWeights.push_back(ConstantAsMetadata::get(One));
        IfInsideTube->setMetadata(LLVMContext::MD_prof,
                                  MDNode::get(Context, BrWeights));
                                  */

        // return inside_tube (congrats! tube works)
        Builder.SetInsertPoint(RetInsideTube);
        Builder.CreateRet(InsideTube);
        // Call memory runtime verify function,
        // which checks memory is READ only or not
        Builder.SetInsertPoint(RetMemRuntimeVerify);
        Value *CallRuntunVerify = Builder.CreateCall(GetVerify, ArgsV,
                                                     FunctionName);
        Builder.CreateRet(CallRuntunVerify);
      } else if (Ret) {
          Ret->eraseFromParent();
          ReturnInst::Create(block->getContext(), True, block);
      }
    }

    DEBUG({
	dbgs() << __FUNCTION__ << ": "
	       << "Extracted Memverif code-bone " << Bone->getName();
	Bone->print(dbgs(), nullptr, false, true);
	dbgs() << "------------------------------------------\n";
      });
  }

  return true;
}

} // end anonymous namespace

namespace apollo {

DefinePassCreator(MemVerifBones);
char MemVerifBones::ID = 0;

MemVerifBones::MemVerifBones()
  : ApolloPassOnSkeleton(ID, skeleton::sk_name<skeleton::code_bones>(), "MemVerifBones") {}

void MemVerifBones::getAnalysisUsage(AnalysisUsage &AU) const {
  AU.addRequired<LoopInfoWrapperPass>();
}

bool MemVerifBones::runOnApollo(Function *F) {
  DEBUG({
      dbgs() << __FUNCTION__ << ":\n"
	     << "Creating memverif code-bones fom Function "
	     << F->getName() << ":\n";
      F->print(dbgs(), nullptr, false, true);
      dbgs() << "------------------------------------------\n";
    });

  const DataLayout &DL = F->getParent()->getDataLayout();
  LoopInfo &LI = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
  Loop *Outermost = function::getOutermostLoopInFunction(F, &LI);
  cfghelper::cfg CFG(*F, LI, true);
  cfghelper::cfg Dom = CFG.dominator();
  cfghelper::cfg PostDom = CFG.postDominator();
  std::vector<Instruction *> Stmts = std::move(collectStatementsToVerify(F));
  std::vector<std::set<Instruction *>> VerifComputations =
      std::move(collectMemVerifComputations(Stmts, PostDom, Dom));
  std::vector<Function *> VerifBones;

  if (!createMemVerifBones(F, Outermost, Stmts, VerifComputations,
			   VerifBones)) {
    // Failed to create valid code-bones
    for (Function* Bone: VerifBones) {
      eraseCodeBone(Bone);
    }
    return true;
  }

  DEBUG(if (VerifBones.empty())
	  {
	    dbgs() << __FUNCTION__ << ": "
		   << "No memverif code-bones created\n"
		   << "Number of statements to verify: "
		   << Stmts.size()
		   << "\n"
		   << "------------------------------------------\n";
	  });

  for (Function *bone : VerifBones) {
    apollo::optimize(bone, DL);
  }

#ifndef NDEBUG
  {
    bool debug_errors = false;
    assert(!verifyModule(*(F->getParent()), &errs(), &debug_errors) &&
	   "Malformed module created by apollo::MemVerifBones");
  }
#endif // NDEBUG

  return true;
}

} // end namespace apollo

INITIALIZE_PASS_BEGIN(MemVerifBones, "APOLLO_MEMVERIF_BONES",
                      "APOLLO_MEMVERIF_BONES", false, false)
INITIALIZE_PASS_DEPENDENCY(LoopInfoWrapperPass)
INITIALIZE_PASS_END(MemVerifBones, "APOLLO_MEMVERIF_BONES",
                    "APOLLO_MEMVERIF_BONES", false, false)
