//===--- CodeBonesUtils.cpp -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016-2019. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Transformations/CodeBones/CodeBonesUtils.h"
#include "Utils/FunctionUtils.h"
#include "Utils/ViUtils.h"
#include "Utils/Utils.h"
#include "Utils/MetadataHandler.h"

#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Vectorize.h"
#include "llvm/Transforms/IPO.h"
#include "llvm/IR/Verifier.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/Analysis/Lint.h"
#include "llvm/Support/Debug.h"

#include <stack>
#include <queue>
#include <set>
#include <cassert>

using namespace llvm;
using namespace apollo;

// Enable debugging information for code-bones-extraction with
// '-debug' or '-debug=codebones-extract'
#define DEBUG_TYPE "codebones-extract"

int apollo::getMaximumLoopId(std::set<Instruction *> &Computations) {
  int Max = 0;
  for (Instruction *I : Computations) {
    if (statement::isStatement(I) || basicscalar::isBasicScalar(I)) {
      const int LoopId =
          cast<ConstantInt>(metadata::getMetadataOperand(I, mdkind::ParentLoop))
              ->getSExtValue();
      Max = std::max<int>(Max, LoopId);
    }
  }
  return Max;
}

static bool isLoopLatch(BasicBlock *BB, Loop *L) {
  BasicBlock *Latch = L->getLoopLatch();
  assert(Latch && "Not unique latch!");
  if (Latch == BB)
    return true;
  for (Loop *Subloop : *L) {
    if (isLoopLatch(BB, Subloop))
      return true;
  }
  return false;
}


void apollo::extractCodeBone(llvm::Function *Bone,
			     llvm::Function *Orig,
			     llvm::Loop *Outermost,
			     std::set<llvm::Instruction *> &Computations,
			     std::map<BasicBlock *, BasicBlock *> &bMap,
			     std::map<Value *, Value *> &iMap) {

  DEBUG({
      dbgs() << __FUNCTION__ << ": "
	<< "extracting from function " << Orig->getName() << "\n";
      dbgs() << "Computations to extract: [\n";
	for (const auto& I: Computations) {
	  I->print(dbgs(), true);
	  dbgs() << "\n";
	}
	dbgs() << "]\n\n";

	dbgs() << "Function " << Orig->getName() << ":\n";
	Orig->print(dbgs(), nullptr, false, true);
	dbgs() << "------------------------------------------\n";
    });


  // Clone the Basic blocks and the control flow instructions
  for (BasicBlock &block : *Orig) {
    BasicBlock* cbblock =
      BasicBlock::Create(block.getContext(), block.getName(), Bone);
    bMap[&block] = cbblock;

    IRBuilder<> Builder(cbblock);
    TerminatorInst *Term = block.getTerminator();
    Instruction *Clone = nullptr;

    // If the terminator is needed for the computations then clone it.
    // but not if it is a loop-latch or the end of function.
    if (isa<ReturnInst>(Term) || isLoopLatch(&block, Outermost)) {
      Clone = Builder.CreateRetVoid();
    } else if (Computations.count(Term)) {
      Clone = Term->clone();
      Builder.Insert(Clone);
    } else {
      // Terminators not used for the computation are replaced by a dummy
      // branch. The instruction operand will be fixed up later
      Clone = Builder.CreateBr(Term->getSuccessor(0));
    }
    assert(Clone != nullptr && "Mishandled block terminator");

    iMap[Term] = Clone;
  }

  // Copy the instructions into the new basic blocks
  {
    for (auto &block_pair: bMap) {
      BasicBlock *BlockOriginal = block_pair.first;
      BasicBlock *BlockClone = block_pair.second;

      IRBuilder<> Builder(BlockClone->getTerminator());
      for (Instruction &inst : *BlockOriginal) {
	// Clone the instruction if it is needed for the computatin. Terminators
	// were already handled
	if (!inst.isTerminator()
	    && Computations.find(&inst) != Computations.end()) {
	  Instruction *Clone = inst.clone();
	  Builder.Insert(Clone);
	  iMap[&inst] = Clone;
	}
      }
    }

    // Fix up the operands in the cloned instructions. In particular, branches
    // and PHI nodes need to be updated
    for (BasicBlock &bone_block: *Bone) {

      // Get the block's predecessors
      std::set<const BasicBlock*>
	pred_set(pred_begin(&bone_block), pred_end(&bone_block));

      // Update the instruction operands
      for (Instruction &bone_inst: bone_block) {
	const int NumOperands = bone_inst.getNumOperands();

	for (int operand_idx = 0; operand_idx < NumOperands; ++operand_idx) {
	  Value *Operand = bone_inst.getOperand(operand_idx);
	  BasicBlock *OperandAsBlock = dyn_cast<BasicBlock>(Operand);

	  // Replace value or block operands with the cloned values or blocks
	  if ((isa<Instruction>(Operand) || isa<Argument>(Operand)) &&
	      iMap.count(Operand))
	    bone_inst.setOperand(operand_idx, iMap[Operand]);
	  else if (OperandAsBlock && !isa<PHINode>(&bone_inst))
	    bone_inst.setOperand(operand_idx, bMap[OperandAsBlock]);
	}

	// Update PHI nodes to remove predecessors that are no longer there
	PHINode *PhiNode = dyn_cast<PHINode>(&bone_inst);
	if (PhiNode) {
	  // Remove references to non-existent predecessors
	  std::list<BasicBlock*> removals;
	  for (auto& operand: PhiNode->operands()) {
	    BasicBlock* incoming = PhiNode->getIncomingBlock(operand);
	    // Check whether the clone is a predecessor
	    BasicBlock* repl = bMap[incoming];
	    if (pred_set.find(repl) == pred_set.end())
	      PhiNode->removeIncomingValue(incoming, false);
	  }

	  // Update the PHI node with the current predecessors.
	  const auto NumPhiOperands = PhiNode->getNumOperands();

	  for (int idx = 0; idx < NumPhiOperands; ++idx) {
	    BasicBlock* incoming = PhiNode->getIncomingBlock(idx);

	    assert(bMap.find(incoming) != bMap.end() &&
		   "Unexpected operand in PHI node");

	    BasicBlock* replacement = bMap[incoming];
	    PhiNode->setIncomingBlock(idx, replacement);
	  }

	}
      }
    }
  }

  // Set the linkage
  Bone->setLinkage(Function::PrivateLinkage);

  DEBUG({
      dbgs() << __FUNCTION__ << ": "
	<< "extracted code-bone " << Bone->getName() << "\n";
	Bone->print(dbgs(), nullptr, false, true);
	dbgs() << "------------------------------------------\n";
    });

#if 0
  // The code-bone body may be invalid because it may have a return type that is
  // not void
  assert(!verifyFunction(*Bone, &errs()) &&
	 "Malformed function created by extractCodeBone");
#ifndef NDEBUG
  lintFunction(*Bone);
#endif // NDEBUG
#endif
}

/// \brief Erase a code-bone from its containing module and remove all
/// references to it.
/// \p Bone The code-bone to erase
void apollo::eraseCodeBone(Function* Bone)
{
  if (!Bone)
    return;

  Bone->dropAllReferences();
  Bone->eraseFromParent();
}

std::vector<Value *> apollo::loadStatementCoefficients(
    Instruction *Stmt, Loop *Outermost, ir::ApolloIRBuilder &Builder,
    std::vector<Value *> &TubeCallValue, const std::string &Str) {

  Function *ParentFunction = Builder.GetInsertBlock()->getParent();
  Function *GetCoef =
      ParentFunction->getParent()->getFunction("get_coefficient");
  Argument *CoefsArg = function::getArgumentByName(ParentFunction, "coefs");
  Constant *Zero = constant::zero64(Stmt->getContext());

  const int StmtId = statement::getStatementId(Stmt);
  const int ParentLoopId =
      cast<ConstantInt>(metadata::getMetadataOperand(Stmt, mdkind::ParentLoop))
          ->getSExtValue();
  Loop *ParentLoop = loop::getLoopWithId(Outermost, ParentLoopId);
  const int Depth = ParentLoop->getLoopDepth();

  std::vector<Value *> Coefs(Depth + 1, nullptr);
  Constant *StmtConstId = Builder.getInt(StmtId, 64);
  for (int coef_id = 0; coef_id < Depth + 1; ++coef_id) {
    const std::string CoefName = utils::concat("stmt.", StmtId, "_cf.", coef_id);
    Constant *CoefNumber = Builder.getInt(coef_id, 64);
    Coefs[coef_id] = Builder.CreateCall(
        GetCoef,
        std::vector<Value *>({CoefsArg, Zero, StmtConstId, CoefNumber}),
        CoefName);
  }

  // Tube call
  std::string TubeName = "tube.";
  int TubeCallId = 0;
  if (Str == "tube")
    TubeCallId = -1;
  if (Str == "verify") {
    TubeCallId = -2;
    TubeName = "tube_width.";
    std::string Name = utils::concat("is_tube.", StmtId);
    Constant *ConstNumber = Builder.getInt((-1), 64);
    TubeCallValue[1] = Builder.CreateCall(
        GetCoef,
        std::vector<Value *>({CoefsArg, Zero, StmtConstId, ConstNumber}), Name);
  }

  const std::string CoefName = utils::concat(TubeName, StmtId);
  Constant *CoefNumber = Builder.getInt(TubeCallId, 64);
  TubeCallValue[0] = Builder.CreateCall(
      GetCoef, std::vector<Value *>({CoefsArg, Zero, StmtConstId, CoefNumber}),
      CoefName);
  return Coefs;
}

std::vector<Value *>
apollo::loadScalarCoefficients(PHINode *Scalar, Loop *Outermost,
                               ir::ApolloIRBuilder &Builder) {

  Function *ParentFunction = Builder.GetInsertBlock()->getParent();
  Function *GetCoef =
      ParentFunction->getParent()->getFunction("get_coefficient");
  Argument *CoefsArg = function::getArgumentByName(ParentFunction, "coefs");
  Constant *One = constant::one64(Scalar->getContext());

  const unsigned ScalarId = basicscalar::getBasicScalarId(Scalar);
  const int ParentLoopId = cast<ConstantInt>(metadata::getMetadataOperand(
                                           Scalar, mdkind::ParentLoop))
                         ->getSExtValue();
  Loop *ParentLoop = loop::getLoopWithId(Outermost, ParentLoopId);
  const unsigned Depth = ParentLoop->getLoopDepth();
  std::vector<Value *> Coefs(Depth + 1, nullptr);
  Constant *ScalarConstId = Builder.getInt(ScalarId, 64);
  for (unsigned coef_id = 0; coef_id < Depth + 1; ++coef_id) {
    const std::string CoefName = 
                      utils::concat("scalar.", ScalarId, "_cf.", coef_id);
    Constant *CoefNumber = Builder.getInt(coef_id, 64);
    Coefs[coef_id] = Builder.CreateCall(
        GetCoef,
        std::vector<Value *>({CoefsArg, One, ScalarConstId, CoefNumber}),
        CoefName);
  }
  return Coefs;
}

std::vector<Value *>
apollo::loadBoundCoefficients(const unsigned LoopId, Loop *Outermost,
                              ir::ApolloIRBuilder &Builder) {

  Function *ParentFunction = Builder.GetInsertBlock()->getParent();
  Function *GetCoef =
      ParentFunction->getParent()->getFunction("get_coefficient");
  Argument *CoefsArg = function::getArgumentByName(ParentFunction, "coefs");
  Constant *Two = constant::two64(ParentFunction->getContext());
  Loop *L = loop::getLoopWithId(Outermost, LoopId);
  const unsigned Depth = L->getLoopDepth();

  std::vector<Value *> Coefs(Depth, nullptr);
  Constant *LoopConstId = Builder.getInt(LoopId, 64);
  for (unsigned coef_id = 0; coef_id < Depth; ++coef_id) {
    const std::string CoefName =
        utils::concat("loop_bound.", LoopId, "_cf.", coef_id);
    Constant *CoefNumber = Builder.getInt(coef_id, 64);
    Coefs[coef_id] = Builder.CreateCall(
        GetCoef, std::vector<Value *>({CoefsArg, Two, LoopConstId, CoefNumber}),
        CoefName);
  }
  return Coefs;
}

std::set<Instruction *> apollo::collectComputations(std::set<Value *> &Initial,
                                                    cfghelper::cfg &PD,
                                                    cfghelper::cfg &DT) {
  Function *F = PD.Nodes[0]->getParent();
  std::set<Value *> ProcessedValues;
  std::set<Instruction *> Computations;
  std::stack<Value *> S;
  for (Value *v : Initial) {
    S.push(v);
  }
  while (!S.empty()) {
    Value *V = S.top();
    S.pop();
    // if already processed skip it.
    bool Processed = ProcessedValues.count(V);
    if (Processed)
      continue;
    ProcessedValues.insert(V);
    Instruction *ThisInst = dyn_cast<Instruction>(V);
    BasicBlock *ThisBlock = dyn_cast<BasicBlock>(V);
    // a basic block depends on the terminators that make it execute.
    if (ThisBlock) {
      for (BasicBlock &block : *F) {
        if (&block != ThisBlock && DT.hasNegihbour(ThisBlock, &block) &&
            !PD.hasNegihbour(&block, ThisBlock))
          S.push(block.getTerminator());
      }
    }
    if (ThisInst) {
      Computations.insert(ThisInst);
      // bool is_statement = statement::isStatement(this_inst);
      const bool IsScalar = basicscalar::isBasicScalar(ThisInst);
      const bool IsPredicted = IsScalar; // || is_statement;
      const bool IsPhiMerge = isa<PHINode>(ThisInst) && !IsScalar;
      const bool IsBranch = isa<BranchInst>(ThisInst);
      // An instruction always depends on its parent block
      BasicBlock *ParentBloack = ThisInst->getParent();
      S.push(ParentBloack);
      if (!IsPredicted) {
        if (IsBranch) {
          BranchInst *ThisBranch = cast<BranchInst>(ThisInst);
          if (ThisBranch->isConditional())
            S.push(ThisBranch->getCondition());
        } else if (IsPhiMerge) {
          PHINode *PhiNode = cast<PHINode>(ThisInst);
          for (unsigned incoming_idx = 0;
               incoming_idx < PhiNode->getNumIncomingValues(); ++incoming_idx) {
            Value *IncomingValue = PhiNode->getIncomingValue(incoming_idx);
            BasicBlock *IncomingBlock = PhiNode->getIncomingBlock(incoming_idx);
            S.push(IncomingValue);
            S.push(IncomingBlock);
            S.push(IncomingBlock->getTerminator());
          }
        } else {
          for (Value *operand : ThisInst->operands()) {
            S.push(operand);
          }
        }
      }
    }
  }
  return Computations;
}

std::vector<Value *> apollo::getBoneVi(Function *Bone) {
  std::vector<Value *> Vi;
  for (Argument &arg : Bone->args()) {
    if (arg.getName().startswith("vi."))
      Vi.push_back(&arg);
  }
  return Vi;
}

std::map<int, Value *> apollo::getAllViValues(Function *Bone, Loop *Outermost,
                                              int ParentLoopId) {
  apollo::ir::ApolloIRBuilder Builder(&*(Bone->getEntryBlock().begin()));
  Constant *Zero = ConstantInt::get(Type::getInt64Ty(Bone->getContext()), 0);
  Constant *One = ConstantInt::get(Type::getInt64Ty(Bone->getContext()), 1);
  const std::vector<Value *> ParentLoopVis = std::move(apollo::getBoneVi(Bone));
  const std::vector<unsigned> ParentLoopIds = std::move(
      loop::getLoopIdVector(loop::getLoopWithId(Outermost, ParentLoopId)));
  assert(ParentLoopVis.size() == ParentLoopIds.size());
  std::map<int, Value *> AllVi;
  for (size_t i = 0; i < ParentLoopIds.size(); ++i) {
    AllVi[ParentLoopIds[i]] = ParentLoopVis[i];
  }
  std::queue<Loop *> ToCompute;
  ToCompute.push(Outermost);
  while (!ToCompute.empty()) {
    Loop *CurLoop = ToCompute.front();
    ToCompute.pop();
    const int CurLoopId = loop::getLoopId(CurLoop);
    const bool Computed = AllVi.count(CurLoopId);
    const bool ExecutesAfter = ParentLoopId < CurLoopId;
    if (!Computed) {
      if (ExecutesAfter) {
        // since it was not executed, the vi value is 0
        AllVi[CurLoopId] = Zero;
      } else {
        // since it was executed, the vi value is the upper bound using
        // the prediction
        const std::vector<unsigned> CurLoopParentIds = std::move(
            loop::getLoopIdVector(loop::getLoopWithId(Outermost, CurLoopId)));
        std::vector<Value *> CurLoopVi;
        for (unsigned cur_loop_parent_id : CurLoopParentIds) {
          if ((int)cur_loop_parent_id != CurLoopId)
            CurLoopVi.push_back(AllVi.at(cur_loop_parent_id));
        }
        CurLoopVi.push_back(One);
        auto Coefs =
            std::move(loadBoundCoefficients(CurLoopId, Outermost, Builder));
        Value *Prediction = Builder.createDotProduct(Coefs, CurLoopVi);
        Prediction = Builder.CreateSub(Prediction, One);
        AllVi[CurLoopId] = Prediction;
      }
    }
    for (Loop *subloop : *CurLoop) {
      ToCompute.push(subloop);
    }
  }
  return AllVi;
}

// \brief Replace basic scalars by their prediction
// \return false if the input \p Bone couldn't be handled true otherwise.
bool apollo::basicScalarLinearFunction(Function *Bone, Loop *Outermost,
                                       std::map<int, Value *> &AllViValues,
                                       std::map<Value *, Value *> &iMap) {

  assert(Outermost->getParentLoop() == 0);

  // Get all basic scalars
  std::vector<PHINode *> BasicScalars;
  for (BasicBlock &block : *Bone) {
    for (Instruction &phi : block) {
      if (basicscalar::isBasicScalar(&phi)) {
        BasicScalars.push_back(cast<PHINode>(&phi));
      }
    }
  }

  // New instructions are created with a Builder
  Instruction *InsertAt = Bone->getEntryBlock().getTerminator();
  ir::ApolloIRBuilder Builder(InsertAt);

  // Iterate over all basic scalars
  for (PHINode *scalar : BasicScalars) {
    const Type* const scalarType = scalar->getType();

    assert(!scalar->getType()->isFloatingPointTy() &&
	   "Floating point scalars are not supported!.");

     if (scalar->getType()->isFloatingPointTy()) {
    	// utils::gracefulExit("Floating point scalars are not supported!.");
        std::cerr << "Floating point scalars are not supported!. Try to re-compile by setting APOLLO_NOLICM: APOLLO_NOLICM= apolloc/apolloc++ TargetCode" << std::endl;
	exit(-1);
    }

    // Only integer or pointers are handled
    if (!scalarType->isIntegerTy() && !scalarType->isPointerTy()) {
      return false;
    }

    // load the coefficients
    std::vector<Value *> Coefs =
        std::move(loadScalarCoefficients(scalar, Outermost, Builder));
    const int Id = basicscalar::getBasicScalarId(scalar);
    const int ParentLoop =
      cast<ConstantInt>
      (metadata::getMetadataOperand(scalar, mdkind::ParentLoop))
      ->getSExtValue();

    // Set the vi vector
    auto ParentLoops =
      std::move(loop::getLoopIdVector(loop::getLoopWithId(Outermost,
							  ParentLoop)));
    std::vector<Value *> Vis;
    for (unsigned i = 0; i < Coefs.size() - 1; ++i) {
      Vis.push_back(AllViValues.at(ParentLoops[i]));
    }
    Vis.push_back(Builder.getInt(1, 64));

    // Create the prediction
    Value *Prediction = Builder.createDotProduct(Coefs, Vis);
    if (scalar->getType()->isIntegerTy()) {
      Prediction = Builder.CreateIntCast(
          Prediction, scalar->getType(),
          cast<IntegerType>(scalar->getType())->getSignBit());
    }
    else if (scalar->getType()->isPointerTy()) {
      Prediction = Builder.CreateIntToPtr(Prediction, scalar->getType());
    }
    else {
      assert(false && "Unexpected type");
    }
    Prediction->setName(utils::concat("scalar.", Id, ".prediction"));
    MDNode *MdNode = scalar->getMetadata(mdkind::ApolloPhiId);
    Instruction* PredAsInst = cast<Instruction>(Prediction);
    PredAsInst->setMetadata(mdkind::ApolloPhiId, MdNode);

    // Update the iMap
    for (std::pair<Value *const, Value *> &mapping : iMap) {
      if (mapping.second == scalar) {
        mapping.second = Prediction;
      }
    }

    // Replace the instruction
    scalar->replaceAllUsesWith(Prediction);
    scalar->eraseFromParent();
  }
  return true;
}

// \return false if the input \p Bone couldn't be handled true otherwise.
bool apollo::statementLinearFunctions(Function *Bone, Loop *Outermost,
                                      std::map<int, Value *> &AllViValues) {

  assert(Outermost->getParentLoop() == 0);
  // replace pointers in statements by their prediction
  std::vector<Instruction *> StatementsInBone;
  for (BasicBlock &block : *Bone) {
    for (Instruction &stmt : block) {
      if (statement::isStatement(&stmt)) {
        StatementsInBone.push_back(&stmt);
      }
    }
  }
  Instruction *InsertAt = Bone->getEntryBlock().getTerminator();
  ir::ApolloIRBuilder Builder(InsertAt);
  for (Instruction *stmt : StatementsInBone) {
    // load the coefficients
    std::vector<Value *> TubeCallValue(1);
    std::vector<Value *> Coefs = std::move(loadStatementCoefficients(
        stmt, Outermost, Builder, TubeCallValue, "tube"));
    // set the vi vector
    const int ParentLoop =
      cast<ConstantInt>
      (metadata::getMetadataOperand(stmt, mdkind::ParentLoop))
      ->getSExtValue();
    auto ParentLoops = std::move(
        loop::getLoopIdVector(loop::getLoopWithId(Outermost, ParentLoop)));
    std::vector<Value *> Vis;
    for (unsigned i = 0; i < Coefs.size() - 1; ++i) {
      Vis.push_back(AllViValues.at(ParentLoops[i]));
    }

    Vis.push_back(Builder.getInt(1, 64));
    // replace the pointer with the prediction
    Value *Ptr = statement::getPointerOperand(stmt);
    Value *LinearPrediction = Builder.createDotProduct(Coefs, Vis);
    LinearPrediction = Builder.CreateIntToPtr(LinearPrediction, Ptr->getType());
    LinearPrediction->setName(utils::concat(
        "stmt.", statement::getStatementId(stmt), ".linearPrediction"));
    const int Val = 2; // see prediction model type
    Value *TubeTypeInt = ConstantInt::get(TubeCallValue[0]->getType(), Val);
    Value *TubeFlag = Builder.CreateICmpEQ(TubeCallValue[0], TubeTypeInt);
    TubeFlag->setName(
        utils::concat("tube_flag.", statement::getStatementId(stmt)));
    // prediction = builder.CreateSelect(TubeFlag, pointer, prediction);
    Value *Prediction =
        SelectInst::Create(TubeFlag, Ptr, LinearPrediction, "", stmt);
    Prediction->setName(
        utils::concat("stmt.", statement::getStatementId(stmt), ".prediction"));
    stmt->replaceUsesOfWith(Ptr, Prediction);
  }

  return true;
}

void apollo::optimize(Function *F, const DataLayout &DL) {
  legacy::FunctionPassManager FPM(F->getParent());

  // Create optimization passes
  FPM.add(createVerifierPass());
  FPM.add(createCFGSimplificationPass());
  FPM.add(createInstructionCombiningPass());
  FPM.add(createSLPVectorizerPass());
  FPM.add(createCFGSimplificationPass());
  FPM.add(createInstructionCombiningPass());

  // Initialize passes
  FPM.doInitialization();

  // Optimize F
  std::vector<Instruction *> UnusedSafeCalls;
  do {
    FPM.run(*F);
    UnusedSafeCalls.clear();
    for (BasicBlock &block : *F) {
      for (Instruction &inst : block) {
        CallInst *CI = dyn_cast<CallInst>(&inst);
        if (CI && CI->getCalledFunction() &&
            function::allowedInNest(CI->getCalledFunction())) {
          const bool NoUses = CI->user_begin() == CI->user_end();
          if (NoUses)
            UnusedSafeCalls.push_back(CI);
        }
      }
    }
    for (Instruction *call : UnusedSafeCalls) {
      call->eraseFromParent();
    }
  } while (!UnusedSafeCalls.empty());

  // Finalize passes
  FPM.doFinalization();
}

namespace cfghelper {

cfg::cfg() {}

cfg::cfg(Function &F, LoopInfo &LI, bool BreakLatch) {

  for (BasicBlock &block : F) {
    Block2Idx[&block] = Nodes.size();
    Nodes.push_back(&block);
  }
  // add a single exit dummy
  const size_t DummyIdx = Nodes.size();
  for (unsigned i = 0; i < Nodes.size(); ++i) {
    BasicBlock *Block = Nodes[i];
    TerminatorInst *Term = Block->getTerminator();
    Loop *L = LI.getLoopFor(Block);
    bool IsLatch = L && L->getLoopLatch() == Block;
    std::set<int> Neigh;
    if (!(IsLatch && BreakLatch)) {
      for (unsigned suc = 0; suc < Term->getNumSuccessors(); suc++) {
        BasicBlock *SucBlock = Term->getSuccessor(suc);
        Neigh.insert(Block2Idx[SucBlock]);
      }
    }
    // if it is an original exit, point to the dummy
    if (Neigh.empty())
      Neigh.insert(DummyIdx);
    this->Neighbours.push_back(Neigh);
  }
  Nodes.push_back(nullptr);
  Block2Idx[nullptr] = DummyIdx;
  this->Neighbours.push_back(std::set<int>());
}

static std::set<int> intersect(const std::set<int> &A, const std::set<int> &B) {
  std::set<int> S;
  for (int e : A) {
    if (B.count(e))
      S.insert(e);
  }
  return S;
}

cfg cfg::reverse() const {
  cfg Reversed;
  Reversed.Nodes = Nodes;
  Reversed.Block2Idx = Block2Idx;
  Reversed.Neighbours = std::vector<std::set<int>>(Nodes.size(), 
                                                   std::set<int>());

  for (unsigned i = 0; i < Nodes.size(); ++i) {
    for (int j : Neighbours[i]) {
      Reversed.Neighbours[j].insert(i);
    }
  }
  return Reversed;
}

cfg cfg::dominator() const {
  cfg DominatorTree;
  DominatorTree.Nodes = Nodes;
  DominatorTree.Block2Idx = Block2Idx;
  // compute the predecessors
  std::vector<std::set<int>> Predecessors(Nodes.size(), std::set<int>());
  size_t node = 0;
  for (node = 0; node < Nodes.size(); ++node) {
    for (int neighbour : Neighbours[node]) {
      Predecessors[neighbour].insert(node);
    }
  }

  std::set<int> EntryNodes;
  for (node = 0; node < Nodes.size(); ++node) {
    if (Predecessors[node].size() == 0)
      EntryNodes.insert(node);
  }

  std::set<int> AllNodes;
  for (node = 0; node < Nodes.size(); ++node) {
    AllNodes.insert(node);
  }

  // initialize the neighbours
  for (node = 0; node < Nodes.size(); ++node) {
    if (EntryNodes.count(node))
      DominatorTree.Neighbours.push_back(std::set<int>({(int)node}));
    else
      DominatorTree.Neighbours.push_back(AllNodes);
  }
  bool Changed = false;
  do {
    Changed = false;
    for (node = 0; node < Nodes.size(); ++node) {
      if (!EntryNodes.count(node)) {
        std::set<int> NewDominators = AllNodes;
        for (int pred : Predecessors[node]) {
          NewDominators = std::move(
              intersect(NewDominators, DominatorTree.Neighbours[pred]));
        }
        NewDominators.insert(node);
        Changed = Changed || NewDominators != DominatorTree.Neighbours[node];
        DominatorTree.Neighbours[node] = NewDominators;
      }
    }
  } while (Changed);
  return DominatorTree;
}

cfg cfg::postDominator() const {
  cfg Reversed = this->reverse();
  return Reversed.dominator();
}

bool cfg::hasNegihbour(BasicBlock *BBa, BasicBlock *BBb) const {
  return this->Neighbours[this->Block2Idx.at(BBa)].count(
      this->Block2Idx.at(BBb));
}

std::ostream &cfg::dump(std::ostream &OS) const {
  OS << "graph {\n";
  OS << "\tn = " << Nodes.size() << "\n";
  for (unsigned i = 0; i < Nodes.size(); ++i) {
    const std::string NodeName =
        Nodes[i] ? Nodes[i]->getName() : "exit_node(nullptr)";
    OS << "\t" << i << ". " << NodeName << " -> [ ";
    if (Neighbours[i].size() == Nodes.size())
      OS << "all nodes ";
    else {
      for (int neigh : Neighbours[i]) {
        OS << neigh << " ";
      }
    }
    OS << "]\n";
  }
  OS << "}\n";
  return OS;
}

} // end namespace cfghelper
