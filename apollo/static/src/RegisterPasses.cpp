//===--- RegisterPasses.cpp -----------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Utils/Utils.h"
#include "SkeletonConfig.h"
#include "LinkAllPasses.h"

#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Transforms/IPO/AlwaysInliner.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Analysis/CFGPrinter.h"
#include "llvm/Transforms/IPO.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Vectorize.h"
#include "llvm/Analysis/TypeBasedAliasAnalysis.h"

using namespace llvm;
namespace apollo {
using namespace skeleton;

// Pass initialization callback.
static void initializeApolloPasses(llvm::PassRegistry &PR) {
  llvm::initializeApolloLoopExtractPass(PR);
  llvm::initializeApolloIgnoreEmptyPass(PR);
  llvm::initializePromoteMemIntrinsicsPass(PR);
  llvm::initializeApolloInstrumentLinearEquationPass(PR);
  llvm::initializeApolloSignatureNormalizePass(PR);
  llvm::initializeApolloPhiHandlingPass(PR);
  llvm::initializeApolloStatementMetadataAdderPass(PR);
  llvm::initializeStatementVerificationAvoidancePass(PR);
  llvm::initializeRegisterStaticInfoPass(PR);
  llvm::initializeRegisterStmtAliasPass(PR);
  llvm::initializeFunctionCallParentPass(PR);
  llvm::initializeApolloCloneLoopFunctionPass(PR);
  llvm::initializeApolloViInsertPass(PR);
  llvm::initializeFunctionCallParentPass(PR);
  llvm::initializeApolloReg2MemPass(PR);
  llvm::initializeInstrumentFixFunctionSignaturePass(PR);
  llvm::initializeApolloInstrumentPhiStatePass(PR);
  llvm::initializeApolloInstrumentViBoundsPass(PR);
  llvm::initializeApolloInstrumentOnConditionPass(PR);
  llvm::initializeApolloInstrumentMemAccessPass(PR);
  llvm::initializeApolloInstrumentIterationCountPass(PR);
  llvm::initializeApolloInstrumentPhiPass(PR);
  llvm::initializeInstrumentFunctionCallPass(PR);
  llvm::initializeBasicScalarPredictPass(PR);
  llvm::initializeJitExportFctPass(PR);
  llvm::initializeSwitchingCallPass(PR);
  llvm::initializeApolloGlobalCombinePass(PR);
}

struct Initializer {
  Initializer() {
    llvm::PassRegistry &PR = *llvm::PassRegistry::getPassRegistry();
    initializeApolloPasses(PR);
  }
};
static Initializer InitializeApollo;

// Command line arguments and categories.
cl::OptionCategory ApolloCategory("Apollo options",
                                  "Configure the apollo framework");

static cl::opt<bool> ApolloEnabled("apollo", cl::desc("Enable apollo."),
                                   cl::init(false), cl::ZeroOrMore,
                                   cl::cat(ApolloCategory));

void registerPreOptimizationPasses(const llvm::PassManagerBuilder &Builder,
                                   llvm::legacy::PassManagerBase &PM) {

  PM.add(createGlobalOptimizerPass());
  PM.add(createIPSCCPPass());
  PM.add(createDeadArgEliminationPass());

  if (Builder.Inliner)
    PM.add(createFunctionInliningPass());

  PM.add(createSROAPass());
  PM.add(createEarlyCSEPass());
  PM.add(createCorrelatedValuePropagationPass());
  PM.add(createReassociatePass());
  PM.add(createSCCPPass());
  PM.add(createCorrelatedValuePropagationPass());
  PM.add(createDeadStoreEliminationPass());
  PM.add(createAggressiveDCEPass());
  // necesary for eliminating invoke instructions
  PM.add(createLowerInvokePass());
  // necesary for creating the phi-nodes.
  PM.add(createPromoteMemoryToRegisterPass());
}

/// \brief This function registers the optimizations equivalent to the
///        optimization level. It wont register loop unswitch.
///        See llvm::PassManagerBuilder::populateModulePassManager as
///        a reference.
static void registerOptimizationPasses(const llvm::PassManagerBuilder &Builder,
                                       llvm::legacy::PassManagerBase &PM) {

  PM.add(createInstructionCombiningPass());
  PM.add(createCFGSimplificationPass());
  PM.add(createPruneEHPass());
  PM.add(createEarlyCSEPass());
  PM.add(createJumpThreadingPass());
  PM.add(createCorrelatedValuePropagationPass());
  PM.add(createCFGSimplificationPass());
  PM.add(createInstructionCombiningPass());
  PM.add(createTailCallEliminationPass());
  PM.add(createCFGSimplificationPass());
  PM.add(createReassociatePass());
  PM.add(createLoopRotatePass());
  if (!getenv("APOLLO_NOLICM")) PM.add(createLICMPass());
  PM.add(createInstructionCombiningPass());
  PM.add(createIndVarSimplifyPass());
  PM.add(createLoopDeletionPass());
  PM.add(createSimpleLoopUnrollPass());
  PM.add(createSCCPPass());
  PM.add(createInstructionCombiningPass());
  PM.add(createJumpThreadingPass());
  PM.add(createCorrelatedValuePropagationPass());
  PM.add(createDeadStoreEliminationPass());
  PM.add(createAggressiveDCEPass());
  PM.add(createCFGSimplificationPass());
  PM.add(createInstructionCombiningPass());
  PM.add(createLoopSimplifyPass());
  PM.add(createIndVarSimplifyPass());
}

void registerPreparationPasses(const llvm::PassManagerBuilder &Builder,
                               llvm::legacy::PassManagerBase &PM) {
  PM.add(apollo::createApolloLoopExtractPass());
  registerOptimizationPasses(Builder, PM);
  PM.add(apollo::createPromoteMemIntrinsicsPass());
  PM.add(apollo::createApolloIgnoreEmptyPass());
  // to re-inline extracted empty apollo_loop functions.
  PM.add(createAlwaysInlinerLegacyPass());
  PM.add(apollo::createApolloSignatureNormalizePass());
  PM.add(apollo::createApolloPhiHandlingPass());
  PM.add(apollo::createApolloStatementMetadataAdderPass());
  PM.add(apollo::createFunctionCallParentPass());
  PM.add(apollo::createStatementVerificationAvoidancePass());
  PM.add(apollo::createRegisterStaticInfoPass());
  PM.add(apollo::createRegisterStmtAliasPass());
  PM.add(createVerifierPass());
}

void registerInstrumentationPasses(llvm::legacy::PassManagerBase &PM) {
  // creation of the instrumented version
  PM.add(apollo::createApolloCloneLoopFunctionPass(
      skeleton::sk_name<skeleton::apollo_loop>(),
      skeleton::sk_name<skeleton::instrumentation>()));
  PM.add(apollo::createInstrumentFixFunctionSignaturePass());
  PM.add(apollo::createApolloInstrumentLinearEquationPass());
  PM.add(apollo::createApolloReg2MemPass(
      skeleton::sk_name<skeleton::instrumentation>()));
  PM.add(apollo::createApolloViInsertPass(
      skeleton::sk_name<skeleton::instrumentation>()));
  PM.add(apollo::createApolloInstrumentViBoundsPass());
  PM.add(apollo::createApolloInstrumentPhiStatePass());
  PM.add(apollo::createApolloInstrumentOnConditionPass());
  PM.add(apollo::createInstrumentFunctionCallPass());
  PM.add(apollo::createApolloInstrumentIterationCountPass());
  PM.add(apollo::createApolloInstrumentMemAccessPass());
  PM.add(apollo::createApolloInstrumentPhiPass());
  PM.add(createVerifierPass());
}

DeclarePassCreator(StoreBones);
DeclarePassCreator(ScalarVerifBones);
DeclarePassCreator(MemVerifBones);
DeclarePassCreator(BoundVerifBones);

void registerCodeBonesPasses(llvm::legacy::PassManagerBase &PM) {
  PM.add(createBasicScalarPredictPass());
  PM.add(createApolloCloneLoopFunctionPass(
      skeleton::sk_name<skeleton::apollo_loop>(),
      skeleton::sk_name<skeleton::code_bones>()));
  PM.add(createApolloSimpleLoopLatchPass(
      skeleton::sk_name<skeleton::code_bones>()));
  PM.add(createStoreBonesPass());
  PM.add(createMemVerifBonesPass());
  PM.add(createBoundVerifBonesPass());
  PM.add(createScalarVerifBonesPass());
  PM.add(createVerifierPass());
}

void registerApolloPasses(const llvm::PassManagerBuilder &Builder,
                          llvm::legacy::PassManagerBase &PM) {
  registerPreOptimizationPasses(Builder, PM);
  registerPreparationPasses(Builder, PM);
  registerInstrumentationPasses(PM);
  registerCodeBonesPasses(PM);
  PM.add(apollo::createJitExportFctPass());
  PM.add(apollo::createSwitchingCallPass());
  PM.add(apollo::createApolloGlobalCombinePass());
  PM.add(createVerifierPass());
}

void RegisterPasses(const llvm::PassManagerBuilder &Builder,
                    llvm::legacy::PassManagerBase &PM) {
  if (ApolloEnabled) {
    if (Builder.OptLevel == 0)
      errs() << "Warning: Even with -O0 Apollo still applies optimizations.\n";
    registerApolloPasses(Builder, PM);
  }
}

static RegisterStandardPasses
    RegisterApolloInitial(llvm::PassManagerBuilder::EP_ModuleOptimizerEarly,
                          RegisterPasses);
static RegisterStandardPasses
    RegisterApolloInitialO0(llvm::PassManagerBuilder::EP_EnabledOnOptLevel0,
                            RegisterPasses);

} // end namespace apollo
