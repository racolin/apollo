//===--- LoopUtils.cpp ----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#include "Utils/Utils.h"
#include "Utils/ConstantUtils.h"
#include "Utils/LoopUtils.h"

#include <queue>
#include <stack>

using namespace llvm;

namespace {

unsigned max(const unsigned A, const unsigned B) {
  if (A > B)
    return A;
  return B;
}

} // end anonymous namespace

namespace apollo {
namespace loop {

unsigned getMaxLoopDepth(Loop *L) {
  unsigned Maximum = 0;
  for (Loop *S : *L) {
    Maximum = max(getMaxLoopDepth(S), Maximum);
  }
  return Maximum + 1;
}

bool isOutermostLoop(Loop *L) { 
  return L->getParentLoop() == 0x0; 
}

Loop *getOutermostLoop(Loop *L) {
  if (isOutermostLoop(L))
    return L;
  return getOutermostLoop(L->getParentLoop());
}

unsigned getLoopId(Loop *L) {
  std::stack<Loop *> ToVisit;
  ToVisit.push(getOutermostLoop(L));
  unsigned Id = 0;
  while (!ToVisit.empty()) {
    Loop *CurrentLoop = ToVisit.top();
    if (CurrentLoop == L)
      return Id;
    ToVisit.pop();
    Id++;
    // Iterate on reverse order. The first subloops must be on top of the stack.
    for (auto subloop = CurrentLoop->rbegin(); 
         subloop != CurrentLoop->rend(); ++subloop) {
      ToVisit.push(*subloop);
    }
  }
  assert(false && "getLoopId: Loop not found in nest!");
  return 42;
}

Loop *getLoopWithId(Loop *L, const unsigned Id) {
  if (getLoopId(L) == Id)
    return L;
  for (Loop *subloop : *L) {
    Loop *LoopWithId = getLoopWithId(subloop, Id);
    if (LoopWithId)
      return LoopWithId;
  }
  return 0x0;
}

unsigned getNumLoops(Loop *L) {
  unsigned Num = 1;
  for (Loop *subloop : *L) {
    Num += getNumLoops(subloop);
  }
  return Num;
}

std::vector<unsigned> getLoopIdVector(Loop *L) {
  const unsigned Id = getLoopId(L);
  std::vector<unsigned> Ids;
  if (!isOutermostLoop(L)) {
    Loop *Parent = L->getParentLoop();
    Ids = getLoopIdVector(Parent);
  }
  Ids.push_back(Id);
  return Ids;
}

unsigned getLoopPosition(Loop *L) {
  unsigned PositionAtSameDepth = 0;
  if (!isOutermostLoop(L)) {
    Loop *Parent = L->getParentLoop();
    for (Loop *subloop : Parent->getSubLoops()) {
      if (subloop == L)
        break;
      PositionAtSameDepth++;
    }
  }
  return PositionAtSameDepth;
}

std::vector<unsigned> getLoopPath(Loop *L) {
  if (isOutermostLoop(L)) {
    return std::vector<unsigned>({0});
  } else {
    std::vector<unsigned> Path = getLoopPath(L->getParentLoop());
    Path.push_back(getLoopPosition(L));
    return Path;
  }
}

std::vector<Value *> getLoopPathConstants(Loop *L) {
  LLVMContext &Context = L->getHeader()->getContext();
  std::vector<unsigned> Path = std::move(getLoopPath(L));
  std::vector<Value *> ConstantPath;
  for (unsigned i : Path) {
    ConstantPath.push_back(constant::getInt64Constant(Context, i));
  }
  return ConstantPath;
}

std::string getLoopPathString(Loop *L) {
  const auto Path = getLoopPath(L);
  std::string LoopPath = "";
  for (unsigned position : Path) {
    LoopPath += utils::concat(position, "_");
  }
  LoopPath.pop_back();
  return LoopPath;
}

Function *getLoopParentFunction(Loop *L) { 
  return L->getHeader()->getParent(); 
}

Module *getLoopParentModule(Loop *L) {
  return getLoopParentFunction(L)->getParent();
}

std::set<BasicBlock *> getLoopExitBlocks(Loop *L) {
  std::set<BasicBlock *> Result;
  SmallVector<BasicBlock *, 8> ExitBlocks;
  L->getExitBlocks(ExitBlocks);
  for (BasicBlock *exitBlock : ExitBlocks) {
    Result.insert(exitBlock);
  }
  return Result;
}

std::set<BasicBlock *> getLoopExitingBlocks(Loop *L) {
  std::set<BasicBlock *> Result;
  SmallVector<BasicBlock *, 8> ExitBlocks;
  L->getExitingBlocks(ExitBlocks);
  for (BasicBlock *exitBlock : ExitBlocks) {
    Result.insert(exitBlock);
  }
  return Result;
}

bool hasSubloops(Loop *L) { 
  return L->begin() != L->end(); 
}

std::string prettyLoopName(Loop *L, bool IsTransform) {
  const unsigned LoopId = loop::getLoopId(L);
  return prettyLoopName(LoopId, IsTransform);
}

std::string prettyLoopName(const unsigned LoopId, bool IsTransform) {
  std::vector<std::string> HeaderNames;
  if (IsTransform)
    HeaderNames = {"x", "y", "z", "w", "v", "u", "q"};
  else
    HeaderNames = {"i", "j", "k", "l", "m", "n", "o"};

  std::string suffix = LoopId < HeaderNames.size()
                           ? HeaderNames[LoopId]
                           : utils::concat(*HeaderNames.rbegin(), ".",
                                           LoopId - HeaderNames.size());

  return utils::concat("for.", suffix);
}

} // end namespace loop
} // end namespace apollo
