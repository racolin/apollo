//===--- CreatePasses.h ---------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef CREATE_PASSES_H
#define CREATE_PASSES_H

#include "Transformations/Preparation/ApolloCloneLoopFunction.h"
#include "Transformations/Preparation/ApolloIgnoreEmpty.h"
#include "Transformations/Preparation/ApolloLoopExtract.h"
#include "Transformations/Preparation/ApolloReg2Mem.h"
#include "Transformations/Preparation/ApolloSignatureNormalize.h"
#include "Transformations/Preparation/ApolloSimpleLoopLatch.h"
#include "Transformations/Preparation/ApolloViInsert.h"
#include "Transformations/Preparation/PromoteMemIntrinsics.h"
#include "Transformations/MetadataAdders/ApolloPhiHandling.h"
#include "Transformations/MetadataAdders/ApolloStatementMetadataAdder.h"
#include "Transformations/MetadataAdders/FunctionCallParent.h"
#include "Transformations/MetadataAdders/StatementVerificationAvoidance.h"
#include "Transformations/Dumpers/RegisterStaticInfo.h"
#include "Transformations/Dumpers/RegisterStmtAlias.h"
#include "Transformations/Instrumentation/ApolloIterationCount.h"
#include "Transformations/Instrumentation/ApolloLinearEquation.h"
#include "Transformations/Instrumentation/ApolloMemAccess.h"
#include "Transformations/Instrumentation/ApolloOnCondition.h"
#include "Transformations/Instrumentation/ApolloPhi.h"
#include "Transformations/Instrumentation/ApolloPhiState.h"
#include "Transformations/Instrumentation/ApolloViBounds.h"
#include "Transformations/Instrumentation/InstrumentFixFunctionSignature.h"
#include "Transformations/Instrumentation/InstrumentFunctionCall.h"
#include "Transformations/Parallel/BasicScalarPredict.h"
#include "BasicPass/CreatePass.h"

namespace apollo {
// Preparation
DeclarePassCreator(ApolloLoopExtract);
DeclarePassCreator(ApolloIgnoreEmpty);
DeclarePassCreator(ApolloSignatureNormalize);
DeclarePassCreator(ApolloPhiHandling);
DeclarePassCreator(ApolloInstrumentLinearEquation);
DeclarePassCreator(ApolloStatementMetadataAdder);
DeclarePassCreator(StatementVerificationAvoidance);
DeclarePassCreator(RegisterStaticInfo);
DeclarePassCreator(RegisterStmtAlias);
DeclarePassCreator_2(ApolloCloneLoopFunction, std::string, std::string);
DeclarePassCreator_1(ApolloReg2Mem, std::string);
DeclarePassCreator_1(ApolloViInsert, std::string);
DeclarePassCreator_1(ApolloSimpleLoopLatch, std::string);
DeclarePassCreator(PromoteMemIntrinsics);
DeclarePassCreator(FunctionCallParent);
// Instrumentation
DeclarePassCreator(InstrumentFixFunctionSignature);
DeclarePassCreator(ApolloInstrumentViBounds);
DeclarePassCreator(ApolloInstrumentPhiState);
DeclarePassCreator(ApolloInstrumentOnCondition);
DeclarePassCreator(ApolloInstrumentMemAccess);
DeclarePassCreator(ApolloInstrumentIterationCount);
DeclarePassCreator(ApolloInstrumentPhi);
DeclarePassCreator(InstrumentFunctionCall);
DeclarePassCreator(BasicScalarPredict);
// Finalization
DeclarePassCreator(JitExportFct);
DeclarePassCreator(SwitchingCall);
DeclarePassCreator(ApolloGlobalCombine);
// Support
DeclarePassCreator_1(ShowModule, std::string);

} // end namespace apollo

namespace llvm {

class PassRegistry;

DeclarePassInitializator(ApolloLoopExtract);
DeclarePassInitializator(ApolloIgnoreEmpty);
DeclarePassInitializator(ApolloSignatureNormalize);
DeclarePassInitializator(PromoteMemIntrinsics);
DeclarePassInitializator(ApolloPhiHandling);
DeclarePassInitializator(ApolloInstrumentLinearEquation);
DeclarePassInitializator(ApolloStatementMetadataAdder);
DeclarePassInitializator(StatementVerificationAvoidance);
DeclarePassInitializator(RegisterStaticInfo);
DeclarePassInitializator(RegisterStmtAlias);
DeclarePassInitializator(ApolloCloneLoopFunction);
DeclarePassInitializator(FunctionCallParent);
DeclarePassInitializator(ApolloReg2Mem);
DeclarePassInitializator(ApolloViInsert);
DeclarePassInitializator(ApolloSimpleLoopLatch);
DeclarePassInitializator(InstrumentFixFunctionSignature);
DeclarePassInitializator(ApolloInstrumentViBounds);
DeclarePassInitializator(ApolloInstrumentPhiState);
DeclarePassInitializator(ApolloInstrumentOnCondition);
DeclarePassInitializator(ApolloInstrumentMemAccess);
DeclarePassInitializator(ApolloInstrumentIterationCount);
DeclarePassInitializator(ApolloInstrumentPhi);
DeclarePassInitializator(InstrumentFunctionCall);
DeclarePassInitializator(BasicScalarPredict);
DeclarePassInitializator(JitExportFct);
DeclarePassInitializator(SwitchingCall);
DeclarePassInitializator(ApolloGlobalCombine);
DeclarePassInitializator(ShowModule);

} // end namespace llvm

#endif
