//===--- ConstantUtils.h --------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_CONSTANT_H
#define APOLLO_CONSTANT_H

#include "Utils/TypeUtils.h"
#include "Utils/GlobalUtils.h"

#include "llvm/IR/Constants.h"
#include "llvm/IR/LLVMContext.h"

#include <string>
#include <sstream>

namespace apollo {
namespace constant {
/// \brief Get an int64 constant.
llvm::Constant *getInt64Constant(llvm::LLVMContext &Context, long Const);

/// \brief Get an int32 constant.
llvm::Constant *getInt32Constant(llvm::LLVMContext &Context, int Const);

/// \brief Get zero int32
llvm::Constant *zero32(llvm::LLVMContext &Context);

/// \brief Get one int32
llvm::Constant *one32(llvm::LLVMContext &Context);

/// \brief Get zero int64
llvm::Constant *zero64(llvm::LLVMContext &Context);

/// \brief Get one int64
llvm::Constant *one64(llvm::LLVMContext &Context);

/// \brief Get one int64
llvm::Constant *two64(llvm::LLVMContext &Context);

/// \brief Given a Module and a vector of ints, it will create a Constant of
///        type "vec" with the values in the vector.
/// \param M a module.
/// \param Values a vector of int.
/// \return A constant struct of type ""
llvm::Constant *getVecConstant(llvm::Module *M, std::vector<unsigned> &Values);

/// \brief getStringConstant creates a zero terminated string in the module
llvm::Constant *getStringConstant(llvm::Module *M, std::string &Str);

/// \brief Given a Module and a vector of ints, it will create a Constant of
///        type "vec" with the values in the vector.
/// \param M a module.
/// \param Values a vector of int.
/// \param Type an integer type.
/// \return A constant struct of type ""
template <typename T>
llvm::Constant *getVecIntConstant(llvm::Module *M, std::vector<T> &Values,
                                  llvm::Type *intTy) {
  assert(intTy->isIntegerTy());
  llvm::LLVMContext &Context = M->getContext();
  llvm::Type *i64Ty = llvm::Type::getInt64Ty(Context);
  llvm::ArrayType *iArrayTy = llvm::ArrayType::get(intTy, Values.size());
  llvm::StructType *vecType = type::getVecType(M, intTy);
  llvm::Constant *ConstSize = llvm::ConstantInt::get(i64Ty, Values.size());
  llvm::Constant *Zero = zero64(M->getContext());
  std::vector<llvm::Constant *> GExpIdx = {Zero, Zero};
  std::vector<llvm::Constant *> ConstValues;

  for (unsigned value : Values) {
    ConstValues.push_back(llvm::ConstantInt::get(intTy, value));
  }

  llvm::Constant *constData = llvm::ConstantArray::get(iArrayTy, ConstValues);
  llvm::GlobalVariable *GvData = llvm::cast<llvm::GlobalVariable>(
      M->getOrInsertGlobal(global::getUnnamedGlobal(M), iArrayTy));
  GvData->setConstant(true);
  GvData->setInitializer(constData);
  GvData->setLinkage(llvm::GlobalVariable::PrivateLinkage);
  llvm::Constant *GExp = llvm::ConstantExpr::getGetElementPtr(
      GvData->getType()->getContainedType(0), GvData, GExpIdx);
  std::vector<llvm::Constant *> Fields = {ConstSize, GExp};
  return llvm::ConstantStruct::get(vecType, Fields);
}

} // end namespace constant
} // end namespace apollo

#endif
