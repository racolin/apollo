//===--- LoopUtils.h ------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_LOOP_UTILS_H
#define APOLLO_LOOP_UTILS_H

#include "llvm/Analysis/LoopInfo.h"

#include <string>
#include <vector>
#include <set>

namespace apollo {
namespace loop {

/// \brief Returns true of L is an outermost loop.
/// \param L a loop.
unsigned getMaxLoopDepth(llvm::Loop *L);

/// \brief Returns true of L is an outermost loop.
/// \param L a loop.
bool isOutermostLoop(llvm::Loop *L);

/// \brief Returns the outermost loop in a nest.
/// \param L a loop.
/// \return The outermost loop of the nest where L lives.
llvm::Loop *getOutermostLoop(llvm::Loop *L);

/// \brief From a loop, obtains its loop id.
/// \param L a Loop.
/// \return The loop id. Assigns the id to loops doing a dfs.
unsigned getLoopId(llvm::Loop *L);

/// \brief get the loop from the nest with the indicated id.
///        If not found assert false.
/// \param The outermost loop of the nest
/// \param The loop id
/// \return The loop whose id is the one indicated.
llvm::Loop *getLoopWithId(llvm::Loop *L, const unsigned LoopId);

/// \brief From a loop, obtains the number of loops.
/// \param L a Loop.
/// \return The number of loops.
unsigned getNumLoops(llvm::Loop *L);

/// \brief From a loop, obtains the loop parent ids.
/// \param L a Loop.
/// \return An std vector with the parent loops ids.
std::vector<unsigned> getLoopIdVector(llvm::Loop *L);

/// \brief Returns the position of the loop at a given depth.
/// \param L a loop.
unsigned getLoopPosition(llvm::Loop *L);

/// \brief It returns a vector with the loop path.
/// \param L a loop
/// \return std::vector<unsigned> representing the loop path.
std::vector<unsigned> getLoopPath(llvm::Loop *L);

/// \brief It returns a vector with the loop path, as ConstantInt.
/// \param L a loop
/// \return std::vector<Value*> representing the loop path.
std::vector<llvm::Value *> getLoopPathConstants(llvm::Loop *L);

/// \brief It returns the loop path encoded as a std::string.
/// \param L a loop
/// \return an std::tring representing the loop path.
std::string getLoopPathString(llvm::Loop *L);

/// \brief Get loop parent function.
/// \param L a loop.
/// \return The function which contains the loop.
llvm::Function *getLoopParentFunction(llvm::Loop *L);

/// \brief Get loop parent module.
/// \param L a loop.
/// \return The module which contains the loop.
llvm::Module *getLoopParentModule(llvm::Loop *L);

/// \brief Get loop exit blocks.
/// \param L a loop.
/// \return A vector of exit blocks.
std::set<llvm::BasicBlock *> getLoopExitBlocks(llvm::Loop *L);

/// \brief Get loop exiting blocks.
/// \param L a loop.
/// \return A vector of exiting blocks.
std::set<llvm::BasicBlock *> getLoopExitingBlocks(llvm::Loop *L);

/// \brief True if the loop has subloops.
/// \param L a loop.
/// \return True if it has subloops.
bool hasSubloops(llvm::Loop *L);

/// \brief to make things more human friendly, I assign a better
///        name to the loops.
/// \param L a loop.
/// \param IsTransform use "x y z" for suffix instead of "i j k".
/// \return New header name.
std::string prettyLoopName(llvm::Loop *L, bool IsTransform = false);

/// \brief to make things more human friendly, I assign a better name
//         to the loops.
/// \param loop id.
/// \param IsTransform use "x y z" for suffix instead of "i j k".
/// \return New header name.
std::string prettyLoopName(const unsigned LoopId, bool IsTransform = false);

} // end namespace loop
} // end namespace apollo

#endif
