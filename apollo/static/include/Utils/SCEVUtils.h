//===--- SCEVUtils.h ------------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef APOLLO_SCEV_UTILS_H
#define APOLLO_SCEV_UTILS_H

#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Analysis/ScalarEvolutionExpressions.h"

namespace apollo {
namespace SCEVUtils {

/// \fn bool isLoopInvariant(llvm::ScalarEvolution *SE,
///                          const llvm::SCEV *Scev, llvm::Loop *L)
/// \brief Check if scev is loop invariant in loop nest.
/// \param SE The scalar evolution analysis.
/// \param Scev The scalar evolution expression considered.
/// \param L The loop nest considered.
/// \return True if is loop invariant, false instead.
bool isLoopInvariant(llvm::ScalarEvolution *SE, const llvm::SCEV *Scev,
                     llvm::Loop *L);

/// \fn bool isAffine(llvm::ScalarEvolution *SE, const llvm::SCEV* Scev,
///                   llvm::Loop* L)
/// \brief Check if scev is affine in loop nest.
/// \param SE The scalar evolution analysis.
/// \param Scev The scalar evolution expression considered.
/// \param L The loop nest considered. It should be the outermost loop.
/// \return True if is affine in all loop nest, false instead.
bool isAffine(llvm::ScalarEvolution *SE, const llvm::SCEV *Scev, llvm::Loop *L);

/// \brief getNonLinearInstructionLevel returns the first loop for which a scev
///        is not linear anymore.
/// \param SE
/// \param Scev
/// \param Outermost
/// \param InnermostLoop
llvm::Loop *getNonLinearLoopLevel(llvm::ScalarEvolution *SE,
                                  const llvm::SCEV *Scev, llvm::Loop *Outermost,
                                  llvm::Loop *Innermost);

} // end namespace SCEVUtils
} // end namespace apollo

#endif
