//===--- MetadataHandler.h ------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef PRAGMA_HANDLER_H
#define PRAGMA_HANDLER_H

#include "llvm/Pass.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Metadata.h"

#include "MetadataKind.h"

#include <string>
#include <vector>

namespace apollo {
namespace metadata {

/// \brief Attach metadata strings to an instruction.
/// \param An instruction.
/// \param mKind a string to become the metadata kind.
/// \param mNode a string to became the metadata node.
void attachMetadata(llvm::Instruction *I, const std::string &mKind,
                    const std::string &mNode);

/// \brief Attach metadata vector of constants to an instruction.
/// \param I an instruction.
/// \param mKind a string to become the metadata kind.
/// \param mNode a vector of constants.
void attachMetadata(llvm::Instruction *I, std::string mKind,
                    const std::vector<llvm::Value *> &mNode);

/// \brief Get the value of the metadata node given an instruction and a
///        metadata kind.
/// \param I an instruction.
/// \param mKind a metadata kind.
/// \param Op the operand number.
/// \return The mNode attached with the given kind.
llvm::Value *getMetadataOperand(llvm::Instruction *I, const std::string &mKind,
                                unsigned Op = 0);

/// \brief Get the number of values attached for the same metdata.
/// \param I an instruction.
/// \param mKind a metadata kind.
/// \return The number of attached values.
unsigned getMetadataNumOperands(llvm::Instruction *I, const std::string &mKind);

/// \brief Get the metadata node, as a string, given an instruction and a
///        metadata kind.
/// \param I an instruction.
/// \param mKind a metadata kind.
/// \return The mNode attached with the given kind.
std::string getMetadataString(llvm::Instruction *I, const std::string &mKind);

/// \brief Checks whether the given instruction has given metadata node.
/// \param I Input instruction.
/// \return True if has metadata node.
bool hasMetadataKind(llvm::Instruction *I, const std::string &mKind);

} // end namespace metadata
} // end namespace apollo

#endif
