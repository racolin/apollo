//===--- CreatePass.h -----------------------------------------------------===//
//
// APOLLO - Automatic speculative POLyhedral Loop Optimizer.
//
//===----------------------------------------------------------------------===//
//
// The BSD 3-Clause License
//
// Copyright (c) 2016. INRIA, CNRS and University of Strasbourg
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors
//    may be used to endorse or promote products derived from this software
//    without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// Main Contributors:
//     Professor Philippe Clauss     <clauss@unistra.fr>
//     Juan Manuel Martinez Caamano  <jmartinezcaamao@gmail.com>
//     Aravind Sukumaran-Rajam       <aravind_sr@outlook.com>
//     Artiom Baloian                <artiom.baloian@inria.fr>
//
// Participated as internships:
//     Willy Wolff
//     Matias Perez
//     Esteban Campostrini
//
//===----------------------------------------------------------------------===//

#ifndef CreatePass_H
#define CreatePass_H

#define DeclarePassCreator(passName) llvm::Pass *create##passName##Pass()

#define DefinePassCreator(passName)                                            \
  llvm::Pass *create##passName##Pass() { return new passName(); }

#define DeclarePassCreator_1(passName, arg1ty)                                 \
  llvm::Pass *create##passName##Pass(arg1ty)

#define DefinePassCreator_1(passName, arg1ty)                                  \
  llvm::Pass *create##passName##Pass(arg1ty a1) { return new passName(a1); }

#define DeclarePassCreator_2(passName, arg1ty, arg2ty)                         \
  llvm::Pass *create##passName##Pass(arg1ty, arg2ty)

#define DefinePassCreator_2(passName, arg1ty, arg2ty)                          \
  llvm::Pass *create##passName##Pass(arg1ty a1, arg2ty a2) {                   \
    return new passName(a1, a2);                                               \
  }

#define DeclarePassCreator_3(passName, arg1ty, arg2ty, arg3ty)                 \
  llvm::Pass *create##passName##Pass(arg1ty, arg2ty, arg3ty)

#define DefinePassCreator_3(passName, arg1ty, arg2ty, arg3ty)                  \
  llvm::Pass *create##passName##Pass(arg1ty a1, arg2ty a2, arg3ty a3) {        \
    return new passName(a1, a2, a3);                                           \
  }

#define DeclarePassCreator_4(passName, arg1ty, arg2ty, arg3ty, arg4ty)         \
  llvm::Pass *create##passName##Pass(arg1ty, arg2ty, arg3ty, arg4ty)

#define DefinePassCreator_4(passName, arg1ty, arg2ty, arg3ty, arg4ty)          \
  llvm::Pass *create##passName##Pass(arg1ty a1, arg2ty a2, arg3ty a3,          \
                                     arg4ty a4) {                              \
    return new passName(a1, a2, a3, a4);                                       \
  }

// for pass initialization.
#define DeclarePassInitializator(passName)                                     \
  void initialize##passName##Pass(PassRegistry &)

#endif
