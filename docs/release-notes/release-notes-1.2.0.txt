# Apollo 1.2.0 - XX YYYYY 2018

## Apollo

- Updated LLVM/Clang to version 6.0.1. The unmodified sources are now
  included, as tar-files, in the APOLLO distribution.

## Build system

- All components of APOLLO are now installed into the installation
  directory. Once installed, APOLLO does not need the build directory
  to be kept.

- Set RPATH on APOLLO libraries to the installation directory. This
  allows APOLLO to be run without having to set up library paths.

- Introduce APOLLO_BUILD_JOBS to specify the maximum number of build
  jobs to use. The replaces NB_JOBS which is still supported but
  deprecated.

- Sources for external dependencies are now included in the APOLLO
  distribution. They are no longer downloaded during a build.

- A new build target 'check' to run the testsuite. This is supported
  by Makefiles ('make check') and Ninja ('ninja check').

- The build type (Debug/Release) for LLVM/Clang is now the same as the
  rest of APOLLO. New build variable APOLLO_LLVM_BUILD_TYPE can be
  used to specify a separate build type for LLVM/Clang.

## Bug fixes

- Valid code using floating point types (float or double) could make
  APOLLO stop with an message about unsupported scalars. This has been
  fixed.

- Code containing try-catch blocks could make APOLLO crash. This has
  been fixed.

