\input texinfo
@c %
@c %  /**-----------------------------------------------------------------**
@c %   **                       APOLLO USER GUIDE                         **
@c %   **-----------------------------------------------------------------**
@c %   **                     apollo-user-guide.tex                       **
@c %   **-----------------------------------------------------------------**
@c %   **               First version: september 1st 2016                 **
@c %   **-----------------------------------------------------------------**/
@c %
@c %

@c % /*************************************************************************
@c %  *                              PART I: HEADER                           *
@c %  *************************************************************************/
@c %**start of header
@setfilename apollo-user-guide.info
@settitle APOLLO User Guide

@set AP APOLLO
@set VERSION 1.1.0
@set UPDATED January 29th 2017
@c @setchapternewpage odd

@c % This is to ask for A4 instead of Letter size document.
@iftex
     @afourpaper
@end iftex

@c %**end of header

@c % /*************************************************************************
@c %  *                 PART II: TITLEPAGE, CONTENTS, COPYRIGHT               *
@c %  *************************************************************************/
@titlepage
@title @value{AP} User Guide
@subtitle Automatic speculative POLyhedral Loop Optimier
@subtitle @value{UPDATED}
@*
@*
@*
@*
@*
@*
@*
@*
@*
@*
@center @image{images/apollo_logo, 150pt}
@author @value{AP} Crew
@end titlepage

@c Output the table of contents at the beginning.
@contents

@c % /*************************************************************************
@c %  *                    PART III: TOP NODE AND MASTER MENU                 *
@c %  *************************************************************************/
@ifnottex
@node Top
@top @value{AP} User Guide

@insertcopying
@end ifnottex

@menu
* Overview::
* Usage::
* Runtime Options::
* Profiling Mode::
@end menu

@c % /*************************************************************************
@c %  *                      PART IV: BODY OF THE DOCUMENT                    *
@c %  *************************************************************************/

@c %  ******************************* OVERVIEW ********************************
@node Overview
@chapter Overview
@value{AP}  is a  runtime automatic  parallelizer of  loops of  any kind
(for, while  or do-while loops) whose  memory references can also  be of
any kind (arrays, variables, linked lists, etc.  through indirections or
pointers). @value{AP}  implements a speculative strategy  that allows to
take  advantage,  at  runtime,  of  loop  parallelizing  and  optimizing
techniques  based on  the  polyhedral  model.  Loop  nests  that may  be
optimized and  parallelized by  @value{AP} exhibit runtime  phases where
the sequence of  memory addresses referenced by  each memory instruction
match a linear or quasi-linear function.

@c %  ********************************** USAGE ********************************
@node Usage
@chapter Usage
A  specific pragma,  @code  {#pragma apollo  dcop},  where @code  {dcop}
stands for  @emph {dynamic  control part},  allows a user  to mark  in a
source code some  loop nests of any kind (for,  while or do-while nested
loops),  in  order to  be  handled  by the  speculative  parallelization
process of  @value{AP}, to take  advantage of the  underlying multi-core
processor  architecture.  The  unique restrictions  are: 
@itemize @bullet
@item   The target loops  must  not  contain  any  instruction  performing  
dynamic  memory allocation,  although  dynamic  allocation is  obviously
allowed outside the target loops.

@item Since  Apollo  does  not  handle  inter-procedural
analyses, the  target loops  should not  generally contain  any function
invocation.  However,  a called function  may be inlined in  some cases,
thus annihilating this issue.
@end itemize
@*
For example:

@example
@group
...
#pragma apollo dcop
@{
   while (i < N && B[i] < 100) @{
      for (j = 0; j < M; ++j) @{
         A[B[i][j] += C[B[j]];
      @}
   @}   
@}
@end group
@end example

After  installation,  @value{AP}  may  be  invoked  using  one  of  both
following commands, for compiling the target source code:
@itemize @bullet
@item apolloc for C programs;
@item apolloc++ for C++ programs.
@end itemize
Additionally, since @value{AP}'s static compiler is based on LLVM/Clang,
any LLVM/Clang  option may be used.  
@*
For example:

@example
apolloc -O3 my_program.c
@end example

The produced  executable file is  a fat binary containing  invocation to
@value{AP}'s runtime function, as well  as important data such as static
analysis information  or code-bones in LLVM  intermediate representation
form. The generated  executable file may be launched by  the user in the
standard way. The  target loop nests will then  be automatically handled
by the speculative parallelization process of @value{AP}.  Additionally,
some runtime options that are described below, may be used.

@c %  ************************ Runtime Options  ******************************
@node Runtime Options
@chapter Runtime Options

@menu
* Backdoor::
* Chunk Size for Profiling Phases::
* Chunk Size for Optimized Phases::
* Optimization Flags::
* Predict All Accesses::
@end menu

All  runtime options  may by  set  by using  some dedicated  environment
variables.

@node Backdoor
@section Backdoor

@value{AP} provides a  powerful backdoor mechanism allowing  to get deep
knowledge of  is going  on inside @value{AP}  during an  execution. This
backdoor  mechanism is  enabled by  defining the  @code{APOLLO_BACKDOOR}
environment variable as following:
@*
@example 
APOLLO_BACKDOOR="<output_option> <todo_option>"
@end example
@*
@code {<output_option>} can be:
@table @code
@item --stdio
Enable outputs printing on stdout
@item --file=FILE_PATH
Print outputs in the given file
@item --tcp 
Send outputs to localhost on port 22222 
@end table
@*
@code {<todo_option>} can be:
@table @code
@item --step
Print @value{AP} runtime phases
@item --info
Print execution information  in details, as for  example, the prediction
model, linear functions, tubes etc.
@item --info_all
Print execution  information in even  more details, as for  example, the
recorded profiling events
@item --trace 
Print trace information
@item --time
Print starting and ending time stamps of events
@end table
@*
For example:
@example
APOLLO_BACKDOOR="--stdio --step" ./my_program
@end example

@node Chunk Size for Profiling Phases
@section Chunk Size for Profiling Phases

@value{AP} allows  to configure the  size of the profiling  phases which
default value is 16:
@*
@example 
APOLLO_INSTR_CHUNK_SIZE=<value>
@end example
@*
For example:
@example
APOLLO_INSTR_CHUNK_SIZE=32  ./my_program
@end example

@node Chunk Size for Optimized Phases
@section Chunk Size for Optimized Phases

@value{AP} allows  to configure the  size of the optimized  phases which
default value is 1024: 
@*
@example 
APOLLO_OPT_CHUNK_SIZE=<value>
@end example
@*
For example:
@example
APOLLO_OPT_CHUNK_SIZE=512  ./my_program
@end example

@node Optimization Flags
@section Optimization Flags

@value{AP} provides several options allowing to configure how the target
loop nests are  optimized. These options are controlled  by defining the
@code{APOLLO_BONES} environment variable as following:
@*
@example 
APOLLO_BONES="<option> <option> ..."
@end example
@*
@code {<option>} can be:
@table @code
@item --control
CLooG option. Optimize the generated code for control overhead (disabled
by default).
@item --delinearize=yes|no|force 
Enable/disable/force de-linearization  of linear memory  references (yes
by default).  Warning: force is unsafe.
@item --fuse=max|smart|no
Pluto option. Switch between maximal  fusion, smart fusion, or no-fusion
(no by default).
@item --identity
Pluto  option.   Use  the  identity  transformation  only  (disabled  by
default).  If this  option  is  used, @value{AP}  will  only attempt  to
parallelize the  target loop nests  by slicing each outermost  loop into
parallel threads.
@item --islsolve
Pluto  option. Set  isl  has  ILP solver  instead  of  PiP (disabled  by
default).
@item --no-split 
Do not  split the  verification code  from the  rest of  the computation
(disabled by default).
@item --rar
Pluto  option.   Consider  read-after-read  dependencies   (disabled  by
default).
@item --simplify
Merge code bones as much as possible (disabled by default).
@item --tile|--notile
Force/disable tiling  instead of  using @value{AP}'s  internal heuristic
for tiling activation or not.
@item --tubewidth=x
Increase the tube width by multiplying de profiled width by x (1 by default).
@item --unroll
Pluto option. Set unroll option (disabled by default).
@end table
@*
For example:
@example
APOLLO_BONES="--rar --tile"   ./my_program
@end example

@node Predict All Accesses
@section Predict All Accesses

By default  @value{AP} stops creating a  prediction model as soon  as it
encounters a non predictable scalars,  memory access or loop bounds. For
debug purposes it can be useful to still predict everything. To override
the default behavior, use the following environment variable:
@*
@example 
APOLLO_PREDICT_ALL=1
@end example

@c %  ************************* Profiling Mode  *******************************
@node Profiling Mode
@chapter Profiling Mode

@menu
* Polyhedral Phases Detector::
@end menu

@value{AP} can  also be  used as  a profiler. In  this case,  instead of
optimizing the code,  @value{AP} gathers and reports  information of the
target  loop nests.   To  ease  the integration  of  different types  of
profiling, @value{AP} provides a flexible mechanism to develop profiling
plugins.   @*  @value{AP} profiling  mode  is  enabled by  defining  the
@code{APOLLO_PROFILING} environment variable as following: @*
@example 
APOLLO_PROFILING="<--profiling_plugin>"
@end example
@*
@code {<profiling_plugin>} can be:
@table @code
@item --polyhedral_phases_detector
The polyhedral phases detector plugin.
@item --polyhedral_phases_detector_full
The polyhedral phases detector plugin with more detailed reporting.
@item --redindex_polyhedral_phases_detector
The  reindex   polyhedral  phases  detector  plugin.   Compared  to  the
polyhedral phases detector  plugin, this plugin tries  to reindex memory
accesses in case of non polyhedral behaviour and then try again to build
a polyhedral model.
@item --redindex_polyhedral_phases_detector_full
The  reindex  polyhedral  phases  detector  plugin  with  more  detailed
reporting.
@end table

@node Polyhedral Phases Detector
@section Polyhedral Phases Detector

The phases  detector plugin computes  and reports information  about the
different polyhedral phases  observed during an execution.  A phases can
be:
@itemize
@item Linear
@item Linear with tubes
@item Non polyhedral
@end itemize
For all types  of phases, the polyhedral phases  detector plugin reports
the lower bound and the upper bound  of the phase (in terms of the outer
most loop iterator).  For both linear  and linear with tubes phases, the
plugin also  reports the  OpenSCoP objects  before and  after polyhedral
optimization.
@*
The polyhedral profiling plugin is enabled as following:
@example
APOLLO_PROFILING="--polyhedral_phases_detector" ./my_program
@end example
@* At the end of the execution, the plugin reports phases information on
the standard output. If this information is saved into a file, it can be
further                 processed                 by                 the
@code{polyhedral_phases_detector_parse_output.py}  script to  get higher
level information about phases.  @* For example
@example
APOLLO_PROFILING="--polyhedral_phases_detector" ./my_program > output
${APOLLO_BIN_PATH}/polyhedral_phases_detector_parse_output.py ./output
@end example
@* The result is the following  for the matrix multiply example provided
along with the @value{AP} distribution:
@*
@example
Phase 0 from 0 to 96 linear
  Chlore stdout:
    interchange([0,0,0,0], 2, 3, 0);
  Chlore stderr:
    <nothing on stderr>
  loop 1 is parallel
@end example
@* The profiler indicates that the  execution of matrix multiply is made
of a single linear phase from iterations  0 to 96 of the outer most loop
in the  loop nest.  In standard  execution mode, for this  single linear
phase, @value{AP} would have performed a loop interchange transformation
and a parallel execution of the loop at depth 1 of the target loop nest.

@node Reindex Polyhedral Phases Detector
@section Reindex Polyhedral Phases Detector

The reindex  polyhedral phases  detector plugin  behaves exactly  as the
polyhedral phases detector plugin described in the previous section. The
only difference  concerns phases that  are not linear. For  such phases,
this plugin  will try to re-index  memory accesses before looking  for a
linear or tube model.

The reindexing step consists in assigning  a new address to every memory
access. To that end, original memory adresses are re indexed by adresses
starting  at 0  and  incremented by  one  every time  a  new address  is
encountered.

@node Profilling Frequency
@section Profiling Frequency

In profiling mode, @value{AP} profiles  by default all the iterations of
the  outer most  loop. Nevertheless,  this behaviour  can be  changed to
profile less iterations in case the complete profiling is to costly. For
that, the following environment variable is provided profiled.: @*
@example 
APOLLO_PROFILING_FREQUENCY=<value>
@end example
@*
For example:
@example
APOLLO_PROFILING_FREQUENCY=100
@end example
This   tells   @value{AP}   to   run   a   profiling   chunk   of   size
APOLLO_INSTR_CHUNK_SIZE every 100 iterations of the outermost loop. As a
consequence,   APOLLO_PROFILING_FREQUENCY    must   be    greater   than
APOLLO_INSTR_CHUNK_SIZE.

@c % /*************************************************************************
@c %  *                       PART VI: END OF THE DOCUMENT                    *
@c %  *************************************************************************/
@c @unnumbered Index

@c @printindex cp

@bye
