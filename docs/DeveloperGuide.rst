======================
APOLLO Developer Guide
======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Introduction
============

This document gives information and guidelines for developers working
on APOLLO. Since APOLLO is implemented as an extension to LLVM/Clang,
the LLVM documentation for developers should also be read. These can
be found on `the LLVM website <https://www.llvm.org/docs>`_.

Getting APOLLO
==============

The latest release of APOLLO can be downloaded from `GForge
<https://http://apollo.gforge.inria.fr/download>`_. The archive should
be unpacked before building.

The development sources for APOLLO can be cloned from the APOLLO git
repository. For read-only access to the sources:

  * ``git clone https://scm.gforge.inria.fr/anonscm/git/apollo/apollo.git``	

To work on APOLLO, clone with read and write access using a GForge username:

  * ``git clone https://<gforge-username>@scm.gforge.inria.fr/authscm/mwahab/git/apollo/apollo.git``

Building APOLLO
================

Build directory
---------------

APOLLO must be built in a separate directory from the sources.

  * ``mkdir <build-dir>``

  * ``cd <build-dir>``


Configuration
-------------

In the build directory, configure the build using CMake:

  * ``cmake <source-dir> [options]``

  where
    ``<source-dir>`` is the path to the source directory
    ``[options]`` are the build options to set.

The available build options are:

  * ``-G ('Unix Makefiles' | 'Ninja')``

  Whether to generate for a Makefile or a Ninja build
  system. (Default: ``Unix Makefiles``)

  * ``-DCMAKE_INSTALL_PREFIX=<installation-path>``

  The installation prefix. The binaries, libraries and data for APOLLO
  are installed in directories under this prefix. (Default: ``/usr/local``)

  * ``-DAPOLLO_BUILD_JOBS=<num>``

  The maximum number of build jobs to run in parallel. (Default: 1)

  * ``-DCMAKE_BUILD_TYPE=(Debug|Release)``

  Whether to build a release or debug version of APOLLO. The Debug
  build includes debugging and other information. (Default: ``Release``)

  * ``-DAPOLLO_LLVM_BUILD_TYPE=(Debug|Release)``

  Whether to build a release or debug version of LLVM/Clang. The LLVM
  Debug build can take a long time to complete. This option allows
  APOLLO to be built in Debug mode with LLVM built in Release
  mode. (Default: the same as CMAKE_BUILD_TYPE)

Build and install
-----------------

If using Unix Makefiles, build and install APOLLO with:

  * ``make install``

If using Ninja, build and install with:

  * ``ninja install``

Examples
--------

Assuming that the path from the build directory to the source
directory is ``../apollo.git``

To build APOLLO with default settings:

.. code-block:: bash
  cmake ../apollo.git
  make install

To build and install APOLLO into a custom directory ``/usr/local/opt``:

.. code-block:: bash
  cmake ../apollo.git -DCMAKE_INSTALL_PREFIX=/usr/local/opt
  make install

To build APOLLO using Ninja with 8 jobs in parallel into ``/usr/local/opt``:

.. code-block:: bash
  cmake -G Ninja ../apollo.git -DCMAKE_INSTALL_PREFIX=/usr/local/opt
        -DAPOLLO_BUILD_JOBS=8
  ninja install

To build a debug version of APOLLO with a release version of LLVM into
``/usr/local/opt``, using Ninja and 8 jobs in parallel:

.. code-block:: bash
  cmake ../apollo.git -DCMAKE_INSTALL_PREFIX=/usr/local/opt
        -DCMAKE_BUILD_TYPE=Debug -DAPOLLO_LLVM_BUILD_TYPE=Release
	-DAPOLLO_BUILD_JOBS=8
  ninja install

Reporting issues and bugs
=========================

The issue and bug tracker for Apollo is on the
`GForge<https://gforge.inria.fr/tracker/?atid=14190&group_id=7938&func=browse>`_. Posts
to the tracker are also sent to the mailing list
`apollo-bugs@lists.gforge.inria.fr<https://lists.gforge.inria.fr/mailman/listinfo/apollo-bugs>`_.

The tracker should be used to report feature requests (as issue type
*Enhancement*) and bugs (issue type *Bug*). 

Bug reports should include enough information to reproduce the
problem. At a minimum:

  * The version of APOLLO.
  * Whether it is a Release or Debug build
  * The full command line used when the bug was triggered
  * The compiler output showing the failure. 
  * (When possible) A small test-case that triggers the bug.

The LLVM documentation has useful information about `preparing bug reports <https://llvm.org/docs/HowToSubmitABug.html>`_.

Note that all posts on the issue tracker are public. Confidential
material should not be posted on the tracker.

Working on APOLLO
=================

APOLLO is implemented as an extension to LLVM/Clang. It consists of
code analyses and transformations which are run statically, as part of
the compiler, or dynamically, during the program runtime. 

The source for the static part of APOLLO is in ``apollo/static``. This
builds a plugin is loaded into LLVM/Clang by the scripts ``apolloc``
and ``apolloc++``. This part of APOLLO is implemented as optimizations
the work on the LLVM IR.

The code for the runtime part is in ``apollo/runtime``. This builds a
library that is linked into the executable and invoked when the
program is run.

APOLLO also uses a version of LLVM/Clang that has been modified to
support the use of the ``#pragma apollo`` construct in input C/C++
files. This is in ``externals/llvm`` and
``externals/llvm/tools/clang``. The sources for the original versions
of LLVM and Clang are in ``externals/packages``. They can be compared
with the the ``externals/llvm`` tree to see the modifications made to
support the APOLLO pragma.


Coding standards
================

APOLLO development follows the `coding standards for LLVM
<http://llvm.org/docs/CodingStandards.html>`_. Although existing code
may deviate from them, all new code must follow these standards.

Particular things to note:

* The development language is C++11.

* Source code line width should fit into 80 columns of text.

* Spaces instead of tabs.

* Indentation must be consistent.

* When working on existing code, follow the code style that is already
  being used. This is to keep code formatting consistent and easier to
  read.

Testing
=======

The APOLLO tests are in directory ``apollo/tests`` and use the
``llvm-lit`` test-driver built as part of LLVM. The tests can be run
from the APOLLO build directory using the ``check`` target:

* If using Makefiles, ``make check``

* If using Ninja, ``ninja check``  

The output from running the tests, including failures, will be in the
same directory as the test sources.

All changes to APOLLO should include tests to check that the changes
work. Before committing a change to APOLLO, all tests must be run and
there must be no new failures. This helps make sure that new work on
APOLLO doesn't introduce regressions.

Diagnostic information
======================

Information about the changes made to a program as it is compiled by
APOLLO can be tracked using the standard LLVM debugging options. In
particular, the LLVM options ``-print-before`` and ``-print-after``
print the LLVM IR of a module before and after a pass, making it
possible to see the changes made by the pass.

To see all LLVM options, including those for debugging the compiler, use::

  echo "" | apolloc -mllvm -help-hidden -E -

Debug builds of the APOLLO compiler can print debugging information
for passes that are run. This is enabled using the LLVM
``-debug-only=`` option, to select a particular set of debugging
information, or ``-debug`` to print all available information. Option
``-debug-only=`` takes a parameter to identify the information to
print, these are listed below.

Note that ``-debug`` and ``-debug-only`` are LLVM options. To use them
with the ``apolloc`` or ``apolloc++`` front-end, use the ``-mllvm``
option.  For example::

  apolloc -mllvm -debug-only=apollo-pass-0

  apolloc++ -mllvm -debug-only=apollo-pass-0

For more information about the ``-debug`` and ``-debug-only=``
options, see the `LLVM programmer's manual
<https://llvm.org/docs/ProgrammersManual.html>` and the section on
`Debugging< https://llvm.org/docs/ProgrammersManual.html#debugging>`.

The ``-debug-only=`` options added by Apollo are:

  * ``-debug-only=apollo-pass-0``: Log the Apollo passes that are run.

  * ``-debug-only=apollo-pass-1``: Log whether Apollo passes change a
    function or module.

  * ``-debug-only=apollo-pass-phi-handling``: Report changes made by
    the ``ApolloPhiHandling`` pass.

  * ``-debug-only=apollo-pass-instrument-phi-state``: Report changes
    made by the ``ApolloInstrumentPhiState`` pass.

  * ``-debug-only=apollo-pass-simple-loop-latch``: Report changes
    made by the ``ApolloSimpleLoopLatch`` pass.

  * ``-debug-only=codebones-boundverif``: Report information from the
    ``BoundVerifBones`` code-bones construction pass.

  * ``-debug-only=codebones-scalarverif``: Report information from the
    ``ScalarVerifBones`` code-bones construction pass.

  * ``-debug-only=codebones-memverif``: Report information from the
    ``MemVerifBones`` code-bones construction pass.

  * ``-debug-only=codebones-stores``: Report information from the
    ``StoreBones`` code-bones construction pass.

  * ``-debug-only=codebones-extract``: Report information generated by
    function for extracting code-bones.

  * ``-debug-only=apollo-jit-export``: Report on the functions
    extracted for JIT compilation by the Apollo code generator.
