======================================================================
                             REQUIREMENTS
======================================================================

Before installing,  please make sure  that you have  already installed
the following packages:

  * autoconf
  * bison
  * build-essential
  * cmake
  * flex
  * git
  * libboost-dev
  * libboost-system-dev
  * libgmp-dev
  * liblapack-dev
  * libtool
  * ninja-build
  * texi2html
  * texinfo

======================================================================
                               BUILD
======================================================================

==== Prepare =====

Unpack the sources to a directory and create a build directory which
is separate from the source directory.

==== Configure =====

In the build directory, configure the build using CMake:

$cmake <srcdir> [options]

where
  <srcdir> is the path to the source directory
  [options] are the build options to set.

The available build options are:

* -DCMAKE_INSTALL_PREFIX=<installation-path>

  The installation prefix. The binaries, libraries and data for Apollo
  are installed in directories under this prefix. (Default: /usr/local)

* -DAPOLLO_BUILD_JOBS=<num>

  The maximum number of build jobs to run in parallel. (Default: 1)

* -DCMAKE_BUILD_TYPE=(Debug|Release)

  Whether to build a release or debug version of Apollo. The Debug
  build includes debugging and other information. (Default: Release)

* -DAPOLLO_LLVM_BUILD_TYPE=(Debug|Release)

  Whether to build a release or debug version of the LLVM
  libraries. The Debug build includes debugging and other information
  and can take a substantial amount of time to complete. This option
  allows Apollo to be built in Debug mode with LLVM built in Release
  mode. (Default: the same as CMAKE_BUILD_TYPE)

* -G ('Unix Makefiles' | 'Ninja')

  Whether to generate for a Makefile or a Ninja build
  system. (Default: 'Unix Makefiles')

==== Build and install ====

If using Unix Makefiles:

$make install

If using Ninja:

$ninja install

==== Examples =====

Assuming that the path to the source directory from the build
directory is '../apollo.git'

To build Apollo with default settings:

$cmake ../apollo.git
$make install

To build and install Apollo into a custom directory '/usr/local/opt':

$cmake ../apollo.git -DCMAKE_INSTALL_PREFIX=/usr/local/opt
$make install

To build Apollo using Ninja with 8 jobs in parallel into '/usr/local/opt':

$cmake -G Ninja ../apollo.git -DCMAKE_INSTALL_PREFIX=/usr/local/opt
    -DAPOLLO_BUILD_JOBS=8
$ninja install

To build a debug version of Apollo with a release version of LLVM into
'/usr/local/opt', using Ninja and 8 jobs in parallel:

$cmake ../apollo.git -DCMAKE_INSTALL_PREFIX=/usr/local/opt
    -DCMAKE_BUILD_TYPE=Debug -DAPOLLO_LLVM_BUILD_TYPE=Release
    -DAPOLLO_BUILD_JOBS=8
$ninja install

===== Runtime information =====

In both release and debug builds of APOLLO, the APOLLO compiler can
print information about the performed transformations:

$apolloc -mllvm -debug-only=apollo-pass-0
$apolloc -mllvm -debug-only=apollo-pass-1
