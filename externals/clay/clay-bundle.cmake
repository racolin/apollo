#///////////////////////////////////////////////////////////////////#
#                          LAST CLAY                                #
#///////////////////////////////////////////////////////////////////#
ExternalProject_Add(clay_external
  PREFIX "${EXTERNALS_FOR_CHLORE}/clay"
  DEPENDS cloog_external
  SOURCE_DIR ${EXTERNALS_FOR_CHLORE}/clay/src/clay-src
  DOWNLOAD_COMMAND
    ${CMAKE_COMMAND} -E
      tar zx ${CMAKE_CURRENT_SOURCE_DIR}/externals/clay/clay.tgz
  BUILD_IN_SOURCE 1
  CONFIGURE_COMMAND
    ./configure
      --with-osl=system
      --with-osl-prefix=${EXTERNALS_FOR_CHLORE}
      --with-clan=no
      --with-candl=no
      --with-cloog=no
      --prefix=${EXTERNALS_FOR_CHLORE}
  BUILD_COMMAND
    make -j${APOLLO_BUILD_JOBS}
  INSTALL_COMMAND
    make install
  )
