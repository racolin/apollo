#///////////////////////////////////////////////////////////////////#
#                          ISL-0.17                                 #
#///////////////////////////////////////////////////////////////////#
ExternalProject_Add(isl_external
  PREFIX ${EXTERNALS_FOR_CHLORE}/isl
  SOURCE_DIR ${EXTERNALS_FOR_CHLORE}/isl/src/isl-src
  BUILD_IN_SOURCE 1
  DOWNLOAD_COMMAND
    ${CMAKE_COMMAND} -E
      tar zx ${CMAKE_CURRENT_SOURCE_DIR}/externals/isl/isl.tgz
  CONFIGURE_COMMAND
    ./configure --prefix=${EXTERNALS_FOR_CHLORE}
  BUILD_COMMAND
    make -j${APOLLO_BUILD_JOBS}
  INSTALL_COMMAND
    make install
)
