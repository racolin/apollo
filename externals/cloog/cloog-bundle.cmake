#///////////////////////////////////////////////////////////////////#
#                          CLOOG 0.18.4                             #
#///////////////////////////////////////////////////////////////////#
ExternalProject_Add(cloog_external
  DEPENDS osl_external isl_external
  PREFIX ${EXTERNALS_FOR_CHLORE}/cloog
  SOURCE_DIR ${EXTERNALS_FOR_CHLORE}/cloog/src/cloog-src
  BUILD_IN_SOURCE 1
  DOWNLOAD_COMMAND
    ${CMAKE_COMMAND} -E
      tar zx ${CMAKE_CURRENT_SOURCE_DIR}/externals/cloog/cloog.tgz
  CONFIGURE_COMMAND
    ./configure
      --with-isl=system
      --with-isl-prefix=${EXTERNALS_FOR_CHLORE}
      --with-osl=system
      --with-osl-prefix=${EXTERNALS_FOR_CHLORE}
      --with-gmp=system
      --prefix=${EXTERNALS_FOR_CHLORE}
  BUILD_COMMAND
    make -j${APOLLO_BUILD_JOBS}
  INSTALL_COMMAND
    make install)
