#///////////////////////////////////////////////////////////////////#
#                          OPENSCOP                                 #
#///////////////////////////////////////////////////////////////////#

# Built with autotools, required for building clay
ExternalProject_Add(osl_external
  PREFIX ${EXTERNALS_FOR_CHLORE}/osl
  SOURCE_DIR ${EXTERNALS_FOR_CHLORE}/osl/src/openscop-src
  BUILD_IN_SOURCE 1
  DOWNLOAD_COMMAND
    ${CMAKE_COMMAND} -E
      tar zx ${CMAKE_CURRENT_SOURCE_DIR}/externals/openscop/openscop.tgz
  CONFIGURE_COMMAND
    ./configure --prefix=${EXTERNALS_FOR_CHLORE}
  BUILD_COMMAND
    make -j${APOLLO_BUILD_JOBS}
  INSTALL_COMMAND
    make install
  )
