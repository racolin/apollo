#///////////////////////////////////////////////////////////////////#
#                          LAST CHLORE                                #
#///////////////////////////////////////////////////////////////////#
ExternalProject_Add(chlore_external
  DEPENDS osl_external cloog_external clay_external
  PREFIX "${CMAKE_CURRENT_BINARY_DIR}/chlore"
  SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}/chlore/src/chlore-src"
  BUILD_IN_SOURCE 1
  DOWNLOAD_COMMAND
    ${CMAKE_COMMAND} -E
      tar zxf ${CMAKE_CURRENT_SOURCE_DIR}/externals/chlore/chlore.tgz
  CMAKE_ARGS
  "-DCMAKE_BUILD_TYPE=Debug"
  "-DCMAKE_INSTALL_PREFIX=${APOLLO_INSTALL}"
  "-DCMAKE_PREFIX_PATH=${EXTERNALS_FOR_CHLORE}"
  )
